# Test collection filtering and pagination feature here.
Feature: Pre-filtered collections
  In order to see collections of particular objects
  As a user
  I want to see collections to which a filter has been pre-applied
  
  Background:
    Given I have reset the database
    And   I have logged in
    
  Scenario: Pre-filtering applied to a string attribute
    Then  I should see that there are a total of "1" "Bobs" in the header
    And   I click on the "Bobs" header
    Then  I should only see "Bobs" where "Name" is "Bob"
    And   I should see "1" "Bobs"
    
  Scenario: Advanced searching on a pre-filtered collection
    And   I click on the "Bobs" header
    And   I open the "Bobs" search
    And   I click on the "Bobs" advanced search link

    # filter on text field with a term that has a match(es)
    When  I filter to see "Bobs" where "Name" is "o"
    And   I submit the "Bobs" search
    Then  I should only see "Bobs" where "Name" is "Bob"
    And   I should see "1" "Bobs"
    # filter on text field with a term that has no matches
    When  I filter to see "Bobs" where "Name" is "Sally"
    And   I submit the "Bobs" search
    Then  I should not see a "Bobs" where "Name" is "Bob"
    And   I should not see a "Bobs" where "Name" is "Sally"
    
  Scenario: Using 'Search All' on a pre-filtered collection
    And   I click on the "Bobs" header
    And   I open the "Bobs" search
    
    # filter on text field with a term that has a match(es)
    And   I search all fields to see "Bobs" with "3"
    And   I submit the "Bobs" search
    Then  I should only see "Bobs" where "Name" is "Bob"
    And   I should see "1" "Bobs"
    # filter on text field with a term that has no matches
    And   I search all fields to see "Bobs" with "Sally"
    And   I submit the "Bobs" search
    Then  I should not see a "Bobs" where "Name" is "Bob"
    And   I should not see a "Bobs" where "Name" is "Sally"
    