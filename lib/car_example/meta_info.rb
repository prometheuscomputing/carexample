# must not have the word m-o-d-u-l-e above the next line (so that a Regexp can figure out the m-o-d-u-l-e name)
module CarExample
  
  # For more information about meta_info.rb, please see project MM, lib/mm/meta_info.rb
  
  # SUGGESTION: Treat "Optional" as meaning "can be nil", and define all constants, even if the value happens to be nil.
  
  # Required
  GEM_NAME = "car_example"
  VERSION  = '1.27.7'
  AUTHORS  = ["Sam Dana", "Michael Faughn"]
  SUMMARY  = %q{Example generated application for testing.}
  
  # Optional
  EMAILS      = ["s.dana@prometheuscomputing.com", "m.faughn@prometheuscomputing.com"]
  HOMEPAGE    = 'https://gitlab.com/prometheuscomputing/carexample'
  DESCRIPTION = %q{CarExample is a test of many of the possible options and features of the UML application generator, to be used with the gui_site project}
  
  LANGUAGE_VERSION = ['>= 2.7'] # Language features and syntax
  RUNTIME_VERSIONS = {
    :mri => ['>= 2.7']
  }
  
  DEPENDENCIES_RUBY = {
    'sqlite3'               => '~> 1.4',
    'gui_site'              => '',
    'car_example_generated' => '~> 2.7',
    'gb_test_generated'     => '~> 0.0'
  }

  DEVELOPMENT_DEPENDENCIES_RUBY = {
    # =================================
    # The remainder are actually development
    # dependencies - but "gem install car_example --development"
    # errors rather than requiring the any requirements of these
    # gems (for example rspec-expectations and rspec-mocks,
    # required by rspec). I figure it's actually fine to
    # include these here, because we only use car_example
    # for tests.  It would be different if we actually wanted
    # to deploy it.
    'simplecov'     => '~> 0.14',
    'cucumber'      => '~> 5.0',
    'rspec'         => '~> 3.4',
    'activesupport' => '~> 6.0',
    # 'mailcatcher'   => '~> 0.6',
    'timecop'       => '~> 0.8',
    'capybara'            => '~> 3.34',
    'capybara-screenshot' => '~> 1.0', # Takes screenshots of capybara failures
    'capybara-selenium' => '~> 0.0', # NOTE: requires chromedriver to be installed. See Readme for instructions
    # 'capybara-email'    => '',
    'selenium-webdriver' => '~> 3.142'
  }
end
