module Automotive  
  class Car
    
    include Workflow
    include WorkflowSequelAdapter

    workflow_column :state

    workflow do

      state :new_car_repair_order_created, :meta => {:roles => [:secretary], :view_type => :document, :page => "NewCarRepairOrderCreated"} do
       event :add_vehicle_information, :transitions_to => :started_vehicle_repair, :if => proc {|vehicle| vehicle.serial}
      end

      state :started_vehicle_repair, :meta => {:roles => [:mechanic], :page => "StartedVehicleRepair", :next_action_display_name => "Add Repaired Components to the Car"} do
        event :add_repaired_components, :transitions_to => :started_vehicle_inspection, :meta => {:roles => [:plant_manager]}, :if => proc {|vehicle| !vehicle.components.empty?}
      end
      
      state :started_vehicle_inspection, :meta => {:roles => [:manager, :quality_inspector], :page => 'StartedVehicleInspection'} do
        event :complete_inspection, :transitions_to => :finished_workflow, :if => proc{|vehicle| vehicle.is_inspection_completed_test}
        event :complete_inspection, :transitions_to => :inspected_vehicle
      end
      
      state :inspected_vehicle, :meta => {:roles => [:all_roles], :page => "InspectedVehicle", :view_type => :custom, :view_url => '/workflow/inspected_vehicle', :next_action_display_name => "Vehicle Inspection Overview"} do
        event :start_tire_inspection, :transitions_to => :started_tire_inspection
        event :start_engine_inspection, :transitions_to => :started_engine_inspection
      end
      
      state :started_tire_inspection, :meta => {:roles => [:all_roles], :page => "StartedTireInspection"} do
        event :inspect_tires, :meta => {:display_name => "Complete Tire Inspection"}, :transitions_to => :started_vehicle_inspection
      end
      
      state :started_engine_inspection, :meta => {:roles => [:all_roles], :page => "StartedEngineInspection"} do
        event :inspect_engine, :transitions_to => :started_vehicle_inspection
      end
      
      state :finished_workflow, :meta => {:roles => [:manager]}
      
    end
    
    #Event methods
    def is_inspection_completed_test
      inspection_completed
    end
    
    # Are these methods necessary? If so, why? -SD
    def add_vehicle_information
    end
    
    def add_repaired_components
    end
    
    def start_engine_inspection
    
    end
    
    def start_tire_inspection
    
    end
    
    def inspect_engine 
    
    end
    
    def inspect_tires
    
    end
    
    #TODO this is just a name it only works with cars at the moment - possibly make it work with inheritance
    def complete_inspection
      # puts "add car information state method is called now"
      # self.save
    end
         
    #Generic Methods present in all workflow objects
    #TODO Possible Title gui method for a gui site object page display. Example:  Add Car Parts
    
    #TODO State to next State string method
    
    # NOTE: Does not seem to be called. No documentation to explain either. Commenting out for now -SD
    # def validate_page_for_transition view_page
    #   allowed_page = get_state_view_name
    #   page_is_allowed = false
    #
    #   if view_page.to_sym == allowed_page || view_page.to_s == allowed_page || view_page == :workflow_home_page
    #     page_is_allowed = true
    #   end
    #
    #   page_is_allowed
    # end
    
    # Called by handle_state_transition -SD
    # Does not seem like code that should live in the Application. -SD
    # Seems to validate that a user is authorized to transition a workflow to its next state. -SD
    def validate_roles_for_transition user_roles
      allowed_roles = self.current_state.meta[:roles]
      user_roles += [:all_roles]
      comparison_state_roles = self.class.make_array_symbols(allowed_roles)
      comparison_user_roles = self.class.make_array_symbols(user_roles)
      user_is_allowed = !(comparison_state_roles & comparison_user_roles).empty?
    end
    
    #This is the default way to get a classes workflow items if the gui workflow option strict role enforcment is enabled
    def self.select_workflow_items_by_role user_roles
      states_for_role = self.select_available_states_by_role(user_roles).collect{|state| state.name.to_s}
      
      all_available_workflow_items = self.where(self.workflow_column => states_for_role).all
      
    end
    
    # Called by select_workflow_items_by_role. -SD
    def self.select_available_states_by_role user_roles
      all_states = self.workflow_spec.states
      
      #Add in :all_roles since it is the generic keyword to allow anyone to see and edit a specific state
      user_roles += [:all_roles]
      
      #Convert all roles to symbols for matching strings against symbols
      comparison_user_roles = self.make_array_symbols(user_roles)
      
      states_for_role = all_states.collect do |state_name, state_object|
        role_meta_info = state_object.meta[:roles]
        comparison_state_roles = self.make_array_symbols(role_meta_info)
        viable_state = !(comparison_state_roles & comparison_user_roles).empty? if role_meta_info
        viable_state ? state_object : nil
      end.compact
      
      states_for_role
    end
    
    # Seems to take an array of role names, then convert them to an array of URL-friendly symbols. -SD
    def self.make_array_symbols array
      array.collect{|role| role.to_s.parameterize.underscore.to_sym} 
    end
    
    # Does not seem to be used, so I'm commenting it out. -SD
    #Currently not used or enforced
    # def self.select_available_events_by_role(user_roles)
    #   all_states = self.select_available_states_by_role(user_role)
    #
    #   #Assumes that to be able to trigger an event the user has to have a role that has access to the state that contains the event
    #   events_for_role = all_states.collect{|state| state.events.collect{|event_name, event_objects| event_objects.collect{|event_object| event_object.meta[:roles] && event_object.meta[:roles].include?(user_role) ? event_object : nil }.compact }}.flatten.compact
    #
    # end
    
    # This seems to be an attempt at generalized code, so it clearly doesn't belong in the application. -SD
    #Normally a state transition is done by calling the event but since the events must be generalized for each application either take a guess or pass it in
    # Parameters:
    #  - event_name is the name of one of a transition event that is valid from the current state
    #  - user_roles is an array of the current user's role names. -SD
    def handle_state_transition(event_name = nil, user_roles = nil)
      # Use the passed event_name, or pick the first available transition event. -SD
      # NOTE: If a state has multiple transition events, this could lead to unexpected behavior. 
      #       Consider removing this fallback strategy and always require event_name. -SD
      event_name = event_name || self.current_state.events.keys.first
      event_name = event_name
      event_method_name = "#{event_name.to_s}!"
      
      # puts "trying #{event_method_name} on #{self.inspect}"
      
      begin
        #Verify role is correct for state transition
        role_enforcement = Gui.option(:workflow_options)[:strict_role_enforcement]
        if role_enforcement
          user_can_transition = self.validate_roles_for_transition user_roles
          set_role_transition_error_message unless user_can_transition
          raise "User Not Allowed" unless user_can_transition
        end
        ChangeTracker.start unless ChangeTracker.started?
        # NOTE: This seems to give up without raising an error if the class doesn't respond to the event method. Probably not good. -SD
        #Attempt state transition
        self.send(event_method_name) if self.respond_to? event_method_name
        # puts "State transition should have been successful"
        ChangeTracker.commit
      rescue Workflow::NoTransitionAllowed => e
        #Transition failed        
        set_conditional_state_transition_error_message event_name
      rescue => e
        #TODO remove the generic rescue and only have specific errors   
        puts e.inspect
      end
    end
    
    # Called by handle_state_transition. Seems to set a workflow error message based on current state and passed event_name. -SD
    def set_conditional_state_transition_error_message event_name
      #TODO create this method based on the event guard somehow
      state_object = self.current_state
      #TODO currently just guess the error message from the first matching event name.  Could possibly concat all error messages for this event name
      message = state_object.events[event_name].first.meta[:error_message] || "The page is missing data needed to advance workflow"
      
      set_workflow_error_message message
    end
    
    # Called by handle_state_transition. Seems to set a workflow error message related to "role transition" -SD
    def set_role_transition_error_message
      message = "This User is not authorized to advance this part of the workflow"
      set_workflow_error_message message
    end
    
    #TODO see if this can be improved
    def set_workflow_error_message message
      Ramaze::Current.session.flash[:workflow_error_message] = message
    end
    
    # Does not seem to be used. Commenting out. -SD
    # def set_workflow_success_message message
    #   Ramaze::Current.sesssion.flash[:workflow_success_message] = message
    # end
    
    # Does not seem to be used. Commenting out. -SD
    # def set_workflow_information_message message
    #   Ramaze::Current.session.flash[:workflow_information_message] = message
    # end
    
    # Called from gui_site's workflow index.haml to get a readable name for the current state. -SD
    # Should not be located in application! -SD
    def readable_state_name
      state_object = self.current_state
      state_name = state_object.meta[:display_name] || state_object.name.to_s
      state_name.titleize
    end
    
    # Called from gui_site's workflow index.haml to get an identifier for the workflow object. -SD
    # Should not be located in application! -SD
    def workflow_identifier
      workflow_identifier_string = "#{self.class.to_title} - #{self.id}"
    end
    
    # Retrieve all currently available events for the current state -SD
    #This is very similar to the find all events by role but without the role restrictions
    # Should not be located in application! -SD
    def available_events
      state_object = self.current_state
      state_object.events
    end
    
    # Called from gui_site's workflow index.haml to get a label for the next action and is paired with the URL from #get_state_page_url. -SD
    # Should not be located in application! -SD
    def readable_next_action
      state_object = self.current_state
      next_action_name = state_object.meta[:next_action_display_name]
      
      #If the next action is not in the meta information then take a guess based on the first event name
      unless next_action_name
        first_event_name = state_object.events.keys.first
        next_action_name = first_event_name.to_s.titleize
      end
      next_action_name
    end
    
    # Returns the view_name tied to the current state. -SD
    # Should not be located in application! -SD
    def get_state_view_name
      view_name = self.current_state.meta[:page] || :details
    end
    
    # Returns the view_type tied to the current state. -SD
    # Should not be located in application! -SD
    def get_state_view_type
      view_type = self.current_state.meta[:view_type] || :normal
    end
    
    # Returns the roles associated with the current state. -SD
    # Should not be located in application! -SD
    def get_state_roles
      states = self.current_state.meta[:roles] || ["all_roles"]
    end
    
    # Builds a gui_site URL to edit the workflow object according to its current state. -SD
    # Should not be located in application! -SD
    def get_state_page_url
      
      view_name = get_state_view_name
      view_type = get_state_view_type
      
      #TODO check if there is a better way to get url in gui site where breadcrumbs are handled
      base_url = Gui.create_url_from_object(self)
      # classifiers = self.class.to_s.split('::')
      # base_url = classifiers.collect {|part| part}.join('/')
      # id = self.id.to_s
      view_params = "?view-name=#{view_name}"
      case view_type
        when :document
          view_params << "&view-type=Document"
          url = "#{base_url}#{view_params}"
        when :custom
          # base_url = self.current_state.meta[:view_url]
          # view_method = view_name.to_s.underscore
          # base_url << view_method
          base_url = self.current_state.meta[:view_url]
          view_params = "?workflow-item-id=#{self.id}&workflow-item-classifier=#{self.class}"
          url = self.current_state.meta[:view_url]
          # view_params = ""
        else
          #stuff for normal views if anything else is needed
          # view_params = "?view-name=#{view_name}"
          
      end
      url = "#{base_url}#{view_params}"

    end
    
  end
  
  class Minivan
    workflow do

      state :new_car_repair_order_created, :meta => {:roles => [:secretary], :view_type => :document, :page => "NewCarRepairOrderCreated"} do
       event :add_vehicle_information, :transitions_to => :started_vehicle_repair, :if => proc {|vehicle| vehicle.serial}
      end

      state :started_vehicle_repair, :meta => {:roles => [:mechanic], :page => "StartedVehicleRepair", :next_action_display_name => "Add Repaired Components to the Car"} do
        event :add_repaired_components, :transitions_to => :started_vehicle_inspection, :meta => {:roles => [:plant_manager]}, :if => proc {|vehicle| !vehicle.components.empty?}
      end
      
      state :started_vehicle_inspection, :meta => {:roles => [:manager, :quality_inspector], :page => 'StartedVehicleInspection'} do
        event :complete_inspection, :transitions_to => :finished_workflow, :if => proc{|vehicle| vehicle.is_inspection_completed_test}
        event :complete_inspection, :transitions_to => :inspected_vehicle
      end
      
      state :inspected_vehicle, :meta => {:roles => [:all_roles], :page => "InspectedVehicle", :view_type => :custom, :view_url => '/workflow/inspected_vehicle', :next_action_display_name => "Vehicle Inspection Overview"} do
        event :start_tire_inspection, :transitions_to => :started_tire_inspection
        event :start_engine_inspection, :transitions_to => :started_engine_inspection
      end
      
      state :started_tire_inspection, :meta => {:roles => [:all_roles], :page => "StartedTireInspection"} do
        event :inspect_tires, :meta => {:display_name => "Complete Tire Inspection"}, :transitions_to => :started_vehicle_inspection
      end
      
      state :started_engine_inspection, :meta => {:roles => [:all_roles], :page => "StartedEngineInspection"} do
        event :inspect_engine, :transitions_to => :started_vehicle_inspection
      end
      
      state :finished_workflow, :meta => {:roles => [:manager]}
      
    end
  end
  
  #TODO Try to fix this somehow.  Hybrid Vehicle has to be given a workflow spec as well or it will cause errors since it inherits from Car even if it isn't used in the workflow.  This is a bit problematic
  class HybridVehicle    
    workflow do

      state :new_car_repair_order_created, :meta => {:roles => [:secretary], :view_type => :document, :page => "NewCarRepairOrderCreated"} do
       event :add_vehicle_information, :transitions_to => :started_vehicle_repair, :if => proc {|vehicle| vehicle.serial}
      end

      state :started_vehicle_repair, :meta => {:roles => [:mechanic], :page => "StartedVehicleRepair", :next_action_display_name => "Add Repaired Components to the Car"} do
        event :add_repaired_components, :transitions_to => :started_vehicle_inspection, :meta => {:roles => [:plant_manager]}, :if => proc {|vehicle| !vehicle.components.empty?}
      end
      
      state :started_vehicle_inspection, :meta => {:roles => [:manager, :quality_inspector], :page => 'StartedVehicleInspection'} do
        event :complete_inspection, :transitions_to => :finished_workflow, :if => proc{|vehicle| vehicle.is_inspection_completed_test}
        event :complete_inspection, :transitions_to => :inspected_vehicle
      end
      
      state :inspected_vehicle, :meta => {:roles => [:all_roles], :page => "InspectedVehicle", :view_type => :custom, :view_url => '/workflow/inspected_vehicle', :next_action_display_name => "Vehicle Inspection Overview"} do
        event :start_tire_inspection, :transitions_to => :started_tire_inspection
        event :start_engine_inspection, :transitions_to => :started_engine_inspection
      end
      
      state :started_tire_inspection, :meta => {:roles => [:all_roles], :page => "StartedTireInspection"} do
        event :inspect_tires, :meta => {:display_name => "Complete Tire Inspection"}, :transitions_to => :started_vehicle_inspection
      end
      
      state :started_engine_inspection, :meta => {:roles => [:all_roles], :page => "StartedEngineInspection"} do
        event :inspect_engine, :transitions_to => :started_vehicle_inspection
      end
      
      state :finished_workflow, :meta => {:roles => [:manager]}
      
    end
  end
  
end
