# Test action button widgets
Feature: Action Buttons
  In order to activate custom behavior on an object
  As a user
  I want to use an action button
  
  Background:
    Given I have reset the database
    And I have logged in
    And I expand the "People" header
    And I create a "People"
  
  Scenario: Action button returns a file
    Given I save the page
    And I set the "Name" to "George"
    And I set the "Family Size" to "99"
    When I click the "Download Info" button
    And I wait for the download to start
    Then I should receive a text file named "George's info.txt"
  
  Scenario: Action button returns a popup
    Given PENDING
  
  Scenario: Action button returns a popup with images
    Given PENDING

  Scenario: Discard unsaved changes
    And I set the "Name" to "George"
    And I set the "Family Size" to "99"
    And I discard changes on the page
    And I click the "Discard" button on the "discard changes" popup
    Then the value of "Name" should be ""
    Then the value of "Family Size" should be ""

  Scenario: Cancelling the discardure of unsaved changes
    And I set the "Name" to "George"
    And I set the "Family Size" to "99"
    And I discard changes on the page
    And I click the "Cancel" button on the "discard changes" popup
    Then the value of "Name" should be "George"
    Then the value of "Family Size" should be "99"