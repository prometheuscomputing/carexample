homepage_item :'Gui_Builder_Profile::User'
# Note, if you have multiple collections that are of the same domain class (as below), you will want to ensure that you use different getters for them, even if the respective collections should contain the same objects.
homepage_item :'People::Person', :getter => 'bobs'
homepage_item :'People::Person', :getter => 'people2'
homepage_item :'Automotive::Mechanic' # just used to test hiding
homepage_item :'Geography::City' # just used to test disabling
homepage_item :'Tools::ToolBox'
homepage_item :'Tools::Tool'
homepage_item :'Tools::Hammer'
homepage_item :'Tools::Wrench'

organizer(:Details, Home) {
  view_ref(:Summary, 'users', :label => 'Users')
  view_ref(:CollectionButtonTest, 'people2', :label => 'CollectionButtonTest')
  view_ref(:Bobs, 'bobs', :label => 'Bobs')
  view_ref(:Summary, 'mechanics', :label => 'Should Be Hidden!')
  view_ref(:Summary, 'cities', :label => 'Cities')
  view_ref(:Summary, 'toolboxes', :label => 'Tool Boxes')
  hide('mechanics')
  disable('cities')
  disable_creation('bobs')
  disable_deletion('bobs')
  html(:label => 'Link Test', :html => proc {
    haml = <<-EOS
    :sass
      .div_style
        text-align: right
        display: block
        
    %div.div_style
      %a.a_style{:href => 'https://prometheuscomputing.com/', :target=> '_blank'} Prometheus Computing (opens in new tab)
    EOS
    Haml::Engine.new(haml.reset_indentation).render(self)
  })
  # Note that the style is specified in two different ways here -- both should work.
  # Note also that one has :label explicitly specified and the other does not.  When unspecified, the label will be set to the text.  The label allows reordering.
  text_label('Tools', :label => 'blue_label', :style => 'font-size:16px; text-align:center; display:block; background-color:lightblue')
  text_label('Gold Label', :style => {'font-size' => '20px', 'text-align' => 'center', 'display' => 'block', 'background-color' => 'gold'})
  reorder('label_Gold Label', 'people', 'people2', 'bobs', 'vehicles', 'users', 'repair_shops', to_beginning: true)
  reorder('blue_label', 'tool_boxes', 'Link Test', to_end: true)
}

collection(:CollectionButtonTest, People::Person, :page_size => 10) do
  inherits_from_spec(:Summary, People::Person, :Collection, false)
  button('download_search_results', :label => 'Download Search Results', :display_result => :file)
end

# destroy(:Details, Gui::Home, :Organizer)
#
# organizer(:Details, Home) {
#   view_ref(:Summary, 'users', :label => 'Users')
# }
