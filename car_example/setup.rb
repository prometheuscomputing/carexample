require 'fileutils'
require 'car_example_generated'

# PID_FILE = File.expand_path('~/Prometheus/data/car_example_generated/pid/car_example.pid')

# Allow CarExample database to be specified via environmental variable
cedb = ENV['CEDB']
db_path = File.expand_path(cedb && !cedb.empty? ? cedb : CarExampleGenerated.db_location)

# Delete existing database
FileUtils.rm(db_path) if File.exist?(db_path) && !$preserve_db

# Set tempfile dir, and delete if existing
tempfile_dir = File.expand_path('~/Prometheus/data/car_example_generated/tmp/')
FileUtils.rm_rf(tempfile_dir) if File.exist?(tempfile_dir)

# Suppress output from Thin webserver
$suppress_output = true

require 'car_example'
require 'car_example/populate_data'
require 'gui_director'
require 'gui_site/launcher'
# setup_app erases ARGV, so back it up, then restore afterward
# argv_backup = ARGV.dup
# application_module = Foundation.setup_app('car_example', LodePath.find('car_example.rb'))
# ARGV.replace(argv_backup)

require LodePath.find('car_example/meta_info.rb')
require LodePath.find('car_example/app_options.rb')

options = {:server => :setup_only, :mode => :live, :db_location => db_path, :testing_mode => ENV['TESTING']}
# Options for :mode are :live and :dev
options[:mode] = :dev if ARGV.first =~ /dev/
ARGV.clear

# 'Run' the application. Since we are using the :setup_only server, this doesn't actually launch, just sets up the application options.
# The actual Ramaze run command is called by Capybara.
# Gui.run parses CLI options, which erases ARGV. Since we may want to re-run that code, restore ARGV afterward
Gui.run(CarExample, options)
# ARGV.replace(argv_backup)
