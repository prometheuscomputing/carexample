# Test collection filtering and pagination feature here.
# FIXME find a good name for this functionality
Feature: We need a good term for this
  In order to search for an object
  As a user
  I want to enter a search term that will look for matching values in all possible fields
  
  Background:
    Given I have reset the database
    And I have logged in
    And I should see that there are a total of "9" "Vehicle" in the header
    And I expand the "Vehicles" header
    And I open the "Vehicle" search
  
# ------------------------------------------ Home Page -------------------------------------------- #

#Start of Home Search All Filter Tests
  Scenario: Searching a collection with Search All on the Home Page
    #Match on a text field
    When I search all fields to see "Vehicle" with "Smart Car"
    And I submit the "Vehicle" search
    Then I should only see "Vehicle" where "Vehicle Model" is "Smart Car"
    And I should see "1" "Vehicle"
    #Match on a numeric field
    When I search all fields to see "Vehicle" with "30000"
    And I submit the "Vehicle" search
    Then I should only see "Vehicle" where "Cost" is "30000"
    And I should see "3" "Vehicle"
    #Match on a date field
    Given I expand the "People" header
    And I open the "People" search
    When I search all fields to see "People" with "1990"
    And I submit the "People" search
    Then I should see a "People" where "Date Of Birth" is "1990-01-04"
    And I should see "2" "People"
    #Match on a date field using partial month/day
    When I search all fields to see "People" with "01-01"
    And I submit the "People" search
    Then I should see a "People" where "Date Of Birth" is "1980-01-01"
    And I should see "2" "People"

#Boolean Logic Searches
  Scenario: Searching using "AND" between search values should narrow the search results down to only fields that contain both values
    #Match on a numeric and text field using AND
    When I search all fields to see "Vehicle" with "30000 AND Mustang"
    And I submit the "Vehicle" search
    Then I should only see "Vehicle" where "Cost" is "30000"
    And I should see "1" "Vehicle"
    #Match on a numeric field using spaces instead of "AND"
    When I search all fields to see "Vehicle" with "30000 Mustang"
    And I submit the "Vehicle" search
    Then I should only see "Vehicle" where "Cost" is "30000"
    And I should see "1" "Vehicle"
    
  Scenario: Searching using "OR" between search values will expand the search results to anything that contains either value
    #Match on a numeric and text field field using "OR"
    When I search all fields to see "Vehicle" with "20000 OR S80"
    And I submit the "Vehicle" search
    Then I should see a "Vehicle" where "Cost" is "20000"
    And I should see a "Vehicle" where "Vehicle Model" is "S80"
    And I should see "3" "Vehicle"

  Scenario: Searching using an "OR" and an "AND" within the same search value.  OR is the higher precedence operator and gets parsed first
    #Match on a numeric field using "OR" and "AND"
    When I search all fields to see "Vehicle" with "20000 OR 30000 AND S40"
    And I submit the "Vehicle" search
    Then I should only see "Vehicle" where "Cost" is "20000"
    And I should only see "Vehicle" where "Vehicle Model" is "S40"
    And I should see "1" "Vehicle"

  Scenario: Using search all pagination controls on the Home Page
    When I view "9" "Vehicle" per page
    And I submit the "Vehicle" search
    Then I should see "9" "Vehicle"
    When I view "1" "Vehicle" per page
    And I submit the "Vehicle" search
    Then I should see "1" "Vehicle"
    And I should only see "Vehicle" where "Vehicle Model" is "S40"
    When I view page "2" of "Vehicle"
    Then I should see "1" "Vehicle"
    And I should only see "Vehicle" where "Vehicle Model" is "S80"
    When I view "2" "Vehicle" per page
    And I submit the "Vehicle" search
    Then I should see "2" "Vehicle"
    And I should see a "Vehicle" where "Vehicle Model" is "Mustang"
    And I should see a "Vehicle" where "Vehicle Model" is "Civic"
    And the previous page button should not be disabled
    And the next page button should not be disabled
    When I click the previous page button
    And I should see a "Vehicle" where "Vehicle Model" is "S40"
    And I should see a "Vehicle" where "Vehicle Model" is "S80"
    
  # Test manual page index out of bounds handling with pagination with search all filter and per page changes
  Scenario: Using pagination controls on the Home Page with page bounds exception handling and search all filter
    When I view "2" "Vehicle" per page
    And I submit the "Vehicle" search

    When I view page "5" of "Vehicle"
    And I should only see "Vehicle" where "Vehicle Model" is "Rebel"

    When I view "3" "Vehicle" per page
    And I submit the "Vehicle" search
    Then I should see "3" "Vehicle"
    And I should see a "Vehicle" where "Vehicle Model" is "Prius"
    And I should see a "Vehicle" where "Vehicle Model" is "Caravan"
    And I should see a "Vehicle" where "Vehicle Model" is "Rebel"

    When I view "2" "Vehicle" per page
    And I submit the "Vehicle" search
    Then I should see "2" "Vehicle"
    And I should see a "Vehicle" where "Vehicle Model" is "Charger"
    And I should see a "Vehicle" where "Vehicle Model" is "Smart Car"

    # Now add filter
    When I search all fields to see "Vehicle" with "30000"
    And I submit the "Vehicle" search
    And I should see "2" "Vehicle"
    And I should see a "Vehicle" where "Vehicle Model" is "S80"
    And I should see a "Vehicle" where "Vehicle Model" is "Mustang"
    # And I should see that there are a total of "9" "Vehicle" in the header
    Then I should see that there are a total of "3" "Vehicle" in the search results header

    # Should ignore new page index when used when also changing filter
    When I view page "2" of "Vehicle"
    When I search all fields to see "Vehicle" with "S"
    And I submit the "Vehicle" search
    Then I should see "2" "Vehicle"
    And I should see a "Vehicle" where "Vehicle Model" is "S40"
    And I should see a "Vehicle" where "Vehicle Model" is "S80"
    Then I should see that there are a total of "5" "Vehicle" in the search results header
  
  # Test next, previous page handling with pagination and filtering
  Scenario: Using search all filter and navigating pagination using previous and next pagination buttons
    When I search all fields to see "Vehicle" with "S"
    When I view "2" "Vehicle" per page
    And I submit the "Vehicle" search
    Then I should see "2" "Vehicle"
    And I should see a "Vehicle" where "Vehicle Model" is "S40"
    And I should see a "Vehicle" where "Vehicle Model" is "S80"
    Then I should see that there are a total of "5" "Vehicle" in the search results header
    When I click the next page button
    Then I should see "2" "Vehicle"
    And I should see a "Vehicle" where "Vehicle Model" is "Mustang"
    And I should see a "Vehicle" where "Vehicle Model" is "Smart Car"
    
  # Test filter clear button, test one, and none search results message (search all)
  Scenario: Using filter, counting search results, and filter reset
    When I search all fields to see "Vehicle" with "Caravan"
    When I view "8" "Vehicle" per page
    And I submit the "Vehicle" search
    And I should only see "Vehicle" whose "Vehicle Model" is "Caravan"
    Then I should see that there are a total of "1" "Vehicle" in the search results header
    # No search results
    When I search all fields to see "Vehicle" with "Edsel"
    And I submit the "Vehicle" search
    Then I should see "0" "Vehicle"
    Then I should see that there are a total of "0" "Vehicle" in the search results header
    When I reset the "Vehicle" filter
    Then I should see "8" "Vehicle"
    
# ------------------------------------------ Object Page -------------------------------------------- #

  Scenario: Searching associated objects using search all on a many-to-many association class collection on an object page
    Given I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I click on the "Owners" header
    # The populated data creates three owners for the S80 named Bob, Sally, and Phil
    And I should see an "Owner" whose "Name" is "Bob"
    And I should see an "Owner" whose "Name" is "Sally"
    And I should see an "Owner" whose "Name" is "Phil"
    And I should see "3" "Owners"
    When I open the "Owners" search
    And I search all fields to see "Owners" with "Sue"
    And I submit the "Owners" search
    Then I should see "0" "Owners"
    When I search all fields to see "Owners" with "Bob AND 66.67 AND Bob's description AND default_user_replaceme.jpeg AND 1990-01-04 AND 3"
    And I submit the "Owners" search
    Then I should see "1" associated "Owner"
    When I reset the "Owners" filter
    When I search all fields to see "Owners" with "Je OR Jo"
    When I show the possible "Owners"
    Then I should see "2" unassociated "Owners"

  # ------------------------------------------ many-to-many
  # Vehicle maintained_by association
  # This association is populated identically to the above one-to-many (Maintainers = Bob, Sally, Phil)
  Scenario: Searching all fields for associated objects on a many-to-many association on an object page
    Given I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I click on the "Maintained By" header
    When I open the "Maintained By" search
    And I search all fields to see "Maintained By" with "1990-01-04"
    And I submit the "Maintained By" search
    Then I should see "2" associated "Maintained By" # Bob, Sally have date_of_birth => '1990-01-04'
    And I should only see "Maintained By" whose "Date Of Birth" is "1990-01-04"
    When I search all fields to see "Maintained By" with "1990-01-04 AND 3"
    And I submit the "Maintained By" search
    Then I should see "1" associated "Maintained By" # Should not include Phil since original d.o.b. filter is still in place
    And I should only see "Maintained By" whose "Date Of Birth" is "1990-01-04" and "Family Size" is "3"
    When I search all fields to see "Maintained By" with "1990-01-04 AND 3 AND Bob's description"
    And I submit the "Maintained By" search
    Then I should see "1" associated "Maintained By"
    And I should only see "Maintained By" whose "Date Of Birth" is "1990-01-04", "Family Size" is "3", and "Description" contains "Bob's description"
    
  # Vehicle maintained_by association
  Scenario: Searching all fields for unassociated objects on a many-to-many association on an object page
    Given I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I click on the "Maintained By" header
    When I open the "Maintained By" search # Entering the filter before toggling show possible.
    And I search all fields to see "Maintained By" with "J" # Should be Jeff, Joe, Jason
    And I show the possible "Maintained By"
    Then I should see "3" unassociated "Maintained By"
    And I should only see "Maintained By" whose "Name" contains "J" and "Type" is "Mechanic"
    When I search all fields to see "Maintained By" with "Jeff"
    And I submit the "Maintained By" search
    Then I should see "1" unassociated "Maintained By"
    And I should only see "Maintained By" whose "Name" is "Jeff" and "Type" is "Mechanic"
    
  # ------------------------------------------ many-to-one (unassociated only)
  # Vehicle maintained_by association
  Scenario: Searching all fields for unassociated objects on a many-to-one association on an object page
    Given I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I click on the "Being Repaired By" header
    And I show the possible "Being Repaired By"
    And I should see "2" unassociated "Being Repaired By"
    When I open the "Being Repaired By" search
    And I search all fields to see "Being Repaired By" with "Tires and More"
    And I submit the "Being Repaired By" search
    Then I should see "1" unassociated "Being Repaired By"
    And I should only see "Being Repaired By" where "Name" is "Tires and More"
    When I search all fields to see "Being Repaired By" with "Non existent Tire Company"
    And I submit the "Being Repaired By" search
    Then I should see "0" unassociated "Being Repaired By"
    
    
  # ------------------------------------------ one-to-one (unassociated only)
  # VIN vehicle association
  Scenario: Searching all fields for unassociated objects on a one-to-one association on an object page
    Given I expand the "VINs" header
    And I create a "VIN" of type "VIN" where "Issue Date" is "1990-01-04" and "Vin" is "1234567890"
    And I click on the "Vehicle" header
    When I open the "Vehicle" search
    And I search all fields to see "Vehicle" with "S80"
    And I show the possible "Vehicle"
    Then I should see "1" unassociated "Vehicle"
    When I search all fields to see "Vehicle" with "Rebel"
    And I submit the "Vehicle" search
    Then I should see "1" unassociated "Vehicle"
    And I should only see "Vehicle" where "Make" is "Honda", "Vehicle Model" is "Rebel", and "Cost" is "5000"
  
  Scenario: Using pagination controls when searching all fields for unassociated objects on a one-to-one association on an object page
    Given I expand the "VINs" header
    And I create a "VIN" of type "VIN" where "Issue Date" is "1990-01-04" and "Vin" is "1234567890"
    And I click on the "Vehicle" header
    And I open the "Vehicle" search
    And I show the possible "Vehicle"
    And I search all fields to see "Vehicle" with "s"
    And I submit the "Vehicle" search
    When I view "1" "Vehicle" per page
    And I submit the "Vehicle" search
    Then I should only see "Vehicle" where "Vehicle Model" is "S40"
    When I click the next page button
    Then I should only see "Vehicle" where "Vehicle Model" is "S80"
    When I view "2" "Vehicle" per page
    And I submit the "Vehicle" search
    Then I should see "2" unassociated "Vehicle"
    And I should see an "Vehicle" where "Vehicle Model" is "Mustang"
    And I should see an "Vehicle" where "Vehicle Model" is "Smart Car"
    When I click the previous page button
    Then I should see "2" unassociated "Vehicle"
    And I should see an "Vehicle" where "Vehicle Model" is "S40"
    And I should see an "Vehicle" where "Vehicle Model" is "S80"
    And the previous page button should be disabled
    And the next page button should not be disabled
    
  Scenario: Search all filter should apply to both associations and unassociations on a many-to-many relationship
    Given I edit the "Vehicle" where "Vehicle Model" is "S80"
    When I click on the "Drivers" header
    And I open the "Drivers" search
    And I search all fields to see "Drivers" with "3"
    And I should only see "Drivers" where "Name" is "Bob"
    When I show the possible "Drivers"
    And I submit the "Drivers" search
    Then I should only see "Drivers" where "Name" is "Phil"
    When I show the current "Drivers"
    And I submit the "Drivers" search
    And I should only see "Drivers" where "Name" is "Bob"

  Scenario: Search all Filtering on association attribute should not break possible association filtering
    Given I edit the "Vehicle" where "Vehicle Model" is "S80"
    When I click on the "Drivers" header
    And I open the "Drivers" search
    And I search all fields to see "Drivers" with "True"
    And I submit the "Drivers" search
    Then I should see "1" "Drivers"
    And I should only see "Drivers" where "Name" is "Bob"
    When I show the possible "Drivers"
    And I submit the "Drivers" search
    Then I should see "0" "Drivers"
    When I show the current "Drivers"
    And I search all fields to see "Drivers" with "False"
    And I submit the "Drivers" search
    Then I should see "0" "Drivers"
    When I show the possible "Drivers"
    Then I should see "0" "Drivers"
    