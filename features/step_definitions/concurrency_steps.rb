# This is ugly but gets the job done...If you have a complex attribute and you need to specify which sub-attribute then you denote it with a colon, <label>:<sub-attribute>.  And example for a RichText with the label 'Description' would be Description:content.  #find_options_in_popup_row will handle it
Then /^the "([^"]*)" popup should have (?:my change|the original value) "([^"]*)" and the change found "([^"]*)" for the "([^"]*)"$/ do |type, my_change, change_found, label|
  data = find_options_in_popup_row(type, label)
  data[:my_changes].should == my_change
  data[:changes_found].should == change_found
end

# Useful for when you can't simply grab the values of an input.
Then /^the "([^"]*)" popup should have (my text changes|the text changes found) "([^"]*)" for the "([^"]*)"$/ do |type, switch, change, label| #"
  which_change = (switch == 'my text changes') ? 'my_changes' : 'changes_found'
  data = find_text_in_popup_row(type, label, which_change)
  data.should == change
end

Then /^the "([^"]*)" popup should show my changes as "([^"]*)" for the "([^"]*)"$/ do |type, change, label| #"
  data = find_text_in_popup_row(type, label, 'my_changes')
  data.should == change
end

Then /^the "([^"]*)" popup should show the concurrent changes as "([^"]*)" for the "([^"]*)"$/ do |type, change, label| #"
  data = find_text_in_popup_row(type, label, 'changes_found')
  data.should == change
end

Then /^the "([^"]*)" popup should( not)? have the selections disabled for the "([^"]*)"$/ do |type, flag, label|
  selectors = find_selectors_in_popup_row(type, label)
  flag = flag.strip if flag
  if flag == 'not'
    selectors[:my_changes][:disabled].should(be false)
    selectors[:changes_found][:disabled].should(be false)
  else
    selectors[:my_changes][:disabled].should(be true)
    selectors[:changes_found][:disabled].should(be true)
  end
end

Then /^the "([^"]*)" popup should have (my changes|the changes found) selected for the "([^"]*)"$/ do |type, switch, label|
  which_change = (switch == 'my changes') ? 'my_changes' : 'changes_found'
  other_change = (switch == 'my changes') ? 'changes_found' : 'my_changes'
  selectors = find_selectors_in_popup_row(type, label)
  expect(selectors[which_change.to_sym][:element]).to be_checked
  expect(selectors[other_change.to_sym][:element]).not_to be_checked
end

When /^I select (my changes|the changes found) for the "([^"]*)" on the "([^"]*)" popup$/ do |switch, label, type|
  which = (switch == 'my changes') ? 'my_changes' : 'changes_found'
  select_in_popup_row(type, label, which)
end

Then /^I should receive the warning "(.*)" on the concurrency popup/ do |warning|
  verify_warning(warning, '.concurrency_warning')
end