# Test collection filtering and pagination feature here.
Feature: Collection filtering
  In order to search for an object
  As a user
  I want to narrow the selection by filtering
  
  Background:
    Given I have reset the database
    And I have logged in
    And I should see that there are a total of "9" "Vehicle" in the header
    And I expand the "Vehicles" header
    And I open the "Vehicle" search
    And I click on the "Vehicle" advanced search link
  
  # ------------------------------------------ Home Page
  # Vehicle collection on Home Page
  Scenario: Searching a collection on the Home Page
    # filter on text field
    When I filter to see "Vehicle" where "Vehicle Model" is "Smart Car"
    And I submit the "Vehicle" search
    Then I should only see "Vehicle" where "Vehicle Model" is "Smart Car"
    And I should see "1" "Vehicle"
    # filter on numeric field
    When I filter to see "Vehicle" where "Vehicle Model" is ""
    And I filter to see "Vehicle" where "Cost" is "30000"
    And I submit the "Vehicle" search
    Then I should only see "Vehicle" where "Cost" is "30000"
    And I should see "3" "Vehicle"

    # Filtering on choice is now tested in enumerations.feature
    When I filter to see "Vehicle" where "Cost" is ""
    # And I filter to see "Vehicle" where "Make" is "Home Made"  
    # And I submit the "Vehicle" search
    # Then I should only see "Vehicle" where "Make" is "Home Made"
    # And I should see "1" "Vehicle"

    # Filter on type field (choice field selecting specific class to search)
    # When I filter to see "Vehicle" where "Make" is ""
    And I filter to see "Vehicle" where "Type" is "Electric Vehicle"
    And I submit the "Vehicle" search
    Then I should only see "Vehicle" where "Type" is one of ["Electric Vehicle" or "Hybrid Vehicle"]
    And I should see "2" "Vehicle"
    # Filter on all 4 above fields
    When I filter to see "Vehicle" where "Vehicle Model" is "S80"
    And I filter to see "Vehicle" where "Cost" is "30000"
    # And I filter to see "Vehicle" where "Make" is "Volvo"
    And I filter to see "Vehicle" where "Type" is "Car"
    And I submit the "Vehicle" search
    Then I should only see "Vehicle" where "Vehicle Model" is "S80"
    And I should only see "Vehicle" where "Cost" is "30000"
    And I should only see "Vehicle" where "Make" is "Volvo"
    And I should only see "Vehicle" where "Type" is "Car"
    And I should see "1" "Vehicle"
  
  Scenario: Using pagination controls on the Home Page
    When I view "9" "Vehicle" per page
    And I submit the "Vehicle" search
    Then I should see "9" "Vehicle"
    When I view "1" "Vehicle" per page
    And I submit the "Vehicle" search
    Then I should see "1" "Vehicle"
    And I should only see "Vehicle" where "Vehicle Model" is "S40"
    When I view page "2" of "Vehicle"
    And I submit the "Vehicle" search
    Then I should see "1" "Vehicle"
    And I should only see "Vehicle" where "Vehicle Model" is "S80"
    When I view "2" "Vehicle" per page
    And I submit the "Vehicle" search
    Then I should see "2" "Vehicle"
    And I should see a "Vehicle" where "Vehicle Model" is "Mustang"
    And I should see a "Vehicle" where "Vehicle Model" is "Civic"
    And the previous page button should not be disabled
    And the next page button should not be disabled
    When I click the previous page button
    And I should see a "Vehicle" where "Vehicle Model" is "S40"
    And I should see a "Vehicle" where "Vehicle Model" is "S80"


  
  # Test manual page index out of bounds handling with pagination with filter and per page changes
  # This test is no longer serves a purpose since I redid the pagination to a list of of page numbers-TB
  @known_failing
  Scenario: Using pagination controls on the Home Page with page bounds exception handling and filter
    When I view "2" "Vehicle" per page
    When I view page "5" of "Vehicle"
    And I submit the "Vehicle" search
    And I should only see "Vehicle" where "Vehicle Model" is "Rebel"

    When I view page "6" of "Vehicle"
    And I submit the "Vehicle" search
    And I should only see "Vehicle" where "Vehicle Model" is "Rebel"

    When I view "3" "Vehicle" per page
    And I submit the "Vehicle" search
    Then I should see "3" "Vehicle"
    And I should see a "Vehicle" where "Vehicle Model" is "Prius"
    And I should see a "Vehicle" where "Vehicle Model" is "Caravan"
    And I should see a "Vehicle" where "Vehicle Model" is "Rebel"

    When I view "2" "Vehicle" per page
    And I submit the "Vehicle" search
    Then I should see "2" "Vehicle"
    And I should see a "Vehicle" where "Vehicle Model" is "Charger"
    And I should see a "Vehicle" where "Vehicle Model" is "Smart Car"

    # Now add filter
    When I filter to see "Vehicle" where "Type" is "Car"
    And I submit the "Vehicle" search
    Then I should see "2" "Vehicle"
    And I should see a "Vehicle" where "Vehicle Model" is "S40"
    And I should see a "Vehicle" where "Vehicle Model" is "S80"
    # And I should see that there are a total of "9" "Vehicle" in the header
    Then I should see that there are a total of "7" "Vehicle" in the search results header

    # Should ignore new page index when used when also changing filter
    When I view page "2" of "Vehicle"
    When I filter to see "Vehicle" where "Vehicle Model" is "S"
    And I submit the "Vehicle" search
    Then I should see "2" "Vehicle"
    And I should see a "Vehicle" where "Vehicle Model" is "S40"
    And I should see a "Vehicle" where "Vehicle Model" is "S80"
    Then I should see that there are a total of "4" "Vehicle" in the search results header


  # Test next, previous page handling with pagination. Confirm total page count

  # Test filter clear button, test one, and none search results message
  Scenario: Using filter, counting search results, and filter reset
    When I filter to see "Vehicle" where "Type" is "Minivan"
    When I view "8" "Vehicle" per page
    And I submit the "Vehicle" search
    And I should only see "Vehicle" whose "Vehicle Model" is "Caravan"
    Then I should see that there are a total of "1" "Vehicle" in the search results header
    # No search results
    When I filter to see "Vehicle" where "Vehicle Model" is "Edsel"
    And I submit the "Vehicle" search
    Then I should see "0" "Vehicle"
    Then I should see that there are a total of "0" "Vehicle" in the search results header
    When I reset the "Vehicle" filter
    Then I should see "8" "Vehicle"
  
  # ------------------------------------------ many-to-many association class
  # Vehicle owners association
  Scenario: Searching associated objects on a many-to-many association class collection on an object page
    Given I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I click on the "Owners" header
    # The populated data creates three owners for the S80 named Bob, Sally, and Phil
    And I should see an "Owner" whose "Name" is "Bob"
    And I should see an "Owner" whose "Name" is "Sally"
    And I should see an "Owner" whose "Name" is "Phil"
    And I should see "3" "Owners"
    When I open the "Owners" search
    And I click on the "Owners" advanced search link
    And I filter to see "Owners" whose "Name" is "Sue"
    And I submit the "Owners" search
    Then I should see "0" "Owners"
    When I filter to see "Owners" whose "Name" is "Bob"
    # TODO: the Percent Ownership filter seems to be broken. Should improve this test
    And I filter to see "Owners" whose "Percent Ownership" is "66.67"
    And I filter to see "Owners" whose "Description" is "Bob's description"
    And I filter to see "Owners" whose "Photo" is "default_user_replaceme.jpeg"
    And I filter to see "Owners" whose "Date Of Birth" is "1990-01-04"
    And I filter to see "Owners" whose "Family Size" is "3"
    And I submit the "Owners" search
    Then I should see "1" associated "Owner"
    When I reset the "Owners" filter
    And I filter to see "Owners" whose "Family Size" is "*"
    And I submit the "Owners" search
    Then I should see "3" associated "Owners"
    When I show the possible "Owners"
    Then I should see "0" unassociated "Owners"
  
  # Vehicle owners association
  Scenario: Searching unassociated objects on a many-to-many association class collection on an object page
    Given I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I click on the "Owners" header
    And I show the possible "Owners"
    And I should see the first "6" unassociated "Owners"
    # And I should only see "Owners" whose "Percent Ownership" is "" # Because it is non-existent for unassociated items
    When I open the "Owners" search
    And I click on the "Owners" advanced search link
    And I filter to see "Owners" whose "Name" is "Bill", "Date Of Birth" is "1977-01-01", and "Type" is "Mechanic"
    And I submit the "Owners" search
    Then I should see "1" unassociated "Owner"
  
  # ------------------------------------------ one-to-many
  # Vehicle occupants association
  Scenario: Searching associated objects on a one-to-many association on an object page
    Given I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I click on the "Occupants" header
    When I open the "Occupants" search
    And I click on the "Occupants" advanced search link
    And I filter to see "Occupants" whose "Date Of Birth" is "1990-01-04"
    And I submit the "Occupants" search
    Then I should see "2" associated "Occupants" # Bob, Sally have date_of_birth => '1990-01-04'
    And I should only see "Occupants" whose "Date Of Birth" is "1990-01-04"
    When I filter to see "Occupants" whose "Family Size" is "3"
    And I submit the "Occupants" search
    Then I should see "1" associated "Occupant" # Should not include Phil since original d.o.b. filter is still in place
    And I should only see "Occupants" whose "Date Of Birth" is "1990-01-04" and "Family Size" is "3"
    When I filter to see "Occupants" whose "Description" contains "Bob's description"
    And I submit the "Occupants" search
    Then I should see "1" associated "Occupant"
    And I should only see "Occupants" whose "Date Of Birth" is "1990-01-04", "Family Size" is "3", and "Description" contains "Bob's description"
  
  # Vehicle occupants association
  Scenario: Searching unassociated objects on a one-to-many association on an object page
    Given I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I click on the "Occupants" header
    When I open the "Occupants" search # Entering the filter before toggling show possible.
    And I click on the "Occupants" advanced search link
    And I filter to see "Occupants" whose "Name" contains "J" and "Type" is "Mechanic" # Should be Jeff, Joe, Jason
    And I show the possible "Occupants"
    Then I should see "3" unassociated "Occupants"
    And I should only see "Occupants" whose "Name" contains "J" and "Type" is "Mechanic"
    When I filter to see "Occupants" whose "Name" is "Jeff"
    And I submit the "Occupants" search
    Then I should see "1" unassociated "Occupant"
    And I should only see "Occupants" whose "Name" is "Jeff" and "Type" is "Mechanic"

  Scenario: Using pagination controls on a one-to-many association on an object page
    Given I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I click on the "Occupants" header
    When I view "1" "Occupant" per page
    And I submit the "Occupants" search
    Then I should see "1" "Occupant"
    And I should only see "Occupants" whose "Name" is "Bob"
    When I view page "2" of "Occupants"
    And I submit the "Occupants" search
    Then I should see "1" "Occupant"
    And I should only see "Occupants" whose "Name" is "Sally"
    When I view "2" "Occupants" per page
    And I submit the "Occupants" search
    Then I should see "1" "Occupant"
    And I should only see "Occupants" whose "Name" is "Phil"
    When I view page "1" of "Occupants"
    And I submit the "Occupants" search
    Then I should see "2" "Occupants"
    Then I should see an "Occupant" whose "Name" is "Bob"
    And I should see an "Occupant" whose "Name" is "Sally"
    And the previous page button should be disabled
    And the next page button should not be disabled
    When I click the next page button
    Then I should see "1" "Occupant"
    And I should only see "Occupants" whose "Name" is "Phil"
    And the previous page button should not be disabled
    And the next page button should be disabled
    When I click the previous page button
    Then I should see "2" "Occupants"
    And I should see an "Occupant" whose "Name" is "Bob"
    And I should see an "Occupant" whose "Name" is "Sally"
    And the previous page button should be disabled
    And the next page button should not be disabled

    
  # ------------------------------------------ many-to-many
  # Vehicle maintained_by association
  # This association is populated identically to the above one-to-many (Maintainers = Bob, Sally, Phil)
  Scenario: Searching associated objects on a many-to-many association on an object page
    Given I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I click on the "Maintained By" header
    When I open the "Maintained By" search
    And I click on the "Maintained By" advanced search link
    And I filter to see "Maintained By" whose "Date Of Birth" is "1990-01-04"
    And I submit the "Maintained By" search
    Then I should see "2" associated "Maintained By" # Bob, Sally have date_of_birth => '1990-01-04'
    And I should only see "Maintained By" whose "Date Of Birth" is "1990-01-04"
    When I filter to see "Maintained By" whose "Family Size" is "3"
    And I submit the "Maintained By" search
    Then I should see "1" associated "Maintained By" # Should not include Phil since original d.o.b. filter is still in place
    And I should only see "Maintained By" whose "Date Of Birth" is "1990-01-04" and "Family Size" is "3"
    When I filter to see "Maintained By" whose "Description" contains "Bob's description"
    And I submit the "Maintained By" search
    Then I should see "1" associated "Maintained By"
    And I should only see "Maintained By" whose "Date Of Birth" is "1990-01-04", "Family Size" is "3", and "Description" contains "Bob's description"
    
  # Vehicle maintained_by association
  Scenario: Searching unassociated objects on a many-to-many association on an object page
    Given I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I click on the "Maintained By" header
    When I open the "Maintained By" search # Entering the filter before toggling show possible.
    And I click on the "Maintained By" advanced search link
    And I filter to see "Maintained By" whose "Name" contains "J" and "Type" is "Mechanic" # Should be Jeff, Joe, Jason
    And I show the possible "Maintained By"
    Then I should see "3" unassociated "Maintained By"
    And I should only see "Maintained By" whose "Name" contains "J" and "Type" is "Mechanic"
    When I filter to see "Maintained By" whose "Name" is "Jeff"
    And I submit the "Maintained By" search
    Then I should see "1" unassociated "Maintained By"
    And I should only see "Maintained By" whose "Name" is "Jeff" and "Type" is "Mechanic"
  
  # ------------------------------------------ many-to-one (unassociated only)
  # Vehicle maintained_by association
  Scenario: Searching unassociated objects on a many-to-one association on an object page
    Given I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I click on the "Being Repaired By" header
    And I show the possible "Being Repaired By"
    And I should see "2" unassociated "Being Repaired By"
    When I open the "Being Repaired By" search
    And I click on the "Being Repaired By" advanced search link
    And I filter to see "Being Repaired By" where "Name" is "Tires and More"
    And I submit the "Being Repaired By" search
    Then I should see "1" unassociated "Being Repaired By"
    And I should only see "Being Repaired By" where "Name" is "Tires and More"
    When I filter to see "Being Repaired By" where "Name" is "Non existent Tire Company"
    And I submit the "Being Repaired By" search
    Then I should see "0" unassociated "Being Repaired By"
  
  # ------------------------------------------ one-to-one (unassociated only)
  # VIN vehicle association
  Scenario: Searching unassociated objects on a one-to-one association on an object page
    Given I expand the "VINs" header
    And I create a "VIN" of type "VIN" where "Issue Date" is "1990-01-04" and "Vin" is "1234567890"
    And I click on the "Vehicle" header
    When I open the "Vehicle" search
    And I click on the "Vehicle" advanced search link
    And I filter to see "Vehicle" where "Vehicle Model" is "S80"
    And I show the possible "Vehicle"
    Then I should see "1" unassociated "Vehicle"
    When I filter to see "Vehicle" where "Vehicle Model" is "Rebel" and "Type" is "Car"
    And I submit the "Vehicle" search
    Then I should see "0" unassociated "Vehicle"
    When I filter to see "Vehicle" where "Vehicle Model" is "Rebel" and "Type" is "Motorcycle"
    And I submit the "Vehicle" search
    Then I should see "1" unassociated "Vehicle"
    And I should only see "Vehicle" where "Make" is "Honda", "Vehicle Model" is "Rebel", and "Cost" is "5000"
  
  Scenario: Using pagination controls on unassociated objects on a one-to-one association on an object page
    Given I expand the "VINs" header
    And I create a "VIN" of type "VIN" where "Issue Date" is "1990-01-04" and "Vin" is "1234567890"
    And I click on the "Vehicle" header
    And I open the "Vehicle" search
    And I click on the "Vehicle" advanced search link
    And I show the possible "Vehicle"
    And I filter to see "Vehicle" where "Vehicle Model" contains "s"
    And I submit the "Vehicle" search
    When I view "1" "Vehicle" per page
    And I submit the "Vehicle" search
    Then I should only see "Vehicle" where "Vehicle Model" is "S40"
    When I click the next page button
    Then I should only see "Vehicle" where "Vehicle Model" is "S80"
    When I view "2" "Vehicle" per page
    And I submit the "Vehicle" search
    Then I should see "2" unassociated "Vehicle"
    And I should see an "Vehicle" where "Vehicle Model" is "Mustang"
    And I should see an "Vehicle" where "Vehicle Model" is "Smart Car"
    When I click the previous page button
    Then I should see "2" unassociated "Vehicle"
    And I should see an "Vehicle" where "Vehicle Model" is "S40"
    And I should see an "Vehicle" where "Vehicle Model" is "S80"
    And the previous page button should be disabled
    And the next page button should not be disabled
  
  # ------------------------------------------ Wildcard filtering
  # Vehicle occupants association
  Scenario: Searching associated objects on a one-to-many association on an object page using a wildcard
    Given I edit the "Vehicle" where "Vehicle Model" is "S40"
    And I click on the "Occupants" header
    When I open the "Occupants" search
    And I click on the "Occupants" advanced search link
    And I filter to see "Occupants" whose "Family Size" is "*"
    And I show the possible "Occupants"
    Then I should see "3" unassociated "Occupants" # Bob, Sally, Phil have a defined family size
    And I should only see "Occupants" whose "Family Size" is not ""
  
  Scenario: Submitting a filter via the enter key
    Given PENDING: Capybara upgrade so native method works
    # Also check that focus is returned to the correct filter_cell once population has finished

  Scenario: Filter should apply to both associations and unassociations on a many-to-many relationship
    Given I edit the "Vehicle" where "Vehicle Model" is "S80"
    When I click on the "Drivers" header
    And I open the "Drivers" search
    And I click on the "Drivers" advanced search link
    And I filter to see "Drivers" where "Family Size" is "3"
    And I should only see "Drivers" where "Name" is "Bob"
    When I show the possible "Drivers"
    And I submit the "Drivers" search
    Then I should only see "Drivers" where "Name" is "Phil"
    When I show the current "Drivers"
    And I submit the "Drivers" search
    And I should only see "Drivers" where "Name" is "Bob"

  Scenario: Filtering on association attribute should not break possible association filtering
    Given I edit the "Vehicle" where "Vehicle Model" is "S80"
    When I click on the "Drivers" header
    And I open the "Drivers" search
    And I click on the "Drivers" advanced search link
    And I filter to see "Drivers" where "Likes Driving" is "True"
    And I submit the "Drivers" search
    Then I should see "1" "Drivers"
    And I should only see "Drivers" where "Name" is "Bob"
    When I show the possible "Drivers"
    And I submit the "Drivers" search
    Then I should see "8" "Drivers"
    When I show the current "Drivers"
    And I filter to see "Drivers" where "Likes Driving" is "False"
    And I submit the "Drivers" search
    Then I should see "0" "Drivers"
    When I show the possible "Drivers"
    Then I should see "8" "Drivers"
    