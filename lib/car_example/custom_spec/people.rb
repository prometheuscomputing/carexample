# People::Person ===========================================
organizer(:Details, People::Person) {
  message('test_message', :label => 'Message Widget Test')
  reorder('test_message', :to_beginning => true)
  
  # inherits_from_spec(:OhMy, OhMy, nil, true) # see above for explanation
  button('download_info', :label => 'Download Serialized Information', :button_text => 'Download Info', :display_result => :file)
  text('description', :label => 'Description', :show_language_selection => true)
  reorder("Download Serialized Information", :after => "Name")
  # test disable_if and hide_if
  is_robot = proc{|arg| arg.name =~ /robot/i}
  disable_if(is_robot, 'description', 'photo', 'date_of_birth', 'last_updated', 'wakes_at', 'family_size', 'weight', 'dependent', 'handedness', 'manifesto', 'lucky_numbers', 'aliases', 'addresses', 'drives', 'maintains', 'occupying', 'repair_shops', 'vehicles', 'download_info')
  # Proc is multi-line in order to make sure multi-line procs are OK with generated site
  is_ghost = proc do |arg|
    arg.name =~ /ghost/i
  end
  hide_if(is_ghost, 'description', 'photo', 'date_of_birth', 'last_updated', 'wakes_at', 'family_size', 'weight', 'dependent', 'handedness', 'manifesto', 'lucky_numbers', 'aliases', 'addresses', 'drives', 'maintains', 'occupying', 'repair_shops', 'vehicles', 'download_info', 'founded')
  hide_if(:is_huge?, 'photo')

  # This is testing the ability to pass the vertex into the proc.  I could not really think of a really good 'domain real' reason to do this within car example but you'll have to trust that such reasons really do exist in other projects.  This does demonstrate the capability to manipulate page behavior based on information about widgets.
  getter_is_name = proc do |person_obj, widget|
    if n = person_obj.name
      n.downcase.gsub(/\s+/, '_') == widget.getter.to_s
    end
  end
  hide_if(getter_is_name, 'description', 'photo', 'date_of_birth', 'last_updated', 'wakes_at', 'family_size', 'weight', 'dependent', 'handedness', 'manifesto', 'lucky_numbers', 'aliases', 'addresses', 'drives', 'maintains', 'occupying', 'repair_shops', 'vehicles', 'download_info')

  # Tests of filtering -- Can't use 'Vehicles' in the labels because it causes Capybara/cukes to whine and cry
  view_ref(:Driving,'drives_expensive', :label => 'Bling Rides', :orderable => true)
  view_ref(:Summary,'electric_vehicles', :label => 'Electric Jalopies', :orderable => true)
  reorder("code_representation", :to_end => true)
  
  html(:label => 'HTML Widget Test', :html => proc{
    haml = <<-EOS
      %input#test{:type => 'hidden', :value => 'test'}
      %div
        %h3
          This is a test of the HTML widget on an object page.
    EOS
    Haml::Engine.new(haml.reset_indentation).render(self)
  })
}
organizer(:Details, Automotive::Mechanic) {
  inherits_from_spec(nil,People::Person,nil,true)
}
organizer(:Details, People::Clown::Clown) {
  inherits_from_spec(nil,People::Person,nil,true)
}

collection(:Bobs, People::Person, :page_size => 100, :filter_value => {:case_insensitive_like => {'name' => 'bob'}}) {
  inherits_from_spec(:Summary, People::Person, :Collection, false)
}

# ----------------------------------------------------------------
organizer(:Details, People::Driving) {
  relabel('Drife', 'Vehicle')
}