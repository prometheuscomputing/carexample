def visit_preferences
  find('.login-name').click
  click_link 'Preferences'
end

# --------------------------------------- Accessibility helpers
def enable_accessibility
  find('label[for="use_accessibility"]').click
  # check "Use accessible interface?"
end
def disable_accessibility
  find('label[for="use_accessibility"]').click
  # uncheck "Use accessible interface?"
end
# --------------------------------------- End Accessibility helpers

# --------------------------------------- Roles helpers
def verify_roles(roles, negative = false)
  current_roles.should_unless(negative) =~ roles
end
def add_roles(roles)
  get_roles.each do |role, _input|
    # input.set(true) if roles.include?(role)
    role.click if roles.include?(role.text)
  end
end
def remove_roles(roles)
  get_roles.each do |role, _input|
    # input.set(false) if roles.include?(role)
    role.click if roles.include?(role.text)
  end
end
def current_roles
  get_roles.select{|_, i| i.checked?}.keys.collect{|label_node| label_node.text}
end
def get_roles
  roles_hash = {}
  within_fieldset 'Roles' do
    inputs = all('input')
    roles = all('label').collect{|l| l}
    roles.each_with_index do |role, i|
      roles_hash[role] = inputs[i]
    end
  end
  roles_hash
end
# Verify whether (or not) roles appear on the page at all
def verify_roles_present(roles, negative = false)
  get_roles.keys.should_unless negative, include(*roles)
end
# --------------------------------------- End Roles helpers


# --------------------------------------- Perspectives helpers
def verify_perspectives(perspectives, negative = false)
  current_perspectives.should_unless(negative) =~ perspectives
end
def add_perspectives(perspectives)
  get_perspectives.each do |perspective, _input|
    # input.set(true) if perspectives.include?(perspective)
    perspective.click if perspectives.include?(perspective.text)
  end
end
def remove_perspectives(perspectives)
  get_perspectives.each do |perspective, _input|
    # input.set(false) if perspectives.include?(perspective)
    perspective.click if perspectives.include?(perspective.text)
  end
end
def current_perspectives
  get_perspectives.select{|_, i| i.checked?}.keys.collect{|label_node| label_node.text}
end
def get_perspectives
  perspectives_hash = {}
  within_fieldset 'Perspectives' do
    # NOTE: must specify visible => false since inputs are now hidden. TODO: see if they can be unhidden -SD
    inputs = all('input', :visible => false)
    perspectives = all('label').collect{|l| l}
    perspectives.each_with_index do |perspective, i|
      perspectives_hash[perspective] = inputs[i]
    end
  end
  perspectives_hash
end
# --------------------------------------- End Perspectives helpers