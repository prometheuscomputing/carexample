# Test user login/registration functionality
#  * Account creation
#  * Account login
Feature: Login & Registration
  In order to use the application
  As a user
  I want to register & login
  
  Background:
    Given I have reset the database
  
  Scenario: Registering a new account
    When I go to the url "/Automotive/Car/1"
    Then I should be at the login page
    When I click Register
    And I enter the username "Garry-Garrinson"
    And I enter the password "Garry2"
    And I re-enter the password "Garry2"
    And I enter the invitation code "sprockets"
    And I submit the registration
    Then I should be at a "Car" page
    And the value of "Vehicle Model" should be "S40"
  
  
  Scenario: Registering a new account (invalid username/passwords/invitation code)
    When I go to the url "/Automotive/Car/1"
    Then I should be at the login page
    When I click Register
    Then I should be at the registration page
    Then the text "Car Example is a commercial product developed by Prometheus Computing." should be on the page
    And the text "For pricing or more information contact Robert Edwards." should be on the page
    And the text "Prometheus Car Example Terms and Conditions" should be on the page
    When I enter the username "Garry"
    # Passwords don't match
    And I enter the password "Garry2"
    And I re-enter the password "Garry23"
    And I enter the invitation code "sprockets"
    And I submit the registration
    Then I should receive the error "Entered passwords did not match"
    Then I should be at the registration page
    # Check correct persistence of form fields
    And the username should be "Garry"
    And the password should be ""
    And the re-entered password should be ""
    And the invitation code should be "sprockets"
    # Short username
    When I enter the username "1"
    And I enter the password "Garry23"
    And I re-enter the password "Garry23"
    And I submit the registration
    Then I should receive the error "Username must be at least 3 characters"
    # Non-alphanumeric username
    When I enter the username "Garry<=>$$$"
    And I enter the password "Garry23"
    And I re-enter the password "Garry23"
    And I submit the registration
    Then I should receive the error "Usernames can only contain letters, numbers, underscores, and hyphens"
    # Short password
    When I enter the username "Garry"
    And I enter the password "123"
    And I re-enter the password "123"
    And I submit the registration
    Then I should receive the error "Password must be at least 5 characters"
    # Non-alphanumeric password
    When I enter the password "Garry23$$??#<?"
    And I re-enter the password "Garry23$$??#<?"
    And I submit the registration
    Then I should receive an error like "Passwords can only contain letters, numbers, space (no tabs) and the following symbols:"
    # Invalid invitation code
    When I enter the password "Garry23"
    And I re-enter the password "Garry23"
    And I enter the invitation code "fake_invitation_code"
    And I submit the registration
    Then I should receive the error "The invitation code you entered is not valid"
    # Test custom password requirement (no asterisks in this case)
    When I enter the username "Garry"
    And I enter the password "Garry23*"
    And I re-enter the password "Garry23*"
    And I enter the invitation code "sprockets"
    And I submit the registration
    Then I should receive the error "Your password may not have an asterisk!"
    # # No email entered
    # When I enter the username "Garry"
    # And I enter the password "Garry23"
    # And I re-enter the password "Garry23"
    # And I enter the invitation code "sprockets"
    # And I submit the registration
    # Then I should receive the error "The E-mail address is required."
    # Email already exists in the system
    When I enter the username "Garry"
    And I enter the password "Garry23"
    And I re-enter the password "Garry23"
    And I enter the invitation code "sprockets"
    And I enter the email "admin@mycompany.net"
    And I submit the registration
    Then I should receive the error "This E-mail address is already in use. Please use a different E-mail address."
    # Bad username/passwords/invitation/email code
    When I enter the username "<3"
    When I enter the password "<=>"
    And I re-enter the password "Garry23"
    And I enter the invitation code "fake_invitation_code"
    And I submit the registration
    Then I should receive the error "Entered passwords did not match"
    Then I should receive the error "Password must be at least 5 characters"
    Then I should receive an error like "Passwords can only contain letters, numbers, space (no tabs) and the following symbols:"
    Then I should receive the error "Username must be at least 3 characters"
    Then I should receive the error "Usernames can only contain letters, numbers, underscores, and hyphens"
    Then I should receive the error "The invitation code you entered is not valid"
    # Then I should receive the error "The E-mail address is required."
    # Good username/passwords/invitation code
    When I enter the username "Garry"
    And I enter the password "Garry23"
    And I re-enter the password "Garry23"
    And I enter the invitation code "sprockets"
    And I enter the email "test@test.test"
    And I submit the registration
    Then I should be at a "Car" page
    And the value of "Vehicle Model" should be "S40"
    
  
  Scenario: Registering a new account with no invitation code when an invitation code is not required.
  # This is dependent on the settings in CarExample meta_info.rb
    When I go to the url "/Automotive/Car/1"
    Then I should be at the login page
    When I click Register
    When I enter the username "Garry"
    When I enter the password "Garry23"
    And I re-enter the password "Garry23"
    And the invitation code should be ""
    And I submit the registration
    Then I should be at a "Car" page
    And the value of "Vehicle Model" should be "S40" 
  
  Scenario: Registering a new account (invalid login with single-time use invitation code)
    Given I have logged in as an administrator
    And I go to the administration panel
    And I add the invitation code "one_time_use_code" with expiration "3000-01-01" and "1" uses remaining
    And I submit the page
    And I sign out
    When I go to the url "/Automotive/Car/1"
    Then I should be at the login page
    When I click Register
    Then I should be at the registration page
    When I enter the username "Garry"
    And I enter the password "Garry2"
    And I re-enter the password "Garry23"
    And I enter the invitation code "sprockets"
    And I enter the email "test@test.test"
    And I submit the registration
    Then I should receive the error "Entered passwords did not match"
    When I enter the password "123"
    And I re-enter the password "123"
    And I enter the email "test@test.test"
    And I submit the registration
    Then I should receive the error "Password must be at least 5 characters"
    When I enter the password "Garry23"
    And I re-enter the password "Garry23"
    And I enter the email "test@test.test"
    And I submit the registration
    Then I should be at a "Car" page
    And the value of "Vehicle Model" should be "S40"
  
  Scenario: Logging in
    When I go to the url "/Automotive/Car/1"
    Then I should be at the login page
    Then the text "Car Example is a commercial product developed by Prometheus Computing." should be on the page
    And the text "For pricing or more information contact Robert Edwards." should be on the page
    And I enter the username "bobby"
    And I enter the password "bobby1"
    And I submit the login
    Then I should be at a "Car" page
    And the value of "Vehicle Model" should be "S40"
  
  Scenario: Logging in (bad password)
    When I go to the url "/Automotive/Car/1"
    Then I should be at the login page
    And I enter the username "bobby"
    And I enter the password "bobbykins"
    And I submit the login
    Then I should receive the error "Invalid username and password"
    And the username should be "bobby"
    And the password should be ""
    When I enter the password "bobby1"
    And I submit the login
    Then I should be at a "Car" page
    And the value of "Vehicle Model" should be "S40"
