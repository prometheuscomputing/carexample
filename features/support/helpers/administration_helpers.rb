require 'timecop'

def visit_user_administration
  click_link 'Administration'
  click_link 'Manage Users'
end
def visit_site_administration
  click_link 'Administration'
  click_link 'Manage Site'
end
def click_custom_control_input_within(container)
  # NOTE: have to click on the containing div instead of input since the actual input seems to be hidden. -SD
  container.find('.custom-control').click
end

# --------------------------------------- User helpers
def administration_add_user(username, password, email)
  password ||= "#{username}#{UserHelpers::PASS_SUFFIX}"
  email ||= "#{username}#{UserHelpers::EMAIL_SUFFIX}"
  fill_in "Username:", :with => username
  fill_in "Password:", :with => password
  fill_in "Email:", :with => email
end
def administration_get_user(username, fieldset = 'Users')
  within_fieldset fieldset do
    first('tr', :text => username, :minimum => 0)
  end
end
def administration_mark_user(username, fieldset = 'Users')
  click_custom_control_input_within(administration_get_user(username, fieldset))
end
def administration_user_exists?(username, fieldset = 'Users')
  !administration_get_user(username, fieldset).nil?
end
# --------------------------------------- End User helpers


# --------------------------------------- Role helpers
def administration_add_role(role)
  fill_in "Add Role:", :with => role
end
def administration_get_role(role)
  within_fieldset 'Roles' do
    first('tr', :text => role, :minimum => 0)
  end
end
def administration_mark_role(role)
  click_custom_control_input_within(administration_get_role(role))
end
def administration_role_exists?(role)
  !administration_get_role(role).nil?
end
# --------------------------------------- End Role helpers


# --------------------------------------- Role Assignment helpers
def administration_assign_roles(username, roles)
  administration_get_roles(username).each do |role, element|
    click_custom_control_input_within(element) if roles.include?(role)
  end
end
def administration_remove_roles(username, roles)
  administration_get_roles(username).each do |role, element|
    click_custom_control_input_within(element) if roles.include?(role)
  end
end
def administration_verify_roles(username, roles, negative = false)
  administration_get_roles(username).each do |role, element|
    # NOTE: have to set visible => false here since styling for input causes it to be hidden by a
    #       styled label element -SD
    expect(element.find('input', :visible => false).checked?).to eq(negative ? false : true) if roles.include?(role)
  end
end
def administration_get_roles(username)
  user_roles = {}
  within_fieldset 'Role Assignments' do
    header = first('tr')
    roles = header.all('th')[1..-1].collect{|th| th.text}
    user_row = first('tr', :text => username)
    user_role_elements = user_row.all('td')
    roles.each_with_index do |role, i|
      user_roles[role] = user_role_elements[i]
    end
  end
  user_roles
end
# --------------------------------------- End Role Assignment helpers


# --------------------------------------- Perspective helpers
def administration_add_perspective(perspective, base_perspective = nil)
  fill_in "Add Perspective:", :with => perspective
  select(base_perspective, :from => "Based On:") if base_perspective
end
def administration_get_perspective(perspective)
  within_fieldset 'Perspectives' do
    first('tr', :text => perspective, :minimum => 0)
  end
end
def administration_mark_perspective(perspective)
  click_custom_control_input_within(administration_get_perspective(perspective))
end
def administration_perspective_exists?(perspective)
  !administration_get_perspective(perspective).nil?
end
def administration_edit_perspective(perspective)
  administration_get_perspective(perspective).click_link('Edit')
end
def administration_add_substitution(master_word, replacement_word)
  fill_in "Master Word/Phrase:", :with => master_word
  fill_in "Replacement Word/Phrase:", :with => replacement_word
end
def administration_get_substitution(master_word, replacement_word = nil)
  row_string = master_word
  row_string += " #{replacement_word}" if replacement_word
  within_fieldset 'Existing Substitutions' do
    first('tr', :text => row_string, :minimum => 0)
  end
end
def administration_mark_substitution(master_word, replacement_word = nil)
  click_custom_control_input_within(administration_get_substitution(master_word, replacement_word))
end
def administration_substitution_exists?(master_word, replacement_word = nil)
  !administration_get_substitution(master_word, replacement_word).nil?
end
# --------------------------------------- End Perspective helpers


# --------------------------------------- Invitation Code helpers
def administration_add_code(code, expires = nil, uses_remaining = nil, roles = [], perspectives = [], time_travel = false)
  # If time travelling, set current time to the day before the expiration date
  # This avoids triggering date validation failure for the invitation code.
  past_time = Chronic.parse('yesterday', :now => Date.parse(expires)) if expires && time_travel
  Timecop.travel(past_time) if past_time
  create_invitation_code(code, expires, uses_remaining, true, roles, perspectives)
  # NOTE: Return to normal time is placed after visit_user_administration so request to create invitation code
  #       has time to finish before time is reset.
  visit_user_administration
  Timecop.return if past_time
end
def administration_get_code(code)
  within_fieldset 'Invitation Codes' do
    first('tr', :text => code, :minimum => 0)
  end
end
def administration_mark_code(code)
  click_custom_control_input_within(administration_get_code(code))
end
def administration_code_exists?(code)
  !administration_get_code(code).nil?
end
def create_invitation_code(code, expires = nil, uses_remaining = nil, on_administration = false, roles = [], _perspectives = [])
  visit_user_administration unless on_administration
  link = wait_until_attached { first('a', :text => 'Create A New Invitation Code') }
  link.click
  fill_in('Code', :with => code)
  if expires
    fill_in('Expiration Date', :with => expires)
    # Dismiss ui-datepicker
    case Capybara.current_driver
    when :chrome, :headless_chrome
      # Can't find ui-datepicker. Maybe already dismissed via 'fill_in'? -SD      
    else
      # Wait for ui-datepicker to appear
      find('.ui-datepicker')
    end
    # NOTE: Clicking on Code field, since if an expiration is set, it will leave open a date widget that blocks other controls
    #       Clicking anywhere outside the date field should remove the widget.
    page.find_field('Code').click
    # Soft wait for the ui-datepicker to disappear
    has_no_css?('.ui-datepicker')
  end
  fill_in('Remaining Uses', :with => uses_remaining) if uses_remaining
  if roles.any?
    within_fieldset 'Roles Granted to Invitee' do
      roles.each do |role|
        click_custom_control_input_within(find('tr', :text => role))
      end
    end
  end
  # NOTE: There's currently no way to add perspectives to an invitation code.
  # if perspectives.any?
  #   click_header('Perspectives')
  #   show_possible('Perspectives')
  #   select_rows('Perspectives', [{'Name' => perspectives}, {}, {}, {}])
  #   click_button_under_header('Add Associations', 'Perspectives')
  # end
  click_button 'Submit'
end
# --------------------------------------- End Invitation Code helpers


# --------------------------------------- Email Requirement helpers
def administration_add_email_requirement(regexp, description, failure_message)
  within_fieldset 'Add a New Email Requirement' do
    fill_in 'Regular Expression:', :with => regexp
    fill_in 'Description:', :with => description
    fill_in 'Failure Message:', :with => failure_message
  end
end
def administration_verify_email_requirement(regexp, negative)
  requirement = administration_get_email_requirement(regexp)
  negative ? expect(requirement).to(be_nil) : expect(requirement).not_to(be_nil)
end
def administration_mark_email_requirement(regexp)
  click_custom_control_input_within(administration_get_email_requirement(regexp))
end
def administration_get_email_requirement(regexp)
  within '#email_requirements' do
    first('tr', :text => regexp, :minimum => 0)
  end
end
# --------------------------------------- End Email Requirement helpers


# --------------------------------------- Password Requirement helpers
def administration_add_password_requirement(regexp, description, failure_message)
  within '#new_password_requirement' do
    fill_in 'Regular Expression:', :with => regexp
    fill_in 'Description:', :with => description
    fill_in 'Failure Message:', :with => failure_message
  end
end
def administration_verify_password_requirement(regexp, negative)
  requirement = administration_get_password_requirement(regexp)
  negative ? expect(requirement).to(be_nil) : expect(requirement).not_to(be_nil)
end
def administration_mark_password_requirement(regexp)
  click_custom_control_input_within(administration_get_password_requirement(regexp))
end
def administration_get_password_requirement(regexp)
  within '#password_requirements' do
    first('tr', :text => regexp, :minimum => 0)
  end
end
# --------------------------------------- End Password Requirement helpers

# --------------------------------------- Organization helpers

# --------------------------------------- End Organization helpers