Then /^I should see "(.*?)" in the tree view$/ do |node_text|
  confirm_tree_view_node(node_text)
end

Then /^I should not see the Display Tree View button/ do
  confirm_tree_view_button_missing
end

And /^I click the "(.*?)" tree node$/ do |node_text|
  click_tree_view_node(node_text)
  sleep(0.8) # Allow time for the ajax and javascript dom manipulations
end

And /^I should see "(.*?)" as the selected node$/ do |node_text|
  confirm_selected_node(node_text)
end

And /^I should not see the tree view/ do
  confirm_tree_view_hidden
end