# Test all non-collection/organizer widgets:
#    * string
#    * time
#    * date
#    * timestamp
#    * fixedpoint (bigint) ?
#    * integer
#    * number (float)
#    * boolean
#    * choice -- These are really associations but the widgets act like attribute widgets
#    * label
#    * link
#    * button
#    * RichText
#    * File
#    * Code
#    * ViewRef

# Test various attribute types here.
Feature: Conditionally hiding simple widgets
  In order to prevent improper changes to objects
  As a user
  I want the ability to change the properties of an object to be conditionally restricted
  
  Background:
    Given I have reset the database
    And   I have logged in
    And   I expand the "People" header
    And   I create a "People"
  
  Scenario: Conditions to hide widgets are not met
    When I set the "Name" to "George"
    And  I save the page
    Then there should be an input for the "Name" attribute
    And  there should be an edit link for the "Description" attribute
    When I choose to edit the "Description" text
    Then there should be a text area for the "Description" attribute
    And  there should be a file upload input for the "Photo" attribute
    And  there should be an input for the "Date Of Birth" attribute
    And  there should be an input for the "Last Updated" attribute
    And  there should be an input for the "Wakes At" attribute
    And  there should be an input for the "Family Size" attribute
    And  there should be an input for the "Weight" attribute
    And  there should be a dropdown for the "Dependent" attribute
    And  there should be a dropdown for the "Handedness" attribute
    And  there should be an input for the "Manifesto" attribute
    And  there should be an input for the "Lucky Numbers" attribute
    And  there should be an input for the "Aliases" attribute
    # If you can click on it then it is not hidden
    And  I click on the "Addresses" header
    And  I click on the "Drives" header
    And  I click on the "Maintains" header
    And  I click on the "Occupying" header
    And  I click on the "Repair Shops" header
    And  I expand the "Vehicles" header
    
  # Using a Proc
  Scenario: Conditions to disable widgets are met (Proc)
    When I set the "Name" to "Ghost"
    And  I save the page
    Then there should be an input for the "Name" attribute
    And  I should not see any mention of "Description"
    And  I should not see any mention of "Photo"
    And  I should not see any mention of "Date Of Birth"
    And  I should not see any mention of "Last Updated"
    And  I should not see any mention of "Wakes At"
    And  I should not see any mention of "Family Size"
    And  I should not see any mention of "Weight"
    And  I should not see any mention of "Dependent"
    And  I should not see any mention of "Handedness"
    And  I should not see any mention of "Manifesto"
    And  I should not see any mention of "Lucky Numbers"
    And  I should not see any mention of "Aliases"
    And  I should not see any mention of "Addresses"
    And  I should not see any mention of "Drives"
    And  I should not see any mention of "Maintains"
    And  I should not see any mention of "Occupying"
    And  I should not see any mention of "Repair Shops"
    And  I should not see any mention of "Vehicles"

  # Using a method
  Scenario: Conditions to disable widgets are met (method)
    Then there should be a file upload input for the "Photo" attribute
    When I set the "Weight" to "900"
    And  I save the page
    And  I should not see any mention of "Photo"
    
  # Using a proc that gets passed the vertex too
  
  # This actually can work in generated site in some cases but it is picky.  You have to know what you want from the vertex and you have to set up html_gui_builder's BaseController#process_template to properly replace what you are trying to get from the vertex.  Example: if your proc wants vertex.widget.getter then that will need to be replaced with that value in the generated haml template.  Obviously it isn't practical or possible to do this for every piece of information from a vertex that might be of interest so we should consider this as non-working within the context of generated_site.
  Scenario: Conditions to disable widgets are met (Proc that gets pased the vertex)
    When I set the "Name" to "George"
    And  I save the page
    And  there should be an input for the "Family Size" attribute
    And  there should be an input for the "Weight" attribute
    # If you can click on it then it is not hidden
    And  I expand the "Vehicles" header
    Then I set the "Name" to "Family Size"
    And  I save the page
    And  I should not see any mention of "Family Size"
    And  there should be an input for the "Weight" attribute
    And  I expand the "Vehicles" header
    When I set the "Name" to "Weight"
    And  I save the page
    And  there should be an input for the "Family Size" attribute
    And  I should not see any mention of "Weight"
    And  I expand the "Vehicles" header
    When I set the "Name" to "Vehicles"
    And  I save the page
    And  there should be an input for the "Family Size" attribute
    And  there should be an input for the "Weight" attribute
    And  I should not see any mention of "Vehicles"
