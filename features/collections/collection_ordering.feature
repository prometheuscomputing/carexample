# Test various types of ordered associations
Feature: Collection ordering
  In order to make a list of objects more meaningful
  As a user
  I want to reorder a list of objects
  
  Background:
    Given I have reset the database
    And I have logged in
    And I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
  
  # Test one-to-many
  # Using Vehicle -> Occupants (ordered) relationship
  Scenario: ordering a one-to-many association
    Given I click on the "Occupants" header
    Then I should see "3" associated "Occupants"
    And I should see the "Occupant" whose "Name" is "Bob" in position "1"
    And I should see the "Occupant" whose "Name" is "Sally" in position "2"
    And I should see the "Occupant" whose "Name" is "Phil" in position "3"
    And I should not see the unsaved changes warning for "Occupants"
    When I drag the "Occupant" whose "Name" is "Bob" to position "3"
    Then I should see the unsaved changes warning for "Occupants"
    When I save the page
    Then I should not see the unsaved changes warning for "Occupants"
    When I click on the "Occupants" header
    Then I should see the "Occupant" whose "Name" is "Sally" in position "1"
    And I should see the "Occupant" whose "Name" is "Phil" in position "2"
    And I should see the "Occupant" whose "Name" is "Bob" in position "3"
    When I drag the "Occupant" whose "Name" is "Phil" to position "1"
    And I drag the "Occupant" whose "Name" is "Sally" to position "3"
    Then I should see the "Occupant" whose "Name" is "Phil" in position "1"
    And I should see the "Occupant" whose "Name" is "Bob" in position "2"
    And I should see the "Occupant" whose "Name" is "Sally" in position "3"
  
  # Test many-to-many
  # Using Vehicle (ordered) -> Maintained By (ordered) relationship
  Scenario: ordering a many-to-many association
    Given I click on the "Maintained By" header
    Then I should see "3" associated "Maintained By"
    And I should not see the unsaved changes warning for "Maintained By"
    And I should see the "Maintained By" whose "Name" is "Bob" in position "1"
    And I should see the "Maintained By" whose "Name" is "Sally" in position "2"
    And I should see the "Maintained By" whose "Name" is "Phil" in position "3"
    When I drag the "Maintained By" whose "Name" is "Bob" to position "3"
    Then I should see the unsaved changes warning for "Maintained By"
    When I save the page
    Then I should not see the unsaved changes warning for "Maintained By"
    When I click on the "Maintained By" header
    Then I should see the "Maintained By" whose "Name" is "Sally" in position "1"
    And I should see the "Maintained By" whose "Name" is "Phil" in position "2"
    And I should see the "Maintained By" whose "Name" is "Bob" in position "3"
    When I drag the "Maintained By" whose "Name" is "Phil" to position "1"
    And I drag the "Maintained By" whose "Name" is "Sally" to position "3"
    Then I should see the "Maintained By" whose "Name" is "Phil" in position "1"
    And I should see the "Maintained By" whose "Name" is "Bob" in position "2"
    And I should see the "Maintained By" whose "Name" is "Sally" in position "3"
    # Now test opposite side
    Given I edit the "Maintained By" whose "Name" is "Bob"
    And I click on the "Maintains" header
    And I should see "1" associated "Maintains"
    And I create a "Maintains" of type "Motorcycle" where "Vehicle Model" is "Triumph" and return
    And I create a "Maintains" of type "Car" where "Vehicle Model" is "Suburban" and return
    And I create a "Maintains" of type "Car" whose "Vehicle Model" is "Escalade" and return
    And I should see "4" associated "Maintains"
    When I drag the "Maintains" whose "Vehicle Model" is "Escalade" to position "2"
    Then I should see the "Maintains" whose "Vehicle Model" is "S80" in position "1"
    And I should see the "Maintains" whose "Vehicle Model" is "Escalade" in position "2"
    And I should see the "Maintains" whose "Vehicle Model" is "Triumph" in position "3"
    And I should see the "Maintains" whose "Vehicle Model" is "Suburban" in position "4"
    And I save the page
    And I click on the "Maintains" header
    Then I should see the "Maintains" whose "Vehicle Model" is "S80" in position "1"
    And I should see the "Maintains" whose "Vehicle Model" is "Escalade" in position "2"
    And I should see the "Maintains" whose "Vehicle Model" is "Triumph" in position "3"
    And I should see the "Maintains" whose "Vehicle Model" is "Suburban" in position "4"
    When I drag the "Maintains" whose "Vehicle Model" is "Suburban" to position "1"
    And I drag the "Maintains" whose "Vehicle Model" is "Triumph" to position "1"
    Then I should see the "Maintains" whose "Vehicle Model" is "Triumph" in position "1"
    And I should see the "Maintains" whose "Vehicle Model" is "Suburban" in position "2"
    And I should see the "Maintains" whose "Vehicle Model" is "S80" in position "3"
    And I should see the "Maintains" whose "Vehicle Model" is "Escalade" in position "4"
    # Ensure that first side didn't change
    When I edit the "Maintains" whose "Vehicle Model" is "S80"
    And I click on the "Maintained By" header
    Then I should see the "Maintained By" whose "Name" is "Phil" in position "1"
    And I should see the "Maintained By" whose "Name" is "Bob" in position "2"
    And I should see the "Maintained By" whose "Name" is "Sally" in position "3"
  
  # Testing Association Class many-to-many - Creating an association to a new object
  # Using Vehicle (ordered) -> Driving -> Drivers (ordered) relationship
  
  Scenario: ordering an Association Class many-to-many association
    Given I click on the "Drivers" header
    And I should see "1" associated "Driver"
    And I create a "Driver" through "Driving" of type "Person" whose "Name" is "Roger" and return
    And I create a "Driver" through "Driving" of type "Person" whose "Name" is "Denise" and return
    And I create a "Driver" through "Driving" of type "Mechanic" whose "Name" is "Alice" and return
    And I should see "4" associated "Drivers"
    When I drag the "Driver" whose "Name" is "Bob" to position "3"
    And I save the page
    And I click on the "Drivers" header
    Then I should see the "Driver" whose "Name" is "Roger" in position "1"
    And I should see the "Driver" whose "Name" is "Denise" in position "2"
    And I should see the "Driver" whose "Name" is "Bob" in position "3"
    And I should see the "Driver" whose "Name" is "Alice" in position "4"
    When I drag the "Driver" whose "Name" is "Bob" to position "1"
    And I drag the "Driver" whose "Name" is "Roger" to position "4"
    And I save the page
    And I click on the "Drivers" header
    Then I should see the "Driver" whose "Name" is "Bob" in position "1"
    And I should see the "Driver" whose "Name" is "Denise" in position "2"
    And I should see the "Driver" whose "Name" is "Alice" in position "3"
    And I should see the "Driver" whose "Name" is "Roger" in position "4"
  
  # Test manual reordering feature available by turning on "Accessibility" in preferences
  
  Scenario: manual ordering of association class many-to-many association (sensible input)
    # Enable accessible interface and return
    Given I go to the preferences panel
    When I enable the accessibility interface
    And I submit the page
    And I go to the home page
    And I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
    # Manually order "Drivers"
    And I click on the "Drivers" header
    And I should see "1" associated "Driver"
    And I create a "Driver" through "Driving" of type "Person" whose "Name" is "Roger" and return
    And I create a "Driver" through "Driving" of type "Person" whose "Name" is "Denise" and return
    And I create a "Driver" through "Driving" of type "Mechanic" whose "Name" is "Alice" and return
    Then I should see "4" associated "Drivers"
    And I should not see the unsaved changes warning for "Drivers"
    # Equivalent to: When I drag the "Driver" whose "Name" is "Bob" to position "3"
    When I manually set the position of the "Driver" whose "Name" is "Bob" to "3"
    And I manually set the position of the "Driver" whose "Name" is "Roger" to "1"
    And I manually set the position of the "Driver" whose "Name" is "Denise" to "2"
    And I should see the unsaved changes warning for "Drivers"
    And I save the page
    And I should not see the unsaved changes warning for "Drivers"
    And I click on the "Drivers" header
    Then I should see the "Driver" whose "Name" is "Roger" in position "1"
    When I should see the "Driver" whose "Name" is "Denise" in position "2"
    And I should see the "Driver" whose "Name" is "Bob" in position "3"
    And I should see the "Driver" whose "Name" is "Alice" in position "4"
    # Equivalent to: When I drag the "Driver" whose "Name" is "Bob" to position "1"
    #                And I drag the "Driver" whose "Name" is "Roger" to position "4"
    When I manually set the position of the "Driver" whose "Name" is "Bob" to "1"
    Then I should see the unsaved changes warning for "Drivers"
    When I manually set the position of the "Driver" whose "Name" is "Roger" to "4"
    And I manually set the position of the "Driver" whose "Name" is "Alice" to "3"
    And I save the page
    And I click on the "Drivers" header
    Then I should not see the unsaved changes warning for "Drivers"
    Then I should see the "Driver" whose "Name" is "Bob" in position "1"
    And I should see the "Driver" whose "Name" is "Denise" in position "2"
    And I should see the "Driver" whose "Name" is "Alice" in position "3"
    And I should see the "Driver" whose "Name" is "Roger" in position "4"
  
  # Test manual reordering feature available by turning on "Accessibility" in preferences
  # This scenario tests behavior when the user accidentally sets duplicate positions
  
  Scenario: manual ordering of association class many-to-many association (duplicate positions)
    # Enable accessible interface and return
    Given I go to the preferences panel
    And I enable the accessibility interface
    And I submit the page
    And I go to the home page
    And I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
    # Manually order "Drivers"
    And I click on the "Drivers" header
    And I should see "1" associated "Driver"
    And I create a "Driver" through "Driving" of type "Person" whose "Name" is "Roger" and return
    And I create a "Driver" through "Driving" of type "Person" whose "Name" is "Denise" and return
    And I create a "Driver" through "Driving" of type "Mechanic" whose "Name" is "Alice" and return
    And I should see "4" associated "Drivers"
    # Set Denise to position 1 (duplicate with Bob)
    When I manually set the position of the "Driver" whose "Name" is "Denise" to "1"
    And I save the page
    And I click on the "Drivers" header
    # Denise gets inserted before Roger
    # Note: Since Bob and Denise both had a position of 1, then either could be correctly placed first
    #       It would be better to have the test query whether Bob and Denise are either in position 1 or 2
    Then I should see the "Driver" whose "Name" is "Bob" in position "1"
    And I should see the "Driver" whose "Name" is "Denise" in position "2"
    And I should see the "Driver" whose "Name" is "Roger" in position "3"
    And I should see the "Driver" whose "Name" is "Alice" in position "4"
  
  # Test manual reordering feature available by turning on "Accessibility" in preferences
  # This scenario tests behavior when the user accidentally sets duplicate positions
  
  Scenario: manual ordering of association class many-to-many association (out of scope positions)
    # Enable accessible interface and return
    Given I go to the preferences panel
    And I enable the accessibility interface
    And I submit the page
    And I go to the home page
    And I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
    # Manually order "Drivers"
    And I click on the "Drivers" header
    And I should see "1" associated "Driver"
    And I create a "Driver" through "Driving" of type "Person" whose "Name" is "Roger" and return
    And I create a "Driver" through "Driving" of type "Person" whose "Name" is "Denise" and return
    And I create a "Driver" through "Driving" of type "Mechanic" whose "Name" is "Alice" and return
    And I should see "4" associated "Drivers"
    # Set Denise to position 10 (maximum should have been 4)
    When I manually set the position of the "Driver" whose "Name" is "Denise" to "10"
    And I save the page
    And I click on the "Drivers" header
    # Denise gets inserted at the end
    Then I should see the "Driver" whose "Name" is "Bob" in position "1"
    And I should see the "Driver" whose "Name" is "Roger" in position "2"
    And I should see the "Driver" whose "Name" is "Alice" in position "3"
    And I should see the "Driver" whose "Name" is "Denise" in position "4"
    # Set Alice to position -1
    When I manually set the position of the "Driver" whose "Name" is "Alice" to "-1"
    And I save the page
    And I click on the "Drivers" header
    # Alice gets inserted at the beginning
    Then I should see the "Driver" whose "Name" is "Alice" in position "1"
    And I should see the "Driver" whose "Name" is "Bob" in position "2"
    And I should see the "Driver" whose "Name" is "Roger" in position "3"
    And I should see the "Driver" whose "Name" is "Denise" in position "4"
    # Check that positions are being reassigned to sane values before being stored.
    # In order to check this:
    # * Set an out-of-range high value for a position
    # * Save the change
    # * Show only 1 item per page
    # * Set an out-of-range position for the first item that is lower than the previously set position
    # * Ensure that the (formerly first) item is now in last place.
    When I manually set the position of the "Driver" whose "Name" is "Bob" to "10"
    And I save the page
    And I click on the "Drivers" header
    And I view "1" "Driver" per page
    And I submit the "Drivers" search
    And I manually set the position of the "Driver" whose "Name" is "Alice" to "9"
    And I save the page
    And I click on the "Drivers" header
    # Alice gets added to the end
    Then I should see the "Driver" whose "Name" is "Roger" in position "1"
    And I should see the "Driver" whose "Name" is "Denise" in position "2"
    And I should see the "Driver" whose "Name" is "Bob" in position "3"
    And I should see the "Driver" whose "Name" is "Alice" in position "4"
  
  # This scenario tests behavior combining pagination and reordering functions.
  
  Scenario: ordering of association class many-to-many association (combination with pagination)
    # Enable accessible interface and return
    Given I go to the preferences panel
    And I enable the accessibility interface
    And I submit the page
    And I go to the home page
    And I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
    # Manually order "Drivers"
    And I click on the "Drivers" header
    And I should see "1" associated "Driver"
    And I create a "Driver" through "Driving" of type "Person" whose "Name" is "Roger" and return
    And I create a "Driver" through "Driving" of type "Person" whose "Name" is "Denise" and return
    And I create a "Driver" through "Driving" of type "Mechanic" whose "Name" is "Alice" and return
    And I should see "4" associated "Drivers"
    # Set pagination to show 2 items per page and go to the second page
    When I view "2" "Driver" per page
    And I submit the "Drivers" search
    And I view page "2" of "Drivers"
    Then I should see the "Driver" whose "Name" is "Denise" in position "1"
    And I should see the "Driver" whose "Name" is "Alice" in position "2"
    # Should now see Denise and Alice
    # Reorder Alice to go before Denise
    When I drag the "Driver" whose "Name" is "Alice" to position "1"
    And I save the page
    And I click on the "Drivers" header
    # Alice and Denise should be in positions 3 and 4
    Then I should see the "Driver" whose "Name" is "Bob" in position "1"
    And I should see the "Driver" whose "Name" is "Roger" in position "2"
    And I should see the "Driver" whose "Name" is "Alice" in position "3"
    And I should see the "Driver" whose "Name" is "Denise" in position "4"
  
  # This scenario tests behavior when combining filtering and reordering functions.
  
  Scenario: ordering of association class many-to-many association (combination with filtering)
    # Enable accessible interface and return
    Given I go to the preferences panel
    And I enable the accessibility interface
    And I submit the page
    And I go to the home page
    And I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
    # Manually order "Drivers"
    And I click on the "Drivers" header
    And I should see "1" associated "Driver"
    And I create a "Driver" through "Driving" of type "Person" whose "Name" is "Roger" and return
    And I create a "Driver" through "Driving" of type "Person" whose "Name" is "Denise" and return
    And I create a "Driver" through "Driving" of type "Mechanic" whose "Name" is "Alice" and return
    And I should see "4" associated "Drivers"
    # Set pagination to show 2 items per page and go to the second page
    And I open the "Drivers" search
    And I click on the "Drivers" advanced search link
    And I filter to see "Drivers" where "Name" is "o"
    And I submit the "Drivers" search
    Then I should see the "Driver" whose "Name" is "Bob" in position "1"
    And I should see the "Driver" whose "Name" is "Roger" in position "2"
    # Reorder Roger to before Bob
    When I drag the "Driver" whose "Name" is "Roger" to position "1"
    And I save the page
    And I click on the "Drivers" header
    Then I should see the "Driver" whose "Name" is "Roger" in position "1"
    And I should see the "Driver" whose "Name" is "Bob" in position "2"
    And I should see the "Driver" whose "Name" is "Denise" in position "3"
    And I should see the "Driver" whose "Name" is "Alice" in position "4"
    # Move Alice to position 3 (setup for next test)
    Given I drag the "Driver" whose "Name" is "Alice" to position "3"
    And I save the page
    And I click on the "Drivers" header
    Then I should see the "Driver" whose "Name" is "Roger" in position "1"
    And I should see the "Driver" whose "Name" is "Bob" in position "2"
    And I should see the "Driver" whose "Name" is "Alice" in position "3"
    And I should see the "Driver" whose "Name" is "Denise" in position "4"
    # Test reordering across filtered items
    And I open the "Drivers" search
    And I click on the "Drivers" advanced search link
    And I filter to see "Drivers" where "Name" is "e"
    And I submit the "Drivers" search
    Then I should see the "Driver" whose "Name" is "Roger" in position "1"
    And I should see the "Driver" whose "Name" is "Alice" in position "2"
    And I should see the "Driver" whose "Name" is "Denise" in position "3"
    # Reorder Roger to be after Alice
    When I drag the "Driver" whose "Name" is "Roger" to position "2"
    And I save the page
    And I click on the "Drivers" header
    Then I should see the "Driver" whose "Name" is "Alice" in position "1"
    And I should see the "Driver" whose "Name" is "Bob" in position "2"
    And I should see the "Driver" whose "Name" is "Roger" in position "3"
    And I should see the "Driver" whose "Name" is "Denise" in position "4"
    
  # This scenario tests behavior combining pagination and reordering functions.
  Scenario: ordering of association class many-to-many association (combination with unsaved changes and pagination change)
    Given I click on the "Maintained By" header
    Then I should see "3" associated "Maintained By"
    And I should not see the unsaved changes warning for "Maintained By"
    And I should see the "Maintained By" whose "Name" is "Bob" in position "1"
    And I should see the "Maintained By" whose "Name" is "Sally" in position "2"
    And I should see the "Maintained By" whose "Name" is "Phil" in position "3"
    When I drag the "Maintained By" whose "Name" is "Bob" to position "3"
    Then I should see the unsaved changes warning for "Maintained By"

    # Changes should be discarded on popup Discard action
    And I submit the "Maintained By" search
    Then I should receive a "unsaved changes" popup warning
    And I click the "Discard Changes" button on the "unsaved changes" popup
    Then I should see the "Maintained By" whose "Name" is "Sally" in position "2"
    And I should see the "Maintained By" whose "Name" is "Phil" in position "3"
    And I should see the "Maintained By" whose "Name" is "Bob" in position "1"

    # Changes should perist after cancellation of popup and be savable
    When I drag the "Maintained By" whose "Name" is "Bob" to position "3"
    And I submit the "Maintained By" search
    Then I should receive a "unsaved changes" popup warning
    And I click the "Cancel" button on the "unsaved changes" popup
    When I save the page
    Then I should not see the unsaved changes warning for "Maintained By"
    When I click on the "Maintained By" header
    Then I should see the "Maintained By" whose "Name" is "Sally" in position "1"
    And I should see the "Maintained By" whose "Name" is "Phil" in position "2"
    And I should see the "Maintained By" whose "Name" is "Bob" in position "3"

  # Insufficient pagination coverage
  Scenario: ordering of association class one-to-many association (with filter and pagination)
    Given I click on the "Occupants" header
    When I view "1" "Occupants" per page
    And I open the "Occupants" search
    And I click on the "Occupants" advanced search link
    And I filter to see "Occupants" where "Name" is "Phil"
    And I submit the "Occupants" search
    Then I should see the "Occupants" whose "Name" is "Phil" in position "1"
    And I should see "1" "Occupants"

  # Insufficient pagination coverage
  Scenario: ordering of association class one-to-many association (with filter and pagination) - 2nd example
    Given I click on the "Maintained By" header
    When I view "1" "Maintained By" per page
    And I open the "Maintained By" search
    And I click on the "Maintained By" advanced search link
    And I filter to see "Maintained By" where "Name" is "Bob"
    And I submit the "Maintained By" search
    Then I should see the "Maintained By" whose "Name" is "Bob" in position "1"
    And I should see "1" "Maintained By"
