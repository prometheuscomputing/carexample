# This file is specifically for testing file concurrency in isolation of other features.  There are a few things that come into play when a file is changed concurrently with other values -- these things are tested elsewhere, so you shouldn't remove any occurrence of testing file attributes from other concurrency tests.
Feature: Saving files with concurrent changes made by another user
  In order to handle issues with editing the same object at the same time
  As a user
  I want to edit the same object with multiple tabs without blindly overiding data
  
  Background:
    Given I have reset the database
    And   I have logged in

  Scenario: Saving an object with concurrently deleted file
    # 1st Tab here - creating File
    When I expand the "People" header
    And  I create a "People" of type "Person" where "Name" is "Hank"
    And  I choose to upload the "Photo" named "default_user_replaceme.jpeg"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    # 2rd Tab here - Deleting File
    When I open a new tab
    And  I expand the "People" header
    And  I edit the "People" where "Name" is "Hank"
    And  I check the delete box for the stored "Photo"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    # Back to 1st Tab - Save with no changes
    When I switch to the 1st tab
    Then there should be a stored "Photo" named "default_user_replaceme.jpeg"
    When I save the page
    Then I should receive the warning "The file for 'Photo' was concurrently deleted"
    # Verify Page contents
    And  there should not be a stored "Photo"
    
  Scenario: Saving an object with concurrently replaced file
    # 1st Tab here - creating File
    When I expand the "People" header
    And  I create a "People" of type "Person" where "Name" is "Hank"
    And  I choose to upload the "Photo" named "default_user_replaceme.jpeg"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    # 2rd Tab here - Replacing File
    When I open a new tab
    And  I expand the "People" header
    And  I edit the "People" where "Name" is "Hank"
    And  I choose to upload the "Photo" named "picture2.jpeg"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    # Back to 1st Tab - Save with no changes
    When I switch to the 1st tab
    Then there should be a stored "Photo" named "default_user_replaceme.jpeg"
    When I save the page
    And  I should not receive a "concurrency warning" popup warning
    Then I should receive the warning "The file for 'Photo' was concurrently replaced"
    # Verify Page contents
    And  there should be a stored "Photo" named "picture2.jpeg"
    
  Scenario: Saving an object with concurrently added file
    # 1st Tab here - No File
    When I expand the "People" header
    And  I create a "People" of type "Person" where "Name" is "Hank"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    # 2rd Tab here - Upload File
    When I open a new tab
    And  I expand the "People" header
    And  I edit the "People" where "Name" is "Hank"
    And  I choose to upload the "Photo" named "picture2.jpeg"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    # Back to 1st Tab - Save with no changes
    When I switch to the 1st tab
    Then there should not be a stored "Photo"
    When I save the page
    Then I should receive the warning "The file for 'Photo' was concurrently added by another user or in another open tab."
    # Verify Page contents
    And  there should be a stored "Photo" named "picture2.jpeg"
    
  Scenario: Saving an object after replacing a file that was concurrently deleted
    # 1st Tab here - creating File
    When I expand the "People" header
    And  I create a "People" of type "Person" where "Name" is "Hank"
    And  I choose to upload the "Photo" named "default_user_replaceme.jpeg"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    # 2rd Tab here - Deleting File
    When I open a new tab
    And  I expand the "People" header
    And  I edit the "People" where "Name" is "Hank"
    And  I check the delete box for the stored "Photo"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    # Back to 1st Tab - Replacing File
    When I switch to the 1st tab
    Then there should be a stored "Photo" named "default_user_replaceme.jpeg"
    And  I choose to upload the "Photo" named "picture2.jpeg"
    When I save the page
    Then I should receive the warning "The file for 'Photo' was concurrently deleted by another user or in another tab but you have replaced it."
    # Verify Page contents
    And  there should be a stored "Photo" named "picture2.jpeg"
    
  # Existing file is replaced in both tabs.
  Scenario Outline: Replacing a file that has been concurrently replaced.
    # 1st Tab here - creating File
    When I expand the "People" header
    And  I create a "People" of type "Person" where "Name" is "Hank"
    And  I choose to upload the "Photo" named "default_user_replaceme.jpeg"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    # 2rd Tab here - Replacing File
    When I open a new tab
    And  I expand the "People" header
    And  I edit the "People" where "Name" is "Hank"
    And  I choose to upload the "Photo" named "picture2.jpeg"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    # Back to 1st Tab - Also replacing the file
    When I switch to the 1st tab
    Then there should be a stored "Photo" named "default_user_replaceme.jpeg"
    And  I choose to upload the "Photo" named "work_performance.gif"
    When I save the page
    And  I should receive a "concurrency warning" popup warning
    
    And the "concurrency warning" popup should have my changes selected for the "Photo"
    And the "concurrency warning" popup should not have the selections disabled for the "Photo"
    And the "concurrency warning" popup should show my changes as "To Be Stored: work_performance.gif" for the "Photo"
    And the "concurrency warning" popup should show the concurrent changes as "Stored File: picture2.jpeg" for the "Photo"
    When I select <which_changes> for the "Photo" on the "concurrency warning" popup
    And  I choose <action> on the concurrency warning popup
    Then I should not receive a "concurrency warning" popup warning
    And  there should be a stored "Photo" named "<photo_name>"
    
    Examples:
      | which_changes      | action  | photo_name           |
      | the changes found  | merge   | picture2.jpeg        |
      | my changes         | merge   | work_performance.gif |
      | the changes found  | abandon | picture2.jpeg        |
      | my changes         | abandon | picture2.jpeg        |
      
  # Existing file is replaced in both tabs.
  Scenario Outline: Replacing a file that has been concurrent replaced -- Popup Cancelled
    # 1st Tab here - creating File
    When I expand the "People" header
    And  I create a "People" of type "Person" where "Name" is "Hank"
    And  I choose to upload the "Photo" named "default_user_replaceme.jpeg"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    # 2rd Tab here - Replacing File
    When I open a new tab
    And  I expand the "People" header
    And  I edit the "People" where "Name" is "Hank"
    And  I choose to upload the "Photo" named "picture2.jpeg"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    # Back to 1st Tab - Also replacing the file
    When I switch to the 1st tab
    Then there should be a stored "Photo" named "default_user_replaceme.jpeg"
    And  I choose to upload the "Photo" named "work_performance.gif"
    When I save the page
    And  I should receive a "concurrency warning" popup warning
    
    And the "concurrency warning" popup should have my changes selected for the "Photo"
    And the "concurrency warning" popup should not have the selections disabled for the "Photo"
    And the "concurrency warning" popup should show my changes as "To Be Stored: work_performance.gif" for the "Photo"
    And the "concurrency warning" popup should show the concurrent changes as "Stored File: picture2.jpeg" for the "Photo"
    When I select <which_changes> for the "Photo" on the "concurrency warning" popup
    And  I choose cancel on the concurrency warning popup
    Then I should not receive a "concurrency warning" popup warning
    And  there should be a pending "Photo" named "work_performance.gif" to be stored
    
    Examples:
      | which_changes      |
      | the changes found  |
      | my changes         |


  # No initial file.  File is added in both tabs.
  Scenario Outline: Concurrent addition of a file.
    # 1st Tab here - No File creation
    When I expand the "People" header
    And  I create a "People" of type "Person" where "Name" is "Hank"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    # 2rd Tab here - Adding a File
    When I open a new tab
    And  I expand the "People" header
    And  I edit the "People" where "Name" is "Hank"
    And  I choose to upload the "Photo" named "picture2.jpeg"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    # Back to 1st Tab - Also adding a File
    When I switch to the 1st tab
    Then there should not be a stored "Photo"
    And  I choose to upload the "Photo" named "work_performance.gif"
    When I save the page
    And  I should receive a "concurrency warning" popup warning
    
    And  the "concurrency warning" popup should have my changes selected for the "Photo"
    And  the "concurrency warning" popup should not have the selections disabled for the "Photo"
    And  the "concurrency warning" popup should show my changes as "To Be Stored: work_performance.gif" for the "Photo"
    And   the "concurrency warning" popup should show the concurrent changes as "Stored File: picture2.jpeg" for the "Photo"
        
    When I select <which_changes> for the "Photo" on the "concurrency warning" popup
    And  I choose <action> on the concurrency warning popup
    Then I should not receive a "concurrency warning" popup warning
    And  there should be a stored "Photo" named "<photo_name>"
    
    Examples:
      | which_changes      | action  | photo_name           |
      | the changes found  | merge   | picture2.jpeg        |
      | my changes         | merge   | work_performance.gif |
      | the changes found  | abandon | picture2.jpeg        |
      | my changes         | abandon | picture2.jpeg        |

  # No initial file.  File is added in both tabs.
  Scenario Outline: Concurrent addition of a file -- Popup Canceled.
    # 1st Tab here - No File creation
    When I expand the "People" header
    And  I create a "People" of type "Person" where "Name" is "Hank"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    # 2rd Tab here - Adding a File
    When I open a new tab
    And  I expand the "People" header
    And  I edit the "People" where "Name" is "Hank"
    And  I choose to upload the "Photo" named "picture2.jpeg"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    # Back to 1st Tab - Also adding a File
    When I switch to the 1st tab
    Then there should not be a stored "Photo"
    And  I choose to upload the "Photo" named "work_performance.gif"
    When I save the page
    And  I should receive a "concurrency warning" popup warning
    
    And  the "concurrency warning" popup should have my changes selected for the "Photo"
    And  the "concurrency warning" popup should not have the selections disabled for the "Photo"
    And  the "concurrency warning" popup should show my changes as "To Be Stored: work_performance.gif" for the "Photo"
    And  the "concurrency warning" popup should show the concurrent changes as "Stored File: picture2.jpeg" for the "Photo"
        
    When I select <which_changes> for the "Photo" on the "concurrency warning" popup
    And  I choose cancel on the concurrency warning popup
    Then I should not receive a "concurrency warning" popup warning
    And  there should be a pending "Photo" named "work_performance.gif"
    
    
    Examples:
      | which_changes      |
      | the changes found  |
      | my changes         |
      
  # # TEST CASE for testing scenario outlines one at a time for better error reporting.
  # Scenario: TEST
  #   # 1st Tab here - No File creation
  #   When I expand the "People" header
  #   And  I create a "People" of type "Person" where "Name" is "Hank"
  #   And  I save the page
  #   And  I should not receive a "concurrency warning" popup warning
  #   # 2rd Tab here - Adding a File
  #   When I open a new tab
  #   And  I expand the "People" header
  #   And  I edit the "People" where "Name" is "Hank"
  #   And  I choose to upload the "Photo" named "picture2.jpeg"
  #   And  I save the page
  #   And  I should not receive a "concurrency warning" popup warning
  #   # Back to 1st Tab - Also adding a File
  #   When I switch to the 1st tab
  #   Then there should not be a stored "Photo"
  #   And  I choose to upload the "Photo" named "work_performance.gif"
  #   When I save the page
  #   And  I should receive a "concurrency warning" popup warning
  #
  #   And  the "concurrency warning" popup should have my changes selected for the "Photo"
  #   And  the "concurrency warning" popup should not have the selections disabled for the "Photo"
  #   And  the "concurrency warning" popup should show my changes as "To Be Stored: work_performance.gif" for the "Photo"
  #   And  the "concurrency warning" popup should show the concurrent changes as "Stored File: picture2.jpeg" for the "Photo"
  #
  #   When I select my changes for the "Photo" on the "concurrency warning" popup
  #   And  I choose cancel on the concurrency popup
  #   Then I should not receive a "concurrency warning" popup warning
  #   And  there should be a stored "Photo" named "work_performance.gif"
  
  
  Scenario: Cancelling a concurrency popup about a file results in an unaltered page state that, when saved again, repeats the concurrency warning.
    # 1st Tab here - creating File
    When I expand the "People" header
    And  I create a "People" of type "Person" where "Name" is "Hank"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    # 2rd Tab here - Deleting File
    When I open a new tab
    And  I expand the "People" header
    And  I edit the "People" where "Name" is "Hank"
    And  I choose to upload the "Photo" named "picture2.jpeg"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    # Back to 1st Tab - Save with no changes
    When I switch to the 1st tab
    Then there should not be a stored "Photo"
    And  I choose to upload the "Photo" named "work_performance.gif"
    When I save the page
    And  I should receive a "concurrency warning" popup warning

    And  the "concurrency warning" popup should have my changes selected for the "Photo"
    And  the "concurrency warning" popup should not have the selections disabled for the "Photo"
    And  the "concurrency warning" popup should show my changes as "To Be Stored: work_performance.gif" for the "Photo"
    And  the "concurrency warning" popup should show the concurrent changes as "Stored File: picture2.jpeg" for the "Photo"

    When I select the changes found for the "Photo" on the "concurrency warning" popup
    And  I choose cancel on the concurrency warning popup
    Then I should not receive a "concurrency warning" popup
    And  there should be a pending "Photo" named "work_performance.gif"
    
    # Saving again should result in the same concurrency warning
    When I save the page
    And  I should receive a "concurrency warning" popup warning

    And  the "concurrency warning" popup should have my changes selected for the "Photo"
    And  the "concurrency warning" popup should not have the selections disabled for the "Photo"
    And  the "concurrency warning" popup should show my changes as "To Be Stored: work_performance.gif" for the "Photo"
    And  the "concurrency warning" popup should show the concurrent changes as "Stored File: picture2.jpeg" for the "Photo"
