def select_first_email
  find('tbody').first('tr').click
end

def select_admin_verify_email
  admin_email = '<admin@mycompany.net>'
  verify_email_subject = 'Verify Email Account'
  find('#messages tbody tr', :text => /#{admin_email}.*#{verify_email_subject}/).click
end

def click_email_link(text = nil)
  link_href = within_frame page.find('.body') do
    find(:link, text)['href']
  end
  visit link_href.sub('http://localhost:7000','')
end

def verify_email_subject(subject)
  email_subject = page.all('.selected td')[2]
  expect(email_subject.text).to eq(subject)
end

def verify_email_to_address(to_address)
  email_to_address = page.all('.selected td')[1]
  expect(email_to_address.text).to eq(to_address)
end

def verify_email_from_address(from_address)
  email_from_address = page.all('.selected td')[0]
  expect(email_from_address.text).to eq(from_address)
end

def verify_email_reset_password_link
  within_frame page.find('.body') do
    email_reset_password_link = first('body p a')
    expect(email_reset_password_link.text).to eq("Reset Password")   
  end
end

def verify_email_account_link
  within_frame page.find('.body') do
    email_reset_password_link = first('body p a')
    expect(email_reset_password_link.text).to eq("Verify This Email Account")   
  end
end