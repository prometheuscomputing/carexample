require 'workflow'
require 'workflow_sequel_adapter'
require_relative 'model_extensions/automotive'
require_relative 'model_extensions/automotive_workflow'
require_relative 'model_extensions/people'
require_relative 'model_extensions/geography'
require_relative 'model_extensions/home'

# Where to get files for testing file attributes
TEST_FILES_DIR = relative('../../test_data/')
TEST_PHOTO     = File.join(TEST_FILES_DIR, 'default_user_replaceme.jpeg')

# business_rules_filename = File.expand_path(File.join(File.dirname(__FILE__), "business_rules.rb"))
# BusinessRules::RuleDsl.instance_eval(File.read(business_rules_filename))


#  Keep these.  They are example of how to do some interesting things in the spec
# module MethodsThatMakeSpecWork
#   def parents(*args); []; end
#   def interfaces(*args); []; end
# end
# module OhMy
#   extend MethodsThatMakeSpecWork
# end
