module CarExample
  
  def self.populate_data
    ChangeTracker.start
    
    # Make some vehicles (9)
    car = Automotive::Car.create(:vehicle_model => 'S40', :cost => 20000, :miles_per_gallon => 30)
    car.make = "Volvo"; car.save 
    s80 = Automotive::Car.create(:vehicle_model => 'S80', :cost => 30000, :miles_per_gallon => 35)
    s80.make = "Volvo"; s80.save 
    car = Automotive::Car.create(:vehicle_model => 'Mustang', :cost => 30000, :miles_per_gallon => 25)
    car.make = "Ford"; car.save 
    car = Automotive::Car.create(:vehicle_model => 'Civic', :cost => 20000, :miles_per_gallon => 35)
    car.make = "Honda"; car.save 
    car = Automotive::Car.create(:vehicle_model => 'Charger', :cost => 40000, :miles_per_gallon => 25)
    car.make = "Dodge"; car.save 
    car = Automotive::Minivan.create(:vehicle_model => 'Caravan', :cost => 10000, :miles_per_gallon => 20, :sliding_doors => true)
    car.make = "Dodge"; car.save 
    car = Automotive::Motorcycle.create(:vehicle_model => 'Rebel', :cost => 5000)
    car.make = "Honda"; car.save 
    car = Automotive::ElectricVehicle.create(:vehicle_model => 'Smart Car', :cost => 10000, :electric_efficiency => 15.7)
    car.make = "Home Made"; car.save 
    car = Automotive::HybridVehicle.create(:vehicle_model => 'Prius', :cost => 30000, :miles_per_gallon => 50, :electric_efficiency => 12.1, :hybrid_type => 'Normal')
    car.make = "Other"; car.save 
  
    # Make some People (3 + 6)
    bob = People::Person.create(:name => 'Bob', :date_of_birth => '1990-01-04', :family_size => 3)
    bob.description = Gui_Builder_Profile::RichText.new(:content => "Bob's description")
    new_code = Gui_Builder_Profile::Code.new(:content => 'class Bob\nend')
    new_code.language = "Ruby"
    bob.code_representation = new_code

    bob.photo = Gui_Builder_Profile::File.from_path(TEST_PHOTO)

    sally = People::Person.create(:name => 'Sally', :date_of_birth => '1990-01-04', :family_size => 1)
    phil = People::Person.create(:name => 'Phil', :date_of_birth => '1980-01-01', :family_size => 3)
    # Add mechanics (6)
    m1 = Automotive::Mechanic.create(:name => 'Rick', :salary => 60000)
    m2 = Automotive::Mechanic.create(:name => 'Bill', :salary => 50000, :date_of_birth => '1977-01-01')
    m3 = Automotive::Mechanic.create(:name => 'Jeff', :salary => 50000)
    m4 = Automotive::Mechanic.create(:name => 'Joe', :salary => 50000)
    m5 = Automotive::Mechanic.create(:name => 'Jason', :salary => 50000)
    m6 = Automotive::Mechanic.create(:name => 'Brandon', :salary => 50000)
  
    # Make a couple RepairShops
    r1 = Automotive::RepairShop.create(:name => 'Tires and More')
    r2 = Automotive::RepairShop.create(:name => 'West-side Tire Service')
    r1.mechanics = [m1, m2, m3, m4]
    r2.mechanics = [m5, m6]
  
    # Make a VIN
    v = Automotive::VIN.create(:issue_date => '4/1/1990', :vin => 1234567890)
  
    # Make a Country
    usa = Geography::Country.create(:name => 'USA')
    usa.add_territory Geography::Territory.create(:name => 'Philippines')
    nc = usa.add_state Geography::State.create(:name => 'North Carolina')
    md = usa.add_state Geography::State.create(:name => 'Maryland')
    cullowhee = nc.add_city(Geography::City.create(:name => 'Cullowhee'))
    sylva = nc.add_city(Geography::City.create(:name => 'Sylva'))
    nc.add_city Geography::City.create(:name => 'Raleigh')
    md.add_city Geography::City.create(:name => 'Gaithersburg')
    
    bob.founded = sylva
    bob.save
  
    # Make a couple addresses
    biltmore_st_345 = Geography::Address.create(:street_name => 'Biltmore St.', :street_number => 345)
    biltmore_st_345.city = cullowhee
    biltmore_st_355 = Geography::Address.create(:street_name => 'Biltmore St.', :street_number => 355)
    biltmore_st_355.city = sylva
    bob.add_address(biltmore_st_345)
    bob.add_address(biltmore_st_355)
  
    # Make Warranties
    full_warranty = Automotive::Warranty.create(:coverage => 'Full')
    partial_warranty = Automotive::Warranty.create(:coverage => 'Partial')
  
    # Make a components and parts
    s80_engine = Automotive::Component.create(:name => 'Engine', :serial => '12', :warranty_void => false, :warranty_expiration_date => '1/1/2100')
    s80.add_component(s80_engine)
    s80_engine_alternator = Automotive::Part.create(:name => 'Alternator', :serial => '1234')
    s80_engine.add_part(s80_engine_alternator)
    s80_engine_piston  = Automotive::Part.create(:name => 'Piston', :serial => '1235')
    s80_engine.add_part(s80_engine_piston)
    # Set a warranty for the part
    s80_engine.warranty = full_warranty
  
    # Initialize VehicleTaxRate (pending modification of generators to call singleton!)
    #vtr = Automotive::VehicleTaxRate.instance
  
    # Add an Ownership relationship
    bob.add_vehicle s80
  
    # Add co-owners
    sally.add_vehicle s80
    phil.add_vehicle s80
  
    # Make a Driving relationship
    bob.drives_add s80
  
    # Populate occupants relationship
    s80.add_occupant bob
    s80.add_occupant sally
    s80.add_occupant phil
  
    # Populate maintained_by relationship
    s80.add_maintained_by bob
    s80.add_maintained_by sally
    s80.add_maintained_by phil
  
    # Create a custom password requirement
    project_options = Gui_Builder_Profile::ProjectOptions.instance
    password_condition = Gui_Builder_Profile::RegularExpressionCondition.create(:description => "No stars!", :failure_message => "Your password may not have an asterisk!")
    # Assigned separately to use custom setter.
    password_condition.regular_expression = '^[^\*]*$'
    password_condition.save
    project_options.add_password_requirement(password_condition)
  
    # Create a custom email requirement
    email_condition = Gui_Builder_Profile::RegularExpressionCondition.create(:description => "No pluses!", :failure_message => "Your email address may not have a plus symbol!")
    # Assigned separately to use custom setter.
    email_condition.regular_expression = '^[^\+]*$'
    email_condition.save
    project_options.add_email_requirement(email_condition)
  
    ChangeTracker.commit
  
    ChangeTracker.start
    # Modify Ownership relationship
    
    bob = People::Person[:name => 'Bob']
    
    bob.weight = 147.0
    bob.save
    ownership = bob.vehicles_associations.first[:through]
    ownership.percent_ownership = 66.67
    ownership.save
    # Modify Driving relationship
    driving = bob.drives_associations.first[:through]
    driving.likes_driving = true
    driving.save
    # Add "West-side Tire Service" RepairShop (not done earlier in order to test change tracking history)
    bob.add_repair_shop(r2)
    
    ChangeTracker.commit
  end
end