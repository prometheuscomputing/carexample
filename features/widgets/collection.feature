# Test collection features here. This includes adding/breaking associations, and deleting associated objects.
# Filtering and ordering are covered separately
Feature: Collections
  In order to manipulate a list of objects
  As a user
  I want to see/edit/create objects in the list
  
  Background:
    Given I have reset the database
    And I have logged in
    And I should see that there are a total of "9" "Vehicle" in the header
  
  # Deletion scenarios
  Scenario: Deleting an object
    When I expand the "Vehicles" header
    And I select the "Vehicle" where "Vehicle Model" is "Smart Car"
    And I click the only "Delete" button under the "Vehicle" header
    And I confirm the homepage deletion of the "Vehicles"
    Then I should see that there are a total of "8" "Vehicles" in the header
    When I expand the "Vehicles" header # Delete reloads page
    Then I should not see a "Vehicle" where "Vehicle Model" is "Smart Car"
  
  Scenario: Deleting an object when the user cancels deletion
    When I expand the "Vehicles" header
    And I select the "Vehicle" where "Vehicle Model" is "Smart Car"
    And I click the only "Delete" button under the "Vehicle" header
    And I cancel the delete popup # Canceled delete does not reload page
    Then I should see that there are a total of "9" "Vehicle" in the header
    And I should see a "Vehicle" where "Vehicle Model" is "Smart Car"
  
  Scenario: Deleting multiple objects
    
  # Navigation scenarios
  Scenario: Editing a listed object
    When I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "Smart Car"
    Then I should be at an "Electric Vehicle" page
    And the value of "Vehicle Model" should be "Smart Car"
    
  Scenario: Editing an Association Class association
    Given I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
    When I click on the "Owners" header
    And I edit the "Ownership" for the "Owner" whose "Name" is "Bob"
    Then I should be at an "Ownership" page
    And the value of "Percent Ownership" should be "66.67"
    
  Scenario: Editing an Association Class object
    Given I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
    When I click on the "Owners" header
    And I edit the "Owner" whose "Name" is "Bob"
    Then I should be at an "Person" page
    And the value of "Name" should be "Bob"
  
  Scenario: Saving and viewing an incomplete Association Class object
    Given I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
    When I click on the "Owners" header
    And I create an "Owner" of type "Ownership" where "Percent Ownership" is "55.5"
    And I save and leave the page
    And I click on the "Owners" header
    # NOTE An assoc class without two ends is an incomplete association.  There is 1 incomplete association here so you will see 3 owners instead of 4.
    Then I should see that there are a total of "3" "Owners" in the header
    And I should see a "Owner" whose "Name" is "Bob", and "Percent Ownership" is "66.67", and "Type" is "Person"
    And I should see a "Owner" whose "Name" is "", and "Percent Ownership" is "55.5", and "Type" is "n/a"
  
  # High-level adding/breaking associations. Further testing is done in the Associations feature.
  Scenario: Adding an association
    Given I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
    When I click on the "Maintained By" header
    Then I should see "3" associated "Maintained By"
    When I open the "Maintained By" search
    And I click on the "Maintained By" advanced search link
    And I filter to see "Maintained By" whose "Name" is "Joe"
    And I show the possible "Maintained By"
    And I select the "Maintained By" whose "Name" is "Joe"
    And I click the only "Add Associations" button under the "Maintained By" header
    And I save the page
    And I click on the "Maintained By" header
    Then I should see "4" associated "Maintained By"
  
  Scenario: Adding multiple associations
    Given I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
    When I click on the "Occupants" header
    Then I should see "3" associated "Occupants"
    When I open the "Occupants" search
    And I click on the "Occupants" advanced search link
    And I filter to see "Occupants" whose "Name" contains "J"
    And I show the possible "Occupants"
    And I select the "Occupant" whose "Name" is "Joe"
    And I select the "Occupant" whose "Name" is "Jason"
    And I click the only "Add Associations" button under the "Occupants" header
    # Check that refreshing doesn't change anything
    And I save the page
    And I click on the "Occupants" header
    Then I should see "5" associated "Occupants"
  
  Scenario: Breaking an association
    Given I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
    When I click on the "Occupants" header
    Then I should see "3" associated "Occupants"
    When I select the "Occupant" whose "Name" is "Sally"
    # NOTE: It is currently necessary to check that break associations is complete before saving the page
    #       Failure to do this can cause the save request to be sent before the populate collection response is received.
    #       This results in the breadcrumbs being lost, due to flash rotation.
    And I click the only "Break Associations" button under the "Occupants" header
    Then I should see the unsaved changes warning for "Occupants"
    And I should not see an "Occupant" whose "Name" is "Sally"
    When I save the page
    And I click on the "Occupants" header
    Then I should see "2" associated "Occupants"
    
  Scenario: Breaking multiple associations
    Given I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
    When I click on the "Maintained By" header
    Then I should see "3" associated "Maintained By"
    When I select the "Maintained By" whose "Name" is "Sally"
    And I select the "Maintained By" whose "Name" is "Phil"
    # NOTE: It is currently necessary to check that break associations is complete before saving the page
    #       Failure to do this can cause the save request to be sent before the populate collection response is received.
    #       This results in the breadcrumbs being lost, due to flash rotation.
    And I click the only "Break Associations" button under the "Maintained By" header
    Then I should see the unsaved changes warning for "Maintained By"
    And I should not see a "Maintained By" whose "Name" is "Sally"
    And I should not see a "Maintained By" whose "Name" is "Phil"
    When I save the page
    And I click on the "Maintained By" header
    Then I should see "1" associated "Maintained By"
  
  Scenario: Adding and breaking multiple fields
    Given PENDING
  
  Scenario: Removing and Adding the same association
    Given PENDING
  
  Scenario: Adding and Removing the same association
    Given I expand the "Vehicles" header
    When I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I click on the "Maintained By" header
    Then I should see "3" associated "Maintained By"
    When I open the "Maintained By" search
    And I click on the "Maintained By" advanced search link
    And I filter to see "Maintained By" whose "Name" is "Joe"
    And I show the possible "Maintained By"
    And I select the "Maintained By" whose "Name" is "Joe"
    And I click the only "Add Associations" button under the "Maintained By" header
    And I select the "Maintained By" whose "Name" is "Joe"
    # NOTE: It is currently necessary to check that break associations is complete before saving the page
    #       Failure to do this can cause the save request to be sent before the populate collection response is received.
    #       This results in the breadcrumbs being lost, due to flash rotation.
    And I click the only "Break Associations" button under the "Maintained By" header
    And I should not see a "Maintained By" whose "Name" is "Joe"
    When I save the page
    And I click on the "Maintained By" header
    Then I should see "3" associated "Maintained By"

  Scenario: Adding and breaking associations for two collections simultaneously
    Given I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
    When I click on the "Occupants" header
    Then I should see "3" associated "Occupants"
    When I select the "Occupant" whose "Name" is "Sally"
    # NOTE: It is currently necessary to check that break associations is complete before saving the page
    #       Failure to do this can cause the save request to be sent before the populate collection response is received.
    #       This results in the breadcrumbs being lost, due to flash rotation.
    And I click the only "Break Associations" button under the "Occupants" header
    Then I should see the unsaved changes warning for "Occupants"
    And I should not see an "Occupant" whose "Name" is "Sally"
    # Close the Occupants header
    When I click on the "Occupants" header
    And I click on the "Maintained By" header
    And I should see "3" associated "Maintained By"
    And I open the "Maintained By" search
    And I click on the "Maintained By" advanced search link
    And I filter to see "Maintained By" whose "Name" is "Joe"
    And I show the possible "Maintained By"
    And I select the "Maintained By" whose "Name" is "Joe"
    And I click the only "Add Associations" button under the "Maintained By" header
    And I save the page
    And I click on the "Occupants" header
    Then I should see "2" associated "Occupants"
    When I click on the "Maintained By" header
    Then I should see "4" associated "Maintained By"
    