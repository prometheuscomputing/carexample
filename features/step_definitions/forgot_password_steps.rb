When /^I click the first email/ do
  select_first_email
end

When /^I go to the email application/ do
  pending
  visit "http://localhost:#{MAILCATCHER_HTTP_PORT}"
end

Then /^I should see a from field containing the address "(.*?)"$/ do |from_address|
  verify_email_from_address(from_address)
end

Then /^I should see a to field containing the address "(.*?)"$/ do |to_address|
  verify_email_to_address(to_address)
end

Then /^I should see a subject field containing the subject "(.*?)"$/ do |subject|
  verify_email_subject(subject)
end

And /^I click the link in the email/ do
  click_email_link
end

Then /^I should see an email containing the correct reset password link/ do
  verify_email_reset_password_link
end

Then /^I should see an email containing the correct verify email link/ do
  verify_email_account_link
  sleep 1 # added because something is going on with the mail verification that causes the testing framework to crash.  Capybara self reports this as being caused by a bug in capybara-webkit.  waiting seems to solve the problem.
end

And /^I submit the reset password page/ do
  click_button 'Reset Password'
end

And /^I click the admin verify account email/ do
  sleep 0.1
  select_admin_verify_email
end

