# Test all non-collection/organizer widgets:
#    * string
#    * time
#    * date
#    * timestamp
#    * integer
#    * number (float)
#    * boolean
#    * choice -- These are really associations but the widgets act like attribute widgets
#    * label
#    * link
#    * button
#    * RichText
#    * File
#    * Code

# Test various attribute types here.
Feature: Attributes
  In order to change an object's data
  As a user
  I want to manipulate its attributes
  
  Background:
    Given I have reset the database
    And   I have logged in
    And   I expand the "People" header
    And   I create a "People" of type "Person"
  
  Scenario: Manipulating String attributes
    When I save the page
    Then the value of "Name" should be ""
    When I set the "Name" to "George"
    And  I save and leave the page
    And  I expand the "People" header
    And  I open the "People" search
    And  I click on the "People" advanced search link
    And  I filter to see "People" whose "Name" is "George"
    And  I submit the "People" search
    Then I should see a "People" whose "Name" is "George"
    When I edit the "People" whose "Name" is "George"
    Then the value of "Name" should be "George"
    When I set the "Name" to ""
    And  I save the page
    Then the value of "Name" should be ""

  Scenario: Testing String handling of non-UTF-8 (US-ASCII) characters and symbols
    # Two Different dashes at the end, first one is US-ASCII, second is UTF-8
    When I set the "Name" to "George – !@#$%^&*()- O'Malley ± ° ÷ ó ò ñ ¢ £ ¥ & © §"
    When I save the page
    Then I should not receive a "concurrency" popup warning
    Then the value of "Name" should be "George – !@#$%^&*()- O'Malley ± ° ÷ ó ò ñ ¢ £ ¥ & © §"
    When I save the page
    Then I should not receive a "concurrency" popup warning
  
  Scenario: Submitting a String modification via the enter key
    Given PENDING: Capybara upgrade so native method works
    When I set the "Name" to "Georgia"
    And  I press enter inside the "Name" field
    And  I refresh the page
    Then the value of "Name" should be "Georgia"
  
  Scenario: Manipulating Integer attributes
    When I save the page
    Then the value of "Family Size" should be ""
    When I set the "Name" to "George"
    And  I set the "Family Size" to "99"
    And  I save and leave the page
    And  I expand the "People" header
    And  I open the "People" search
    And  I click on the "People" advanced search link
    And  I filter to see "People" whose "Family Size" is "99"
    And  I submit the "People" search
    Then I should see a "People" whose "Name" is "George" and "Family Size" is "99"
    When I edit the "People" whose "Name" is "George"
    Then the value of "Family Size" should be "99"
    # Test clearing value
    When I set the "Family Size" to ""
    And  I save the page
    Then the value of "Family Size" should be ""
    # Test hex entry with 0x1a (26)
    When I set the "Family Size" to "0x1a"
    And  I save the page
    Then the value of "Family Size" should be "26"
    # Test octal entry with 010 (8)
    When I set the "Family Size" to "010"
    And  I save the page
    Then the value of "Family Size" should be "8"
    # Test binary entry with 0b10 (2)
    When I set the "Family Size" to "0b10"
    And  I save the page
    Then the value of "Family Size" should be "2"
  
  Scenario: Bad Input for an Integer attribute
    When I set the "Family Size" to "Not an int"
    And  I set the "Name" to "George"
    And  I save the page
    Then I should receive the error "Could not save the 'Family Size' field. Please enter an integer."
    And  the value of "Family Size" should be ""
    And  the value of "Name" should be "George"

  Scenario: Manipulating Floating Point attributes
    When I save the page
    Then the value of "Weight" should be ""
    When I set the "Name" to "George"
    And  I set the "Weight" to "150.5"
    And  I save and leave the page
    And  I expand the "People" header
    And  I open the "People" search
    And  I click on the "People" advanced search link
    And  I filter to see "People" whose "Weight" is "150.5"
    And  I submit the "People" search
    Then I should see a "People" whose "Name" is "George" and "Weight" is "150.5"
    When I edit the "People" whose "Name" is "George"
    Then the value of "Weight" should be "150.5"
    When I set the "Weight" to ""
    And  I save the page
    Then the value of "Weight" should be ""
  
  Scenario: Bad Input for a Floating Point attribute
    When I set the "Weight" to "Not a float"
    And  I set the "Name" to "George"
    And  I save the page
    Then I should receive the error "Could not save the 'Weight' field. Please enter a float."
    And  the value of "Family Size" should be ""
    And  the value of "Name" should be "George"
  
  # Note: this does not use the datepicker widget, and will not catch JS errors.
  Scenario: Manipulating Date attributes
    When I save the page
    Then the value of "Date Of Birth" should be ""
    When I set the "Name" to "George"
    And  I set the "Date Of Birth" to "1990-01-31"
    And  I save and leave the page
    And  I expand the "People" header
    And  I open the "People" search
    And  I click on the "People" advanced search link
    And  I filter to see "People" whose "Date Of Birth" is "1990-01-31"
    And  I submit the "People" search
    Then I should see a "People" whose "Name" is "George" and "Date Of Birth" is "1990-01-31"
    When I filter to see "People" whose "Date Of Birth" is "31/01/1990"
    And  I click on the "People" advanced search link
    And  I submit the "People" search
    Then I should see a "People" whose "Name" is "George" and "Date Of Birth" is "1990-01-31"
    When I edit the "People" whose "Name" is "George"
    Then the value of "Date Of Birth" should be "January 31, 1990"
    When I set the "Date Of Birth" to ""
    And  I save the page
    Then the value of "Date Of Birth" should be ""
  
  Scenario: Bad Input for a Date attribute
    When I set the "Date Of Birth" to "99999999999999999"
    And  I set the "Name" to "George"
    And  I save the page
    Then I should receive the error "Could not save the 'Date Of Birth' field. Please enter a date."
    And  the value of "Date Of Birth" should be ""
    And  the value of "Name" should be "George"
  
  # Note: this does not use the datetimepicker widget, and will not catch JS errors.
  Scenario: Manipulating Timestamp attributes
    When I save the page
    Then the value of "Last Updated" should be ""
    When I set the "Name" to "George"
    And  I set the "Last Updated" to "2013-05-01 2:03:03am"
    Then the value of "Last Updated" should be "May 1, 2013 2:03:03am"
    And  I save and leave the page
    And  I expand the "People" header
    And  I open the "People" search
    And  I click on the "People" advanced search link
    And  I filter to see "People" whose "Last Updated" is "2013-05-01 2:03:03am"
    And  I submit the "People" search
    Then I should see a "People" whose "Name" is "George" and "Last Updated" is "2013-05-01 2:03:03am"
    When I edit the "People" whose "Name" is "George"
    Then the value of "Last Updated" should be "May 1, 2013 2:03:03am"
    When I set the "Last Updated" to ""
    Then the value of "Last Updated" should be ""
    And  I save the page
    Then the value of "Last Updated" should be ""
  
  Scenario: Bad Input for a Timestamp attribute
    When I set the "Last Updated" to "99999999999999999"
    And  I set the "Name" to "George"
    And  I save the page
    Then I should receive the error "Could not save the 'Last Updated' field. Please enter a timestamp."
    And  the value of "Last Updated" should be ""
    And  the value of "Name" should be "George"
  
  Scenario: Manipulating Boolean attributes
    When I save the page
    Then the value of "Dependent" should be ""
    When I set the "Name" to "George"
    And  I set the "Dependent" to "True"
    And  I save and leave the page
    And  I expand the "People" header
    And  I open the "People" search
    And  I click on the "People" advanced search link
    And  I filter to see "People" whose "Dependent" is "True"
    And  I submit the "People" search
    Then I should see a "People" whose "Name" is "George" and "Dependent" is "True"
    When I edit the "People" whose "Name" is "George"
    Then the value of "Dependent" should be "True"
    When I set the "Dependent" to ""
    And  I save the page
    Then the value of "Dependent" should be ""
  
  Scenario: Manipulating Choice attributes
    When I save the page
    Then the value of "Handedness" should be ""
    When I set the "Name" to "George"
    And  I set the "Handedness" to "Right Handed"
    And  I save and leave the page
    And  I expand the "People" header
    And  I open the "People" search
    And  I click on the "People" advanced search link
    And  I filter to see "People" whose "Name" is "George"
    And  I submit the "People" search
    Then I should see a "People" whose "Name" is "George" and "Handedness" is "Right Handed"
    When I edit the "People" whose "Name" is "George"
    Then the value of "Handedness" should be "Right Handed"
    When I set the "Handedness" to ""
    And  I save the page
    Then the value of "Handedness" should be ""
  
  Scenario: Manipulating File attributes
    When I save the page
    Then there should not be a stored "Photo"
    And  the value of "Photo" should be "No Stored File"
    When I set the "Name" to "George"
    And  I choose to upload the "Photo" named "default_user_replaceme.jpeg"
    When I save and leave the page
    And  I expand the "People" header
    And  I open the "People" search
    And  I click on the "People" advanced search link
    And  I filter to see "People" whose "Photo" is "default_user_replaceme.jpeg"
    And  I submit the "People" search
    Then I should see a "People" whose "Name" is "George" and "Photo" is "default_user_replaceme.jpeg"
    When I edit the "People" whose "Name" is "George"
    Then there should be a stored "Photo" named "default_user_replaceme.jpeg"
    And  the delete box should not be disabled for the stored "Photo"
    When I check the delete box for the stored "Photo"
    When I save the page
    Then there should not be a stored "Photo"
    # Check deletion cancellation
    When I choose to upload the "Photo" named "default_user_replaceme.jpeg"
    And  I save the page
    Then there should be a stored "Photo" named "default_user_replaceme.jpeg"
    And  I check the delete box for the stored "Photo"
    And  I uncheck the delete box for the stored "Photo"
    # And I cancel the file deletion
    And  I save the page
    Then there should be a stored "Photo" named "default_user_replaceme.jpeg"
    # Check that the delete checkbox is disabled on new uploads
    And  the delete box should not be disabled for the stored "Photo"
    When I choose to upload the "Photo" named "picture2.jpeg"
    Then the delete box should be disabled for the stored "Photo"
    When I save the page
    Then there should be a stored "Photo" named "picture2.jpeg"
    # Check file download - Needs to be checked last. Will move the capybara node
    When I download the stored "Photo"
    Then I should receive a jpeg file named "picture2.jpeg"

  Scenario: Manipulating File attributes (from a nested organizer widget)
    When I save and leave the page
    And  I expand the "People" header
    And  I submit the "People" search
    Then I should see a "People" whose "Name" is "Bob" and "Photo" is "default_user_replaceme.jpeg"
    When I edit the "People" whose "Name" is "Bob"
    And  I click on the "Address" header
    And  I edit the "Address" where "Street Number" is "345"
    And  I click on the "Person" header
    Then there should be a stored "Photo" named "default_user_replaceme.jpeg"
    And  the delete box should not be disabled for the stored "Photo"
    When I check the delete box for the stored "Photo"
    And  I save the page
    And  I click on the "Person" header
    Then there should not be a stored "Photo"
    # Check deletion cancellation
    When I choose to upload the "Photo" named "default_user_replaceme.jpeg"
    And  I save the page
    And  I click on the "Person" header
    Then there should be a stored "Photo" named "default_user_replaceme.jpeg"
    When I check the delete box for the stored "Photo"
    And  I uncheck the delete box for the stored "Photo"
    # And I cancel the file deletion
    And  I save the page
    And  I click on the "Person" header
    Then there should be a stored "Photo" named "default_user_replaceme.jpeg"
    # Check that the delete checkbox is disabled on new uploads
    And  the delete box should not be disabled for the stored "Photo"
    When I choose to upload the "Photo" named "picture2.jpeg"
    Then the delete box should be disabled for the stored "Photo"
    When I save the page
    And  I click on the "Person" header
    Then there should be a stored "Photo" named "picture2.jpeg"
    # Check file download - Needs to be checked last. Will move the capybara node
    When I download the stored "Photo"
    Then I should receive a jpeg file named "picture2.jpeg"

  Scenario: Checking attribute success messages
    When I save the page
    Then the value of "Name" should be ""
    When I set the "Name" to "George"
    And  I save and leave the page
    Then I should receive the message "Updated the fields: Name."

  Scenario: Checking lack of attribute messages (with multiline, non-UTF8 Text)
    # Create and initialize all the fields
    When I fill in the field "Description" with the following body:
      """

      ŸÈ(E Grave) —(Em Dash) ¦(Broken Bar) »(Right Double Guillemet)

      Test text here \n \n\n \r \r\r \r\n

      """
    When I save the page
    Then I should receive the message "Updated the fields: Description, Code Representation."
    Then I should not receive a "concurrency" popup warning
    And  I save the page
    Then I should not receive any messages
    Then I should not receive a "concurrency" popup warning
    Then the value of "Description" should be "ŸÈ(E Grave) —(Em Dash) ¦(Broken Bar) »(Right Double Guillemet) Test text here \n \n\n \r \r\r \r\n"

  # NOTE: Need to find libary to parse the text fields with, and convert html ascii symbols to html utf-8 symbols
  #       without having to resave the page again.
  Scenario: Checking lack of attribute messages (with HTML ASCII symbols)
    # Create and initialize all the fields
    When I fill in the field "Description" with the following body:
      """

      Test Text \n \n\n \r \r\r \r\n

      Test text here &amp&quot&#039&lt&gt &amp; (ascii symbols)

      """
    When I save the page
    # Now save with no changes, should see message.
    Then I should not receive a "concurrency" popup warning
    Then I should receive the message "Updated the fields: Description, Code Representation."
    And  I save the page
    Then I should not receive a "concurrency" popup warning
    Then I should not receive any messages
    Then the value of "Description" should be "Test Text \n \n\n \r \r\r \r\n Test text here &amp&quot&#039&lt&gt & (ascii symbols)"
    When I choose to edit the "Description" text
    Then the value of "Description" should contain "Test text here &amp&quot&#039&lt&gt &amp; (ascii symbols)"
