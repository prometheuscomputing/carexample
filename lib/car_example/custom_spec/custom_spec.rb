# This is an example of how you can create what is essentially an interface-like or mixin organizer.  Companion code can be found in model_extensions.rb.  Note that OhMy is a module, not a class.  People::Person implements #oh_my.  It should also be possible to implement #oh_my in the OhMy module but that would mean that People::Person would have to include it. Done the way demonstrated here, People::Person does not need to include the OhMy module.

# organizer(:OhMy, OhMy) {
#   string('oh_my', :label => 'Wowzers!')
# }


# ----------------------------------------------------------------
# Enabling things that were disabled in the spec when they should not have been.  This is due to bad code in specGen
organizer(:Details, People::Clown::NestingDoll) {
  enable('inner_doll', 'outer_doll')
}

organizer(:Details, Automotive::Part) {
  enable('part_of')
}


