begin
  require 'rspec'
rescue LoadError
  require 'rubygems' unless ENV['NO_RUBYGEMS']
  gem 'rspec'
  require 'rspec'
end

RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = [:should, :expect]
  end
end

GUI_SITE_PORT = ENV['GUI_SITE_PORT'] ? ENV['GUI_SITE_PORT'].to_i : 3000

# Don't delete the database
$preserve_db = true
require relative('../car_example/setup')
