# For testing disabling of creation / deletion / addition / removal
organizer(:Details, Automotive::VehicleTaxRate) {
  disable_removal('for_country')
  disable_creation('for_country')
  disable_addition('for_country')
  disable_deletion('for_country')
  disable_traversal('for_country')
}

# to-one
organizer(:Details, Automotive::Warranty) {
  disable_creation('warrantieds')
}

# to-many (containment)
organizer(:Details, Geography::Country) {
  disable_traversal('territories')
  disable_deletion('territories')
}

# to-one
organizer(:Details, Automotive::RepairShop) {
  disable_traversal('chief_mechanic')
  disable_addition('customers')
  disable_removal('mechanics')
}

# to-one
organizer(:Details, Automotive::Car) {
  disable_deletion('vin')
}
