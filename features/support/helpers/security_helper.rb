def verify_package_not_accessible(package, except_classes = [])
  pkg = package.to_const
  package_consts = pkg.constants
  package_consts.each do |const|
    next if except_classes.include?(const.to_s)
    klass_const = pkg.const_get(const)
    next unless klass_const < Sequel::Model # do not interogate primitive types because they won't have a controller or template
    next if klass_const.enumeration? # do not interogate enumerations because they won't have templates...wait, why not?  Yea, they do have them...
    next if klass_const.abstract? # You get a 500 error if you try to do this for any abstract class anyway.  That isn't what we are testing here.
    next if klass_const.properties.any? {|_,v| v[:associates]} && !klass_const.association_class?
    url = "/#{Gui.route_for(klass_const)}/new/"
    visit(url)
    verify_page_error
  end
end
def verify_package_redirects_to_login(package, except_classes = [])
  pkg = package.to_const
  package_consts = pkg.constants
  package_consts.each do |const|
    next if except_classes.include?(const.to_s)
    klass_const = pkg.const_get(const)
    visit("/#{klass_const.to_s.to_ramaze_url}/new/")
    verify_login_page
  end
end
def verify_urls_redirect_to_login(urls)
  urls.each do |url|
    visit url
    verify_login_page
  end
end
# --------------------------------------- Login Verifiers
def verify_page_error(negative = false)
  (is_404_error || is_500_error || is_403_error).should_unless(negative, (be true))
end
def is_403_error
  !!(text =~ /403/)
end
def is_404_error
  !!(text =~ /404/)
end
def is_500_error
  !!(text =~ /500/)
end
def verify_403_error
  expect(text).to match(/403/)
end
def verify_404_error
  expect(text).to match(/404/)
end
def verify_500_error
  expect(text).to match(/500/)
end
def verify_login_page
  within_fieldset 'Login' do
    expect(self).to have_content("The Prometheus Use Case Editor is a commercial product developed by Prometheus Computing.")
  end
end