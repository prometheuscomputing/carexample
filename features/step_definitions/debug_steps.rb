When /^eval ?\(?(.*)\)?$/ do |string|
  eval string
end

When /^screenshot(.*)$/ do |description|
  description = 'custom_screenshot' if description.nil? || description.empty?
  screenshot description.strip
end

When /^I test$/ do
  # log Rainbow(KeywordKeeper.get.inspect).cadetblue # please leave this here.  It is a reminder of how to hack Gherkin / Cucumber to be able to "know" which keyword was invoked in the Gherkin.
  # raise
  true
end
