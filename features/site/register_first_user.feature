# Test user login/registration functionality
#  * Account creation
#  * Account login
Feature: Login & Registration
  In order to use the application
  As a user
  I want to register the first user and be logged in
  
  Background:
    Given I have a database with no users
  
  Scenario: Registering a new account
    Then I go to the home page
    And screenshot 'home'
    And  I enter the username "first"
    And  I enter the password "first1"
    And  I re-enter the password "first1"
    And  I submit the registration
    Then I should be at the home page
    # First user should be an admin and therefore should see the "Manage Site" link
    Then I click the topbar "Administration" link
