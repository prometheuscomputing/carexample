# Test csrf protection, login requirement, and other security related functions.
Feature: Security
  In order to prevent unauthorized access or modification to data
  As an administrator
  I want to ensure that there is no unauthorized access
  
  Background:
    Given I have reset the database
    And I stay logged out
  
  
  Scenario: User attempts to register with a blank or invalid invitation code
    # It is entirely possible to register without an invitiation code if the option to do so is set in meta_info.rb
    # Then "phillip" should not be able to register with the invitation code ""
    Then "phillip" should not be able to register with the invitation code "bad_code"
  
  Scenario: Normal users can't access administration options
    When I have logged in as a user
    Then I should not be able to access the administration panel
    And I should not be able to access any objects in the "Gui_Builder_Profile" package except for "BinaryData", "RichText", "RichTextImage", "Code", and "File"
  
  Scenario: User attempts URL manipulation to delete a restricted object via an association
    When I have logged in as a user
    When I go to the url "/Gui_Builder_Profile/RegularExpressionCondition/1/password_requirement_for/delete/?view-classifier=Gui_Builder_Profile::RichText"
    Then I should see that I do not have permission
  
  # Fails because some pages return "action not found: ..."
  @known_failing
  @low_priority
  Scenario: Users must login to access all pages except login and registration pages
    And I should be redirected to login on accessing any objects in the "Gui_Builder_Profile" package
    And I should be redirected to login on accessing any objects in the "Automotive" package
    And I should be redirected to login on accessing any objects in the "Geography" package
    And I should be redirected to login on accessing any objects in the "People" package
    # PENDING
    And I should be redirected to login on accessing:
      | /                               |
      | /foo                            |
      | /foo/                           |
      | /index/                         |
      | /gui/                           |
      | /populate_organizer/            |
      | /gb_users/                      |
      | /gb_users/index/                |
      | /gb_users/foo/                  |
      | /gb_users/logout/               |
      | /gb_users/preferences/          |
      | /gb_users/user_admin/                |
      | /gb_users/edit_perspective/     |
      | /tasks/                         |
      | /tasks/index/                   |
      | /tasks/foo/                     |
      | /tasks/launch_process/          |
      | /tasks/perform_task/            |
      | /tasks/claim/                   |
      | /tasks/claim_and_perform_task/  |
      | /tasks/complete/                |
      | /tasks/cancel/                  |
      | /change_tracker/                |
      | /change_tracker/index/          |
      | /change_tracker/foo/            |
      | /change_tracker/history/        |
  
  # Not a good example. See if this can be improved
  # @needs_work
  # Scenario: XSS attack via URL
  #   Given I have logged in as a user
  #   When I go to the url "/People/Person/1/?name=bob<script>$('body').html('<div style=\'color:red\'>XSS ATTACK!</div>')</script>"
  #   Then I should not see the text "XSS ATTACK"
  #   When I save the page
  #   Then I should not see the text "XSS ATTACK"
  #   And I should see the attributes "Name", "Description", "Photo", "Date Of Birth", "Last Updated", "Family Size", "Weight", "Dependent", and "Handedness"
  #   And I should see the associations "Addresses", "Drives", "Maintains", "Occupying", "Repair Shops", and "Vehicles"
  
  # Fails because RichText is vulnerable to XSS attack in detailed and collection views
  @known_failing
  Scenario: XSS attack via RichText attribute
    Given I have logged in as a user
    And I expand the "People" header
    And I edit the "People" whose "Name" is "Bob"
    When I set the "Description" to "<script>$('body').html('<div style=\'color:red\'>XSS ATTACK!</div>')</script>"
    And I save the page
    # PENDING
    Then I should not see only the text "XSS ATTACK!"
    And I should see the attributes "Name", "Description", "Photo", "Date Of Birth", "Last Updated", "Family Size", "Weight", "Dependent", and "Handedness"
    And I should see the associations "Addresses", "Drives", "Maintains", "Occupying", "Repair Shops", and "Vehicles"
    And the value of "Description" should be "<script>$('body').html('<div style=\'color:red\'>XSS ATTACK!</div>')</script>"
    When I go to the previous page
    And I expand the "People" header
    Then I should see a "People" whose "Name" is "Bob", "Description" contains "XSS ATTACK!"
  
  # Fails because String is vulnerable to XSS attack in collection view
  @known_failing
  Scenario: XSS attack via String attribute
    Given I have logged in as a user
    And I expand the "People" header
    And I edit the "People" whose "Name" is "Bob"
    When I set the "Name" to "Bob<script>$('body').html('<div style=\'color:red\'>XSS ATTACK!</div>')</script>"
    And I save the page
    And I should see the attributes "Name", "Description", "Photo", "Date Of Birth", "Last Updated", "Family Size", "Weight", "Dependent", and "Handedness"
    And I should see the associations "Addresses", "Drives", "Maintains", "Occupying", "Repair Shops", and "Vehicles"
    And the value of "Name" should be "Bob<script>$('body').html('<div style=\'color:red\'>XSS ATTACK!</div>')</script>"
    When I go to the previous page
    And I expand the "People" header
    # PENDING
    Then I should not see only the text "XSS ATTACK!"
    And I should see a "People" whose "Name" is "Bob<script>$('body').html('<div style=\'color:red\'>XSS ATTACK!</div>')</script>"
  
  # Fails because File is vulnerable to XSS attack in detailed and collection views
  @known_failing
  Scenario: XSS attack via File attribute
    Given I have logged in as a user
    And I expand the "People" header
    And I edit the "People" whose "Name" is "Bob"
    When I choose to upload the "Photo" named "<script>$('body').html('<div>XSS ATTACK!</div>')</script>.jpeg"
    And I save the page
    Then I should not see only the text "XSS ATTACK!"
    And I should see the attributes "Name", "Description", "Photo", "Date Of Birth", "Last Updated", "Family Size", "Weight", "Dependent", and "Handedness"
    And I should see the associations "Addresses", "Drives", "Maintains", "Occupying", "Repair Shops", and "Vehicles"
    # PENDING
    And the value of "Photo" should be "<script>$('body').html('<div>XSS ATTACK!</div>')</script>.jpeg"
    When I download the stored "Photo"
    Then I should receive a jpeg file named "<script>$('body').html('<div>XSS ATTACK!</div>')</script>.jpeg"
    When I go to the previous page
    And I expand the "People" header
    Then I should not see only the text "XSS ATTACK!"
    And I should see a "People" whose "Name" is "Bob" and "Photo" is "<script>$('body').html('<div>XSS ATTACK!</div>')</script>.jpeg"
    When I edit the "People" whose "Name" is "Bob"
    And I check the delete box for the stored "Photo"
    And I save the page
    Then there should not be a stored "Photo"
  
  # Fails because Date attributes currently crash on non-date input
  @known_failing
  Scenario: XSS attack via Date attribute
    Given I have logged in as a user
    And I expand the "People" header
    And I edit the "People" whose "Name" is "Bob"
    When I set the "Date Of Birth" to "<script>$('body').html('<div style=\'color:red\'>XSS ATTACK!</div>')</script>"
    And I save the page
    # PENDING
    And I should see the attributes "Name", "Description", "Photo", "Date Of Birth", "Last Updated", "Family Size", "Weight", "Dependent", and "Handedness"
    And I should see the associations "Addresses", "Drives", "Maintains", "Occupying", "Repair Shops", and "Vehicles"
    And the value of "Date Of Birth" should be ""
    When I go to the previous page
    And I expand the "People" header
    Then I should not see only the text "XSS ATTACK!"
    Then I should see a "People" whose "Name" is "Bob" and "Date Of Birth" is ""
  
  # These are not implemented because invalid numeric values are not submitted to the server, and choices are not user-editable.
  # Implement these tests if that changes.
  #Scenario: XSS attack via Integer/Float/Choice attributes
  
  # Implementation is low priority because our meta header explicitly defines UTF-8 encoding.
  # @low_priority
  # Scenario: UTF-7 XSS attack on RichText
  #   When I expand the "People" header
  #   And I edit the "People" whose "Name" is "Bob"
  #   And I set the "Description" to "+ADw-script+AD4-alert(document.cookie)+ADw-/script+AD4-"
  #   And I save the page
  #   Then there should not be a popup

  # Currently incomplete because basic XSS checking is not working.
  # Need to revise once that is complete
  @known_failing
  @needs_work
  Scenario: Markdown XSS attack on RichText
    Given I have logged in as a user
    And I expand the "People" header
    And I edit the "People" whose "Name" is "Bob"
    When I set the "Description" to ![foobar](javascript:$\('body'\).html\('<div style="color:red">XSS ATTACK</div>'\);)!
    And I save the page
    Then I should not see only the text "XSS ATTACK"
    When I click the "foobar" link
    # PENDING
    Then I should not see only the text "XSS ATTACK"
    And I should see the attributes "Name", "Description", "Photo", "Date Of Birth", "Last Updated", "Family Size", "Weight", "Dependent", and "Handedness"
    And I should see the associations "Addresses", "Drives", "Maintains", "Occupying", "Repair Shops", and "Vehicles"
    # Test that RichText isn't broken
    When I set the "Description" to "Finished"
    And I save the page
    Then the value of "Description" should be "Finished"
  
  # Scenario: CSRF attack
  # 
  # Scenario: SQL injection attack
  # 
  # Scenario: Non-existant pages should return 404s