require 'csv'
module Gui
  class Home
    include Gui::PolicyMethods
    
    def current_user(user = nil)
      @user = user if user
      @user
    end
    
    # For testing collection_button.  This is called when a button widget within a collection on the homepage is clicked.  The value of 'widget_getter' of the button widget in question is 'download_search_results'.
    # FIXME write cukes (if you can).  There are currently no tests for this.
    def download_search_results(user, opts)
      collection_getter = opts[:collection_getter] || {}
      advanced_filter   = Gui.build_query_from_filter(opts[:advanced_filter] || {})
      simple_filter     = Gui.build_query_from_search_all_filter(opts[:simple_filter] || {})
      filter = advanced_filter + simple_filter
      current_user(user)
      search_results = get_authorized_property(self, collection_getter, {filter:filter})
      current_user(nil)
      data = []
      csv = CSV.generate(:headers => true) do |c|
        c << ['Name', 'Description', 'Weight']
        search_results.each do |sr|
          c << sr.to_collection_download
        end
      end
      info = self.class.get_info(collection_getter)
      {data:csv, type:'download', blob_type:'text/csv;charset=utf-8;', filename:"#{info[:class].demodulize}_search_#{Time.now.strftime("%Y.%m.%d_at_%k:%M")}.csv"}.to_json
    end
  end
end
