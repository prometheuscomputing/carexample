organizer(:Details, Automotive::ElectricVehicle) {
  reorder('vehicle_model', :to_beginning => true)
}

organizer(:Details, Automotive::HybridVehicle) {
  reorder('Drivers', :to_beginning => true)
}

organizer(:Details, Automotive::Minivan) {
  # TODO: relabel should not reorder
  relabel("Vehicle Model", "Model")
  hide("Replacement For")
  order("Make", "Model", "Cost", "Sliding Doors", "Miles Per Gallon", "Owners", "Drivers", "Occupants", "Maintained By",
    "Being Repaired By", "Components", "Warranty", "Warranty Expiration Date", "Warranty Void", "Serial", "VIN", "Registered At")
}
