def replace_database(db_backup)
  # NOTE: this sleep code is commented out since it should no longer be needed with an in-memory database. -SD
  # Ensure that at least a minimum_time_between_resets occurs between resets
  # If that time has not elapsed, sleep up to that minimum
  # This avoids a database locking issue that occurs if it is reset too frequently
  # minimum_time_between_resets = 1
  # if $last_reset_time
  #   time_since_last_reset = Time.now - $last_reset_time
  #   sleep(minimum_time_between_resets - time_since_last_reset) if time_since_last_reset < minimum_time_between_resets
  # end
  
  clear_dbs
  import_tables db_backup
  # $last_reset_time = Time.now

end

def clear_dbs
  db = Sequel::DATABASES.first
  db.tables.each do |table|
    db[table].delete
    # max_attempts = 5
    # sleep_time = 0.2
    # count = 0
    # success = false
    # while !success && (count < max_attempts)
    #   db.foreign_keys = false
    #   begin
    #     count += 1
    #     db[table].delete unless db[table].count == 0
    #     success = true
    #   rescue Sequel::DatabaseError, SQLite3::ConstraintException, Sequel::ForeignKeyConstraintViolation, Sequel::UniqueConstraintViolation => e
    #     puts "Rescued #{e.class} failure while deleting the '#{table}' table. Attempt (#{count})."
    #     puts e.message
    #     sleep sleep_time
    #   end
    # end
  end
end

def backup_database
  db_backup = {}
  db = Sequel::DATABASES.first
  db.tables.each do |table|
    db_backup[table] = db[table].all
  end
  db_backup
end

def import_tables db_backup
  db = Sequel::DATABASES.first
  db.tables.each do |table|
    db[table].multi_insert db_backup[table]
  end
end

def backup_database_to_file(path)
  # Save database
  File.open(path, 'wb') {|f| f.write(Marshal.dump(backup_database))}
end

def replace_database_from_file(path)
  replace_database(Marshal.load(File.binread(path)))
end