Feature: Permissions
  In order to keep certain users from viewing or editing objects or attributes that they do not have permission to
  As an administrator
  I want to be warned when attempting to duplicate marked fields
  
  Background:
    # Given I have reset the database
    # And I have logged in as an administrator
    # And I expand the "People" header
  
  Scenario: Disabling editing of a class for a user role
    Given PENDING: implementation of permissions feature.
  # 
  # Scenario: Disabling viewing of a class for a user role
  #   Given PENDING: implementation of permissions feature.
  # 
  # Scenario: Disabling editing of an attribute/association for a user role
  #   Given PENDING: implementation of permissions feature.
  # 
  # Scenario: Disabling viewing of an attribute/association for a user role
  #   Given PENDING: implementation of permissions feature.
  # 
  # Scenario: Disabling editing of an instance for a user role
  #   Given PENDING: implementation of permissions feature.
  # 
  # Scenario: Disabling viewing of an instance for a user role
  #   Given PENDING: implementation of permissions feature.
  #   