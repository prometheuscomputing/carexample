Feature: Saving objects with concurrent changes made by another user
  In order to handle issues with editing the same object at the same time
  As a user
  I want to edit the same object with multiple tabs without blindly overiding data
  
  Background:
    Given I have reset the database
    And   I have logged in
  

  Scenario: Saving an object with numeric field changes with concurrent numeric changes - Abandon Changes Resolution
    When I expand the "Vehicles" header
    And  I create a "Vehicle" of type "Motorcycle" where "Make" is "Other", "Vehicle Model" is "Tron Bike", and "Cost" is "99999"
    And  I save the page

    # Switch to the 2nd tab
    When I open a new tab
    And  I expand the "Vehicles" header
    When I view "100" "Vehicle" per page
    And  I submit the "Vehicle" search
    And  I edit the "Vehicle" where "Vehicle Model" is "Tron Bike"
    And  I set the "Cost" to "88888"
    And  I save the page

    # Switch to the 1st tab
    And  I switch to the 1st tab
    # Verify that the Cost on page is the same as when we left
    Then the value of "Cost" should be "99999"
    When I set the "Cost" to "77777"
    And  I save the page
    Then I should receive a "concurrency warning" popup warning
    And  the "concurrency warning" popup should have my change "77777" and the change found "88888" for the "Cost"
    When I abandon my changes on the "concurrency warning" popup
    Then the value of "Cost" should be "88888"

  Scenario: Saving an object with no field changes with concurrent string changes - Cancel Resolution
    When I expand the "People" header
    And  I create a "People" of type "Person" where "Name" is "Greg", "Family Size" is "3", and "Weight" is "150"
    And  I save the page

    # 2nd Tab here
    When I open a new tab
    And  I expand the "People" header
    And  I edit the "People" where "Name" is "Greg"

    # 3rd Tab here
    When I open a new tab
    And  I expand the "People" header
    And  I edit the "People" where "Name" is "Greg"
    Then the value of "Name" should be "Greg"
    When I set the "Name" to "Gregory"
    And  I save the page
    Then I should not receive a "concurrency warning" popup warning
    And  the value of "Name" should be "Gregory"

    When I switch to the 2nd tab
    Then the value of "Name" should be "Greg"
    And  I save the page
    Then I should receive a "concurrency warning" popup warning
    And  the "concurrency warning" popup should have the original value "Greg" and the change found "Gregory" for the "Name"

    # The values on the page should not have changes. When saving again, should get the concurrency popup again.
    When I cancel the "concurrency warning" popup
    Then the value of "Name" should be "Greg"
    When I save the page
    Then I should receive a "concurrency warning" popup warning
    When I cancel the "concurrency warning" popup
    Then I should not still see a "concurrency warning" popup warning
    When I save the page
    Then I should receive a "concurrency warning" popup warning
    When I switch to the 1st tab


  Scenario: Saving an object with numeric field changes with concurrent numeric changes - Merge Resolution
    When I expand the "People" header
    And  I create a "People" of type "Person" where "Name" is "Gregorovich", "Family Size" is "30", and "Weight" is "180.0"
    And  I save the page

    # 2nd Tab here
    When I open a new tab
    And  I expand the "People" header
    And  I edit the "People" where "Name" is "Gregorovich"
    When I set the "Weight" to "267.0"

    # 3rd Tab here
    When I open a new tab
    And  I expand the "People" header
    And  I edit the "People" where "Name" is "Gregorovich"
    Then the value of "Weight" should be "180.0"
    When I set the "Weight" to "150.0"
    And  I save the page
    Then the value of "Weight" should be "150.0"
    And  I should not receive a "concurrency warning" popup warning

    When I switch to the 2nd tab
    Then the value of "Weight" should be "267.0"
    And  I save the page
    Then I should receive a "concurrency warning" popup warning
    And  the "concurrency warning" popup should have my change "267.0" and the change found "150.0" for the "Weight"

    # Choosing default selected merge.
    When I merge on the "concurrency warning" popup
    Then the value of "Weight" should be "267.0"
    # Shouldn't be able to resolve the concurrency through canceling.
    And  I should not receive a "concurrency warning" popup warning
    When I switch to the 1st tab


  Scenario: Saving an object with no concurrent changes should not generate popup
    Given I expand the "People" header
    And  I create a "People" of type "Person" where "Name" is "Hackathon", "Family Size" is "30", "Weight" is "180.0", and "Date Of Birth" is "May 4, 1990"
    And  I choose to upload the "Photo" named "default_user_replaceme.jpeg"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning

    # 2rd Tab here
    When I open a new tab
    And  I expand the "People" header
    And  I edit the "People" where "Name" is "Hackathon"
    And  I save the page

    # Back to 1st Tab - Save 
    And  I switch to the 1st tab
    And  I set the "Description" to "Bob's description\n\nThis is a new line\n\n This is another!\n\nFinished."
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    Then the value of "Description" should be "Bob's description\n\nThis is a new line\n\n This is another!\n\nFinished."

  Scenario: Saving an object with unset boolean that was concurrently set
    # 1st Tab here
    When I expand the "People" header
    And  I create a "People" of type "Person" where "Name" is "Hank"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning

    # 2rd Tab here
    When I open a new tab
    And  I expand the "People" header
    And  I edit the "People" where "Name" is "Hank"
    And  I set the "Dependent" to "True"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning

    # Back to 1st Tab - Save with no changes
    When I switch to the 1st tab
    When I save the page
    Then I should receive a "concurrency warning" popup warning

    # Check popup boolean contents
    And  the "concurrency warning" popup should have my change "" and the change found "True" for the "Dependent"
    And  the "concurrency warning" popup should have my changes selected for the "Dependent"
    And  the "concurrency warning" popup should not have the selections disabled for the "Dependent"

    When I merge on the "concurrency warning" popup
    Then I should not receive a "concurrency warning" popup warning

    # Verify Page contents
    And  the value of "Dependent" should be ""

  Scenario: Saving an object with concurrently changing file, boolean, text, and timestamp
    # 1st Tab here - creating File
    When I expand the "People" header
    And  I create a "People" of type "Person" where "Name" is "Hank", "Family Size" is "30", "Weight" is "180.0", and "Date Of Birth" is "May 4, 1990"
    And  I choose to upload the "Photo" named "default_user_replaceme.jpeg"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning

    # 2rd Tab here - Deleting File
    When I open a new tab
    And  I expand the "People" header
    And  I edit the "People" where "Name" is "Hank"
    And  I check the delete box for the stored "Photo"
    And  I set the "Date Of Birth" to "January 4, 1990"
    And  I set the "Dependent" to "True"
    And  I set the "Description" to "Totally Crazy Description Here"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning

    # Back to 1st Tab - Save with no changes
    When I switch to the 1st tab
    Then there should be a stored "Photo" named "default_user_replaceme.jpeg"
    When I save the page
    Then I should receive a "concurrency warning" popup warning

    Then I should receive the warning "The file for 'Photo' was concurrently deleted" on the concurrency popup

    # Check popup boolean contents
    And  the "concurrency warning" popup should have my change "" and the change found "True" for the "Dependent"
    And  the "concurrency warning" popup should have my changes selected for the "Dependent"
    And  the "concurrency warning" popup should not have the selections disabled for the "Dependent"

    # Check popup timestamp contents
    And  the "concurrency warning" popup should have my change "May 4, 1990" and the change found "January 4, 1990" for the "Date Of Birth"
    And  the "concurrency warning" popup should have my changes selected for the "Date Of Birth"
    And  the "concurrency warning" popup should not have the selections disabled for the "Date Of Birth"

    # Check popup text contents
    And  the "concurrency warning" popup should have my change "" and the change found "Totally Crazy Description Here" for the "Description:Content"
    And  the "concurrency warning" popup should have my changes selected for the "Description"

    # Change two options to the concurrent changes.
    When I select the changes found for the "Description" on the "concurrency warning" popup
    When I select the changes found for the "Date Of Birth" on the "concurrency warning" popup
    Then the "concurrency warning" popup should have the changes found selected for the "Date Of Birth"

    When I merge on the "concurrency warning" popup
    And  I should not receive a "concurrency warning" popup warning

    # Verify Page contents
    And  there should not be a stored "Photo"
    And  the value of "Dependent" should be ""
    # And the concurrent change should still be here
    And  the value of "Date Of Birth" should be "January 4, 1990"
    And  the value of "Description" should be "Totally Crazy Description Here"

  # Currently, the concurrency and uniqueness popup alternate back and forth,
  # unable to commit their changes without the other being resolved first.
  # Unknown as to how to handle this so far
  Scenario: Saving an object with concurrent changes and uniqueness issue
    # 1st Tab here - creating seperate object to have uniqueness error with
    When I expand the "People" header
    And  I create a "People" of type "Person" where "Name" is "Paul"
    And  I save the page
    Then I should not receive a "concurrency warning" popup warning
    And  I should not receive a "uniqueness" popup warning


    # 2nd Tab here - creating second object
    When I open a new tab
    And  I expand the "People" header
    And  I create a "People" of type "Person" where "Name" is "Cool Guy Phillip", "Family Size" is "30", and "Weight" is "180.0"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning
    And  I should not receive a "uniqueness" popup warning

    # 3rd Tab here - Making concurrency changes here
    When I open a new tab
    And  I expand the "People" header
    And  I edit the "People" where "Name" is "Cool Guy Phillip"
    And  I set the "Family Size" to "3"
    And  I set the "Dependent" to "True"
    And  I set the "Name" to "Funny Man 5"
    And  I save the page
    Then I should not receive a "concurrency warning" popup warning
    And  I should not receive a "uniqueness" popup warning

    # Back to 2nd Tab - Save with changes
    When I switch to the 2nd tab
    Then the value of "Name" should be "Cool Guy Phillip"
    And  the value of "Family Size" should be "30"
    When I set the "Name" to "Paul"
    When I save the page
    # Should get concurrency popup first
    Then I should receive a "concurrency warning" popup warning
    And  I should not receive a "uniqueness" popup warning
    And  the "concurrency warning" popup should have my change "30" and the change found "3" for the "Family Size"
    And  the "concurrency warning" popup should have my change "" and the change found "True" for the "Dependent"
    And  the "concurrency warning" popup should have my change "Paul" and the change found "Funny Man 5" for the "Name"
    When I merge on the "concurrency warning" popup
    # Should get uniqueness popup only
    Then I should not receive a "concurrency warning" popup warning
    # Didn't get uniqueness?
    And  I should receive a "uniqueness" popup warning
    When I continue anyway on the "uniqueness" popup
    # Should not get any popup
    Then I should not receive a "concurrency warning" popup warning
    And  I should not receive a "uniqueness" popup warning
    When I switch to the 1st tab

  # We were recovering files based on using ChangeTracker.  We no longer use ChangeTracker for anything related to concurrency.  We have talked about saving deleted files in a temp folder and recovering them from there but that has not yet been implemented.
  @known_failing
  Scenario: Recovering file attribute
    # Create person and upload photo
    When I expand the "People" header
    And  I create a "People" of type "Person" where "Name" is "Fire Guy"
    And  I choose to upload the "Photo" named "default_user_replaceme.jpeg"
    And  I save the page
    Then there should be a stored "Photo" named "default_user_replaceme.jpeg"

    # Delete the photo in another tab
    When I open a new tab
    And  I expand the "People" header
    And  I edit the "People" where "Name" is "Fire Guy"
    And  I check the delete box for the stored "Photo"
    And  I save the page
    Then there should not be a stored "Photo"

    # Go Back to first tab and save person again
    When I switch to the 1st tab
    Then there should be a stored "Photo" named "default_user_replaceme.jpeg"
    When I save the page
    Then I should receive a "concurrency warning" popup warning
    And  the "concurrency warning" popup should have my text changes "Restore File: default_user_replaceme.jpeg" for the "Photo"
    And  the "concurrency warning" popup should have the text changes found "No Stored File" for the "Photo"
    When I merge on the "concurrency warning" popup
    Then there should be a stored "Photo" named "default_user_replaceme.jpeg"

  # Currently, we can't save the ordering of collections across continue edits
  @known_failing
  Scenario: Persisting ordering of associated collection
    When I expand the "Vehicles" header
    And  I edit the "Vehicle" where "Vehicle Model" is "S80"
    When I click on the "Maintained By" header
    Then I should see "3" associated "Maintained By"
    And  I should see the "Maintained By" whose "Name" is "Bob" in position "1"
    And  I should see the "Maintained By" whose "Name" is "Sally" in position "2"
    And  I should see the "Maintained By" whose "Name" is "Phil" in position "3"
    When I drag the "Maintained By" whose "Name" is "Bob" to position "3"

    When I open a new tab
    When I expand the "Vehicles" header
    And  I edit the "Vehicle" where "Vehicle Model" is "S80"
    And  I set the "Serial" to "123456"
    And  I save the page

    When I switch to the 1st tab
    And  I save the page
    Then I should receive a "concurrency warning" popup warning
    And  the "concurrency warning" popup should have my change "" and the change found "123456" for the "Serial"
    When I merge on the "concurrency warning" popup

    And  I click on the "Maintained By" header
    Then I should see "3" associated "Maintained By"
    And  I should see the "Maintained By" whose "Name" is "Sally" in position "1"
    And  I should see the "Maintained By" whose "Name" is "Phil" in position "2"
    And  I should see the "Maintained By" whose "Name" is "Bob" in position "3"

  Scenario: Persisting additions, deletions, and breakings of associations - many-to-many
    And  I expand the "Vehicles" header
    And  I create a "Vehicle" of type "Motorcycle" where "Make" is "Other", "Vehicle Model" is "Ninja 500R", and "Cost" is "10000"
    And  I click on the "Owners" header
    And  I show the possible "Owners"
    And  I should see "8" "Owners"
    When I select the "Owner" whose "Name" is "Rick"
    And  I select the "Owner" whose "Name" is "Bill"
    And  I select the "Owner" whose "Name" is "Jeff"
    And  I click the only "Add Associations" button under the "Owners" header
    And  I save the page

    And  I click on the "Owners" header
    When I select the "Owner" whose "Name" is "Rick"
    And  I click the only "Break Associations" button under the "Owners" header
    When I select the "Owner" whose "Name" is "Bill"
    And  I click the only "Delete" button under the "Owners" header
    And  I confirm the delete popup
    When I show the possible "Owners"
    And  I select the "Owner" whose "Name" is "Phil"
    And  I click the only "Add Associations" button under the "Owners" header

    When I open a new tab
    When I expand the "Vehicles" header
    When I view "100" "Vehicle" per page
    And  I submit the "Vehicle" search
    And  I edit the "Vehicle" where "Vehicle Model" is "Ninja 500R"
    And  I set the "Serial" to "123456"
    And  I save the page

    When I switch to the 1st tab
    And  I save the page
    Then I should receive a "concurrency warning" popup warning
    # Doesn't matter what's on the popup
    When I merge on the "concurrency warning" popup
    And  I save the page
    And  I click on the "Owners" header
    Then I should see "2" associated "Owners"
    And  I should see a "Owners" where "Name" is "Phil"
    And  I should see a "Owners" where "Name" is "Jeff"
  
  Scenario: Tabs are working
    When I expand the "People" header
    And  I edit the "People" whose "Name" is "Bob"
    And  I set the "Name" to "Bob Sr."
    When I open a new tab
    When I expand the "People" header
    And  I edit the "People" whose "Name" is "Bob"
    And  I set the "Name" to "Bob Jr."
    When I open a new tab
    When I expand the "People" header
    And  I edit the "People" whose "Name" is "Bob"
    And  I set the "Name" to "Bobby"
    # Test tab environments
    When I switch to the 1st tab
    Then the value of "Name" should be "Bob Sr."
    When I switch to the 2nd tab
    Then the value of "Name" should be "Bob Jr."
    When I switch to the 3rd tab
    Then the value of "Name" should be "Bobby"
    When I switch to the 1st tab
