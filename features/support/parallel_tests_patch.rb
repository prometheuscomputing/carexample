# I haven't taken the time to figure out how to get this stuff to load before #run gets called.  Instead I have just hacked the gem itself, adding the stuff detailed below directly in.
if defined?(ParallelTests)
  module ParallelTests
  class CLI
    def run(argv)
      options = parse_options!(argv)

############################################################################
# ADD this stuff into gem!
options[:prometheus_env_other] = 'ERASE_SCROLLBACK=false CLEAR_CAPYBARA_SCREENSHOTS=true'
options[:prometheus_env_db]    = "CEDB=#{ENV['CEDB']}"
options[:prometheus_env_port]  = "GUI_SITE_PORT=#{ENV['GUI_SITE_PORT']}"
############################################################################

      ENV['DISABLE_SPRING'] ||= '1'

      num_processes = ParallelTests.determine_number_of_processes(options[:count])
      num_processes *= (options[:multiply] || 1)

      options[:first_is_1] ||= first_is_1?

      if options[:execute]
        execute_shell_command_in_parallel(options[:execute], num_processes, options)
      else
        run_tests_in_parallel(num_processes, options)
      end
    end
  end
  module Gherkin
    class Runner < ParallelTests::Test::Runner

      class << self
        def run_tests(test_files, process_number, num_processes, options)
          combined_scenarios = test_files

          if options[:group_by] == :scenarios
            grouped = test_files.map { |t| t.split(':') }.group_by(&:first)
            combined_scenarios = grouped.map {|file,files_and_lines| "#{file}:#{files_and_lines.map(&:last).join(':')}" }
          end

          sanitized_test_files = combined_scenarios.map { |val| WINDOWS ? "\"#{val}\"" : Shellwords.escape(val) }

          options[:env] ||= {}
          options[:env] = options[:env].merge({'AUTOTEST' => '1'}) if $stdout.tty? # display color when we are in a terminal

############################################################################
# AND ADD this stuff to the gem also
base_port_number = options[:prometheus_env_port].slice /(?<=\=).+/
port_number = (base_port_number.to_i + process_number.to_i).to_s
db_opt = options[:prometheus_env_db].sub('.db', "#{port_number}.db")
port = options[:prometheus_env_port].sub(base_port_number, port_number)
cmd = [
  options[:prometheus_env_other],
  db_opt,
  port,
############################################################################
            executable,
            (runtime_logging if File.directory?(File.dirname(runtime_log))),
            cucumber_opts(options[:test_options]),
            *sanitized_test_files
          ].compact.reject(&:empty?).join(' ')
          puts "\n\nCMD: #{cmd.inspect}\n\n"
          execute_command(cmd, process_number, num_processes, options)
        end
      end
    end
  end
end
end