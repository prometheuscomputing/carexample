# Hey, you.  Read the README that is here in the step_definitions folder!
When /^I choose (.*) on the (.*) popup$/ do |action, type|
  case action
  when "merge"
    merge(type)
  when "abandon"
    abandon_changes(type)
  when "cancel"
    cancel_popup(type)
  end
end

When /^I cancel the "([^"]*)" (?:prompt|popup)$/ do |type| #"
  cancel_popup(type)
end

When /^I click the "([^"]*)" button on the "([^"]*)" popup$/ do |button, type|
  click_popup_button(type, button)
end

When /^I abandon creation and view existing item on the "([^"]*)" popup$/ do |type| #"
  abandon_creation_and_view_existing(type)
end

When /^I abandon creation and use existing item on the "([^"]*)" popup$/ do |type| #"
  abandon_creation_and_use_existing(type)
end

When /^on the "([^"]*)" popup, I select the conflicting "([^"]*)"( (?:where|whose) .+)?$/ do |type, _, conditions_str| #"
  select_conflicting(type, parse_conditions(conditions_str))
end

When /^I continue anyway on the "([^"]*)" popup/ do |type| #"
  continue_anyway(type)
end

When /^I abandon my changes on the "([^"]*)" popup$/ do |type| #"
  abandon_changes(type)
end

When /^I merge on the "([^"]*)" popup$/ do |type| #"
  merge(type)
end

Then /^I should( not)? (?:receive|still see|still receive) a "([^"]*)" popup(?:\s)?(.+)?/ do |negative, type, state| #"
  verify_popup_conflict(type, negative, state)
end