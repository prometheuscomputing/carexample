# Ensure that associations/attributes inherited from interfaces are available.
Feature: Interfaces
  In order to fully edit all of an object's properties
  As a user
  I want to be able to manipulate attributes and associations that are due to an interface implementation
  
  Background:
    Given I have reset the database
    And I have logged in

  
  Scenario: Normal interface implementation
    When I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I click on the "Components" header
    And I edit the "Component" where "Name" is "Engine"
    And I click on the "Parts" header
    And I edit the "Part" where "Name" is "Alternator"
    # Test Alternator Part
    Then the value of "Name" should be "Alternator"
    And the value of "Serial" should be "1234"
  
  Scenario: Complex interface implementation
    When I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I click on the "Components" header
    And I edit the "Component" where "Name" is "Engine"
    Then the value of "Name" should be "Engine"
    And the value of "Serial" should be "12"
    And the value of "Warranty Void" should be "False"
    And the value of "Warranty Expiration Date" should be "January 1, 2100"
    And I should see the associated "Warranty"
    When I click on the "Warranty" header
    And I edit the "Warranty"
    Then the value of "Coverage" should be "Full"
    When I click on the "Warrantied" header
    And I edit the "Warrantied" where "Serial" is "12" # Can't see name since all 'warrantieds' may not have one
    Then I should be at a "Component" page
    
  Scenario: To-one association to an interface (Interface implements a choice)
    When I click on the "Repair Shops" header
    And I edit the "Repair Shop"
    And I click on the "Location" header
    And I create a "Location" of type "Country" where "Name" is "Baltostan"
    And I save and leave the page
    And I click on the "Location" header
    Then I should see that there are a total of "1" "Location" in the header
    And I should see the associated "Location" where "Name" is "Baltostan"
    And I click the only "Break Association" button under the "Location" header
    And I save the page
    And I click on the "Location" header
    And I create a "Location" of type "City" where "Name" is "Cullowhee"
    And I save and leave the page
    And I click on the "Location" header
    Then I should see that there are a total of "1" "Location" in the header
    And I should see the associated "Location" where "Name" is "Cullowhee"
  
  # Fails due to to-one composition deletes not working
  @known_failing
  Scenario: To-one composition association to an interface (Interface implements a choice)
    When I click on the "Warranties" header
    And I create a "Warranty" where "Coverage" is "Replacement"
    And I click on the "Replacement" header
    And I create a "Replacement" of type "Component" where "Name" is "Replacement Cylinder"
    And I save and leave the page
    And I click on the "Replacement" header
    Then I should see that there are a total of "1" "Replacement" in the header
    And I should see the associated "Replacement"
    When I click the only "Delete" button under the "Replacement" header
    And I confirm the delete popup
    And I create a "Replacement" of type "Car" where "Vehicle Model" is "Replacement Car"
    And I save and leave the page
    And I click on the "Replacement" header
    Then I should see that there are a total of "1" "Replacement" in the header
    And I should see the associated "Replacement"
    When I edit the "Replacement"
    Then the value of "Vehicle Model" should be "Replacement Car"