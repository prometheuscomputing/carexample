Feature: Custom pages
  In order to add a customized page or pages to the site
  As a site designer
  I want to add a custom page
  
  Background:
    Given I have reset the database
    And I have logged in
  
  Scenario: Check that JS is available
    When I click the topbar "Vehicle Info" link
    And I click the "View All Vehicles" button
    Then I should see "9" vehicle rows
  
  Scenario: Check that CSS is available
    When I click the topbar "Vehicle Info" link
    And I select "Rebel" from the "View Vehicle Details:"
    Then I should see the text "Data on selected vehicle:"
  
  Scenario: Check that images are available
    When I click the topbar "Vehicle Info" link
    Then I should see the image "batmobile.png"