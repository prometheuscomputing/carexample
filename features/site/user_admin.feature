# Test org admin panel functionality
#  * Role Assignment Management
#  * Invitation Code Management


Feature: Administration Panel
  In order to manage users and privileged settings
  As an administrator
  I want to interact with the administrator panels
  
  Background:
    Given I have reset the database
    And   I have logged in as an administrator
  
  Scenario: Adding roles
    When I go to the site administration panel
    And  I add a role named "Editor"
    And  I submit the page
    And  I add a role named "Secretary"
    And  I submit the page
  
  
  Scenario: Adding/Deleting a user
    When I go to the user administration panel
    And  I add a user named "phillip" with the password "phillip1" with the email "phillip@test.com"
    And  I submit the page
    Then there should be a user named "phillip"
    And  "phillip" should be able to login with the password "phillip1"
    When I go to the user administration panel
    And  I mark the user named "phillip" to be deleted
    And  I submit the page
    Then there should not be a user named "phillip"
    And  "phillip" should not be able to login with the password "phillip1"
  
  Scenario: Adding/Removing a role from a user
    When I create some roles
    And  I go to the user administration panel
    Then I create a user and make sure I can add and remove roles to that user
    
  
  Scenario: Adding/Removing an email requirement
    When I go to the user administration panel
    When I add an email requirement with the regular expression ".*@mycompany.net", the description "Require Company Email Addresses", and the failure message "You must use a company address (<your_name>@mycompany.net)"
    And  I submit the page
    Then I should see an email requirement with the regular expression ".*@mycompany.net"
    When I mark the email requirement with the regular expression ".*@mycompany.net" to be deleted
    And  I submit the page
    Then I should not see an email requirement with the regular expression ".*@mycompany.net"
    
  
  Scenario: Using all options in user admin panel simultaneously
    When I go to the site administration panel
    And  I add a role named "Editor"
    And  I add a perspective named "Stakeholder"
    And  I add a password requirement with the regular expression "^.{5,10}$", the description "Check password length", and the failure message "Your password must be between 5 and 10 characters"
    And  I submit the page
    And  I add a role named "Manager"
    And  I add a perspective named "Executive"
    And  I add a password requirement with the regular expression "^[^\?]*$", the description "No question marks", and the failure message "Your password may not contain '?' characters"
    And  I submit the page
    And  I add a role named "Sub-manager"
    And  I add a perspective named "Accounting" based on "Stakeholder"
    And  I add a password requirement with the regular expression "[^\!]*", the description "No exclamation marks", and the failure message "Your password may not contain '!' characters"
    And  I submit the page
    
    # First round of changes
    When I go to the user administration panel
    And  I add a user named "phillip"
    And  I add an email requirement with the regular expression ".*@mycompany.net", the description "Require Company Email Addresses", and the failure message "You must use a company address (<your_name>@mycompany.net)"
    And  I submit the page
    
    # Check first round
    Then there should be a user named "phillip"
    And  I should see an email requirement with the regular expression ".*@mycompany.net"
    
    # Second round of changes
    When I add a user named "george"
    And  I assign "leroy" to the "Editor" role
    And  I assign "bobby" to the "Administrator" role
    And  I add an email requirement with the regular expression "^.{15,30}$", the description "Length Check", and the failure message "Invalid Length"
    And  I submit the page
    
    # Check second round
    Then there should be a user named "george"
    And  I should see that "leroy" has the "Administrator" and "Editor" roles
    And  I should see that "bobby" has the "Administrator" role
    And  I should see an email requirement with the regular expression "^.{15,30}$"
    
    # Final round of changes
    When I mark the user named "phillip" to be deleted
    And  I add a user named "billy"
    And  I assign "leroy" to the "Manager" role
    And  I remove "leroy" from the "Editor" role
    And  I remove "bobby" from the "Administrator" role
    And  I assign "phillip" to the "Administrator", "Editor" and "Manager" roles
    And  I mark the email requirement with the regular expression ".*@mycompany.net" to be deleted
    And  I add an email requirement with the regular expression "^[^\?]*$", the description "No question marks", and the failure message "Your email may not contain '?' characters"
    And  I submit the page
    
    # Check final round
    # Users
    Then there should be a user named "leroy"
    And  there should be a user named "bobby"
    And  there should be a user named "george"
    And  there should be a user named "billy"
    And  there should not be a user named "phillip"
    # Ro le assignments
    And  I should see that "leroy" has the "Administrator" and "Manager" roles
    And  I should see that "leroy" does not have the "Sub-manager" role
    And  I should see that "bobby" does not have the "Administrator", "Manager", or "Sub-manager" roles
    And  I should see that "george" does not have the "Administrator", "Manager", or "Sub-manager" roles
    And  I should see that "billy" does not have the "Administrator", "Manager", or "Sub-manager" roles
    # Em ail requirements
    And  I should see an email requirement with the regular expression "^.{15,30}$"
    And  I should see an email requirement with the regular expression "^[^\?]*$"
    And  I should not see an email requirement with the regular expression ".*@mycompany.net"
  