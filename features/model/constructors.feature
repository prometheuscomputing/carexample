Feature: Constructors
  In order to create a object instance that is pre-populated with data
  As a user
  I want to invoke a constructor
  
  Background:
    Given I have reset the database
    And I have logged in as a user
    And I expand the "People" header
  
  Scenario: Using a constructor to create an instance (saved)
    When I create a "People" of type "Biker"
    Then the value of "Weight" should be "200.5"
    And I should see that there are a total of "1" "Vehicles" in the header
    # Save the page until viewing pending associations in a collection is fixed
    When I save the page
    And I expand the "Vehicles" header
    And I edit the "Vehicle"
    Then I should be at a "Motorcycle" page
    And the value of "Make" should be "Other"
    When I click on the "VIN" header
    Then I should see the associated "VIN"
  
  Scenario: Overwriting a constructor-defined attribute
    When I create a "People" of type "Biker"
    And I set the "Name" to "Barney"
    And I set the "Weight" to "150.0"
    When I save the page
    Then the value of "Name" should be "Barney"
    Then the value of "Weight" should be "150.0"
  
  # Fails because pending associations to as-yet uncreated objects do not have attributes yet
  # and can't be displayed properly
  @known_failing
  Scenario: Viewing constructor associations before saving the page
    When I create a "People" of type "Biker"
    And I expand the "Vehicles" header
    Then I should see a "Vehicle" where "Make" is "Other"