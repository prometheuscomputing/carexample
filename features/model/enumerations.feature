# Test various attribute types here.
Feature: Enumerations
  In order to change an object's data
  As a user
  I want to manipulate the attribute values of enumerated types
  
  Background:
    Given I have reset the database
    And I have logged in as an administrator
      
  # enumerations are now associations so can't be filitered on like a column
  @known_failing
  Scenario: Filtering on enumeration value
    And I expand the "People" header
    And I create a "People"
    When I save the page
    Then the value of "Handedness" should be ""
    When I set the "Name" to "George"
    And I set the "Handedness" to "Right Handed"
    And I save and leave the page
    And I expand the "People" header
    And I open the "People" search
    And I filter to see "People" whose "Handedness" is "Right Handed"
    And I submit the "People" search
    Then I should see a "People" whose "Name" is "George" and "Handedness" is "Right Handed"
    
  Scenario: Rendering a collection where the association class has an enumeration property
    And I expand the "Vehicles" header
    And I create a "Vehicle" of type "Motorcycle" where "Vehicle Model" is "Ninja 500R", and "Cost" is "10000"
    Given I click on the "Owners" header
    When I create an "Owner" of type "Ownership" where "Percent Ownership" is "12.3", and "Payment Frequency" is "none"
    And I click on the "Owner" header
    And I create an "Owner" of type "Person" whose "Name" is "New Owner"
    And I save the page
    When I expand the "Vehicles" header
    Then I should see a "Vehicle" whose "Payment Frequency" is "none" and "Vehicle Model" is "Ninja 500R"
  
  Scenario: Adding & using a new enumeration literal
    Given I go to the site administration panel
    When I click the "Manage Enumerations" link
    And I click on the "Handedness" header
    And I create a "Handedness" where "Value" is "No Hands"
    And I save the page
    And I click the "Enumerations" link
    # Test visibility on Enumerations Management page
    And I click on the "Handedness" header
    Then I should see a "Handedness" where "Value" is "No Hands"
    # Test assignment of new enumeration value
    When I go to the home page
    And I expand the "People" header
    And I create a "People" whose "Name" is "Ralph" and "Handedness" is "No Hands"
    And I save the page
    Then the value of "Handedness" should be "No Hands"
    # Test view of enumeration literal in collection on home page
    When I go to the home page
    And I expand the "People" header
    And I open the "People" search
    And I click on the "People" advanced search link
    And I filter to see "People" whose "Name" is "Ralph"
    And I submit the "People" search
    Then I should see a "People" whose "Name" is "Ralph" and "Handedness" is "No Hands"
  
  Scenario: Changing the value of an existing enumeration literal
    Given I go to the site administration panel
    When I click the "Manage Enumerations" link
    And I click on the "Foreign Vehicle Maker" header
    And I edit the "Foreign Vehicle Maker" where "Value" is "Volvo"
    And I set the "Value" to "Volvo Cars"
    And I save and leave the enumeration page
    # Test value on Enumerations Management page
    When I click on the "Foreign Vehicle Maker" header
    Then I should see a "Foreign Vehicle Maker" where "Value" is "Volvo Cars"
    # Test view of enumeration literal in collection on home page
    When I go to the home page
    And I expand the "Vehicles" header
    Then I should see a "Vehicle" where "Vehicle Model" is "S40" and "Make" is "Volvo Cars"
    Then I should see a "Vehicle" where "Vehicle Model" is "S80" and "Make" is "Volvo Cars"
    