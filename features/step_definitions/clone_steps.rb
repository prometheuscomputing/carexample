# --------------------------------------- Clone steps
Then /^I should be prompted to clone this "([^"]*)"(?: and its (.+))?$/ do |type, composition_associations_str| #"
  verify_clone_prompt(type, parse_quoted_string_list(composition_associations_str))
end
When /^I confirm the clone/ do
  confirm_clone
end
When /^I cancel the clone/ do
  cancel_clone
end
Then /^I should see both the original "([^"]*)" and the clone in the breadcrumbs/ do |type| #"
  # Verify that last two breadcrumbs are of given type
  bc = breadcrumbs
  bc[-2].text.strip.should == type
  bc[-1].text.strip.should == type
end
When /^I choose the composer association "([^"]*)"/ do |composer| #"
  select_composer_association(composer)
end
When /^I choose to clone the "([^"]*)" into the same "([^"]*)"/ do |_, composer|
  # Should be able to see the "same" option with no change to selected composer association
  clone_into_same(composer)
end
When /^I choose to clone the "([^"]*)" into a new "([^"]*)"/ do |_, composer|
  clone_into_new(composer)
end
When /^I choose to clone the "([^"]*)" into the "([^"]*)"( (?:where|whose) .+)?/ do |_, _, conditions_string|
  clone_into_existing(parse_conditions(conditions_string))
end
When /^I open the composer search/ do
  open_composer_filter
end
When /^I submit the composer search/ do
  submit_composer_search
end
When /^I filter to see composers ((?:where|whose) .+)/ do |conditions_string|
  enter_composer_filter(parse_conditions(conditions_string))
end
When /^I view "\d+" composers per page/ do |items_per_page|
  set_composers_per_page(items_per_page)
end
When /^I select the composer( (?:where|whose) .+)?/ do |conditions_string|
  select_composer_row(parse_conditions(conditions_string))
end
Then /^the selected composer association should be "([^"]*)"/ do |composer| #"
  verify_composer_association(composer)
end
Then /^the same composer option should be selected/ do #"
  verify_same_composer_selected
end
Then /^the new composer option should be selected(?: with the composer type "([^"]*)")?/ do |composer_type| #"
  verify_new_composer_selected(composer_type)
end
# --------------------------------------- End Clone steps