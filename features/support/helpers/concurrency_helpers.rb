def find_row_by_name(popup, name)
  popup.find("tr[for='" + name + "']")
end

def find_options_in_popup_row(type, duplex_label)
  label, sub_attribute = split_duplex_label(duplex_label)
  data = {}
  popup = find_popup(type)
  row = find_row_by_name(popup, label)
  if sub_attribute
    data[:my_changes] = row.find("td.my_changes .widget_data[sub_attribute=#{sub_attribute}]").value.titleize
    data[:changes_found] = row.find("td.changes_found .widget_data[sub_attribute=#{sub_attribute}]").value.titleize
  else
    data[:my_changes] = row.find("td.my_changes .widget_data").value.titleize
    data[:changes_found] = row.find("td.changes_found .widget_data").value.titleize
  end
  data
end

def split_duplex_label duplex_label
  duplex_label.split(/:/)
end

def find_text_in_popup_row(type, label, klass)
  row = find_row_by_name(find_popup(type), label)
  row.find("td.#{klass}").text.strip
end

def find_selectors_in_popup_row(type, duplex_label)
  label, _ = split_duplex_label(duplex_label)
  my_changes = {}
  changes_found = {}
  row = find_row_by_name(find_popup(type), label)
  my_changes[:element] = row.find("td.changes_selector[for='my_changes'] input.concurrency_selector")
  my_changes[:disabled] = row.find("td.changes_selector[for='my_changes'] input.concurrency_selector").disabled?
  changes_found[:element] = row.find("td.changes_selector[for='changes_found'] input.concurrency_selector")
  changes_found[:disabled] = row.find("td.changes_selector[for='changes_found'] input.concurrency_selector").disabled?
  {:my_changes => my_changes, :changes_found => changes_found}
end

def select_in_popup_row(type, label, which)
  row = find_row_by_name(find_popup(type), label)
  row.find("td.changes_selector[for='#{which}'] input.concurrency_selector").click
end