# This file requires the other _helpers.rb files in the same folder.
# Cucumber loads these automatically, so they aren't explicitly required here.
require 'sequel'
require 'sequel/extensions/inflector'

# Add has_image? matcher
class Capybara::Session
  def has_image?(src)
    has_xpath?("//img[contains(@src,\"#{src}\")]")
  end
  
  def reset_tabs
    case Capybara.current_driver
    when :chrome, :headless_chrome
      switch_to_window(Capybara::Window.new(self, driver.window_handles[0]))
    else
      puts "reset_tabs is not yet implemented for other drivers."
    end
  end

  def open_new_tab
    case Capybara.current_driver
    when :poltergeist, :chrome, :headless_chrome
      # Only get one shot at opening new tabs using poltergeist, so create 4
      unless driver.window_handles.count > 1
        open_new_window
        open_new_window
        open_new_window
        open_new_window
      end
      current_tab = driver.window_handles.index(driver.current_window_handle)
      next_tab = current_tab + 1
      switch_to_window(Capybara::Window.new(self, driver.window_handles[next_tab]))
    else
      open_tab_script = 'window.open("/", "' + String.random + '", "height=800,width=1000");'
      execute_script(open_tab_script)
      driver.browser.window_focus(driver.window_handles.last)
    end
  end

  def switch_to_tab(position)
    index = position.to_i - 1
    if Capybara.current_driver == :poltergeist
      switch_to_window(Capybara::Window.new(self, index.to_s))
    else
      switch_to_window(Capybara::Window.new(self, driver.window_handles[index]))
    end
  end
end

# Generic Capybara helper methods
class Capybara::Node::Element
  def has_image?(src)
    has_xpath?("//img[contains(@src,\"#{src}\")]")
  end

  def classes
    self[:class].split(' ')
  end
  
  def has_class?(cls)
    classes.include?(cls)
  end
 
  # Updated Capybara to 1.1, might no longer be necessary 
  # def hover
  #   case Capybara.current_driver
  #   when :chrome, :firefox, :headless_chrome
  #     @session.driver.browser.action.move_to(self.native).perform
  #   when :webkit
  #     trigger(:mouseover)
  #   when :poltergeist
  #     native.hover
  #   else
  #     puts "Attempting to hover for unknown driver: #{Capybara.current_driver}"
  #     # If trigger fails, there is also a JQuery solution like this:
  #     # page.execute_script('$("#element").trigger("mouseenter")')
  #     trigger(:mouseover)
  #   end
  # end
  
  def trigger_event(event)
    case Capybara.current_driver
    when :chrome, :firefox, :headless_chrome
      element_id = self[:id]
      element_name = self[:name]
      if element_id && !element_id.empty?
        Capybara.current_session.execute_script("$('#{tag_name}##{element_id}').first().trigger('#{event}')")
      elsif element_name && !element_name.empty?
        puts "Warning: attempting to use name #{element_name} to uniquely identify element #{self.inspect}"
        Capybara.current_session.execute_script("$('#{tag_name}[name=#{element_name}]').first().trigger('#{event}')")
      else
        raise "No id for element #{self.inspect} when triggering event #{event}"
      end
    when :webkit, :poltergeist
      trigger(event)
    else
      puts "Attempting to trigger #{event} for unknown driver: #{Capybara.current_driver}"
      trigger(event)
    end
  end
  
  # Define a function that converts an Xpath selector to a CSS selector
  # NOTE: this method is not specific to this element and should be moved -SD
  def define_xpath_selector_function
    xpath_selector_function = <<-EOS
    function _x(STR_XPATH) {
        var xresult = document.evaluate(STR_XPATH, document, null, XPathResult.ANY_TYPE, null);
        var xnodes = [];
        var xres;
        while (xres = xresult.iterateNext()) {
            xnodes.push(xres);
        }
        return xnodes;
    }
    EOS
    page.execute_script(xpath_selector_function)
  end
  
  # Driver dependent keypress implementation
  def press_key(key)
    case Capybara.current_driver
    when :webkit
      case key
      when :enter
        key_code = 13
        # char_code = nil
      else
        raise "No implementation for this key yet!"
      end
      # NOTE: this code is supposed to work for capybara-webkit, but results in a Capybara::Webkit::InvalidResponseError
      # self.base.invoke('keypress', false, false, false, false, key_code, char_code);
      xpath_selector = self.path
      define_xpath_selector_function
      script = "var e = jQuery.Event('keypress');"
      script += "e.which = #{key_code};"
      script += "$(_x(\"#{xpath_selector}\")).trigger(e);"
      page.execute_script(script)
    else
      self.native.send_keys key
    end
  end
end

class String
  def to_underscored_attribute_name
    downcase.gsub(/\W+/, '_')
  end
  def to_underscored_table_name
    to_underscored_attribute_name
  end
  def to_underscored_package_name
    self.gsub(/([a-z])([^a-z])/, '\1_\2').downcase
  end
  def self.random_string(length = 10)
    (0...length).map{ ('a'..'z').to_a[rand(26)] }.join
  end
end

# To help with failed comparisons, override Time's inspect method to show milliseconds
class Time
  def inspect
    self.strftime "%Y-%m-%d %H:%M:%S:%9L %z"
  end
end

class Object
  def should_unless(negate = false, *args)
    negate ? self.should_not(*args) : self.should(*args)
  end
  def should_not_unless(negate = false, *args)
    should_unless(!negate, *args)
  end
end

class Hash
  # Check if this hash has the same values as another hash.
  # Only keys in the target hash will be checked.
  # In the case where the source hash has a string and the target hash has an array,
  #   will return true if any element in the array matches the source string
  def has_values?(target, case_sensitive = false)
    target.keys.each {|key|
      this_value = self[key]
      return false unless this_value
      target_value = target[key]
      if this_value.is_a?(String) && target_value.is_a?(Array)
        if case_sensitive
          return false unless target_value.include?(this_value)
        else
          return false unless target_value.any?{|tv| tv.casecmp(this_value).zero?}
        end
      else
        if case_sensitive
          return false unless this_value == target_value
        else
          return false unless this_value.casecmp(target_value).zero?
        end
      end
    }
    return true
  end
  # Check if this hash contains the values from the target hash
  # Only keys in the target hash will be checked.
  # In the case where the source hash has a string and the target hash has an array,
  #   will return true if any element in the array matches the source string
  def contains_values?(target, case_sensitive = false)
    target.keys.each {|key|
      this_value = self[key]
      return false unless this_value
      target_value = target[key]
      if this_value.is_a?(String) && target_value.is_a?(Array)
        if case_sensitive
          return false unless target_value.any?{|tv| tv.include?(this_value)}
        else
          return false unless target_value.any?{|tv| tv.downcase.include?(this_value.downcase)}
        end
      else
        if case_sensitive
          return false unless this_value.include?(target_value)
        else
          return false unless this_value.downcase.include?(target_value.downcase)
        end
      end
    }
    return true
  end
end

# This helper rescues any NodeNotAttachedError's that are raised, and re-attempts after a short sleep.
def wait_until_attached &block
  max_attempts = 5
  sleep_time = 0.2
  count = 0
  success = false
  result = nil
  # Continue until success, or reached max attemps, or until result is found (if isn't the first iteration)
  while !success && (count < max_attempts) && (count == 0 || result == nil)
    count += 1
    begin
      if Capybara.current_driver == :webkit
        begin
          result = yield
          success = true
        rescue Capybara::Webkit::NodeNotAttachedError
          puts "Rescuing NodeNotAttachedError (#{count})."
          sleep sleep_time
        end
      else
        result = yield
        success = true
      end
    rescue Capybara::ElementNotFound # This can occur when the page has not yet reloaded
      puts "Rescuing ElementNotFound (#{count})."
      sleep sleep_time
    end
  end
  # Could raise error, but will fail anyway when can't find object to click on, etc...
  puts "Warning: Could not find object within time limit" if result == nil && count == max_attempts
  result
end

def wait_until(attempts = 5, delta = 0.2, &block)
  count = 0
  success = false
  while !success && (count < attempts)
    count += 1
    success = yield
    sleep delta unless success
  end
  success
end

# Custom RSpec matchers for hash value checking
RSpec::Matchers.define :have_values do |expected|
  match do |actual|
    raise "This matcher only supports hashes at the moment" unless expected.is_a?(Hash) && actual.is_a?(Hash)
    actual.has_values?(expected)
  end
  failure_message_for_should do |actual|
    "expected hash to have values:\n" +
    "  Expected: #{expected.inspect}\n" +
    "  Got:      #{actual.inspect}"
  end
  failure_message_for_should_not do |actual|
    "expected hash not to have values:\n" +
    "  Not Expected: #{expected.inspect}\n" +
    "  Got:          #{actual.inspect}"
  end
end
RSpec::Matchers.define :contain_values do |expected|
  match do |actual|
    raise "This matcher only supports hashes at the moment" unless expected.is_a?(Hash) && actual.is_a?(Hash)
    actual.contains_values?(expected)
  end
  failure_message_for_should do |actual|
    "expected hash to contain values:\n" +
    "  Expected: #{expected.inspect}\n" +
    "  Got:      #{actual.inspect}"
  end
  failure_message_for_should_not do |actual|
    "expected hash not to contain values:\n" +
    "  Not Expected: #{expected.inspect}\n" +
    "  Got:          #{actual.inspect}"
  end
end
RSpec::Matchers.define :have_class do |expected|
  match do |actual|
    raise "This matcher only supports Capybara elements" unless actual.is_a?(Capybara::Node::Element)
    actual.classes.include?(expected)
  end
  failure_message_for_should do |actual|
    "expected Capybara element to have class:\n" +
    "  Expected: #{expected}\n" +
    "  Got:      #{actual.classes.inspect}"
  end
  failure_message_for_should_not do |actual|
    "expected Capybara element not to have class:\n" +
    "  Not Expected: #{expected}\n" +
    "  Got:          #{actual.classes.inspect}"
  end
end

# ----------------- Various UI Helpers -----------------
def verify_homepage(app_title = nil)
  # Verify .app_title
  expect(find('.app_title').text.strip).to eq(app_title) if app_title
  expect(URI.parse(current_url).path).to eq('/')
  # BAD.  This is mixing functionality.  Should be verifying the homepage by testing breadcrumb functionality!!  That should be a separate test! FIXME test this somewhere else, like in the breadcrumbs spec.
  # # Verify last breadcrumb (Had issue with finding additional text outside of class element)
  # breadcrumbs = find('#breadcrumbs')
  # last_breadcrumb = breadcrumbs.all('.breadcrumb').last
  # expect(last_breadcrumb.text.strip).to eq('Home')
end

def verify_form_error(error)
  find('.invalid-feedback', :text => error, :normalize_ws => true)
end

def verify_error(message, parent_div_selector = nil)
  selector = parent_div_selector ? "#{parent_div_selector} .message_error" : ".message_error"
  find(selector, :text => message, :normalize_ws => true)
end

def verify_warning(message, parent_div_selector = nil)
  selector = parent_div_selector ? "#{parent_div_selector} .message_warning" : ".message_warning"
  find(selector, :text => message, :normalize_ws => true)
end

# This is used in cases where string matching is very difficult due to escape characters.
def verify_error_like(message, parent_div_selector = nil)
  selector = parent_div_selector ? "#{parent_div_selector} .message_error" : ".message_error"
  verify_like(message, selector)
end

# This is used in cases where string matching is very difficult due to escape characters.
def verify_warning_like(message, parent_div_selector = nil)
  selector = parent_div_selector ? "#{parent_div_selector} .message_warning" : ".message_warning"
  verify_like(message, selector)
end

def verify_like(expected_message, selector)
  found_message = find(selector, :normalize_ws => true).text
  expect(found_message =~ Regexp.new(Regexp.escape(expected_message))).to be_truthy
end

def verify_success_message(message)
  find('.message_success', :text => message, :normalize_ws => true)
end
def verify_no_success_message(message)
  # NOTE: No auto-waiting here, so this might intermittently pass when it shouldn't
  expect(first('.message_success', :text => message, :normalize_ws => true, :minimum => 0)).to be_nil
end
def verify_no_messages
  # NOTE: No auto-waiting here, so this might intermittently pass when it shouldn't
  expect(first('.message_success', :minimum => 0)).to be_nil
  expect(first('.message_error', :minimum => 0)).to be_nil
end
# ----------------- Step Parsing Helpers -----------------
# Parse an english list of conditions into 4 types of hash matchers (equals, contains, and their negatives)
def parse_conditions(conditions_string)
  equals_conditions = {}
  contains_conditions = {}
  not_equals_conditions = {}
  not_contains_conditions = {}
  if conditions_string
    conditions_string.scan(/"([^"]*)" (is|includes|contains|is not|does not contain)( one of)? ("([^"]*)"|\[[^\]]*\])/).each do |attribute, match_type, _, multiple_values, single_value|
      value = single_value
      value ||= parse_quoted_string_list(multiple_values)
      
      case match_type
      when 'is'
        equals_conditions[attribute] = value
      when 'includes', 'contains'
        contains_conditions[attribute] = value
      when 'is not'
        not_equals_conditions[attribute] = value
      when 'does not contain'
        not_contains_conditions[attribute] = value
      end
    end
  end
  [equals_conditions, contains_conditions, not_equals_conditions, not_contains_conditions]
end

# Parse an english description of attributes into a hash
def parse_attributes(attributes_string)
  attributes = {}
  if attributes_string
    attributes_string.scan(/(?:"([^"]+)"|!([^!]+)!) (?:is|to) (?:"([^"]*)"|!([^!]+)!)/).each do |quoted_attribute, exclamation_attribute, quoted_value, exclamation_value|
      attribute = quoted_attribute || exclamation_attribute
      value = quoted_value || exclamation_value
      attributes[attribute] = value
    end
  end
  attributes
end

# Parse an english description of objects into an array of hashes
def parse_objects_description(description_string)
  descriptions = []
  if description_string
    description_string.scan(/"?(\d+|an?)"? "([^"]*)" ?(where|whose|named "[^"]*")?(( [^"]*?"[^"]*" (is|includes|contains|is not|does not contain)( one of)? ("([^"]*)"|\[[^\]]*\]))*)/).each do |number, type, condition_type, conditions_string|    
      num_items = number =~ /a/ ? 1 : number.to_i
      type = type.singularize if num_items > 1
      case condition_type
      when 'where', 'whose'
        conditions = parse_conditions(conditions_string) if num_items == 1
      when /^named "([^"]*)"/
        conditions = [{"Name" => $1}, {}, {}, {}]
      end
      descriptions << {:count => num_items, :type => type, :conditions => conditions}
    end
  end
  descriptions
end

# Parse an English description of scope
# Examples:
# - the last "Drives" document association item
# - the association popup
def parse_scope(scope_string)
  return nil unless scope_string
  scope = nil
  scope_types = [
    'document association',
    'document association item',
    '(\w+) popup'
  ]
  scope_string.scan(/^(a|an|the)( first| last)?(?: "([^"]*)")? (#{scope_types.join('|')})(?: in (.+))?$/) do |_article, order, header, type, _popup, superscope|
    possible_scopes = []
    case type
    when 'document association item'
      w = get_widget(header, parse_scope(superscope))
      # List style document association
      possible_scopes = w.all(:xpath, './div/ul/li[contains(concat(" ",normalize-space(@class)," ")," document_association_item ")]', :visible => true)
      # Table style document association
      possible_scopes = w.all(:xpath, './div/table/tbody/tr[contains(concat(" ",normalize-space(@class)," ")," document_association_item ")]', :visible => true) unless possible_scopes.any?
    when / popup$/
      raise "Cannot parse superscope for a popup." if superscope
      #TODO Sorry I'm cheating if this is ever used for another popup besides the add association popup fixme since explicit names for popup ids apparently screws every test -TB
      #it's really not much different than the already stupid assumption that the name will always be _popup.  These complicated regex nested functions are gross
      # popup_id = "##{popup}_popup"
      popup_id = "#addAssociationModal"
      possible_scopes = [find(popup_id)]
    else
      raise "Can't process scope with type: #{type}"
    end
    
    case order && order.strip
    when 'first'
      scope = possible_scopes.first
    when 'last'
      scope = possible_scopes.last
    else
      raise "Ambiguous scope match. Found #{possible_scopes.inspect}." unless possible_scopes.length == 1
      scope = possible_scopes.first
    end
  end
  raise "Couldn't find scope: #{scope_string}" unless scope
  scope
end

def parse_quoted_string_list(input)
  return [] unless input
  # nil added in order to help handle widgets for which the label does not appear on the page (e.g. html widget)
  input.scan(/"([^"]*)"|nil/).collect{|string, _| string}
end

def parse_params(params_string)
  params = {}
  keys = []
  values = []
  parse_quoted_string_list(params_string).each_with_index do |item, i|
    if i.even?
      keys << item
    else
      values << item
    end
  end
  keys.each_with_index do |key, i|
    params[key] = values[i]
  end
  params
end

# Transform a getter into a label using the same process that magicdraw_extensions does.
# This method is used only when verifying items directly from the DB, and should be used sparingly.
# NOTE: this was copied from magicdraw_extensions' String#to_title method
def getter_to_label(getter)
  # Parse camel casing to spaces
  getter.to_s.gsub(/([a-z])([A-Z])/, '\1 \2').gsub(/([A-Z]+)([A-Z][a-z])/, '\1 \2').
  # Remove leading and trailing spaces and underscores
  gsub(/^[_ ]*/, '').gsub(/[_ ]*$/, '').
  # Parse underscoring to spaces
  gsub(/([A-Za-z])_([A-Za-z])/, '\1 \2').
  # Capitalize each word in the parsed string
  gsub(/\b([a-z])/){|x| x[-1..-1].upcase}
end
