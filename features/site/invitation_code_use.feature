Feature: Invitation Code Use
  In order to use the system
  As a new user
  I want register a new account using an invitation code
  
  Background:
    Given I have reset the database
    
  Scenario: I can register with an invitation code.
    When I go to the home page  
    When I click Register
    And I enter the username "Garry"
    And I enter the password "Garry2"
    And I re-enter the password "Garry2"
    And I enter the invitation code "sprockets"
    And I submit the registration
    Then I should be at the home page