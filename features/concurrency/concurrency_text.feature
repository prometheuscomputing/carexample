# This file is specifically for testing text widget concurrency in isolation of other features.

# TODO: Make these tests more comprehensive.  The one here (as of Feb. 26 2016) was adapted from a test in concurrency.feature.

Feature: Saving text with concurrent changes made by another user
  In order to handle issues with editing the same object at the same time
  As a user
  I want to edit the same object with multiple tabs without blindly overiding data
  
  Background:
    Given I have reset the database
    And   I have logged in

  Scenario: Saving an object with concurrently changing text
    # 1st Tab here - creating File
    When I expand the "People" header
    And  I create a "People" of type "Person" where "Name" is "Hank"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning

    # 2rd Tab here - Deleting File
    When I open a new tab
    And  I expand the "People" header
    And  I edit the "People" where "Name" is "Hank"
    And  I set the "Description" to "Totally Crazy Description Here"
    And  I save the page
    And  I should not receive a "concurrency warning" popup warning

    # Back to 1st Tab - Save with no changes
    When I switch to the 1st tab
    When I save the page
    Then I should receive a "concurrency warning" popup warning

    # Check popup text contents
    And  the "concurrency warning" popup should have my change "" and the change found "Totally Crazy Description Here" for the "Description:Content"
    And  the "concurrency warning" popup should have my changes selected for the "Description:Content"

    When I select the changes found for the "Description" on the "concurrency warning" popup
    When I merge on the "concurrency warning" popup
    And  I should not receive a "concurrency warning" popup warning

    # Verify Page contents
    And  the value of "Description" should be "Totally Crazy Description Here"