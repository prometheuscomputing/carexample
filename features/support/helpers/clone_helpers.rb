# --------------------------------------- Clone helpers
def verify_clone_prompt(type, composition_associations = [])
  clone_popup_text = find('#cloneDomainObjModal .cloning-div').text
  expect(clone_popup_text).to match(/^Clone this #{type}/)
  composition_associations.each do |composition_association|
    expect(clone_popup_text).to include(composition_association)
  end
end
def confirm_clone
  click_popup_button('cloneDomainObj', 'Clone')
end
def cancel_clone
  cancel_popup('cloneDomainObj')
end
def select_composer_association(composer)
  clone_popup = find('#cloneDomainObjModal')
  clone_popup.select(composer, :from => 'composer_getter')
end
def verify_composer_association(composer)
  expect(find('#cloneDomainObjModal')).to have_select('composer_getter', :selected => composer)
end
def clone_into_same(composer)
  find('#cloneDomainObjModal').choose("The same #{composer}")
end
def verify_same_composer_selected
  expect(find('#cloneDomainObjModal').find('input#same_composer_option').checked?).to(be true)
end
def clone_into_new(composer_type)
  clone_popup = find('#cloneDomainObjModal')
  clone_popup.first('.composer_association:not(.hidden) .new_composer_option', :visible => true).set(true)
  if type_select = clone_popup.first('.composer_association:not(.hidden) .new_composer_type', :visible => true)
    type_select.find(:option, composer_type).select_option
  end
end
# Verifies both that the 'A new ' radio is selected and if applicable that the
#   selected composer type is the passed type
def verify_new_composer_selected(composer_type = nil)
  clone_popup = find('#cloneDomainObjModal')
  expect(clone_popup.find('.composer_association:not(.hidden) .new_composer_option', :visible => true).checked?).to(be true)
  if composer_type
    expect(clone_popup).to have_select('new_composer_type', :selected => composer_type)
  end
end
def clone_into_existing(conditions = [])
  select_composer_row(conditions)
end
def composer_collection_widget
  find('#cloneDomainObjModal').find('.collection', :visible => true)
end
def open_composer_filter
  open_filter_for(composer_collection_widget)
end
def submit_composer_search
  submit_search(composer_collection_widget)
end
def set_composers_per_page(items_per_page)
  set_items_per_page(composer_collection_widget, items_per_page)
end
def enter_composer_filter(conditions)
  enter_filter(composer_collection_widget, conditions)
end
def select_composer_row(conditions)
  select_rows(composer_collection_widget, conditions)
end
# --------------------------------------- End Clone helpers