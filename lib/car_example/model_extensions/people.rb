module People
  class Person
    should_be_unique :name

    constructor(:biker) do |person|
      ChangeTracker.start if !ChangeTracker.started?
      person.family_size = 1
      person.weight = 200.5
      person.dependent = false
      person.save
      m = Automotive::Motorcycle.create(:make => 'Other')
      m.vin = Automotive::VIN.create(:vin => '')
      person.add_vehicle(m)
    end
    
    # used to test Gui::Home#download_search_results
    def to_collection_download
      [name,description_content,weight]
    end
    
    def testing_code
    end

    # derived_attribute :oh_my, String
    # def oh_my
    #   "Oh My!"
    # end
    
    derived_attribute :test_message, String
    def test_message
      "test_message"
    end
    
    derived_attribute :download_info, File
    def download_info
      filename = "#{name}'s info.txt"
      data = YAML.dump(values)
      {:content => data, :filename => filename, :mime_type => 'text/csv'}
    end

    # Define co_owners derived association ------------------------------------
    derived_association :co_owners, 'People::Person', :type => :one_to_many
    def co_owners(*args)
      # Get all owned vehicles' owners
      vehicles_owners = vehicles.collect{|v| v.owners}.flatten
      # Remove current person from list
      vehicles_owners.delete(self)
      # Return unique co-owners
      vehicles_owners.uniq
    end
    def co_owners_count(*args)
      co_owners(*args).count
    end
    def co_owners=(*args)
      # For now, this isn't needed
    end
    def add_co_owner(co)
      # Add the new co_owner to the first vehicle
      first_vehicle = vehicles({}, 1).first
      return unless first_vehicle
      first_vehicle.add_owner(co)
    end
    alias co_owners_add add_co_owner
    def remove_co_owner(co)
      vehicles.each do |v|
        v.owners({:name => co.name}).each do |o|
          v.remove_owner(o) if o.name == co.name
        end
      end
    end
    alias co_owners_remove remove_co_owner
    def co_owners_type; [People::Person, Automotive::Mechanic, People::Clown::Clown]; end
    def co_owners_unassociated(filter = {}, limit = nil, offset = 0, order = {})
      
      type_filters = []
      if filter.is_a?(Array)
        filter.each {|f| (type_filters << f.delete('type')) if f.is_a?(Hash) }
        filter.delete({})
      else
        (type_filters << filter.delete('type')) if filter.is_a?(Hash)
      end
      
      all_people = (People::Person.filter(filter).all +
       Automotive::Mechanic.filter(filter).all +
       People::Clown::Clown.filter(filter).all -
       co_owners(filter) - [self]
      )
      limit ? all_people[offset..offset+limit-1] : all_people
    end
    def co_owners_unassociated_count(*args)
      co_owners_unassociated(*args).count
    end
    # End co_owners derived association ---------------------------------------
    
    # Define motorcycles alias association --------------------------------------
    alias_association :motorcycles, 'Automotive::Motorcycle', :type => :one_to_many, :alias_of => :vehicles
    
    # Define expensive_vehicles alias association
    alias_association :expensive_vehicles, 'Automotive::Vehicle', :type => :one_to_many,
      :alias_of => :vehicles, :filter => {:cost => 25000..99999999}
    
    # Define volvos alias association
    alias_association :volvos, 'Automotive::Vehicle', :type => :one_to_many,
      :alias_of => :vehicles, :filter => {:cost => 100..99999999, :make => 'Volvo'}
      
    # Define electric_vehicles alias association (should return both ElectricVehicle and HybridVehicle types)
    alias_association :electric_vehicles, 'Automotive::ElectricVehicle', :type => :one_to_many, :alias_of => :vehicles
    
    # Demonstrates how to filter using an inequality operator (hash syntax does not work)
    alias_association :drives_expensive, 'Automotive::Vehicle', :type => :many_to_many, :alias_of => :drives, :filter => [Sequel::SQL::BooleanExpression.new(:>=, :cost, 10000)]
    
    derived_attribute :co_owner_names, String
    def co_owner_names
      co_owners.collect{|co| co.name}.join(', ')
    end
    def co_owner_names=(value)
      names = value.split(/\s*,\s*/)
      existing_co_owner_names = co_owners.collect{|co| co.name}
      new_co_owner_names = names - existing_co_owner_names
      new_co_owners = new_co_owner_names.collect{|name|
        People::Person[:name => name] ||
          Automotive::Mechanic[:name => name] ||
          People::Clown::Clown[:name => name]
      }.compact
      new_co_owners.each {|co_owner|
        add_co_owner(co_owner)
      }
    end

    # Test of 'generated' images. This is faked here by just reading the contents of an existing image.
    derived_attribute :work_performance, Gui_Builder_Profile::File
    def work_performance
      raise "Can't find TEST_FILES dir (undefined)" unless defined?(TEST_FILES_DIR)
      path = File.join(TEST_FILES_DIR, 'work_performance.gif')
      Gui_Builder_Profile::File.from_path(path, :temporary => true)
    end
    def work_performance=(value); end

    derived_attribute :name_length, Integer
    def name_length
      name.length
    end
    def name_length=(value); end
    
    def is_huge?
      weight && weight > 400
    end
  end
end
