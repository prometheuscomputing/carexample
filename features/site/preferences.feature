Feature: Preferences
  In order to edit user-specific settings
  As a user
  I want to manage my preferences
  
  Background:
    Given I have reset the database
    And I have logged in as an administrator
    And I go to the site administration panel
    And I add the roles "Editor", "Manager", and "Sub-manager"
    And I add the perspectives "Stakeholder", "Accounting", and "Executive"
    And I sign out
    And I have logged in as a user
    And I go to the preferences panel
  
  Scenario: Adding Removing myself from perspectives
    When I add myself to the "Accounting" and "Executive" perspectives
    And I submit the page
    Then I should be a member of the "Accounting" and "Executive" perspectives
    When I remove myself from the "Accounting" perspectives
    And I submit the page
    Then I should be a member of the "Executive" perspectives
    And I should not be a member of the "Stakeholder", or "Accounting" perspectives
  
  Scenario: Changing password
    When I enter the old password "bobby1"
    And I enter the new password "foobar"
    And I re-enter the new password "foobar"
    And I submit the page
    Then I should receive the message "Successfully changed password"
    When I sign out
    Then "bobby" should be able to login with the password "foobar"
    
  Scenario: Changing email
    When I enter the email "bobbytest@test.com"
    And I submit the page
    Then I should receive the message "Successfully changed email. Please verify this email by following the instructions sent to bobbytest@test.com."
    And I should receive the message "This account hasn't verified its email account yet. Do this by visiting the Confirm Email sent to your account when it was created or by reentering a new email address below."
    
    #Verify that the email was sent
    When I go to the email application
    And I click the first email
    Then I should see a from field containing the address "<no_reply@prometheuscomputing.com>"
    And I should see a to field containing the address "<bobbytest@test.com>"
    And I should see a subject field containing the subject "Verify Email Account"
    
    #Verify the email using the link
    When I click the link in the email
    Then I should see the text "The email account bobbytest@test.com has been verified."
    
    #Check preferences again to make sure that the email has been verified
    When I go to the preferences panel
    Then I should not receive the message "This account hasn't verified its email account yet. Do this by visiting the Confirm Email sent to your account when it was created or by reentering a new email address below."
    
    
  Scenario: Changing email (unsuccessful)
    When I enter the email "admin@mycompany.net"
    And I submit the page
    Then I should receive the error "This E-mail address is already in use. Please use a different E-mail address."
  
  Scenario: Changing password (unsuccessful)
    # passwords don't match
    When I enter the old password "bobby1"
    And I enter the new password "foobar1"
    And I re-enter the new password "foobar"
    And I submit the page
    # Then I should receive the error "Entered passwords did not match"
    Then I should see the form error "The entered new password and re-enter new password fields must match"
    # Incorrect old password
    When I enter the old password "bobbykins"
    And I enter the new password "foobar"
    And I re-enter the new password "foobar"
    And I submit the page
    Then I should receive the error "Incorrect old password entered"
    # Invalid chars in password
    When I enter the old password "bobby1"
    And I enter the new password "fooÅ<=>"
    And I re-enter the new password "fooÅ<=>"
    And I submit the page
    Then I should receive an error like "Passwords can only contain letters, numbers, space (no tabs) and the following symbols:"
    # Short password
    When I enter the old password "bobby1"
    And I enter the new password "1234"
    And I re-enter the new password "1234"
    And I submit the page
    # NOTE: this behavior is not possible with an actual browser. The actual behavior is described below
    # Then I should receive the error "Password must be at least 5 characters"
    Then I should see the form error "The new password must have a length of 5 or more characters"
    # Test custom password requirement (No asterisks)
    When I enter the old password "bobby1"
    And I enter the new password "foobar*"
    And I re-enter the new password "foobar*"
    And I submit the page
    Then I should receive the error "Your password may not have an asterisk!"
    # All problems at once
    When I enter the old password "bobbykins"
    And I enter the new password "12*<"
    And I re-enter the new password "foo<=>"
    And I submit the page
    Then I should see the form error "The entered new password and re-enter new password fields must match"
    And I re-enter the new password "12*<"
    And I submit the page
    Then I should see the form error "The new password must have a length of 5 or more characters"
    When I enter the new password "12*<5"
    And I re-enter the new password "12*<5"
    And I submit the page
    Then I should receive the error "Incorrect old password entered"
    And I should receive the error "Your password may not have an asterisk!"
    # Good password
    When I enter the old password "bobby1"
    And I enter the new password "foobar"
    And I re-enter the new password "foobar"
    And I submit the page
    Then I should receive the message "Successfully changed password"
    When I sign out
    Then "bobby" should be able to login with the password "foobar"
  
  Scenario: Manipulating perspectives and password simultaneously
    When I enter the old password "bobby1"
    And I enter the new password "foobar"
    And I re-enter the new password "foobar"
    And I add myself to the "Accounting" and "Executive" perspectives
    And I submit the page
    And I should be a member of the "Accounting" and "Executive" perspectives
    And I should receive the message "Successfully changed password"
    When I enter the old password "foobar"
    And I enter the new password "foobar2"
    And I re-enter the new password "foobar2"
    And I add myself to the "Stakeholder" perspective
    And I remove myself from the "Executive" perspective
    And I submit the page
    And I should be a member of the "Accounting" and "Stakeholder" perspectives
    And I should not be a member of the "Executive" perspective
    And I should receive the message "Successfully changed password"
    When I sign out
    Then "bobby" should be able to login with the password "foobar2"
  

    