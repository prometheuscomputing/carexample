def confirm_tree_view_node(node_text)
  node = find_tree_view_node(node_text)
  expect(node.text).to eq(node_text)
end

def find_tree_view_node(node_text)
  find_link(node_text)
end

def get_currently_selected_node
  page.find('.jstree-clicked')
end

def confirm_tree_view_button_missing
  page.should have_no_selector('.split_container_into_panes')
end

def click_tree_view_node(node_text)
  find_tree_view_node(node_text).click
end

def confirm_selected_node(node_text)
  expect(get_currently_selected_node.text).to eq(node_text)
end

def confirm_tree_view_hidden
  expect(page).to have_no_selector('.split_pane_container .jstree')
end