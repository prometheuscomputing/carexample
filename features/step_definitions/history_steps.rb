Then /^I should see "(\d+)" versions?/ do |num_versions|
  verify_versions(num_versions.to_i)
end
Then /^version "(\d+)" should state that (.+) (?:was|were) modified/ do |version, modified_str|
  verify_version_modified(version.to_i, parse_quoted_string_list(modified_str))
end
When /^I select version "(\d+)"/ do |version|
  select_version(version.to_i)
end
Then /^I should see "(\d+)" changes?/ do |num_changes|
  verify_changes(num_changes.to_i)
end
Then /^I should see that "(.*)" changed from "(.*)" to "(.*)"$/ do |attribute, old_value, new_value|
  verify_change(attribute, old_value, new_value)
end
When /^I select the change to "(.*)"/ do |property|
  select_change(property)
end
Then /^I should see the differences between "(.*)" and "(.*)"/ do |old_value, new_value|
  verify_diff(old_value, new_value)
end