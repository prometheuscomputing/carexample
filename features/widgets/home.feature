# Test home page functionality. Should only include high-level home-page specific tests. Most other functions will be tested by collection and object_page features.
Feature: Home Page
  In order to manipulate top-level objects
  As a user
  I want to see/edit/create top-level objects from a central home page
  
  Background:
    Given I have reset the database
  
  Scenario: Looking at existing Vehicles
    Given I have logged in
    Then I should be at the "Car Example" home page
    And I should see that there are a total of "9" "Vehicle" in the header
    When I expand the "Vehicles" header
    Then I should see the first "8" "Vehicle"