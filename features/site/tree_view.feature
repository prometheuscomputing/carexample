# Test TreeView functionality. 
Feature: TreeView
  In order to view objects and their related object heirarchy in a tree structure
  As a user
  I want to see/view objects from a tree like view
  
  Background:
    Given I have reset the database
    And I have logged in
  
  Scenario:  Hidden Tree View Button on the Home Page
    Given I should be at the "Car Example" home page
    Then I should not see the Display Tree View button
    
  Scenario:  Display Tree View
    Given I click on the "Countries" header
    And I edit the "Countries" where "Name" is "USA"
    Then I click the "Display Tree View" button
    Then I should see "USA" in the tree view
    
  Scenario:  Navigate using the Tree View
    Given I click on the "Countries" header
    And I edit the "Countries" where "Name" is "USA"
    Then I click the "Display Tree View" button
    And I click the "Maryland" tree node
    Then I should be at a "State" page
    And I should see "Maryland" as the selected node
    Then I click the "Remove Tree View" button
    And I should not see the tree view
    
  Scenario: Editing and saving an object after finding it using the Tree View
    Given I click on the "Countries" header
    And I edit the "Countries" where "Name" is "USA"
    Then I click the "Display Tree View" button
    And I click the "Cullowhee" tree node
    Then I should be at a "City" page
    When I set the "Name" to "WCU"
    And I save the page
    Then I should be at a "City" page
    Then the value of "Name" should be "WCU"
    
  @known_failing_in_cucumber
  Scenario: Adding and Removing Associations after finding an object using the Tree View
    Given I click on the "Countries" header
    And I edit the "Countries" where "Name" is "USA"
    Then I click the "Display Tree View" button
    And I click the "Cullowhee" tree node
    Then I should be at a "City" page
    When I click on the "State" header
    And I click the only "Break Association" button under the "State" header
    And I save the page
    
    #Adding a repair shop to Sylva
    Then I go to the home page
    And I click on the "Countries" header
    Then I edit the "Countries" where "Name" is "USA"
    Then I click the "Display Tree View" button
    And I click the "Sylva" tree node
    Then I should be at a "City" page
    When I click on the "Repair Shops" header
    And I show the possible "Repair Shops"
    When I select the "Repair Shop" whose "Name" is "West-side Tire Service"
    And I click the only "Add Associations" button under the "Repair Shops" header
    And I save the page
    And I click on the "Repair Shops" header
    Then I should see that there are a total of "1" "Repair Shops" in the header
    And I should see "1" associated "Repair Shops"
    
  Scenario: Sorting a column in Tree View
    Given I click on the "Components" header
    And I edit the "Component" where "Serial" is "12"
    Then I click the "Display Tree View" button
    And I click the "Automotive::Car" tree node
    Then I should be at a "Car" page
    And I click on the "Owners" header
    When I click on the "Family Size" column header
    Then I should see the "Owner" whose "Name" is "Sally" in position "1"
    And I click on the "Family Size" column header again
    Then I should see the "Owner" whose "Name" is "Bob" in position "1"