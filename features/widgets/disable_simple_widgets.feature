# Test all non-collection/organizer widgets:
#    * string
#    * time
#    * date
#    * timestamp
#    * fixedpoint (bigint) ?
#    * integer
#    * number (float)
#    * boolean
#    * choice -- These are really associations but the widgets act like attribute widgets
#    * label
#    * link
#    * button
#    * RichText
#    * File
#    * Code
#    * ViewRef

# Test various attribute types here.
Feature: Conditionally disabling attribute widgets
  In order to prevent improper changes to objects
  As a user
  I want the ability to change the properties of an object to be conditionally restricted
  
  Background:
    Given I have reset the database
    And   I have logged in
    And   I expand the "People" header
    And   I create a "People"
  
  Scenario: Conditions to disable widgets are not met
    When I set the "Name" to "George"
    And  I save the page
    Then there should be an input for the "Name" attribute
    And  there should be an edit link for the "Description" attribute
    When I choose to edit the "Description" text
    Then there should be a text area for the "Description" attribute
    And  there should be a file upload input for the "Photo" attribute
    And  there should be an input for the "Date Of Birth" attribute
    And  there should be an input for the "Last Updated" attribute
    And  there should be an input for the "Wakes At" attribute
    And  there should be an input for the "Family Size" attribute
    And  there should be an input for the "Weight" attribute
    And  there should be a dropdown for the "Dependent" attribute
    And  there should be a dropdown for the "Handedness" attribute
    And  there should be an input for the "Manifesto" attribute
    And  there should be an input for the "Lucky Numbers" attribute
    And  there should be an input for the "Aliases" attribute
    # Then there should be an input for the "Addresses" attribute
    # Then there should be an input for the "Drives" attribute
    # Then there should be an input for the "Maintains" attribute
    # Then there should be an input for the "Occupying" attribute
    # Then there should be an input for the "Repair Shops" attribute
    # Then there should be an input for the "Vehicles" attribute
    
  Scenario: Conditions to disable widgets are met
    When I set the "Name" to "Robot"
    And  I save the page
    Then there should be an input for the "Name" attribute
    And  there should not be an edit link for the "Description" attribute
    And  there should not be a text area for the "Description" attribute
    And  there should not be a file upload input for the "Photo" attribute
    And  there should be a disabled "input" for the "Date Of Birth" attribute
    And  there should be a disabled "input" for the "Last Updated" attribute
    And  there should be a disabled "input" for the "Wakes At" attribute
    And  there should be a disabled "input" for the "Family Size" attribute
    And  there should be a disabled "input" for the "Weight" attribute
    And  there should be a disabled "dropdown" for the "Dependent" attribute
    And  there should be a disabled "dropdown" for the "Handedness" attribute
    And  there should be a disabled "input" for the "Manifesto" attribute
    And  there should be a disabled "input" for the "Lucky Numbers" attribute
    And  there should be a disabled "input" for the "Aliases" attribute
    # Then there should not be an input for the "Addresses" attribute
    # Then there should not be an input for the "Drives" attribute
    # Then there should not be an input for the "Maintains" attribute
    # Then there should not be an input for the "Occupying" attribute
    # Then there should not be an input for the "Repair Shops" attribute
    # Then there should not be an input for the "Vehicles" attribute
