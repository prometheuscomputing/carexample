module UserHelpers
  include Capybara::DSL
  
  const_set?('ADMIN', 'admin')
  const_set?('ADMINS', ['leroy', 'moss'])
  const_set?('USERS', ['bobby', 'george'])
  const_set?('PASS_SUFFIX', '1')
  const_set?('EMAIL_SUFFIX', '@mycompany.net')
  const_set?('INVITE_CODE', 'sprockets')
  const_set?('ADMIN_INVITE_CODE', 'sprockets_admin')
  
  def set_test_user(user)
    @test_user = user
  end
  
  def test_user
    @test_user
  end
  
  def setup_users
    return if Gui_Builder_Profile::User.count > 0
    register_users
  end
  
  def register_users
    register(ADMIN, ADMIN, :first_user)
    create_invitation_code(INVITE_CODE)
    create_invitation_code(ADMIN_INVITE_CODE, nil, nil, false, ['Administrator'], perspectives = [])
    sign_out
    users  = USERS
    admins = ADMINS
    users.each do |username|
      register(username)
      sign_out
    end
    admins.each do |username|
      register(username, nil, ADMIN_INVITE_CODE)
      sign_out
    end
  end
  
  def click_register
    if has_css?('.registration_button')
      find('.registration_button').click
      return
    end
    if has_css?('.registration_link')
      find('.registration_link').click
      return
    end
    find('.registration_button') # force failure if nothing to click
  end
    
  # Sign in (as a normal user by default)
  def sign_in(username, password = nil)
    visit '/' unless on_login_page?
    password ||= username == 'admin' ? username : "#{username}#{PASS_SUFFIX}"
    enter_user_info(username, password)
    submit_login
  end
  
  def submit_login
    find('.login_button').click
  end
  
  def submit_forgot_email
    email_me_button = first('button', :text => 'Email Me')
    email_me_button.click
  end
  
  # Register a username/password. Assumes that noone is signed in.
  def register(username, password = nil, invite_code = nil, email = nil)
    visit '/gb_users/register'
    password ||= "#{username}#{UserHelpers::PASS_SUFFIX}"
    email ||= "#{username}#{EMAIL_SUFFIX}"
    enter_user_info(username, password, password, email)
    # Enter invite code unless we are registering the first user
    enter_invitation_code(invite_code || INVITE_CODE) unless invite_code == :first_user
    submit_registration
  end
  
  def enter_invitation_code(code)
    fill_in('Invitation Code:', :with => code)
  end
  
  def submit_registration
    # registration_button = first('input[value="Agree and Register"]')
    # registration_button = first('//button[text()="Register"]')
    registration_button = first('.registration_button')
    registration_button.click
  end
  
  def enter_user_info(username, password = nil, reentered_password = nil, email = nil)
    enter_username(username)
    enter_password(password)
    reenter_password(password) if reentered_password
    enter_email(email) if email
  end
  
  def sign_out
    find('.login-name').click
    click_link 'Log Out'
  end
  
  def signed_in?
    !first('.login-name', :minimum => 0).nil?
    # !first('a', :text => 'Log Out').nil?
  end
  
  # Password Change helpers
  def verify_password_change_prompt
    expect(first('fieldset').first('legend').text.strip).to eq('Change Password')
  end
  def confirm_password_change
    # first('input[value="Change Password"]').click
    click_button "Change Password"
  end
  def skip_password_change
    # first('input[value="Skip for now"]').click
    click_button "Skip for now"
  end
  def enter_username(username)
    fill_in "Username:", :with => username
  end
  def enter_new_username(_username)
    raise "Not implemented yet"
  end
  def enter_password(password)
    fill_in "password", :with => password
  end
  def enter_new_password(password)
    fill_in "new_password", :with => password
  end
  def enter_old_password(password)
    fill_in "old_password", :with => password
  end
  def reenter_password(password)
    fill_in "reenter_password", :with => password
  end
  def reenter_new_password(password)
    fill_in "reenter_new_password", :with => password
  end
  
  # --------------------------------------- Login Verifiers
  def verify_username(username)
    expect(find_field("Username:").value).to eq(username)
  end
  def verify_password(password)
    expect(find_field("password").value).to eq(password)
  end
  def verify_reentered_password(password)
    expect(find_field("reenter_password").value).to eq(password)
  end
  def verify_invitation_code(code)
    expect(find_field("Invitation Code:").value).to eq(code)
  end
  def verify_on_login_page
    expect(on_login_page?).to eq(true)
  end
  def on_login_page?
    page.has_css?('.login_button')
  end
  def verify_login(username, password = nil)
    sign_in(username, password)
    verify_homepage
  end
  def verify_user_login(username, password = nil)
    verify_login(username, password)
    verify_no_administration_links
  end
  def verify_admin_login(username, password = nil)
    verify_login(username, password)
    verify_administration_link
  end
  def verify_cannot_login(username, password = nil, warning = "Invalid username and password")
    sign_in(username, password)
    expect(first('.message_error', :text => warning, :normalize_ws => true)).not_to be_nil
    verify_not_logged_in
  end
  def verify_administration_link
    expect(administration_link).not_to be_nil
  end
  def verify_no_administration_link
    expect(administration_link).to be_nil
  end
  def verify_user_administration_link
    expect(user_administration_link).not_to be_nil
  end
  def verify_no_user_administration_link
    expect(user_administration_link).to be_nil
  end
  def verify_site_administration_link
    expect(site_administration_link).not_to be_nil
  end
  def verify_no_site_administration_link
    expect(site_administration_link).to be_nil
  end
  def verify_no_administration_links
    expect(find('#user_bar').first('a', :text => 'Administration', :minimum => 0)).to be_nil
    # verify_no_user_administration_link
    # verify_no_site_administration_link
  end
  def administration_link
    find('#user_bar').first('a', :text => 'Administration').click
  end
  def user_administration_link
    #This might break depending on how it is used.  If it checks for site or user admin without the dropdown link it will break now
    find('#user_bar').first('a', :text => 'Administration').click
    find('#user_bar').first('a', :text => 'Manage Users', :minimum => 0)
  end
  def site_administration_link
    #This might break depending on how it is used.  If it checks for site or user admin without the dropdown link it will break now
    find('#user_bar').first('a', :text => 'Administration').click
    find('#user_bar').first('a', :text => 'Manage Site', :minimum => 0)
  end
  def verify_not_logged_in
    if page.has_css?('#user_bar')
      expect(find('#user_bar').first('a', :text => 'Login')).not_to be_nil
    else
      # if you aren't logged in then there is no user_bar
      true
    end
  end
  def verify_no_user_administration_panel
    visit('/gb_users/user_admin')
    verify_page_error
  end
  def verify_no_site_administration_panel
    visit('/gb_users/site_admin')
    verify_page_error
  end
  # --------------------------------------- End Login Verifiers
  
  
  # --------------------------------------- Registration verifiers
  def verify_on_registration_page
    find('.registration_button')
  end
  def verify_register(username, password = nil, code = nil)
    register(username, password, code)
    verify_homepage
  end
  def verify_user_register(username, password = nil, code = nil)
    verify_register(username, password, code)
    verify_no_administration_links
  end
  def verify_admin_register(username, password = nil, code = nil)
    verify_register(username, password, code)
    verify_administration_link
  end
  def verify_site_admin_register(username, password = nil, code = nil)
    verify_register(username, password, code)
    verify_administration_link
  end
  def verify_cannot_register(username, password = nil, code = nil, warning = '')
    register(username, password, code)
    find('.message_error', :text => warning, :normalize_ws => true)
    verify_not_logged_in
  end
  # --------------------------------------- End Registration verifiers
  
  # --------------------------------------- Forgot Password verifiers
  
  def verify_on_forgot_password_page
    expect(first('fieldset').first('legend').text.strip).to eq('Forgot Password')
  end
  
  def enter_email(email)
    fill_in "email", :with => email
  end
  
  # --------------------------------------- End Registration verifiers
end