# This file contains stuff that just makes the testing experience better for the tester

# this clears the terminal/console...at least on OS X it does.
def clear_terminal(erase_scrollback_history = nil)
  if erase_scrollback_history
    puts "\e[H\e[2J\e[3J"
  else # preserves scrollback
    puts "\e[H\e[2J"
  end
end

module Capybara
  module Screenshot    
    def self.screenshot(filename_prefix=nil)
      filename_prefix = 'screenshot' if filename_prefix.nil? || filename_prefix.empty?
      filename_prefix = filename_prefix.strip.gsub(/ +/, "_")
      saver = Saver.new(Capybara, Capybara.page, false, filename_prefix)
      saver.save
      {:html => nil, :image => saver.screenshot_path}
    end
    # was hoping to stop the saving of HTML but this didn't work
    # def self.screenshot_and_save_page
    #   saver = Saver.new(Capybara, Capybara.page)
    #   saver.save
    #   {:html => SAVE_HTML_PAGES_ON_ERROR ? saver.html_path : nil, :image => saver.screenshot_path}
    # end
    class << self
      alias screen_shot screenshot
    end
  end
  module DSL
    # Adds class methods to Capybara module and gets mixed into
    # the current scope during Cucumber and RSpec tests
    def screenshot(filename_prefix=nil)
      scr_info = Capybara::Screenshot.screenshot(filename_prefix)
      log "Saved screenshot to: #{scr_info[:image]}" # and html to #{scr_info[:html]}
    end
    
    def screenshot_save_page_and_report
      scr_info = screenshot_and_save_page
      puts " Manual screenshot - HTML: #{scr_info[:html]}"
      puts " Manual screenshot -  PNG: #{scr_info[:image]}"
    end
  end
end
