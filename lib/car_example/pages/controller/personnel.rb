class Personnel < Gui::Controller
  map '/personnel/'
  
  def public_page?
    false
  end
  
  def index
    raw_redirect '/personnel/select_person'
  end
  
  def select_person
    @people = People::Person.all
  end
  
  def personnel_report
    #error_404 unless request.post?
    person_id = request.safe_params['select_person']
    raw_redirect "/People/Person/#{person_id}/?view-type=Document"
  end
end