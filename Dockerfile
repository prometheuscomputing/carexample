FROM ruby:2.7.1-buster as environment_setup

COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN bundle install
RUN gem install pundit

WORKDIR /app
COPY bin bin
COPY car_example/setup.rb car_example/setup.rb
COPY car_example/dockerpath.yaml car_example/lode_path.yaml
COPY lib lib
WORKDIR /

CMD gem list && ./app/bin/car_example_start_gui_server --mode dev --database "/database/SQL.db"


# CMD ls -lah app/car_example && cat app/car_example/lode_path.yaml && ./app/bin/car_example_start_gui_server --mode dev --database "/database/SQL.db"
