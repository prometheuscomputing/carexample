Then /^I should be at the clear view selection page$/ do
  page.should have_content('Please select a')
end
When /^I select the "(.+)" clear view$/ do |class_name|
  select_clear_view(class_name)
end
When /^I render the "([^"]*)"( (?:where|whose) .+)?$/ do |header, conditions_string| #"
  edit_object(header, parse_conditions(conditions_string))
end
When /^I render a new "([^"]*)"(?: of type "(.+)")?$/ do |header, type| #"
  # Note that this method is misnamed, since this isn't associating. It is only picking an entry from the creation dropdown
  associate_to_new_object(header, type, nil, false, nil, nil, true)
end


#Section Steps
When("I click on the section heading {string}") do |heading_text|
  heading_label = get_section_heading(heading_text)
  heading_label.click
  wait_for_section_content(heading_text)
end