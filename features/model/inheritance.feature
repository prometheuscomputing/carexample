# Ensure that attributes/associations inherited from superclass(es) are available
Feature: Inheritance
  In order to fully edit all of an object's properties
  As a user
  I want to be able to manipulate inherited attributes and associations
  
  Background:
    Given I have reset the database
    And I have logged in
    And I expand the "Vehicles" header
  
  Scenario: Normal inheritance
    Given I view "9" "Vehicle" per page
    And I submit the "Vehicle" search
    When I edit the "Vehicle" where "Vehicle Model" is "Caravan" # Minivan
    Then the value of "Sliding Doors" should be "True"
    And the value of "Miles Per Gallon" should be "20"
    And the value of "Cost" should be "10000"
    And I should see that there are a total of "0" "Components" in the header
  
  Scenario: Multiple inheritance
    When I edit the "Vehicle" where "Vehicle Model" is "Prius" # Hybrid Vehicle
    Then the value of "Hybrid Type" should be "Normal"
    And the value of "Miles Per Gallon" should be "50"
    And the value of "Electric Efficiency" should be "12.1"
    And the value of "Cost" should be "30000"
    And I should see that there are a total of "0" "Components" in the header