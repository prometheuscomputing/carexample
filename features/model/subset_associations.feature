Feature: Subset associations
  In order to interact with subset associations
  As a user
  I want to manipulate subset associations
  
  Background:
    Given I have reset the database
    And I have logged in as a user
    And I expand the "People" header
    And I edit the "People" whose "Name" is "Bob"
    And I click on the "Addresses" header
    And I edit the "Address" where "Street Number" is "345"
    And I click on the "Registered Vehicles" header
  
  Scenario: Adding an item to an association constrained to be a subset of another association
    When I show the possible "Registered Vehicles"
    Then I should see "1" unassociated "Registered Vehicle" where "Vehicle Model" is "S80"
    When I select the "Registered Vehicle"
    And I click the only "Add Associations" button under the "Registered Vehicles" header
    # Ensure that subset associations may not create a new object
    Then I should not see an option to create a new "Registered Vehicle"
    Then I should see "1" pending "Registered Vehicle" where "Vehicle Model" is "S80"
    When I show the possible "Registered Vehicles"
    Then I should see "0" "Registered Vehicles"
  
  # The behavior in this case is currently to allow subset associations to continue to contain objects
  # after they have been removed from the superset association.
  @known_failing_in_cucumber
  Scenario: Removing an item from the superset collection after associating in the subset collection
    Given I show the possible "Registered Vehicles"
    And I select the "Registered Vehicle"
    And I click the only "Add Associations" button under the "Registered Vehicles" header
    And I save the page
    And I go to the previous page
    When I expand the "Vehicles" header
    And I select the "Vehicle" where "Vehicle Model" is "S80"
    And I click the only "Break Associations" button under the "Vehicles" header
    And I save the page
    And I click on the "Addresses" header
    And I edit the "Address" where "Street Number" is "345"
    And I click on the "Registered Vehicles" header
    And I edit the "Registered Vehicle"
    Then I should be at a "Car" page
    And the value of "Vehicle Model" should be "S80"
    When I save and leave the page
    Then I should be at an "Address" page