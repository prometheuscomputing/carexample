# --------------------------------------- Generic Attribute helpers
def verify_attributes_simple(attr_hash, container = nil)
  attribute  = attr_hash[:attribute_name]
  value      = attr_hash[:value]
  match_type = attr_hash[:match_type]

  local_widget = widget(attribute, container)
  case match_type
  when 'be'
    attribute_value = get_attribute_value(attribute, nil, true, local_widget).to_s
    expect(attribute_value).to eq(value)
  when 'include', 'includes', 'contains', 'contain'
    expect(get_attribute_value(attribute, nil, true, local_widget).to_s).to include(value)
  when 'not be'
    expect(get_attribute_value(attribute, nil, true, local_widget).to_s).not_to eq(value)
  when 'does not contain', 'not contain'
    expect(get_attribute_value(attribute, nil, true, local_widget).to_s).not_to include(value)
  when 'be contained in'
    expect(value).to include(get_attribute_value(attribute, nil, true, local_widget).to_s)
  when 'not be contained in'
    expect(value).not_to include(get_attribute_value(attribute, nil, true, local_widget).to_s)
  end
end
def verify_attributes(conditions, container = nil)
  equals_conditions       = conditions[0]
  contains_conditions     = conditions[1]
  not_equals_conditions   = conditions[2]
  not_contains_conditions = conditions[3]
  ws = widgets(container)
  
  equals_conditions.each do |attribute, value|
    expect(get_attribute_value(attribute, ws)).to eq(value)
  end
  contains_conditions.each do |attribute, value|
    expect(get_attribute_value(attribute, ws)).to include(value)
  end
  not_equals_conditions.each do |attribute, value|
    expect(get_attribute_value(attribute, ws)).not_to eq(value)
  end
  not_contains_conditions.each do |attribute, value|
    expect(get_attribute_value(attribute, ws)).not_to include(value)
  end
end
def verify_attributes_present(attributes, container)
  ws = widget_infos(widgets(container))
  attributes.each do |attribute|
    widget = ws.select{|w| w[:label] == attribute}.first
    expect(widget).not_to be_nil
  end
end
def verify_attribute_input(label, selector, present = true)
  w = widget(label)
  if present
    w.find(selector)
    # w.assert_selector(selector)
  else
    w.assert_no_selector(selector)
  end
end

def verify_disabled_attribute_input(label, selector)
  w = widget(label)
  expect(w.find(selector).disabled?).to(be true)
end

def get_attribute_value(attribute, from = nil, strip_file_text = true, widget = nil)
  w  ||= widget(attribute, from)
  info = widget_info(w)
  raise "Could not find info for attribute: #{attribute}" unless info
  case info[:type]
  when 'file'
    get_file_attribute_value(attribute, :from => from, :strip_file_text => strip_file_text, :widget => widget)
  when 'choice'
    value = info[:input].value
    value = '' if value == '-'
    value
  when 'text'
    if w.first('a.widget_toggle_link', :visible => true, :minimum => 0)
      w.find('.pretty_view').text(:normalize_ws => true)
    else
      w.find('textarea.widget_data').value
    end
  else
    info[:input].value
  end
end

def get_file_attribute_value(attribute, opts = {})
  from            = opts[:from]
  # widget          = opts[:widget]
  strip_file_text = (opts[:strip_file_text] != false)
  w  ||= widget(attribute, from)
  info = widget_info(w)
  raise "Could not find info for attribute: #{attribute}" unless info
  begin
    value_div = info[:widget_node].find('.no_stored_file')
  rescue Capybara::ElementNotFound # You know you love this...
    value_div = info[:widget_node].find('.stored_file .stored_filename')
  end
  field_text = value_div.text
  if strip_file_text
    field_text = field_text.gsub(/Stored File:/, '').strip.gsub(/Download$/, '').strip
  end
  #TODO Probably won't work with document file widget since it is different now
  field_text.empty? ? nil: field_text
end

# Enters a hash of attributes and values into fields on the current page
# press_enter, if set to true, will attempt to simulate pressing enter after entering the last specified attribute value
def enter_attributes(attributes, scope = nil, press_enter = false)
  inert_element = find('#prometheus_statement')
  ws    = widgets(scope)
  num_attributes = attributes.length
  attributes.each_with_index do |attribute_and_value, index|
    attribute, value = attribute_and_value
    last_attribute = (index + 1 == num_attributes)
    w = ws.select{|wi| widget_info(wi)[:label] == attribute}.first
    raise "Could not find widget matching '#{attribute}' to enter value: #{value}\nAvailable widgets were labeled #{ws.collect{|wi| widget_info(wi)[:label]}}" unless w
    info = widget_info(w)
    is_document_widget = (info[:type] =~ /^document_/)
    # Click on the input before entering value, since this is a better simulation of a user's actions when entering an attribute
    # This will also trigger any JS that is bound to the click event, which would otherwise be bypassed.
    if is_document_widget
      # Click on document widgets to expose .edit_view (which will show and focus the input)
      w.click
    else
      # Click the 'edit' button on a richtext unless already visible
      # edit_richtext(info) if info[:type] =~ /text/ && w.first('a.widget_toggle_link', :visible => true)
      #Not sure why this was ever giving it Info??? -TB
      edit_richtext(w) if info[:type] =~ /text/ && w.first('a.widget_toggle_link', :visible => true, :minimum => 0)
      # Files should not be directly clicked on, since this opens a file upload dialog and capybara crashes with an "end of file reached" error.
      info[:input].click unless info[:type] =~ /file/
    end
    case info[:type]
    when /file/
      value = File.join(TEST_FILES_DIR,value) unless [:poltergeist, :chrome, :headless_chrome].include?(Capybara.current_driver)
      info[:input].set(value)
    when /choice/, /boolean/
      value = '-' if value.empty?
      info[:input].find("option", :text => value).select_option
    when /text/
      enter_richtext(attribute, value)
    when /timestamp/, /time/, /date/
      # Verify that date picker JQuery widget popped up
      find('#ui-datepicker-div')
      datetimepick(value, :from => '#'+info[:input][:id])
    else
      
      #Click it again and set it to nothing because it doesn't work without it.  why I don't know clearview sucks- TB
      # if is_document_widget
      #   w.click
      #   # info[:input].click
      #   info[:input].set(nil)
      #   # info[:input].click
      #   w.click
      # end
      if press_enter  
        info[:input].send_keys(value, :enter) if press_enter && last_attribute
      else
        info[:input].set(value)      
        info[:input].trigger_event(:change) unless Capybara.current_driver == :poltergeist
      end
          
    end
    # Blur widgets unless enter has been pressed (page will reload in this case)
    blur_attribute(info[:input]) if !(last_attribute && press_enter)
    # NOTE: Instead of manually triggering a 'blur' event, click the page title to blur the input
    #       This avoids issues where 'blur' doesn't correctly dismiss date picker widgets.
    inert_element.click if !(last_attribute && press_enter)
    # page.find('body').click if !(last_attribute && press_enter)
  end
end

def blur_attribute(input)
  input.trigger_event(:blur)
end

def confirm_delete_popup
  click_popup_button('collectionDeleteSelected', 'Confirm Delete')
end
def cancel_delete_popup
  cancel_popup('collectionDeleteSelected')
end

#TODO I don't know if I need these two new helpers or not.  It depends on how confirm and delete popup is used - TB
def confirm_richtext_delete_popup
  click_popup_button('deleteConfirmation', 'Delete')
end
def cancel_richtext_delete_popup
  cancel_popup('deleteConfirmation')
end

# More granular controls for RichTexts
def edit_richtext(widget)
  widget.find('a.widget_toggle_link').click
  expect(widget).to have_css('textarea', :visible => true)
end
def edit_html_richtext(widget)
  widget.find('a.widget_toggle_link').click
  expect(widget).to have_css('textarea', :visible => false)
end

def enter_richtext(attribute, value)
  widget_info(widget(attribute))[:input].set(value)
end
def verify_richtext(attribute, value)
  expect(widget_info(widget(attribute))[:input].value).to eq(value)
end
# Warning: Can pass even if language dropdown is broken. Capybara cannot trigger language dropdown
def set_richtext_markup(attribute, markup)
  widget_info(widget(attribute))[:language].find(:option, markup).select_option
end
def verify_richtext_markup_options(attribute, options)
  options.each do |option|
    widget_info(widget(attribute))[:language].find(:option, option)
  end
end
def verify_richtext_markup_language(attribute, markup)
  expect(get_richtext_markup(attribute)).to eq(markup)
end
def get_richtext_markup(attribute)
  widget_info(widget(attribute))[:language].value
end
def upload_richtext_image(image)
  enter_richtext_image(image)
end
def delete_richtext_image(image)
  img = find_popup('richTextImageUpload').find(:xpath, ".//img[contains(@alt,'#{image}')]")
  raise "Failed to find image" unless img
  img_container = img.first(:xpath, ".//..//..")
  raise "Failed to find image container" unless img_container
  img.hover
  img_container.find('.delete_richtext_image').click
end
def click_image_upload_button(attribute)
  link = widget(attribute).find(:xpath, './/a[text()="Upload Image"]')
  link.click # This click doesn't result in the modal being launched in the test for deleting a RichTextImage.  Note -- just because it may not be a problem in your development environment doesn't mean it isn't a problem on someone else's.  Do not delete the second click here unless you have consulted with all of the people who regularly run these cukes.
  # Wait manually here since we don't know whether the modal was launched or not, so find may fail
  sleep(0.1)
  # Click a second time if the first click failed to open the upload modal
  # TODO: figure out why the first click sometimes fails to launch modal -SD
  link.click unless [:chrome, :headless_chrome].include?(Capybara.current_driver) || first('#richTextImageUploadModal')
end
def enter_richtext_image(image)
  # find("#richTextImageUploadModal").first('.image_upload_file').set(File.join(TEST_FILES_DIR, image))
  within('#richTextImageUploadModal') do
    attach_file('richTextImageUploadFileInput', File.join(TEST_FILES_DIR, image), :make_visible => true)
  end
end
def confirm_richtext_image_upload
  click_popup_button('richTextImageUpload', 'Upload')
end
def cancel_richtext_image_upload
  cancel_popup('richTextImageUpload')
end
def verify_upload_image_popup(negative = false)
  verify_popup('richTextImageUpload', negative)
end
def confirm_richtext_contains_image(attribute, image)
  widget_div = widget(attribute)
  expect(widget_div).to have_image(image)
  img = widget_div.find(:xpath, "//img[contains(@src,\"/#{image}\")]")
  case Capybara.current_driver
  when :chrome, :firefox, :headless_chrome
    expect(img[:naturalWidth].to_i).not_to eq(0)
  when :webkit, :poltergeist
    url = current_url
    verify_image_url(img[:src])
    visit(url)
  else
    raise "Unsupported driver. Cannot verify image."
  end
end
def confirm_image_does_not_exist(attribute, image)
  widget_div = widget(attribute)
  # Img tag will exist since it hasn't been removed from the RichText markup
  #widget_div.should_not have_image(image)
  img = widget_div.find(:xpath, "//img[contains(@src,\"/#{image}\")]")
  case Capybara.current_driver
  when :chrome, :firefox, :headless_chrome
    expect(img[:naturalWidth].to_i).to eq(0)
  when :webkit, :poltergeist
    url = current_url
    # verify_image_url(expand_url(img[:src]), negate = true)
    verify_image_url(img[:src], negate = true)
    visit(url)
  else
    raise "Unsupported driver. Cannot verify image."
  end
end
def get_field_container(attribute, container = nil)
  classifier = container ? get_classifier(container) : get_page_classifier
  container ||= find('.root').first('.view_form')
  underscored_widget_name = "#{classifier}___#{attribute.to_underscored_attribute_name}_widget"
  container.first("##{underscored_widget_name}")
end
def get_classifier(container)
  container.first('.data_classifier').value.split('::').last
end

def click_checkbox(checkbox_label)
  containing_label = page.all('label', :text => checkbox_label).first
  checkbox = containing_label.find('.boolean_checkbox')
  checkbox.click
end

def verify_checkbox_checked(checkbox_label, negative = false)
  containing_label = page.all('label', :text => checkbox_label).first
  checkbox = containing_label.find('.boolean_checkbox')
  checkbox.checked?.should_unless(negative)
end

def slider_number_slider(number_slider_label, new_value)
  label_element = page.find('.textlabel', :text => number_slider_label)
  number_slider_selector = "##{label_element[:id]} + .document_number_slider .number_slider"
  number_slider = page.all(number_slider_selector).first
  slide_number_slider(number_slider, new_value)
end

def verify_number_slider_value(number_slider_label, value)
  label_element = page.find('.textlabel', :text => number_slider_label)
  number_slider_selector = "##{label_element[:id]} + .document_number_slider"
  number_slider = page.find(number_slider_selector)
  number_slider_text = number_slider.find('.number_slider_handle').text
  number_slider_text.should == value
end
  
# --------------------------------------- End Generic Attribute helpers


# --------------------------------------- File-specific helpers
# Verify a file is stored with the given filename.
# Will check existence if filename is not specified.
# Can be negated by setting negative to true.
def verify_stored_filename(file_attribute, filename = nil, negative = false)
  filename = 'No Stored File' if negative
  value    = get_file_attribute_value(file_attribute)
  value.should == filename
end
def verify_to_be_stored_filename(file_attribute, filename = nil, negative = false)
  file_widget = widget(file_attribute, nil)
  # to_be_stored_filename_text = get_attribute_value(file_attribute, nil, false)
  to_be_stored_filename_text = file_widget.find('.stored_file').text
  filename_match             = to_be_stored_filename_text.match(/To Be Stored:(.*)$/)
  to_be_stored_filename      = filename_match[1].strip if filename_match
  if filename
    to_be_stored_filename.should_unless(negative) == filename
  else
    to_be_stored_filename.should_not_unless(negative, be_nil)
  end
end
# Set the file attribute with the given path
def choose_file(attribute, file_path)
  enter_attributes({attribute => file_path})
end
# Confirm a file deletion dialog
# NOTE: this could cause a conflict since there are other "Delete" buttons
#       Will rewrite if it becomes an issue
def confirm_file_delete
  confirm_delete_popup
end
def cancel_file_delete
  cancel_delete_popup
end
# Delete the stored file from the given file attribute
def toggle_delete_file(attribute)
  widget(attribute).find('.delete_label').click
end
# Confirm that the delete checkbox is enabled\disabled
def confirm_disabled_delete(attribute, enabled = nil)
  if enabled.nil?
    expect(widget(attribute).find("input.attribute_delete_checkbox[data_name='delete']").disabled?).to(be true)
    # expect(widget(attribute).find('.delete_label').disabled?).to(be true)
  else # Should be enabled - ( not disabled )
    expect(widget(attribute).find("input.attribute_delete_checkbox[data_name='delete']").disabled?).to(be false)
    # expect(widget(attribute).find('.delete_label').disabled?).to(be false)
  end
end
# Download the stored file from the given file attribute
def download_file(attribute)
  widget(attribute).click_link 'Download'
end
# --------------------------------------- End File-specific helpers


# These helpers assist with checking domain_objs that have been pulled from the DB.
# They should only be used when other options of verification are impossible.
# --------------------------------------- DB access domain_obj helpers
# Verifies that a domain object meets the conditions array (as produced via #parse_conditions(...))
def verify_object_attributes(domain_obj, conditions)
  attr_hash = get_attribute_hash_for_obj(domain_obj)
  expect(attr_hash).to have_values(conditions[0])
  expect(attr_hash).to contain_values(conditions[1])
  expect(attr_hash).not_to have_values(conditions[2]) unless conditions[2].empty?
  expect(attr_hash).not_to contain_values(conditions[3]) unless conditions[3].empty?
end

# Get a hash of attribute labels and values for a domain_obj
def get_attribute_hash_for_obj(domain_obj)
  attr_hash = {}
  domain_obj.class.attributes.each do |attribute, _|
    val = domain_obj.send(attribute)
    case val
    when Gui_Builder_Profile::File
      val = val.filename
    when Gui_Builder_Profile::RichText
      val = val.content
    end
    attr_hash[getter_to_label(attribute)] = val
  end
  attr_hash
end
# --------------------------------------- end DB access domain_obj helpers
