# --------------------------------------- Security steps
Then /^I should( not)? be able to access the (site |user )?administration panel/ do |negative, admin_type|
  case admin_type
  when 'site '
    if negative
      verify_no_site_administration_link
      verify_no_site_administration_panel
    else
      verify_user_administration_link
      verify_site_administration_link
    end
  else # user
    if negative
      verify_no_administration_links
      verify_no_user_administration_panel
      verify_no_site_administration_panel
    else
      verify_user_administration_link
    end
  end
end
Then /^I should not be able to access any objects in the "([^"]*)" package(?: except for (.+?))?$/ do |package, except_classes_str| #"
  verify_package_not_accessible(package, parse_quoted_string_list(except_classes_str))
end
Then /^I should be redirected to login on accessing any objects in the "([^"]*)" package(?: except for (.+?))?$/ do |package, except_classes_str| #"
  verify_package_redirects_to_login(package, parse_quoted_string_list(except_classes_str))
end
Then /^I should be redirected to login on accessing:/ do |url_table|
  urls = url_table.raw.flatten
  verify_urls_redirect_to_login(urls)
end
Then /^I should see that the page was not found$/ do
  verify_404_error
end
Then /^I should see that I do not have permission/ do
  verify_403_error
end
# --------------------------------------- End Security steps


When /^I post to "([^"]*)" with (.+)/ do |url, params_str| #"
  puts "url: #{url}, params: #{parse_params(params_str)}"
  $rack_browser.post url, parse_params(params_str)
end
