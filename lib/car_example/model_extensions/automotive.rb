module Automotive
  class Component
    # FIXME Why is this here? Component has no property :serial!!
    is_unique :serial
  end
  class Vehicle
    should_be_unique :vehicle_model
  end
  class VIN
    # In real life a VIN is alpha-numeric...it is typed as an integer in the model
    is_unique :vin
  end
end
