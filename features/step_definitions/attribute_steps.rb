# --------------------------------------- General Attribute steps
When /^I set the (.+?)(?: for (.+?))?( and press enter)?$/ do |attributes_string, scope_string, press_enter|
  parsed_attrs = parse_attributes(attributes_string)
  enter_attributes(parsed_attrs, parse_scope(scope_string), press_enter)
end
Then /^the value of "(.*)" should (.+?) ["'](.*)["']$/ do |attribute, match_type, value| #'
  verify_attributes_simple({:attribute_name => attribute, :value => value, :match_type => match_type})
end
# Used for multi-line editing in the text fields
When /^I fill in the field "(.+?)"(?: for (.+))? with the following body:$/ do |attribute, scope_string, body|
  enter_attributes({attribute => body}, parse_scope(scope_string))
end
# Used for multi-line viewing in the text fields
Then /^the value of the field "(.*)" should (.+?):$/ do |attribute, match_type, value|
  verify_attributes_simple({:attribute_name => attribute, :value => value, :match_type => match_type})
end
When /^I confirm the delete popup$/ do
  confirm_delete_popup
end
When /^I cancel the delete popup/ do
  cancel_delete_popup
end
When /^I confirm the delete richtext image popup$/ do
  confirm_richtext_delete_popup
end
When /^I cancel the delete richtext image popup$/ do
  cancel_richtext_delete_popup
end
Then /^there should( not)? be an? (file upload input|input|dropdown|text area|edit link) for the "(.*)" attribute$/ do |hidden, type, label| #'
  case type
  when 'input'
    css_selector = 'input'
  when 'dropdown'
    css_selector = 'select'
  when 'text area'
    css_selector = 'textarea'
  when 'edit link'
    css_selector = '.widget_toggle_link'
  when 'file upload input'
    # NOTE: it seems that the actual 'input' element is hidden for these, and what's visible is the following selector -SD
    css_selector = '.custom-file-label'
  else
    raise 'unknown input type'
  end
  verify_attribute_input(label, css_selector, !hidden)
end

Then("there should be a disabled {string} for the {string} attribute") do |input_type, attribute_name|
  case input_type
  when 'input'
    css_selector = 'input'
  when 'dropdown'
    css_selector = 'select'
  when 'text area'
    css_selector = 'textarea'
  when 'edit link'
    css_selector = '.widget_toggle_link'
  else
    raise 'unknown input type'
  end
  verify_disabled_attribute_input(attribute_name, css_selector)
end

When /^I click the checkbox with the label "(.*)"$/ do |checkbox_label|
  click_checkbox(checkbox_label)
end

Then /^The "(.*)" checkbox should( not)? be checked$/ do |checkbox_label, negative|
  verify_checkbox_checked(checkbox_label, negative)
end

When /^I slide the number slider with the label "(.*)" to the value "(.*)"$/ do |number_slider_label, new_value|
  slider_number_slider(number_slider_label, new_value)
end

Then /^I should see the number slider with the label "(.*)" have a value of "(.*)"$/ do |number_slider_label, value|
  verify_number_slider_value(number_slider_label, value)
end

# --------------------------------------- End General Attribute steps


# --------------------------------------- RichText steps
When /^I choose to edit the "([^"]+)" text( for HTML)?$/ do |attribute, is_html| #"
  if is_html
    edit_html_richtext(widget(attribute))
  else
    edit_richtext(widget(attribute))
  end
end
When /^I select the markup language "([^"]+)" for "([^"]+)"$/ do |markup, attribute|
  set_richtext_markup(attribute, markup)
end
Then /^the "([^"]*)" markup language's options should contain (.*)$/ do |dropdown, options| #"
  values = options.scan(/"([^"]+)"/).flatten
  verify_richtext_markup_options(dropdown, values)
end
Then /^the markup language of "([^"]+)" should be "([^"]+)"$/ do |attribute, markup|
  verify_richtext_markup_language(attribute, markup)
end
When /^I upload the image "([^"]+)"$/ do |image| #"
  upload_richtext_image(image)
end
When /^I delete the image "([^"]+)"$/ do |image| #"
  delete_richtext_image(image)
end
When /^I choose to upload an image for the "([^"]+)" text$/ do |attribute| #"
  click_image_upload_button(attribute)
end
When /^I confirm the image upload$/ do
  confirm_richtext_image_upload
end
When /^I cancel the image upload$/ do
  cancel_richtext_image_upload
end
Then /^I should( not)? see the "Upload Image" popup$/ do |negative|
  verify_upload_image_popup(!!negative)
end
# NOTE: this step navigates away from the current page and then back again. Use accordingly
Then /^the "([^"]+)" text should( not)? contain the image "([^"]+)"$/ do |attribute, negate, image|
  if negate
    confirm_image_does_not_exist(attribute, image)
  else
    confirm_richtext_contains_image(attribute, image)
  end
end
# --------------------------------------- End RichText steps


# --------------------------------------- File steps
Then /^there should( not)? be a stored "([^"]+)"(?: named "([^"]+)")?/ do |negative, file_attribute, filename|
  verify_stored_filename(file_attribute, filename, negative)
end
Then /^there should( not)? be a pending "([^"]+)"(?: named "([^"]+)")?/ do |negative, file_attribute, filename|
  verify_to_be_stored_filename(file_attribute, filename, negative)
end
When /^I choose to upload the "(.*?)" named "([^"]*?)"/ do |attribute, value| #"
  enter_attributes({attribute => File.join(TEST_FILES_DIR, value)})
end
When /^I (un)?check the delete box for the stored "(.*?)"/ do |_, attribute|
  toggle_delete_file(attribute)
end
Then /^the delete box should( not)? be disabled for the stored "(.*?)"$/ do |enable, attribute|
  confirm_disabled_delete(attribute, enable)
end
# Now using checkboxes, method no longer needed
# When /^I confirm the file deletion/ do
#   confirm_file_delete
# end
# Now using checkboxes, method no longer needed
# When /^I cancel the file deletion/ do
#   cancel_file_delete
# end
When /^I download the stored "(.*?)"/ do |attribute|
  download_file(attribute)
end
When /^I wait for the download to start$/ do
  # This step handles downloads that use the meta tag 'http-equiv'=>'refresh'
  # TODO: stop using meta refresh tag
  sleep(1) 
end
# If more comprehensive download checking is needed, see here: http://collectiveidea.com/blog/archives/2012/01/27/testing-file-downloads-with-capybara-and-chromedriver/
Then /^I should receive a ?(text|jpeg|binary)? file(?: named "([^"]+)")?/ do |type, filename| #"
  verify_download(filename, type)
end
# --------------------------------------- End File Steps