# Test Sorting collection features here. This includes sorting after filtering
Feature: Collections Sorting
  In order to make it easier to find what I am looking for
  As a user
  I want to be able to (alphanumerically?) sort a collection when I click on a column heading
  
Background:
  Given I have reset the database
  And I have logged in

Scenario:  Sorting a collection in ascending order on the home page
  Given I expand the "People" header
  When I click on the "Name" column header
  Then I should see the "People" whose "Name" is "Bill" in position "1"
  And I click the next page button
  Then I should see the "People" whose "Name" is "Sally" in position "1"
  And I click the previous page button
  # Checking if it sorts by type correctly 
  And I click on the "Type" column header
  Then I should see the "People" whose "Type" is "Mechanic" in position "1"
  # Then I should see the "People" whose "Name" is "Rick" in position "1"
  
Scenario:  Sorting a collection in descending order on the home page
  Given I expand the "People" header
  When I click on the "Name" column header
  And I click on the "Name" column header again
  Then I should see the "People" whose "Name" is "Sally" in position "1"
  And I click the next page button
  Then I should see the "People" whose "Name" is "Bill" in position "1"
  
Scenario:  Sorting a collection in ascending order on an object page
  Given I expand the "Vehicles" header
  And I edit the "Vehicle" where "Vehicle Model" is "S80"
  Then I click on the "Owners" header
  And I click on the "Date Of Birth" column header
  Then I should see the "Owner" whose "Date Of Birth" is "1980-01-01" in position "1"

Scenario:  Sorting a collection in descending order on an object page
  Given I expand the "Vehicles" header
  And I edit the "Vehicle" where "Vehicle Model" is "S80"
  Then I click on the "Owners" header
  And I click on the "Date Of Birth" column header
  And I click on the "Date Of Birth" column header again
  Then I should see the "Owner" whose "Date Of Birth" is "1990-01-04" in position "1"

Scenario:  Filtering and then sorting on the filtered fields
  Given I expand the "People" header
  And I open the "People" search
  And I click on the "People" advanced search link
  When I filter to see "People" where "Name" is "o"
  And I submit the "People" search
  And I should see "4" "People"
  Then I click on the "Name" column header
  Then I should see the "People" whose "Name" is "Brandon" in position "2"
  And I click on the "Name" column header again
  Then I should see the "People" whose "Name" is "Joe" in position "1"
  
Scenario:  A derived column shouldn't be allowed to be sorted (for now)
  Given I expand the "People" header
  And I click on the "Name" column header
  And I should see the "People" whose "Name" is "Bill" in position "1"
  When I click on the "Handedness" column header
  Then I should see the "People" whose "Name" is "Bill" in position "1"