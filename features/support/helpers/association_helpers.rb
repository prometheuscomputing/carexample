# General association helpers that are not specific to either Collections or Organizers
def verify_message_warning_presence(header, be_or_not_be = nil)
  if !be_or_not_be.nil?
    expect(get_header(header).find('.unsaved_changes_message', :visible => false).text).to eq('')
  else
    expect(get_header(header).find('.unsaved_changes_message').text).to eq('Unsaved Changes')
  end
end
# Click the named button under the given header
# The button param can either be text specifying the button's text, or
# a hash specifying a locator for the button (and optionally the text as well)
def click_button_under_header(button, header, scope = nil, only_button = false)
  w = get_widget(header, scope)
  # puts "click_button_under_header: #{button}, #{header}, #{scope}, #{only_button}"
  # puts "Found widget: #{w.inspect}"
  # NOTE: clicking on the last matching button here since document associations
  # internally load content and buttons that may also match
  # This could be rewritten with an awareness af what widget type is matched by header
  case button
  when String
    button_text = button
  when Hash
    button_text = button[:text]
    button_locator = button[:locator]
  end
  button_args = button_locator ? [button_locator, {:text => button_text}] : [:button, button_text]
  button_element = if only_button
    w.find(*button_args)
  else
    # Sleep, since this won't automatically wait to see if the element appears
    sleep 0.1
    w.all(*button_args).last
  end
  # NOTE: if this fails due to button_element being nil, try setting only_button to true if that is your case. -SD
  button_element.click
  
  # w.all(:button, button).last.click
  # This goes a bit too fast and won't wait for the button action to occur before moving on.
  # A small sleep amount seems to correct this
  sleep 0.1
end

# Toggle a collection either show or hide it
def click_header(header)
  get_header_label(header).click
  # Add a small sleep to allow time for the collection to expand
  # TODO: do this instead by searching for visible content item
  sleep 0.2
end

def expand_header(header)
  header = get_header(header, :collapsed => true)
  sleep 0.2
  header.click
  # puts "Clicked"
  # sleep 0.1
  # Ensure header has loaded by finding .content_data within widget
  w = header.find(:xpath, '../..')
  w.find '.content_data'
end

def click_column_header(header)
  get_column_header(header).click
end

# Check whether a particular header is on the page
def header_exists?(header)
  get_header(header) rescue nil
end

# Get a Collection/Organizer header given its text
def get_header(header, options = {})
  # Unless String is passed, assume that header is already a header element
  return header unless header.is_a?(String)
  header_selector = '.view_header'
  header_selector = '.collapsed .view_label' if options[:collapsed]
  header_row = find(header_selector, :text => header)
  header_row
end

def get_column_header(header)
  return header unless header.is_a?(String)
  find('.collection_content th', :text => header)
end

def get_header_label(header)
  # Unless String is passed, assume that header is already a header element
  return header unless header.is_a?(String)
  #label = wait_until_attached{ first('.view_label', :text => header) }
  find('.view_label', :text => header)
end

# Get a view widget given the header's text
def get_widget(header, scope = nil, options = {})
  # Unless String is passed, assume that header is already a widget element
  return header unless header.is_a?(String)
  has_scope = !scope.nil?
  # NOTE: Using page as scope here, due to the odd case where we are using widgets on a page without a root widget.
  #       Example: Enumerations Management
  scope ||= page #||= get_root_widget
  label_selector = '.view_label'
  label_selector = '.collapsed .view_label' if options[:collapsed]
  label = scope.find(label_selector, :text => header, :visible => false)
  unless label
    scope_string = has_scope ? "within #{scope.inspect}" : 'on page'
    raise "Could not find widget label #{scope_string} with text: #{header} (selector for label was: #{label_selector})"
  end
  widget = label.find(:xpath, '../..')
  # puts widget.inspect
  # puts widget.classes.inspect
  # Wait for content to load if necessary
  expect(widget).to have_css('.content_data')
  widget
end

def get_popup_widget(header)
  # Unless String is passed, assume that header is already a widget element
  return header unless header.is_a?(String)
  # label = wait_until_attached { first('.ui-dialog.ui-widget').find('.view_label', :text => header, :visible => false) }
  #TODO hope for the best with this selector it probably works most of the time -TB 
  label = wait_until_attached { first('.modal.show').find('.view_label', :text => header, :visible => false) }
  raise "Could not find wiget label on popup with text: #{header}" unless label
  label.find(:xpath, '../..')
end

def get_root_widget
  find('.root.widget')
end

# Check whether a collection header is for a Collection
def is_collection_header?(header_div)
  header_div.has_class?('collection_header')
end
# Check whether a collection header is for an Organizer
def is_organizer_header?(header_div)
  header_div.has_class?('organizer_header')
end
# Check whether a collection widget is for a Collection
def is_collection_widget?(widget_div)
  widget_div.has_class?('collection')
end
# Check whether a collection widget is for an Organizer
def is_organizer_widget?(widget_div)
  widget_div.has_class?('organizer')
end

def edit_object(header, conditions)
  h = get_header(header)
  if h && is_organizer_header?(h)
    edit_organizer(header, conditions)
  else
    edit_row(header, conditions)
  end
end

def verify_object_link(header, negate = false)
  h = get_header(header)
  w = get_widget(header)
  if is_organizer_header?(h)
    w.should_unless(negate, have_css('.organizer_content a.save_and_go_link'))
  else
    w.should_unless(negate, have_css('.collection_content_data a.save_and_go_link'))
  end
end

def verify_row(header, conditions, state, not_pending = '')
  pending = (not_pending == '') ? true : false
  verify_associtation_row_state(header, conditions, state, pending)
end

# Verify that the descriptions are present in a deletion popup
# If negate is true, then check that the popup does not contain the given description. (but the popup should still exist)
# Since newlines are not present in text from capybara, the string is split on the string 'The '
# and the tests will fail if that string is present as the value of an attribute.
def verify_deletion_warning_for(object_descriptions, negate = false)
  delete_popup = find('#collectionDeleteSelectedModal')
  # heading_text = delete_popup.find('.modal-body p').text
  # delete_items = delete_popup.all('.items-to-delete li')
  raise "No delete popup found" unless delete_popup
  # puts "Getting text for element: #{delete_popup.inspect}"
  # Delay here so that popup text has time to load. This is instant on webkit, but Chrome seems to miss the "Are you sure..." text if it goes too fast.
  sleep 0.1
  delete_popup_text = delete_popup.text
  raise "No deletion warning found" unless delete_popup_text && !delete_popup_text.empty?
  expect(delete_popup_text).to match(/Are you sure you want to delete the following items:/)
  # deleting_type = deleting_type_matches.first[0]
  object_descriptions.each do |description|
    if description[:count] > 0
      deleted_objects = delete_popup_text.split('The ')
      deleted_objects.shift # Shift off empty string before first 'The'
      deleted_objects.shift # Shift off first deleted object (we aren't interested in the primary object)
      type_occurrences = deleted_objects.select{|str| str =~ /^#{description[:type]}/}
      matching_occurrences = type_occurrences.select{|o| 
        if description[:conditions]
          description[:conditions][0].all?{|name, value|
            case name
            when 'Name'
              o =~ /^#{description[:type]} named: #{value}/
            when 'Title'
              o =~ /^#{description[:type]} titled: #{value}/
            else
              o =~ /#{name}: #{value}/
            end
          }
        else
          true
        end
      }
      matching_occurrences.count.should_unless(negate) == description[:count]
    else
      raise "Cannot verify deletion description of 0 items"
    end
  end
end

# Verify associations are present on the current page
def verify_associations_present(associations, negative = false)
  associations.each do |association|
    get_header(association).should_not_unless(negative) == nil
  end
end
  
