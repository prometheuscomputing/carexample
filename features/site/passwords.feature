Feature: Managing Password Requirements
  In order ensure that users are required to have good passwords
  As an administrator
  I want to specify and enforce password constraints
  
  Background:
    Given I have reset the database
    And I have logged in as an administrator
    And I go to the site administration panel
  
  Scenario: Adding/Removing a password requirement
    When I add a password requirement with the regular expression "^[a-zA-Z0-9\!\@\#\$\%\^\&\*\_\+\-\/]*$", the description "Check password characters", and the failure message "You must enter valid password characters (alphanumeric + !@#$%^&*_+-/)"
    And I submit the page
    And I add a password requirement with the regular expression "^.{5,10}$", the description "Check password length", and the failure message "Your password must be between 5 and 10 characters"
    And I submit the page
    Then I should see a password requirement with the regular expression "^[a-zA-Z0-9\!\@\#\$\%\^\&\*\_\+\-\/]*$"
    And I should see a password requirement with the regular expression "^.{5,10}$"
    When I mark the password requirement with the regular expression "^.{5,10}$" to be deleted
    And I submit the page
    Then I should not see a password requirement with the regular expression "^.{5,10}$"
  
  
  Scenario: Enforcement of password requirements when adding a user via the administration panel
    Given I add a password requirement with the regular expression "^[a-zA-Z0-9\!\@\#\$\%\^\&\*\_\+\-\/]*$", the description "Check password characters", and the failure message "You must enter valid password characters (alphanumeric + !@#$%^&*_+-/)"
    And I submit the page
    And I add a password requirement with the regular expression "^.{5,10}$", the description "Check password length", and the failure message "Your password must be between 5 and 10 characters"
    And I submit the page
    And I go to the user administration panel
    And I add a user named "phillip" with the password "verylongpassword"
    And I submit the page
    Then I should receive the error "Your password must be between 5 and 10 characters"
    And there should not be a user named "phillip"
    When I add a user named "phillip" with the password "badchar?"
    And I submit the page
    Then I should receive the error "You must enter valid password characters (alphanumeric + !@#$%^&*_+-/)"
    And there should not be a user named "phillip"
    When I add a user named "phillip" with the password "longpasswordwithbadchar?"
    And I submit the page
    Then I should receive the error "Your password must be between 5 and 10 characters"
    And I should receive the error "You must enter valid password characters (alphanumeric + !@#$%^&*_+-/)"
    And there should not be a user named "phillip"
    When I add a user named "phillip" with the password "goodpass"
    And I submit the page
    Then there should be a user named "phillip"
    
  Scenario: Enforcement of password requirements when adding a site admin via the site administration panel
    Given I add a password requirement with the regular expression "^[a-zA-Z0-9\!\@\#\$\%\^\&\*\_\+\-\/]*$", the description "Check password characters", and the failure message "You must enter valid password characters (alphanumeric + !@#$%^&*_+-/)"
    And I submit the page
    And I add a password requirement with the regular expression "^.{5,10}$", the description "Check password length", and the failure message "Your password must be between 5 and 10 characters"
    And I submit the page
    When I add a user named "newadmin" with the password "longpasswordwithbadchar?"
    And I submit the page
    Then I should receive the error "Your password must be between 5 and 10 characters"
    And I should receive the error "You must enter valid password characters (alphanumeric + !@#$%^&*_+-/)"
    And there should not be an administrator named "newadmin"
    When I add a user named "newadmin" with the password "goodpass"
    And I submit the page
    Then there should be an administrator named "newadmin"
  
  Scenario: Enforcement of password requirements during user registration
    Given I add a password requirement with the regular expression "^[a-zA-Z0-9\!\@\#\$\%\^\&\*\_\+\-\/]*$", the description "Check password characters", and the failure message "You must enter valid password characters (alphanumeric + !@#$%^&*_+-/)"
    And I submit the page
    And I add a password requirement with the regular expression "^.{5,10}$", the description "Check password length", and the failure message "Your password must be between 5 and 10 characters"
    And I submit the page
    And I sign out
    When I register as "phillip" with the password "verylongpassword"
    Then I should receive the error "Your password must be between 5 and 10 characters"
    When I register as "phillip" with the password "badchar?"
    Then I should receive the error "You must enter valid password characters (alphanumeric + !@#$%^&*_+-/)"
    When I register as "phillip" with the password "longpasswordwithbadchar?"
    Then I should receive the error "Your password must be between 5 and 10 characters"
    And I should receive the error "You must enter valid password characters (alphanumeric + !@#$%^&*_+-/)"
    When I register as "phillip" with the password "goodpass"
    Then I should be at the home page
  
  Scenario: Enforcement of password requirements during login of an existing user
    Given I add a user named "phillip" with the password "verylongpassword"
    And I add a password requirement with the regular expression "^.{5,10}$", the description "Check password length", and the failure message "Your password must be between 5 and 10 characters"
    And I submit the page
    And I sign out
    When I go to the url "/Automotive/Motorcycle/new"
    When I log in as "phillip" with the password "verylongpassword"
    Then I should receive the error "Your password must be between 5 and 10 characters"
    And I should be prompted to change my password
    When I enter the new password "badpÅss<=>"
    And I re-enter the new password "badpÅss<=>"
    And I confirm the password change
    Then I should receive an error like "Passwords can only contain letters, numbers, space (no tabs) and the following symbols:"
    And I should be prompted to change my password
    When I enter the new password "goodpass"
    And I re-enter the new password "goodpass"
    And I confirm the password change
    Then I should be at a "Motorcycle" page
    When I sign out
    Then "phillip" should be able to login with the password "goodpass"
    # Check that I can't revisit the password change page
    When I go to the url "/gb_users/change_password"
    Then I should see that I do not have permission to change the password
  
  Scenario: Enforcement of password requirements during login of an existing user (Skipped initially)
    Given I add a user named "phillip" with the password "verylongpassword"
    And I add a password requirement with the regular expression "^.{5,10}$", the description "Check password length", and the failure message "Your password must be between 5 and 10 characters"
    And I submit the page
    And I sign out
    When I go to the url "/Automotive/Motorcycle/new"
    And I log in as "phillip" with the password "verylongpassword"
    Then I should receive the error "Your password must be between 5 and 10 characters"
    And I should be prompted to change my password
    When I skip the password change
    Then I should be at a "Motorcycle" page
    When I sign out
    And I go to the url "/Automotive/Motorcycle/new"
    And I log in as "phillip" with the password "verylongpassword"
    Then I should receive the error "Your password must be between 5 and 10 characters"
    And I should be prompted to change my password
    When I enter the new password "goodpass"
    And I re-enter the new password "goodpass"
    And I confirm the password change
    Then I should be at a "Motorcycle" page
    When I sign out
    Then "phillip" should be able to login with the password "goodpass"
    # Check that I can't revisit the password change page
    When I go to the url "/gb_users/change_password"
    Then I should see that I do not have permission to change the password