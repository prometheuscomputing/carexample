# Test various attribute types here.
Feature: RichText attributes
  In order to change an object's data
  As a user
  I want to manipulate its attributes
  
  Background:
    Given I have reset the database
    And I have logged in
    And I expand the "People" header
    And I create a "People"

  Scenario: Changing the markup language and validating options
    Then the "Description" markup language's options should contain "Markdown", "LaTeX" and "Plain"
    And the "Description" markup language's options should contain "HTML" and "Textile"
    When I select the markup language "LaTeX" for "Description"
    And I save the page
    And I choose to edit the "Description" text
    Then the markup language of "Description" should be "LaTeX"
    Then the "Description" markup language's options should contain "Markdown", "LaTeX" and "Plain"
    And the "Description" markup language's options should contain "HTML" and "Textile"
    When I select the markup language "HTML" for "Description"
    And I save the page
    And I choose to edit the "Description" text for HTML
    Then the markup language of "Description" should be "HTML"
    When I select the markup language "Textile" for "Description"
    And I save the page
    And I choose to edit the "Description" text
    Then the markup language of "Description" should be "Textile"
    When I select the markup language "Plain" for "Description"
    And I save the page
    And I choose to edit the "Description" text
    Then the markup language of "Description" should be "Plain"
    When I select the markup language "Markdown" for "Description"
    And I save the page
    And I choose to edit the "Description" text
    Then the markup language of "Description" should be "Markdown"
    When I select the markup language "Kramdown" for "Description"
    And I save the page
    And I choose to edit the "Description" text
    Then the markup language of "Description" should be "Kramdown"
  
  Scenario: Testing RichText handling of trailing\leading newline characters
    When I fill in the field "Description" with the following body:
      """

      Line A

      """
    # Capybara appears to be doubling the amount of newlines, must use 'contain' to verify before save.
    Then the value of "Description" should contain "Line A"
    When I save the page
    Then I should not receive a "concurrency" popup warning
    Then the value of "Description" should be "Line A"
    When I choose to edit the "Description" text
    Then the value of "Description" should be "Line A"
    When I save the page
    Then I should not still receive a "concurrency" popup warning
    When I choose to edit the "Description" text
    Then the value of "Description" should be "Line A"
  
  @fails_webkit
  Scenario: Testing RichText handling of middle newline characters
    When I fill in the field "Description" with the following body:
      """
      Line A

      Line B
      """
    When I save the page
    Then I should not receive a "concurrency" popup warning
    Then the value of "Description" should be  "Line A Line B"
    When I choose to edit the "Description" text
    # BUG: Capybara-webkit appears to be doubling the amount of newlines when inserting, so this step will fail
    Then the value of the field "Description" should contain:
      """
      Line A

      Line B
      """

  Scenario: Testing RichText handling of US-ASCII characters
    # Two Different dashes at the end, first one is US-ASCII, second is UTF-8
    When I set the "Description" to "This is George's Description – -"
    Then the value of "Description" should be "This is George's Description – -"
    When I save the page
    Then I should not still see a "concurrency" popup warning
    Then the value of "Description" should be "This is George's Description – -"
    When I save the page
    Then I should not still see a "concurrency" popup warning
    Then the value of "Description" should be "This is George's Description – -"

  Scenario: Uploading an image (Confirmed)
    When I set the "Description" to "New Description"
    And I choose to upload an image for the "Description" text
    And I upload the image "picture2.jpeg"
    And I confirm the image upload
    Then the value of "Description" should include 'New Description'
    Then the value of "Description" should include '![Image Caption](description/images/picture2.jpeg "Image Title")'
    When I save the page
    Then the value of "Description" should be "New Description"
    And the "Description" text should contain the image "picture2.jpeg"
    When I choose to edit the "Description" text
    And I choose to upload an image for the "Description" text
    And I upload the image "default_user_replaceme.jpeg"
    And I confirm the image upload
    Then the value of "Description" should include 'New Description'
    Then the value of "Description" should include '![Image Caption](description/images/picture2.jpeg "Image Title")'
    Then the value of "Description" should include '![Image Caption](description/images/default_user_replaceme.jpeg "Image Title")'
    When I save the page
    Then the value of "Description" should be "New Description"
    And the "Description" text should contain the image "default_user_replaceme.jpeg"
    And the "Description" text should contain the image "picture2.jpeg"
    When I choose to edit the "Description" text
    Then the value of "Description" should include 'New Description'
    Then the value of "Description" should include '![Image Caption](description/images/picture2.jpeg "Image Title")'
    Then the value of "Description" should include '![Image Caption](description/images/default_user_replaceme.jpeg "Image Title")'
  
  Scenario: Uploading an image (Canceled)
    When I set the "Description" to "New Description"
    And I choose to upload an image for the "Description" text
    And I upload the image "picture2.jpeg"
    And I confirm the image upload
    Then the value of "Description" should include 'New Description'
    Then the value of "Description" should include '![Image Caption](description/images/picture2.jpeg "Image Title")'
    When I save the page
    Then the value of "Description" should be "New Description"
    And the "Description" text should contain the image "picture2.jpeg"
    When I choose to edit the "Description" text
    And I choose to upload an image for the "Description" text
    And I should see the "Upload Image" popup
    And I upload the image "default_user_replaceme.jpeg"
    And I cancel the image upload
    Then I should not see the "Upload Image" popup
    Then the value of "Description" should include 'New Description'
    Then the value of "Description" should include '![Image Caption](description/images/picture2.jpeg "Image Title")'
    When I save the page
    Then the value of "Description" should be "New Description"
    And the "Description" text should contain the image "picture2.jpeg"
  
  Scenario: Deleting an uploaded image (Confirmed)
    Given I set the "Description" to "New Description"
    And I choose to upload an image for the "Description" text
    And I upload the image "picture2.jpeg"
    And I confirm the image upload
    And I choose to upload an image for the "Description" text
    And I should see the "Upload Image" popup
    And I upload the image "default_user_replaceme.jpeg"
    And I confirm the image upload
    And I save the page
    And I choose to edit the "Description" text
    And I choose to upload an image for the "Description" text
    When I delete the image "picture2.jpeg"
    And I confirm the delete richtext image popup
    And I cancel the image upload
    And I save the page
    Then the "Description" text should not contain the image "picture2.jpeg"
  
  Scenario: Deleting an uploaded image (Canceled)
    Given I set the "Description" to "New Description"
    And I choose to upload an image for the "Description" text
    And I upload the image "picture2.jpeg"
    And I confirm the image upload
    And I choose to upload an image for the "Description" text
    And I upload the image "default_user_replaceme.jpeg"
    And I confirm the image upload
    And I save the page
    And I choose to edit the "Description" text
    And I choose to upload an image for the "Description" text
    When I delete the image "picture2.jpeg"
    And I cancel the delete richtext image popup
    And I cancel the image upload
    And I save the page
    Then the "Description" text should contain the image "picture2.jpeg"
    Then the "Description" text should contain the image "default_user_replaceme.jpeg"
  
  Scenario: Deleting an uploaded image and continuing to edit
    Given I set the "Description" to "New Description"
    And I choose to upload an image for the "Description" text
    And I upload the image "picture2.jpeg"
    And I confirm the image upload
    And I save the page
    And I choose to edit the "Description" text
    And I choose to upload an image for the "Description" text
    And I delete the image "picture2.jpeg"
    And I confirm the delete richtext image popup
    And I cancel the image upload
    And I choose to upload an image for the "Description" text
    And I upload the image "default_user_replaceme.jpeg"
    And I confirm the image upload
    And I save the page
    Then the "Description" text should contain the image "default_user_replaceme.jpeg"
    
  Scenario: Uploading an image on a through class association
    Given I go to the previous page
    And I expand the "People" header
    And I edit the "People" whose "Name" is "Bob"
    And I click on the "Drives" header
    And I click the "Edit Driving" link
    When I set the "Car Review" to "New Car Review"
    And I choose to upload an image for the "Car Review" text
    And I upload the image "default_user_replaceme.jpeg"
    And I confirm the image upload
    When I click on the "Driver" header
    And I set the "Description" to "New Description"
    And I choose to upload an image for the "Description" text
    And I upload the image "picture2.jpeg"
    And I confirm the image upload
    And I save the page
    When I click on the "Driver" header
    Then the "Description" text should contain the image "picture2.jpeg"
    And the "Car Review" text should contain the image "default_user_replaceme.jpeg"