# --------------------------------------- Uniqueness helpers
def id_for_popup(type)
  # This might not work if active support isn't required
  "##{type.underscore.gsub(/\s+/, '_').camelize(:lower)}Modal"
end

# Find a popup modal window given the 'type' of the popup.
# In many cases, the type is just the first portion of the modal's ID
#   so the ID can be determined as "##{type}Modal".
# However, in some cases (not sure which exactly), the code in id_for_popup is required
#   to determine the id. -SD 
def find_popup(type)
  popup = find(id_for_popup(type))
  # I believe this sleep is necessary because the model fades in. -SD
  # TODO: Find a way to remove these sleeps
  sleep(0.1)
  popup
end

# Verifies that a popup does not appear
# Uses a small sleep to allow some time for the popup to appear and thus fail the check
def ensure_popup_does_not_appear(type)
  sleep(0.1)
  ensure_popup_is_closed(type)
end

# Verifies that a popup that was present is closed
def ensure_popup_is_closed(type)
  # I believe this sleep is necessary because the model fades out. -SD
  sleep(0.1)
  expect(page).not_to have_css(id_for_popup(type))
end
  
def click_popup_button(type, button)
  # popup_container(type).click_button(button)
  # result = find('#closeDiscardChangesModal')
  # page.evaluate_script("$('##{type.underscore.gsub(/\s+/, '_').camelize(:lower)}Modal').modal('hide')")
  
  button_element = find_popup(type).find('button', :text => button)
  button_element.click
  # Sleep if the popup is not being dismissed. In that case, we can just
  #   verify that the popup has been closed.
  # The only case I can identify where a button is clicked on a popup and the popup isn't dismissed
  #  are the pagination related buttons on association creation dialogs.
  # TODO: replace sleep with code that checks that pagination buttons work appropriately
  #       e.g. check that page was changed successfully, number of results limited... -SD
  if ['Go', '<', '>'].include?(button) || button.match?(/^\d+$/)
    sleep(0.1)
  else
    ensure_popup_is_closed(type)
  end
end

def cancel_popup(type)
  sleep 0.1
  find_popup(type).click_button('Cancel')
  sleep 0.1
  ensure_popup_is_closed(type)
end

def abandon_creation_and_view_existing(type)
  find_popup(type).click_button('Abandon Creation & View Existing Item')
end

def abandon_creation_and_use_existing(type)
  find_popup(type).click_button('Abandon Creation & Use Existing Item')
end

def continue_anyway(type)
  find_popup(type).click_button('Continue Anyway')
end

def merge(type)
  find_popup(type).click_button('Merge')
end

def abandon_changes(type)
  find_popup(type).click_button('Abandon Your Changes')
end

def verify_popup(type, negative = false)
  if negative
    ensure_popup_does_not_appear(type)
    nil
  else
    find_popup(type).should_not == nil
  end
end

def verify_popup_conflict(type, negative = false, state = nil)
  popup = verify_popup(type, negative)
  expect(popup.has_class?(state)).to(be true) if popup
end

def select_conflicting(type, conditions)
  select_rows(conflicting_collection_widget(type), conditions)
end

def conflicting_collection_widget(type)
  find_popup(type).find('.collection', :visible => true)
end
# --------------------------------------- End Uniqueness helpers