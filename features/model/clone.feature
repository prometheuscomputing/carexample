# Test clone functionality
Feature: Object Cloning
  In order to save time when creating objects similar to an existing object
  As a user
  I want to clone an existing object
  
  Background:
    Given I have reset the database
    And I have logged in as a user
  
  Scenario: Cloning an object with no composer associations
    Given I expand the "People" header
    And I edit the "People" whose "Name" is "Bob"
    When I click the "Clone" button
    Then I should be prompted to clone this "Person" and its "Addresses"
    When I confirm the clone
    Then I should see the success message "Successfully cloned Person. Now viewing clone."
    And I should see both the original "Person" and the clone in the breadcrumbs
    # Confirm that compositions are clones as well
    When I click on the "Addresses" header
    And I edit the "Address" where "Street Number" is "355"
    And I set the "Street Name" to "Biltless St."
    And I save the page
    And I go back "2" pages
    And I click on the "Addresses" header
    Then I should see an "Address" where "Street Number" is "355" and "Street Name" is "Biltmore St."
  
  # This issue fixed by SSA v5.7.1
  Scenario: Cloning an object with no composer association (polymorphic example)
    Given I expand the "Vehicles" header
    And I edit the "Vehicle" whose "Vehicle Model" is "S80"
    When I click the "Clone" button
    Then I should be prompted to clone this "Car" and its "Vin" and "Components"
    When I confirm the clone
    Then I should see the success message "Successfully cloned Car. Now viewing clone."
    And I should see both the original "Car" and the clone in the breadcrumbs
  
  Scenario: Cloning an object with no composer association and canceling the clone
    Given I expand the "People" header
    And I edit the "People" whose "Name" is "Bob"
    When I click the "Clone" button
    Then I should be prompted to clone this "Person" and its "Addresses"
    When I cancel the clone
    Then I should see only one "Person" in the breadcrumbs
  
  Scenario: Cloning an object into the same composer
    Given I expand the "People" header
    And I edit the "People" whose "Name" is "Bob"
    And I click on the "Addresses" header
    And I edit the "Address" where "Street Number" is "355"
    When I click the "Clone" button
    # This pause necessary in some environments
    And I wait for 0.5 seconds
    Then I should be prompted to clone this "Address"
    When I choose to clone the "Address" into the same "Person"
    And I confirm the clone
    Then I should see the success message "Successfully cloned Address. Now viewing clone."
    When I set the "Street Name" to "New Street"
    And I set the "Street Number" to "123"
    And I save the page
    And I go back "2" pages
    Then I should see that there are a total of "3" "Addresses" in the header
    When I click on the "Addresses" header
    # Verify original
    Then I should see an "Address" where "Street Name" is "Biltmore St." and "Street Number" is "355"
    # Verify clone
    Then I should see an "Address" where "Street Name" is "New Street" and "Street Number" is "123"
  
  Scenario: Cloning an object into a new composer
    Given I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I click on the "Components" header
    And I edit the "Component" where "Serial" is "12"
    When I click the "Clone" button
    Then I should be prompted to clone this "Component" and its "Parts"
    When I choose to clone the "Component" into a new "Motorcycle"
    # This shouldn't have been passing earlier. It has the JS now to pass, but it isn't passing in the cukes for some reason.
    #   It's not necessary either. We can confirm that it was selected by the later steps.
    # Then the new composer option should be selected
    And I confirm the clone
    Then I should see the success message "Successfully cloned Component. Now viewing clone."
    # Verify new Vehicle
    When I click on the "Vehicle" header
    And I edit the "Vehicle"
    Then I should be at a "Motorcycle" page
    And the value of "Vehicle Model" should be ""
    And I should see that there are a total of "1" "Components" in the header
    # Verify cloned Parts
    When I click on the "Components" header
    And I edit the "Component"
    And I click on the "Parts" header
    And I edit the "Part" where "Name" is "Alternator"
    And I set the "Name" to "Wombat"
    And I save and leave the page
    # Should be Home > Car > Component > Component  at this point
    And I go to the previous page
    And I click on the "Parts" header
    Then I should see a "Part" where "Name" is "Alternator"
    And I should not see a "Part" where "Name" is "Wombat"
  
  Scenario: Cloning an object into an existing composer
    Given I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I click on the "Components" header
    And I edit the "Component" where "Serial" is "12"
    When I click the "Clone" button
    Then I should be prompted to clone this "Component" and its "Parts"
    When I choose to clone the "Component" into the "Vehicle" where "Vehicle Model" is "S40"
    And I confirm the clone
    Then I should see the success message "Successfully cloned Component. Now viewing clone."
    # Verify existing Vehicle
    When I click on the "Vehicle" header
    And I edit the "Vehicle"
    Then the value of "Vehicle Model" should be "S40"
    And I should see that there are a total of "1" "Components" in the header
    # Verify cloned Parts
    When I click on the "Components" header
    And I edit the "Component"
    And I click on the "Parts" header
    And I edit the "Part" where "Name" is "Alternator"
    And I set the "Name" to "Wombat"
    And I save and leave the page
    # Should be Home > Car > Component > Component  at this point
    And I go to the previous page
    And I click on the "Parts" header
    Then I should see a "Part" where "Name" is "Alternator"
    And I should not see a "Part" where "Name" is "Wombat"
  
  Scenario: Cloning a root object into no composer
    Given I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I click on the "Components" header
    And I edit the "Component" where "Serial" is "12"
    When I click the "Clone" button
    Then I should be prompted to clone this "Component" and its "Parts"
    When I choose the composer association "None"
    And I confirm the clone
    Then I should see the success message "Successfully cloned Component. Now viewing clone."
    # Verify no Vehicle
    And I should see that there are a total of "0" "Vehicle" in the header
    # Verify cloned Parts
    And I click on the "Parts" header
    And I edit the "Part" where "Name" is "Alternator"
    And I set the "Name" to "Wombat"
    And I save and leave the page
    # Should be Home > Car > Component > Component  at this point
    And I go to the previous page
    And I click on the "Parts" header
    Then I should see a "Part" where "Name" is "Alternator"
    And I should not see a "Part" where "Name" is "Wombat"
  
  Scenario: Cloning an object into a new composer of a different type
    Given I click on the "Countries" header
    And I edit the "Countries" where "Name" is "USA"
    And I click on the "States" header
    And I edit the "State" where "Name" is "North Carolina"
    And I click on the "Cities" header
    And I edit the "Cities" where "Name" is "Cullowhee"
    When I click the "Clone" button
    Then I should be prompted to clone this "City"
    When I choose the composer association "Territory"
    And I choose to clone the "City" into a new "Territory"
    And I confirm the clone
    Then I should see the success message "Successfully cloned City. Now viewing clone."
    And I should see that there are a total of "0" "State" in the header
    And I should see that there are a total of "1" "Territory" in the header
    # Test cloning again into new State
    When I click the "Clone" button
    Then I should be prompted to clone this "City"
    When I choose the composer association "State"
    And I choose to clone the "City" into a new "State"
    And I confirm the clone
    Then I should see the success message "Successfully cloned City. Now viewing clone."
    And I should see that there are a total of "1" "State" in the header
    And I should see that there are a total of "0" "Territory" in the header
  
  Scenario: Cloning an object into an existing composer of a different type
    # Will test both ways to ensure ordering has no effect
    Given I click on the "Countries" header
    And I edit the "Countries" where "Name" is "USA"
    And I click on the "States" header
    And I edit the "State" where "Name" is "North Carolina"
    And I click on the "Cities" header
    And I edit the "Cities" where "Name" is "Cullowhee"
    When I click the "Clone" button
    Then I should be prompted to clone this "City"
    When I choose the composer association "Territory"
    And I choose to clone the "City" into the "Territory" where "Name" is "Philippines"
    And I confirm the clone
    Then I should see the success message "Successfully cloned City. Now viewing clone."
    And I should see that there are a total of "0" "State" in the header
    And I should see that there are a total of "1" "Territory" in the header
    When I click on the "Territory" header
    And I edit the "Territory"
    Then the value of "Name" should be "Philippines"
    # Test cloning again into existing State
    Given I go to the previous page
    When I click the "Clone" button
    Then I should be prompted to clone this "City"
    When I choose the composer association "State"
    And I choose to clone the "City" into the "State" where "Name" is "Maryland"
    And I confirm the clone
    Then I should see the success message "Successfully cloned City. Now viewing clone."
    And I should see that there are a total of "1" "State" in the header
    And I should see that there are a total of "0" "Territory" in the header
    When I click on the "State" header
    And I edit the "State"
    Then the value of "Name" should be "Maryland"
  
  Scenario: Checking default selected composer association when cloning
    Given I click on the "Countries" header
    And I edit the "Countries" where "Name" is "USA"
    And I click on the "States" header
    And I edit the "State" where "Name" is "North Carolina"
    And I click on the "Cities" header
    And I edit the "Cities" where "Name" is "Cullowhee"
    When I click the "Clone" button
    Then the selected composer association should be "State"
    And the same composer option should be selected
    When I choose the composer association "Territory"
    Then the new composer option should be selected
    And I confirm the clone
    When I click the "Clone" button
    Then the selected composer association should be "Territory"
    When I choose the composer association "State"
    Then the new composer option should be selected
    When I cancel the clone
    And I click on the "Territory" header
    And I click the only "Break Association" button under the "Territory" header
    And I save the page
    When I click the "Clone" button
    # Ordering is uncertain unless we decide to alphabetize at some point
    #Then the selected composer association should be "State"
    Then the new composer option should be selected