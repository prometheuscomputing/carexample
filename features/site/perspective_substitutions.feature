Feature: Perspectives
  In order to view data in a different perspective
  As a user
  I want to use one or more perspectives
  
  Background:
    Given I have reset the database
    And I have logged in as an administrator
    And I go to the site administration panel
    And I add the perspectives "Stakeholder", "Accounting", and "Executive"
    And I edit the perspective named "Stakeholder"
    And I enter the substitution "Electric Vehicle" to "Pev"
    And I submit the page
    And I enter the substitution "Sport Utility Vehicle" to "SUV"
    And I submit the page
    And I enter the substitution "electric efficiency" to "elec. eff."
    And I submit the page
    And I go to the site administration panel
    And I edit the perspective named "Accounting"
    And I enter the substitution "Developer" to "Software engineer"
    And I submit the page
    And I go to the home page
    And I expand the "People" header
    And I edit the "People" whose "Name" is "Bob"
    And I set the "Description" to "Developer who worked on the Electric Vehicle with an electric efficiency of 20%"
    And I save the page
    And I sign out
    And I have logged in as a user
  
  # Fails because perspective substitutions are currently broken
  Scenario: Using a perspective
    Given I go to the preferences panel
    And I add myself to the "Stakeholder" perspective
    And I submit the page
    And I go to the home page
    And I expand the "People" header
    When I edit the "People" whose "Name" is "Bob"
    Then the value of "Description" should be "Developer who worked on the Pev with an elec. eff. of 20%"
  
  Scenario: Using multiple perspectives
    Given I go to the preferences panel
    And I add myself to the "Stakeholder" and "Accounting" perspectives
    And I submit the page
    And I go to the home page
    And I expand the "People" header
    When I edit the "People" whose "Name" is "Bob"
    Then the value of "Description" should be "Software engineer who worked on the Pev with an elec. eff. of 20%"
  
  Scenario: Maintaining capitalization when transforming
    Given I go to the preferences panel
    And I add myself to the "Stakeholder" and "Accounting" perspectives
    And I submit the page
    And I go to the home page
    And I expand the "People" header
    When I edit the "People" whose "Name" is "Bob"
    And I set the "Description" to "Enjoys driving a Sport Utility Vehicle"
    And I save the page
    Then the value of "Description" should be "Enjoys driving a SUV"
  
  # TODO: define behavior in this situation
  # Scenario: Using perspectives that define differing substitutions
  #   Given I sign out
  #   And I sign in as an administrator
  #   And I go to the preferences panel
  #   And I edit the perspective named  "Accounting"
  #   And I enter the substitution "Electric Vehicle" to "Personal E.V."
  #   And I submit the page
  #   And I sign out
  #   And I sign in as a user
  #   And I expand the "People" header
  #   And I edit the "People" whose "Name" is "Bob"