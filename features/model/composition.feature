# Test composition association properties:
#  * Cascading deletion

# Also test composer association properties
#  * Should function normally
Feature: Compositions
  In order to change an object's composition associations to other objects
  As a user
  I want to manipulate its composition associations
  
  Background:
    Given I have reset the database
    And I have logged in
    And I click on the "Countries" header
  
  Scenario: Viewing composition and composer associations
    When I edit the "Countries" where "Name" is "USA"
    Then I should see that there are a total of "2" "States" in the header
    And I should see that there are a total of "1" "Territories" in the header
    When I click on the "States" header
    And I edit the "State" where "Name" is "North Carolina"
    And I click on the "Country" header
    Then I should see the associated "Country" where "Name" is "USA"
    And I should see that there are a total of "3" "Cities" in the header
    When I click on the "Cities" header
    And I edit the "Cities" where "Name" is "Cullowhee"
    And I click on the "State" header
    Then I should see the associated "State" where "Name" is "North Carolina"

  Scenario: Deleting an object with a single level of composition objects
    Given I expand the "People" header
    And I select the "People" whose "Name" is "Bob"
    And I click the only "Delete" button under the "People" header
    Then I should not be warned that I will also delete a "Binary Data"
    And I should be warned that I will also delete "2" "Addresses"
    And I should be warned that I will also delete an "Address" where "street_name" is "Biltmore St." and "street_number" is "345"
    And I should be warned that I will also delete an "Address" where "street_name" is "Biltmore St." and "street_number" is "355"
    # NOTE: this step fails inermittently using headless chrome. Need to debug -SD
    When I confirm the homepage deletion of the "People"
    And I expand the "People" header
    Then I should see that there are a total of "8" "People" in the header

  
  Scenario: Deleting a composition organizer with a one to one relationship
    Given I expand the "People" header
    When I create a "People" of type "Clown" where "Name" is "Clowney"
    And I save the page
    And I click on the "Dolls" header
    When I create a "Doll" of type "Nesting Doll" where "Color" is "Red"
    And I save the page
    And I click on the "Inner Doll" header
    When I create an "Inner Doll" of type "Nesting Doll" where "Color" is "Green"
    And I save the page
    When I click on the "Outer Doll" header
    And I edit the "Outer Doll" where "Color" is "Red"
    And I click on the "Inner Doll" header
    And I click the only "Delete" button under the "Inner Doll" header
    When I confirm the delete popup
    # Changed functionality, the deleted doll should re-appear in collection content as a pending deletion
    # And I click on the "Inner Doll" header
    Then I should see that there are a total of "1" "Inner Doll" in the header
    And I should see "1" pending deletion "Inner Doll"
    When I save the page
    And I click on the "Inner Doll" header
    Then I should see that there are a total of "0" "Inner Doll" in the header
    And I should see "0" "Inner Doll"
  
  Scenario: Deleting a root level object with multiple levels of composition objects
    When I select the "Countries" where "Name" is "USA"
    And I click the only "Delete" button under the "Countries" header
    Then I should be warned that I will also delete "2" "States", "4" "Cities", and "1" "Territory"
    And I should be warned that I will also delete a "State" named "North Carolina" and a "State" named "Maryland"
    When I confirm the homepage deletion of the "Countries"
    And I click on the "Countries" header
    Then I should see that there are a total of "0" "Countries" in the header
    And the "State" in package "Geography" with id "1" should not exist
    And the "State" in package "Geography" with id "2" should not exist
    And the "City" in package "Geography" with id "1" should not exist
    And the "City" in package "Geography" with id "2" should not exist
    And the "City" in package "Geography" with id "3" should not exist
    And the "City" in package "Geography" with id "4" should not exist
    And the "Territory" in package "Geography" with id "1" should not exist
  
  Scenario: Creating a composition association
    Given I edit the "Countries" where "Name" is "USA"
    When I click on the "States" header
    Then I should not be able to show possible "States"
    When I create a "State" where "Name" is "California"
    And I save and leave the page
    Then I should see that there are a total of "3" "States" in the header
  
  # TODO: Change this step if the expected behavior for composer relationships is changed
  Scenario: Disassociating and reassociating a composer association
    Given I edit the "Countries" where "Name" is "USA"
    And I click on the "States" header
    And I edit the "State" where "Name" is "North Carolina"
    And I click on the "Country" header
    When I click the only "Break Association" button under the "Country" header
    And I save the page
    Then I should see that there are a total of "0" "Country" in the header
    When I click on the "Country" header
    And I show the possible "Country"
    And I select the "Country" where "Name" is "USA"
    And I click the only "Add Association" button under the "Country" header
    And I save the page
    Then I should see that there are a total of "1" "Country" in the header
    
  Scenario: Root Composition association
    Given I expand the "Vehicles" header
    And I edit the "Vehicles" where "Vehicle Model" is "S80"
    When I click on the "Components" header
    Then I should not be able to show possible "Components"
    When I create a "Component" where "Name" is "Trunk"
    And I save and leave the page
    Then I should see that there are a total of "2" "Components" in the header

  # One To Many Recursive Composition
  # Part ----> Parts
  Scenario: Adding and traversing recursive composition associations in a one-to-many association
    Given I expand the "Vehicles" header
    When I edit the "Vehicles" where "Vehicle Model" is "S80"
    And I click on the "Components" header
    And I edit the "Components" where "Name" is "Engine"
    And I click on the "Parts" header
    And I edit the "Parts" where "Name" is "Alternator"
    And I click on the "Part Of" header
    Then I should see "0" "Part Of"
    When I click on the "Sub Parts" header
    Then I should see "0" "Sub Parts"
    When I create a "Sub Parts" of type "Part" where "Name" is "Alternator Brush"
    And I click on the "Part Of" header
    Then I should not be able to show possible "Part Of"
    And I should see that there are a total of "1" "Part Of" in the header
    And I click on the "Sub Parts" header
    Then I should see "0" "Sub Parts"
    And I save the page
    When I click on the "Part Of" header
    Then I should not be able to show possible "Part Of"
    And I should see that there are a total of "1" "Part Of" in the header
    And I click on the "Sub Parts" header
    Then I should see "0" "Sub Parts"
    When I create a "Sub Parts" of type "Part" where "Name" is "Brush Head Bristles"
    And I click on the "Part Of" header
    Then I should not be able to show possible "Part Of"
    And I should see that there are a total of "1" "Part Of" in the header
    And I click on the "Sub Parts" header
    Then I should see "0" "Sub Parts"
    And I save the page
    When I click on the "Part Of" header
    Then I should not be able to show possible "Part Of"
    And I should see that there are a total of "1" "Part Of" in the header
    And I click on the "Sub Parts" header
    Then I should see "0" "Sub Parts"
    Then the value of "Name" should be "Brush Head Bristles"
    And I edit the "Part Of" where "Name" is "Alternator Brush"
    Then the value of "Name" should be "Alternator Brush"
    When I click on the "Part Of" header
    And I edit the "Part Of" where "Name" is "Alternator"
    Then the value of "Name" should be "Alternator"

  # One To One Recursive Composition
  # Clown --> NestingDolls 1--> <--1
  
  Scenario: Adding and traversing recursive composition associations in a one-to-one association
    Given I expand the "People" header
    When I create a "People" of type "Clown" where "Name" is "Clowney"
    And I save the page
    And I click on the "Dolls" header
    When I create a "Doll" of type "Nesting Doll" where "Color" is "Red"
    And I save the page
    And I click on the "Inner Doll" header
    When I create an "Inner Doll" of type "Nesting Doll" where "Color" is "Green"
    And I save the page
    And I click on the "Inner Doll" header
    When I create an "Inner Doll" of type "Nesting Doll" where "Color" is "Blue"
    And I save the page
    Then the value of "Color" should be "Blue"
    When I click on the "Outer Doll" header
    And I edit the "Outer Doll" where "Color" is "Green"
    Then the value of "Color" should be "Green"
    When I click on the "Outer Doll" header
    And I edit the "Outer Doll" where "Color" is "Red"
    Then the value of "Color" should be "Red"