# Helpers specific to Collections

# --------------------------------------- Deletion
def confirm_homepage_delete(header)
  confirm_delete_popup
  # Homepage deletes currently reload the page. They do not re-expand the collection, so use this
  # condition to wait for the reload to finish.
  begin
    get_header(header, :collapsed => true)
  rescue
    sleep 5
    get_header(header, :collapsed => true)
  end
end
# Note that other helpers used by collections (such as confirm_delete_popup) can be found 
#   in attribute_helpers.rb. I'm not sure why they're located there. -SD
# --------------------------------------- End Deletion


# --------------------------------------- Show Possible Associations
def toggle_show_possible(header)
  widget = get_widget(header)
  is_widget_collapsed = widget.has_class?('collapsed')
  puts "Usage Error: Calling 'toggle_show_possible' method on widget that is collapsed." if is_widget_collapsed
  expect(is_widget_collapsed).to eq(false)
  get_show_possible(header, widget).click
end
def show_possible(header)
  show_possible_checkbox = get_show_possible(header)
  show_possible_checkbox.click
end
def show_current(header)
  show_possible_checkbox = get_show_possible(header)
  show_possible_checkbox.click
end
def get_show_possible(header, widget = nil)
  widget ||= get_widget(header)
  #widget.find('input.toggle_view_all_listings')
  # Find label
  label = widget.find('.toggle_view_all_listings_label')
  # get containing div
  label.find(:xpath, '..')
end
def verify_show_possible_not_present(header)
  w = get_widget(header)
  expect(w.first('input.toggle_view_all_listings', :minimum => 0)).to be_nil
end
# --------------------------------------- End Show Possible Associations


# --------------------------------------- Toggle Filter
def toggle_filter_for(header)
  case Capybara.current_driver
  when :chrome, :headless_chrome
    get_show_filter_link(header).click
  else # capybara-webkit
    # TODO: switch this back to .click when positioning bug is fixed
    # Currently, calling .click will fail to click due to overlapping element at position 0,0, which is incorrect -SD
    get_show_filter_link(header).trigger('click')
  end
end
def open_filter_for(header)
  toggle_filter_for(header) unless filter_is_visible?(header)
end
def get_show_filter_link(header)
  w = get_widget(header)
  w.find('th.filter_toggle_cell').find('.search-icon')
end
def filter_is_visible?(header)
  w = get_widget(header)
  w.find('tr.table_filter_row').visible?
end
def open_advanced_filter(header)
  get_advanced_filter_link(header).click
end
def get_advanced_filter_link(header)
  w = get_widget(header)
  w.find('.advanced_search_link')
end
def open_popup_advanced_filter(header)
  popup_widget_header = get_popup_widget(header)
  toggle_filter_for(popup_widget_header)
  get_advanced_filter_link(popup_widget_header).click
end

# --------------------------------------- End Toggle Filter


# --------------------------------------- Filter Entry
# TODO: speed up this method. It is very slow
def enter_filter(header, filters)
  equals_filter = filters[0]
  contains_filter = filters[1]
  # not_equals_filter = filters[2]
  # not_contains_filter = filters[3]
  # Currently gui_site does not have a way to specify equals.
  filter = contains_filter.merge(equals_filter)
  w = get_widget(header)
  table = get_table_element(w)
  filter_row = w.first('.table_filter_row')
  unless filter_row
    # Click on the magnifying glass, and look again for filter
    w.first('.filter_toggle_cell').click
    w.find('.table_filter_row')
  end
  headers_with_indices, num_headers = get_table_headers_with_indices(table, :specific_col_texts => filter.keys)
  filter.each do |attribute, value|
    col_index = headers_with_indices.select{|h| h[1] == attribute}.first[0]
    f = table.all('.table_filter_row > td')[col_index].first('.filter')
    if f
      case f.tag_name
      when 'input'
        f.set(value)
      when 'select'
        value.delete!(' ') if attribute == 'Type'
        value = '-' if value.empty? # Empty value on choice is denoted by hyphen
        f.find('option', :text => value).select_option
      else
        puts "Unknown type for filter #{attribute}"
      end
    else
      raise "Could not find filter for attribute #{attribute} among #{input_names.inspect}"
    end
  end
end

def enter_search_all_filter(header, search_terms)
  search_terms = search_terms
  # puts filter.inspect
  # puts search_terms.inspect
  w = get_widget(header)
  table = get_table_element(w)
  search_all_filter_row = table.find('.table_search_all_filter_row')
  search_all_filter_input = search_all_filter_row.find('.search_all_filter')
  search_all_filter_input.set(search_terms)
end

def reset_filter(header)
  w = get_widget(header)
  w.click_button('Clear Filter')
  # Should really only be using functionality open to the user.
  # empty_filter = {}
  # get_collection_attributes(header).each do |attribute|
  #   empty_filter[attribute] = ''
  # end
  # enter_filter(header, [empty_filter, {}, {}, {}])
end
# --------------------------------------- End Filter Entry


# --------------------------------------- Pagination Controls
def submit_search(header)
  w = get_widget(header)
  w.click_button('Go')
end
def set_items_per_page(header, items_per_page)
  scope = page
  scope = get_widget(header) if header
  input = scope.find('input[name=page_size]')
  # On linux, using chromedriver v76, input.set appends the new value to the existing value.
  # Setting to nil first seems to correct this.
  input.set(nil)
  input.set(items_per_page)
end
def set_page(header, page_number)
  w = get_widget(header)
  # w.find('input[name=page_number]').set(page_number)
  w.find('li.page-item a', :text => page_number).click
end
def confirm_current_page page_index
  # current_page_index = find_element_by_selector('input[name="page_number"]').value
  current_page_index = find_element_by_selector('li.page-item.active a').text.to_i
  expect(current_page_index).to eq(page_index)
end
def confirm_total_pages total_pages
  # current_total_pages = find_element_by_selector('span.total_pages').text
  current_total_pages = all('li.page-item a')[-2].text.to_i
  expect(current_total_pages).to eq(total_pages)
end

def confirm_search_results_total search_results
  search_results_on_page = find_element_by_selector('span.search_results_count').text
  search_results = search_results.to_i
  if search_results < 1
    expect(search_results_on_page).to eq("Search found no results.")
  elsif search_results == 1
    expect(search_results_on_page).to eq("Search found #{search_results} record.")
  else
    expect(search_results_on_page).to eq("Search found #{search_results} records.")
  end
end
# --------------------------------------- End Pagination Controls


# --------------------------------------- Bottom Controls
def associate_to_object(header, conditions, through = nil, through_attributes = nil, already_expanded = false)
  page_type = get_page_type
  click_header(header) unless already_expanded
  if through
    create_via_dropdown(header, through)
    enter_attributes(through_attributes) if through_attributes
    click_header(header)
  end
  toggle_filter_for(header)
  enter_filter(get_widget(header), conditions)
  show_possible(header)
  select_rows(header, [{}, {}, {}, {}])
  click_button_under_header("Add Associations", header)
  save_object_page
  go_to_last(page_type)
  click_header(header)
end

def associate_to_new_object(header, type, attributes = nil, go_back = false, through = nil, through_attributes = nil, already_expanded = false)
  # FIXME #get_page_type makes the assumption that the page we are leaving has a breadcrumb.  Why in the world would you have to have a breadcrumb on the page you are leaving in order to go back to it from the page you are going to?? Writing methods like this means that they break if you change the underlying implementation.
  page_type = get_page_type if go_back
  click_header(header) unless already_expanded
  if through
    create_via_dropdown(header, through)
    enter_attributes(through_attributes) if through_attributes
    click_header(header)
  end
  create_via_dropdown(header, type, attributes)
  if go_back
    save_object_page
    go_to_last(page_type)
    click_header(header)
  end
end
# Creates a new object using a dropdown menu
def create_via_dropdown(header, type = nil, attributes = nil)
  w = get_widget(header)
  raise "Could not find widget for header: #{header}" unless w
  creation_dropdown_button = w.find('.dropdown-toggle')
  # creation_dropdown_link.hover
  # Sleep so dropdown has time to be setup by JS?
  sleep 0.1
  creation_dropdown_button.click
  
  creation_dropdown = w.find('.dropdown-menu', :visible => true)
  # creation_dropdown = w.find('.dropdown_menu', :visible => true)
  
  creation_links = creation_dropdown.all('a.traversal_link')
  if type
    creation_dropdown.has_css?(type, :visible => true)
    creation_link = creation_dropdown.find(:link, :text => type, :visible => true)
    # creation_link ||= creation_dropdown.find(:link, :text => type, :visible => false)
    # NOTE: hovering on the creation dropdown link a second time was added here to prevent 
    #       intermittent failures when clicking on the creation link (due to the item not being visible).
    creation_link.click
    # creation_dropdown_link.hover
    # creation_link.hover
    # creation_link.click
  else # No type given. Click on link with same text as header or click first link in list
    header_link = creation_links.select{|cl| cl.text == header}.first
    header_link ? header_link.click : creation_links.first.click
  end
  enter_attributes(attributes) if attributes && attributes.any?
end
def verify_creation_dropdown(header, negate = false)
  w = get_widget(header)
  w.should_unless(negate, have_css('ul.dropdown_menu'))
end
# --------------------------------------- End Bottom Controls

# --------------------------------------- Table Rows
def select_rows(header, conditions)
  rows = get_rows(header, conditions)
  rows.each do |row|
    # row.find('td.checkbox').first('input').set(true)
    case Capybara.current_driver
    when :webkit
      row.find('td.checkbox').first('.custom-control').click
    else
      row.find('td.checkbox .custom-control').click
    end
  end
end

def edit_row(header, conditions)
  # get_rows call does not work on the newer capybara 2.+. It never finds anything past the header and filter rows, reguardless of wait.
  row = get_rows(header, conditions).first
  raise "Could not find #{header} matching conditions: #{conditions.inspect}" unless row
  row.all('a').select{|link| link.text.include?('Edit')}.first.click
end

def get_row_position(header, conditions)
  get_row_index(header, conditions) + 1
end

def get_row_index(header, conditions)
  _, index = get_rows_with_indices(header, conditions).first
  index
end

def drag_row_to_position(header, conditions, position)
  get_widget(header)
  _, index = get_rows_with_indices(header, conditions).first
  destination_index = position - 1
  
  case Capybara.current_driver
  when :chrome, :firefox, :poltergeist, :headless_chrome
    # Bad DOM manipulation since nothing seems to work in selenium.
    move_row_to_index(header.to_underscored_table_name, index, destination_index)
  when :webkit
    # simulate_drag_row_to_index(header.to_underscored_table_name, index, destination_index)
    move_row_to_index(header.to_underscored_table_name, index, destination_index)
    # drag_row_to_index(header, index, destination_index)
  # when :poltergeist
  #   drag_row_to_index(header, index, destination_index)
  else
    raise "drag_row_to_position currently does not support: #{Capybara.current_driver}"
  end
  # These don't currently work but might in future (Capybara's #drag_to implementation is incompatible with JQueryUI's sortable function)
  #destination_row = get_row_at_index(header, destination_index)
  #options_row = w.find('tr.options') # Might need this if we need to drag to destination + 1
  #row.drag_to destination_row
  #row.drag_to destination_row.first('td')
end

def document_drag_row_to_position(header, conditions, position)
  w = get_widget(header)
  _, index = get_rows_with_indices(header, conditions).first
  destination_index = position - 1

  document_move_row_to_index(w, index, destination_index)
end

# Set a row to a given position via the accessible interface option
def set_row_to_position(header, conditions, position)
  get_widget(header)
  row, _ = get_rows_with_indices(header, conditions).first
  row.first('.position_field').set(position)
end

def edit_association_class_row(header, conditions, association_class_name = nil)
  row = get_rows(header, conditions).first
  if association_class_name
    row.click_link "Edit #{association_class_name}"
  else
    row.all('a').select{|link| link.text.include?('Edit')}.last.click
  end
end

def row_is_visible?(header, conditions)
  get_rows(header, conditions).any?
end

def verify_associtation_row_state(header, conditions, state = '', pending = false)
  pending_delete_class = 'pending_deletion'
  # pending association class is the same for break/add. They are never both displayed at same time
  pending_association_change_class = 'pending_assoc'
  unassociation_class = 'unassociated_object'
  expect(get_rows(header, conditions).count).to eq(1)
  row = get_rows(header, conditions).first
  # deletion, addition, breaking
  if pending
    case state
    when 'deletion'
      pending_delete = true
      expect(row[:class]).to match(pending_delete_class)
    when 'addition'
      pending_assoc_add = true
    when 'breaking'
      pending_assoc_break = true
      expect(row[:class]).to match(pending_association_change_class)
      expect(row[:class]).to match(unassociation_class)
    end
  else
    case state
    when 'deletion'
      pending_assoc_add = true
      # not really a pending_assoc, just skipping the 'not delete' check below
    when 'addition'
      pending_delete = true
      pending_assoc_break = true
      # not really a delete, just skipping the 'not delete' check below
    when 'breaking'
      pending_delete = true
      pending_assoc_add = true
      # not really a delete, just skipping the 'not delete' check below
    else
      pending_delete = false
      pending_assoc_add = false
      pending_assoc_break = false
    end
  end

  expect(row[:class]).not_to have_content(pending_delete_class) if !pending_delete
  expect(row[:class]).not_to have_content(pending_association_change_class) if !(pending_assoc_add || pending_assoc_break)
  expect(row[:class]).not_to have_content(unassociation_class) if !pending_assoc_break
end

# Checks that the currently shown rows match a given filter
def verify_collection_filter(header, filters)
  equals_filter = filters[0]
  contains_filter = filters[1]
  not_equals_filter = filters[2]
  not_contains_filter = filters[3]
  keys = equals_filter.keys | contains_filter.keys | not_equals_filter.keys | not_contains_filter.keys
  collection_data = get_collection(header, keys)
  collection_data.each do |row_hash|
    expect(row_hash).to have_values(equals_filter)
    expect(row_hash).to contain_values(contains_filter)
    expect(row_hash).not_to have_values(not_equals_filter) unless not_equals_filter.empty?
    expect(row_hash).not_to contain_values(not_contains_filter) unless not_contains_filter.empty?
  end
end

# Returns an array of row elements paired with their indices for each currently shown row
def get_rows_with_indices(header, conditions)
  equals_conditions = conditions[0]
  contains_conditions = conditions[1]
  not_equals_conditions = conditions[2]
  not_contains_conditions = conditions[3]
  keys = equals_conditions.keys | contains_conditions.keys | not_equals_conditions.keys | not_contains_conditions.keys
  collection_data = get_collection(header, keys)
  raise "No collection_data found for #{header}. Keys were: #{keys.inspect}" unless collection_data
  # Get indices of matching rows in data
  matches = collection_data.select{|r|
    r.has_values?(equals_conditions) && 
    r.contains_values?(contains_conditions) && 
    (not_equals_conditions.empty? || !r.has_values?(not_equals_conditions)) && 
    (not_contains_conditions.empty? || !r.contains_values?(not_contains_conditions))
  }
  matches.collect{|match| [match[:row], match[:index]]}
end

# Returns an array of row elements for each currently shown row
def get_rows(header, conditions)
  get_rows_with_indices(header, conditions).collect{|row, _| row}
end

def get_row_at_index(header, index)
  get_rows(header, [{}, {}, {}, {}])[index]
end

# Get the domain object row elements for a given collection widget
def get_collection_rows(collection_widget)
  collection_widget.all('.domain_obj_row')
end
# --------------------------------------- End Table Rows


# --------------------------------------- Tables
# Print the table for the given header to stdout
def print_table(header)
  require 'terminal-table'
  table = get_collection(header)
  unless table.any?
    puts Terminal::Table.new :rows => []
    return
  end
  headers = table.first.keys
  rows = table.collect{|hash| headers.collect {|key| hash[key]} }
  puts Terminal::Table.new :rows => [headers] + rows
end

# Returns an array of hashes with information about each row
# 1. Data from the table as an array of hashes
#    The hashes within this array have 2 special keys: :index and :element
#    :index is the index of the row
#    :element is the row's element
# Can accept a second argument that specifies exact column headers (as an array of Strings)
def get_collection(header, specific_columns = nil)
  widget = get_widget(header)
  if widget.has_class? 'document_association'
    if widget.has_class? 'list_widget'
      get_document_association_list(widget, specific_columns)
    elsif widget.has_class? 'table_widget'
      get_document_association_table(widget, specific_columns)
    end
  else
    if widget.has_class? 'table_widget'
      get_collection_table(widget, specific_columns)
    else
      raise "Don't know how to get collection for #{header}. Classes were: #{widget.classes.inspect}"
    end
  end
end

def get_collection_table(widget, specific_columns = nil)
  get_table_data(widget, :specific_col_texts => specific_columns, :ignore_rows => [], :ignore_cols => [0,1])
end
def get_document_association_list(widget, specific_columns = nil)
  get_list_data(widget, :specific_col_texts => specific_columns, :ignore_rows => [], :ignore_cols => [])
end
def get_document_association_table(widget, specific_columns = nil)
  get_table_data(widget, :specific_col_texts => specific_columns, :ignore_rows => [], :ignore_cols => [])
end

# Get the table element from a widget element (or a widget collection element)
def get_table_element(widget)
  container = widget
  unless container.has_class?('content_data')
    # There are a couple cases here. Sometimes there are multiple, pre-loaded .content_data blocks available, and we must pick the first.
    # In other cases, .content_data is populated via ajax and we must use #find to get it (since, unlike #first, #find will wait until it is populated).
    # Handle first case
    inner_container = container.first('.content_data')
    # Handle second case
    inner_container ||= container.find('.content_data')
    container = inner_container
  end
    
  container.tag_name == 'table' ? container : container.find('table')
end

# Returns an array of column header strings
def get_collection_attributes(header)
  w = get_widget(header)
  attributes, num_headers = get_table_headers_with_indices(get_table_element(w)).collect{|_, attribute| attribute}
  # Ignore empty headers or "Edit ..." columns
  attributes.reject!{|attribute| attribute.nil? || attribute.empty? || attribute =~ /^Edit /}
  attributes
end

# Given a table widget, returns an array of hashes of its contents
# Accepts options:
#  :ignore_rows => Array of row indices to ignore. Defaults to [0] to ignore first row (usually a header).
#  :ignore_cols => Array of column indices to ignore. Defaluts to [].
#  :specific_col_texts => Set to an array of column headers to specify a subset of columns to include. Defaults to nil
def get_table_data(widget, options = {})
  table = get_table_element(widget)
  options = {:ignore_rows => [0], :ignore_cols => []}.merge(options)
  table_data = []
  #if widget.has_class?('.document_association')
  attributes, num_headers = get_table_headers_with_indices(table, options)
  trs = table.all(:xpath, './tbody/tr[contains(concat(" ",normalize-space(@class)," ")," domain_obj_row ")]')
  # Get row data for each attribute
  trs.each_with_index do |tr, i|
    next if options[:ignore_rows].include?(i)
    row_data = {}
    row_cells = tr.all(:xpath, './td')
    # Adjust row_cells to match number of headers. This will remove extra left-hand columns that were not visible in the header
    # This happens in the case of td.document_association_item_actions using the Chrome driver, since nothing is visible for that column at the header level
    row_cells = row_cells[(row_cells.count - num_headers)..-1]
    attributes.each do |index, attribute|
      row_data[attribute] = row_cells[index].text.strip
    end
    row_data[:index] = i
    row_data[:row] = tr
    table_data << row_data
  end
  table_data
end

# Given a list widget, returns an array of hashes of its contents
# NOTE: currently checks value of hidden input fields, not pretty_view text.
# Accepts options:
#  :ignore_rows => Array of row indices to ignore. Defaults to [0] to ignore first row (usually a header).
#  :ignore_cols => Array of column indices to ignore. Defaluts to [].
#  :specific_col_texts => Set to an array of column headers to specify a subset of columns to include. Defaults to nil
def get_list_data(widget, options = {})
  options = {:ignore_rows => [0], :ignore_cols => []}.merge(options)
  list_data = []
  #if widget.has_class?('.document_association')
  attributes = get_list_item_attributes_with_indices(widget, options)
  return [] if attributes.empty?
  view_content = widget.first('.content_data')
  rows = view_content.all(:xpath, '*[contains(@class,"domain_obj_row")]', :visible => true)
  # Get row data for each attribute
  rows.each_with_index do |row, i|
    next if options[:ignore_rows].include?(i)
    row_data = {}
    view_content = row.first('.view_content')
    row_widgets = view_content.all('.widget')
    attributes.each do |index, attribute|
      row_data[attribute] = row_widgets[index].first('.widget_data', :visible => false).value
    end
    row_data[:index] = i
    row_data[:row] = row
    list_data << row_data
  end
  list_data
end

# Get table headers and indices with given options
# Returns an array of 2-element arrays where each 2-element array contains:
# 1. index of column in table
# 2. column name
# Accepts options:
#  :ignore_cols => Array of column indices to ignore. Defaluts to [].
#  :specific_col_texts => Set to an array of column headers to specify a subset of columns to include. Defaults to nil
def get_table_headers_with_indices(table, options = {})
  options[:ignore_cols] ||= []
  attributes = []
  header_row = table.all('tr').first
  header_ths = header_row.all('th')
  # Get header texts and store in attributes
  attr_names = []
  header_ths.each_with_index do |th, i|
    next if options[:ignore_cols].include?(i)
    header_text = th.text.strip
    attr_names << header_text
    attributes << [i, header_text] unless options[:specific_col_texts] && !options[:specific_col_texts].include?(header_text)
  end
  missing_attrs = options[:specific_col_texts] - attr_names if options[:specific_col_texts]
  raise "Could not find attributes: #{missing_attrs.inspect} among #{attr_names.inspect} (Note: match is case sensitive)" if missing_attrs.any?
  [attributes, header_ths.count]
end

def get_list_item_attributes_with_indices(widget, options = {})
  options[:ignore_cols] ||= []
  attributes = []
  first_row = widget.first('.domain_obj_row .view_content', :visible => true)
  return [] unless first_row
  row_widgets = first_row.all(:xpath, '*[contains(@class,"widget")]')
  # Get header texts and store in attributes
  attr_names = []
  row_widgets.each_with_index do |w, i|
    next if options[:ignore_cols].include?(i)
    widget_label = w.first('.widget_label', :visible => false, :minimum => 0)
    label = widget_label.text(:all) if widget_label
    attr_names << label if label
    attributes << [i, label] unless options[:specific_col_texts] && !options[:specific_col_texts].include?(label)
  end
  missing_attrs = options[:specific_col_texts] - attr_names if options[:specific_col_texts]
  raise "Could not find attributes: #{missing_attrs.inspect} among #{attr_names.inspect} (Note: match is case sensitive)" if missing_attrs.any?
  attributes
end
# --------------------------------------- End Tables


# --------------------------------------- Association Counts
# Verify association count by text on association header (ex: "5 entries")
def verify_association_header_count(header, number)
  expect(get_association_header_count(header)).to eq(number)
end

def verify_association_search_results_count(header, number)
  expect(get_association_search_results_count(header)).to eq(number)
end

# Get the association count from text on association header
def get_association_header_count(header)
  h = get_header(header)
  # Can either be attached to the header, or in the collection widget
  count_string = h.find('.entry_count').text.strip
  count = case count_string
  when 'None'
    0
  when 'Present'
    1
  when /(\d+) entr(y|ies)/
    $1.to_i
  else
    raise "Unable to get count for header: #{header}"
  end
  count
end

# Get the association count from text on association header
def get_association_search_results_count(header)
  w = get_widget(header)
  count_string = w.find('.search_results_count').text.strip
  count = case count_string
  when 'Search found no results.'
    0
  when 'Search found 1 record.'
    1
  when /Search found (\d+) records./
    $1.to_i
  else
    raise "Unable to get search results count for header: #{header}."
  end
  count
end

# Verify the number and type of shown rows in a collection
def verify_association_contents(header, number, assoc_status = nil)
  w = get_widget(header)
  # Short sleep so that all rows have time to appear. Without this, some are missed intermittently.
  sleep 0.2
  rows = get_collection_rows(w)
  associated_count = 0
  unassociated_count = 0
  pending_count = 0
  pending_deletion = 0

  rows.each do |row|
    if row.has_class?('unassociated_object')
      unassociated_count += 1
    elsif row.has_class?('pending_assoc')
      pending_count += 1
    elsif row.has_class?('pending_deletion')
      pending_deletion += 1
    else
      associated_count += 1
    end
  end

  case assoc_status
  when 'unassociated'
    expect(unassociated_count).to eq(number)
  when 'associated'
    expect(associated_count).to eq(number)
  when 'pending'
    expect(pending_count).to eq(number)
  when 'pending deletion'
    expect(pending_deletion).to eq(number)
  else
    expect(unassociated_count + associated_count + pending_count + pending_deletion).to eq(number)
  end
end
# --------------------------------------- End Association Counts
