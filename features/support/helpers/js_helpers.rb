def define_move_row_to_index
  page.execute_script %!
    function move_row_to_index(row, destination_index) {
      table = row.parent();
      current_index = table.find('.domain_obj_row').index(row);
      row.insertAfter(table.find('.domain_obj_row:eq('+destination_index+')'));
    }
  !
end

def slide_number_slider(widget, new_value)
  page.execute_script %!
    //Programatically set the slider value to simulate a slide
    var widget = $('##{widget[:id]}');
    widget.slider('value', '#{new_value.to_i}');
  !
end

def move_row_to_index(table_name, current_index, destination_index)
  page.execute_script %!
    // Move a row to the given index in the same collection
    function move_row_to_index(row, destination_index) {
      var table = row.parent().parent();
      var current_index = table.find('.domain_obj_row').index(row);
      var destination_row = table.find('.domain_obj_row:eq('+destination_index+')');
      // Move the row to the destination index
      if (destination_index > current_index) {
        row.insertAfter(destination_row);
      } else {
        row.insertBefore(destination_row)
      }
      // Reorder positions since sortable update event was never fired (and is hard to manually fire).
      reorder_positions(row);
      
      // Trigger unsaved positioning flag and display unsaved changes message
      var view_widget = table.parents('.view_widget').first()
      addUnsavedPositioningFlag(view_widget);
      displayWidgetChangesMessage(view_widget);
    }
    var source = $('table[name^=#{table_name}] .domain_obj_row:eq(#{current_index})');
    move_row_to_index(source, #{destination_index});
  !
end

def document_move_row_to_index(widget, current_index, destination_index)
  page.execute_script %!
    // Move a row to the given index in the same collection
    function move_row_to_index(row, destination_index) {
      var table = row.parent().parent();
      var current_index = table.find('.domain_obj_row').index(row);
      var destination_row = table.find('.domain_obj_row:eq('+destination_index+')');
      // Move the row to the destination index
      if (destination_index > current_index) {
        row.insertAfter(destination_row);
      } else {
        row.insertBefore(destination_row);
      }
      // Reorder positions since sortable update event was never fired (and is hard to manually fire).
      document_reorder_positions(row);
      
      // Trigger unsaved positioning flag and display unsaved changes message
      var view_widget = table.parents('.view_widget').first();
      addDocumentUnsavedPositioningFlag(view_widget);
      //displayWidgetChangesMessage(view_widget);
    }
    var source = $('##{widget[:id]} .domain_obj_row:eq(#{current_index})');
    move_row_to_index(source, #{destination_index});
  !
end

def simulate_drag_row_to_index(table_name, current_index, destination_index)
  move = destination_index - current_index
  puts "table_name: #{table_name}"
  puts "current_index: #{current_index}"
  puts "destination_index: #{destination_index}"
  page.execute_script(File.read(File.expand_path('../../js/jquery.simulate.drag-sortable/jquery.simulate.drag-sortable.js', __FILE__)))
  page.execute_script("
    var source = $('table[name^=#{table_name}] .domain_obj_row').eq(#{current_index});
    source.simulateDragSortable({move: #{move}});
  ")
end

def drag_row_to_index(header, current_index, destination_index)
  w = get_widget(header)
  rows = w.all(".domain_obj_row")
  row = rows[current_index]
  dest_row = rows[destination_index]
  puts "Dragging from #{row.text} to #{dest_row.text}"
  row.drag_to(dest_row)
end