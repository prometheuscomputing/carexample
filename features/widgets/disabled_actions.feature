# TODO: Add containment examples as well.

Feature: Disabling association related actions (Add / Remove/ Delete / Create)
  In order to prevent me from doing things I shouldn't do
  As a user
  I want to make sure that some activities can be disabled
  
  Background:
    Given I have reset the database
    And   I have logged in

  Scenario: Can not add a to-one association
    When I go to the url "/Automotive/VehicleTaxRate/new"
    And  I click on the "For Country" header
    Then I should not be able to show possible "For Country"
    And  I should not see an option to create a new "For Country"
    
  Scenario: Can not add a to-many association
    When I click on the "Repair Shops" header
    And  I create a "Repair Shop"
    And  I click on the "Customers" header
    Then I should not be able to show possible "Customers"
    And  I should not see an option to create a new "Customers"
    
  Scenario: Can not remove a to-one association
    When I click on the "Countries" header
    And  I create a "Countries"
    And  I click on the "Vehicle Tax Rate" header
    And  I create a "Vehicle Tax Rate"
    Then I should not be able to remove a "Country"
    
  
  Scenario: Can not remove a to-many association
    When I click on the "Repair Shops" header
    And  I create a "Repair Shop"
    And  I click on the "Mechanics" header
    And  I create a "Mechanics"
    And  I save and leave the page
    Then I should not be able to remove a "Mechanics"
    
  Scenario: Can not create a new object for a to-one association
    When I go to the url "/Automotive/VehicleTaxRate/new"
    And  I click on the "Country" header
    Then I should not see an option to create a new "Country"
    
  Scenario: Can not create a new object for a to-many association
    When I click on the "Warranties" header
    And  I create a "Warranties"
    And  I click on the "Warrantieds" header
    Then I should not see an option to create a new "Warrantieds"
    
  # Scenario: Can not delete an object from a to-one association
  #   And  PENDING
    # because this is currently only possible if it is a containment association
  
  Scenario: Can not delete an object from a to-many association -- Note: this means only that there is no delete button
    When I click on the "Countries" header
    And  I create a "Countries"
    And  I click on the "Territories" header
    And  I create a "Territories"
    And  I save and leave the page
    And  I click on the "Territories" header
    And  I select the "Territories"
    Then I should not be able to delete a "Territories"
    
  Scenario: Can not delete an object from a to-one containment association -- Note: this means only that there is no delete button
    When I expand the "Vehicles" header
    And  I create a "Vehicle" of type "Car"
    And  I click on the "VIN" header
    And  I create a "VIN"
    And  I save and leave the page
    And  I click on the "VIN" header
    Then I should not be able to delete the "VIN"
    
  
  Scenario: Can not traverse a to-one association
    When I click on the "Repair Shops" header
    And  I create a "Repair Shop"
    And  I click on the "Chief Mechanic" header
    And  I create a "Chief Mechanic"
    And  I save and leave the page
    And  I click on the "Chief Mechanic" header
    Then I should not see an option to visit a "Chief Mechanic"
    
  Scenario: Can not traverse a to-many association
    When I click on the "Countries" header
    And  I create a "Countries"
    And  I click on the "Territories" header
    And  I create a "Territories"
    And  I save and leave the page
    And  I click on the "Territories" header
    Then I should not see an option to visit a "Territories"
  
  
    
    
