When /^I go to the user administration panel/ do
  visit_user_administration
end
When /^I go to the site administration panel/ do
  visit_site_administration
end
When /^I submit the page/ do
  click_button 'Submit'
end

# --------------------------------------- User steps
When /^I add an? (administrator|user) named "(.*?)"(?: with the password "(.*?)")?(?: with the email "(.*?)")?/ do |_, username, password, email|
  administration_add_user(username, password, email)
end
Then /^there should( not)? be an? (administrator|user) named "(.*?)"/ do |negative, user_type, username|
  fieldset = case user_type
  when 'user'; 'Users'
  when 'administrator'; 'Administrators'
  end
  administration_user_exists?(username, fieldset).should_unless(negative) == true
end
When /^I mark the (administrator|user) named "(.*?)" to be deleted/ do |user_type, username|
  fieldset = case user_type
  when 'user'; 'Users'
  when 'administrator'; 'Administrators'
  end
  administration_mark_user(username, fieldset)
end

# Please let us try to avoid writing step definitions like this.  It is a total pain to follow what is going on and it should be refactored into something much more readable.
Then /"([^"]*)" should( not)? be able to (login|register)(?: with the password "([^"]*)")?(?:(?: and)? with the invitation code "([^"]*)")? ?(as an administrator|as a user|with the roles? .+?|with the perspectives? .+?)?(?: \(receives "(.*)"\))?$/ do |username, negative, login, password, code, user_type, warning| #"
  register = (login == 'register')
  sign_out if signed_in?
  if negative
    register ? verify_cannot_register(username, password, code, warning) : verify_cannot_login(username, password, warning)
  else
    case user_type
    when 'as an administrator'
      register ? verify_admin_register(username, password, code) : verify_admin_login(username, password)
    when 'as a user'
      register ? verify_user_register(username, password, code) : verify_user_login(username, password)
    when /^with the role/
      register ? verify_register(username, password, code) : verify_login(username, password)
      visit_preferences
      verify_roles(parse_quoted_string_list(user_type))
    when /^with the perspective/
      register ? verify_register(username, password, code) : verify_login(username, password)
      visit_preferences
      verify_perspectives(parse_quoted_string_list(user_type))
    else
      register ? verify_register(username, password, code) : verify_login(username, password)
    end
    sign_out
  end
  if UserHelpers.test_user
    sign_in(UserHelpers.test_user)
    # TODO: remove user specific code here. Should instead be a part of feature tests.
    case UserHelpers.test_user
    when UserHelpers::ADMIN
      visit_user_administration
    end
  end
end
# --------------------------------------- End User steps


# --------------------------------------- Role steps
When /^I add a role named "(.*?)"/ do |role|
  administration_add_role(role)
end
When /^I add the roles (.+)/ do |roles_str|
  parse_quoted_string_list(roles_str).each do |role|
    administration_add_role(role)
    click_button 'Submit'
  end
end
Then /^there should( not)? be a role named "(.*?)"/ do |negative, role|
  administration_role_exists?(role).should_unless(negative) == true
end
When /^I mark the role named "(.*?)" to be deleted/ do |role|
  administration_mark_role(role)
end
# --------------------------------------- End Role steps


# --------------------------------------- Role Assignment steps
When /^I assign "([^"]*)" to the (.+) roles?/ do |username, roles_string| #"
  administration_assign_roles(username, parse_quoted_string_list(roles_string))
end
When /^I remove "([^"]*)" from the (.+) roles?/ do |username, roles_string| #"
  administration_remove_roles(username, parse_quoted_string_list(roles_string))
end
Then /^I should see that "([^"]*)" (has|does not have) the (.+) roles?/ do |username, negative_string, roles_string| #"
  negative = (negative_string == 'does not have')
  administration_verify_roles(username, parse_quoted_string_list(roles_string), negative)
end
# --------------------------------------- End Role Assignment steps


# --------------------------------------- Perspective steps
When /^I add a perspective named "([^"]*)"(?: based on "([^"]*)")?/ do |perspective, base_perspective|#"
  administration_add_perspective(perspective, base_perspective)
end
When /^I add the perspectives (.+)/ do |perspectives_str|
  parse_quoted_string_list(perspectives_str).each do |perspective|
    administration_add_perspective(perspective)
    click_button 'Submit'
  end
end
Then /^there should( not)? be a perspective named "(.*?)"/ do |negative, perspective|
  administration_perspective_exists?(perspective).should_unless(negative) == true
end
When /^I mark the perspective named "(.*?)" to be deleted/ do |perspective|
  administration_mark_perspective(perspective)
end
When /^I edit the perspective named "(.*?)"/ do |perspective|
  administration_edit_perspective(perspective)
end
When /^I enter the substitution "(.*?)" to "(.*?)"/ do |master_word, replacement_word|
  administration_add_substitution(master_word, replacement_word)
end
Then /^I should( not)? see the substitution "(.*?)" to "(.*?)"/ do |negative, master_word, replacement_word|
  administration_substitution_exists?(master_word, replacement_word).should_unless(negative) == true
end
When /^I mark the substitution on "(.*?)" to be deleted/ do |master_word|
  administration_mark_substitution(master_word)
end
# --------------------------------------- End Perspective steps


# --------------------------------------- Email Requirement steps
When /^I add an email requirement with the regular expression "(.*)", the description "(.*)", and the failure message "(.*)"/ do |regexp, description, failure_message|
  administration_add_email_requirement(regexp, description, failure_message)
end
Then /^I should( not)? see an email requirement with the regular expression "(.*)"/ do |negative, regexp|
  administration_verify_email_requirement(regexp, negative)
end
When /^I mark the email requirement with the regular expression "(.*)" to be deleted/ do |regexp|
  administration_mark_email_requirement(regexp)
end
# --------------------------------------- End Email Requirement steps


# --------------------------------------- Password Requirement steps
When /^I add a password requirement with the regular expression "(.*)", the description "(.*)", and the failure message "(.*)"/ do |regexp, description, failure_message|
  administration_add_password_requirement(regexp, description, failure_message)
end
Then /^I should( not)? see a password requirement with the regular expression "(.*)"/ do |negative, regexp|
  administration_verify_password_requirement(regexp, negative)
end
When /^I mark the password requirement with the regular expression "(.*)" to be deleted/ do |regexp|
  administration_mark_password_requirement(regexp)
end
# --------------------------------------- End Password Requirement steps


# --------------------------------------- Invitation Code steps
When /^I add the invitation code "([^"]*)"(?: with (?:expiration "([^"]*)")?(?: and )?(?:"(-?\d)+" uses remaining)?)?(?: that grants(?: the roles (.+?))?(?: the perspectives? (.+?))?)?( in the past)?$/ do |code, expires, uses_remaining,  roles_str, perspectives_str, time_travel|
  administration_add_code(code, expires, uses_remaining, parse_quoted_string_list(roles_str), parse_quoted_string_list(perspectives_str), time_travel)
end
Then /^I should( not)? see the invitation code "(.*?)"/ do |negative, code|
  administration_code_exists?(code).should_unless(negative) == true
end
When /^I mark the invitation code "(.*?)" to be deleted/ do |code|
  administration_mark_code(code)
end
# --------------------------------------- End Invitation Code steps

When /^I create some roles$/ do
  many_steps(<<-GHERKIN)
    When I go to the site administration panel
    And  I add a role named "Editor"
    And  I submit the page
    And  I add a role named "Secretary"
    And  I submit the page
  GHERKIN
end

When /^I create a user and make sure I can add and remove roles to that user$/ do
  many_steps(<<-GHERKIN)
    And  I add a user named "phillip"
    And  I submit the page
    When I assign "phillip" to the "Editor" and "Secretary" roles
    And  I submit the page
    Then I should see that "phillip" has the "Editor" and "Secretary" roles
    When I remove "phillip" from the "Secretary" role
    And  I submit the page
    Then I should see that "phillip" does not have the "Secretary" role
  GHERKIN
end