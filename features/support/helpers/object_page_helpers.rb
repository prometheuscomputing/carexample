# High level object page helpers for things not related to associations/attributes

def go_back_via_browser(steps = 1)
  steps.times do
    page.evaluate_script('window.history.back()')
  end
end

def verify_page_type(page_type)
  expect(get_page_type).to eq(page_type)
end

def get_page_type
  last_breadcrumb.text.strip
end

def get_page_classifier
  get_page_type.delete(' ')
end

# Get an array of all breadcrumbs
def breadcrumbs
  find('.breadcrumb_trail').all('.navigational_link')
end

# Get the last visited breadcrumb (should be the current page)
def last_breadcrumb
  breadcrumbs.last
end

def save_object_page(verify_text = 'Car Example')
  find('#save_and_stay').click
  expect(page).to(have_content(verify_text))
end

def discard_changes_and_stay(verify_text = 'Car Example')
  find('#discard_and_stay').click
  expect(page).to(have_content(verify_text))
end

def save_and_leave_object_page(verify_text = 'Car Example')
  save_and_leave_button = find('#save_and_leave')
  #page.execute_script("$('#save_and_leave.submission_button').focus();")
  save_and_leave_button.click
  expect(page).to(have_content(verify_text))
end

# Refresh the current page by clicking on the last breadcrumb
def refresh_page
  last_breadcrumb.click
end

def go_to_previous_page(num_pages_to_go_back = 1)
  bc = breadcrumbs[(num_pages_to_go_back + 1) * -1]
  if bc
    bc.click
    sleep(0.2)
  else
    # Assume that Home was last.  TODO: not a safe assumption??
    visit '/'
  end
end

def go_to_first(type)
  breadcrumbs_of_type(type)[0].click
end
def go_to_second(type)
  breadcrumbs_of_type(type)[1].click
end
def go_to_last(type)
  breadcrumbs_of_type(type)[-1].click
end

def breadcrumbs_of_type(type)
  breadcrumbs.select{|b| b.text.strip == type}
end

def verify_organizer(header, conditions)
  verify_attributes(conditions, widget(header))
end

def verify_breadcrumbs(breadcrumb_strings)
  expect(breadcrumbs.collect{|b| b.text.strip}).to eq(breadcrumb_strings)
end

# Verify a breadcrumb href points to the correct object
# NOTE: This method uses DB access to check linked objects. Use with caution.
#       This was necessary to be able to verify a breadcrumb without leaving
#       the current page (so as to avoid changing the breadcrumbs).
def verify_breadcrumb(_breadcrumb, object_description, is_new = nil)
  if object_description == 'Home'
    crumb = breadcrumbs_of_type('Home').first
    # Under poltergeist, the :href will include the host and scheme (it isn't the same as the HTML anchor's href)
    href = Capybara.current_driver == :poltergeist ? crumb.native.attributes['href'] : crumb[:href]
    expect(href).to eq('/')
  elsif is_new == true
    href = breadcrumbs_of_type(object_description).first[:href]
    expect(href).to match(/\/#{object_description}\/new/)
  else # existing object
    description = object_description.first
    type = description[:type]
    href = breadcrumbs_of_type(type).first[:href]
    # Check DB to validate description conditions
    _, ramaze_pkg, id_str = href.match(/\/([^\/]+)\/#{type}\/(\d+)\//).to_a
    # pkg = ramaze_pkg.to_camelcase.to_const_case_insensitive # Stop this dependence on fuzzy matching constants
    pkg = Object.const_get(ramaze_pkg.to_camelcase)
    # klass = type.to_const(pkg)
    klass = pkg.const_get(type)
    obj = klass[id_str.to_i]
    raise "Unable to find object in DB from URL: #{href}" unless obj
    verify_object_attributes(obj, description[:conditions])
  end
end

def verify_copyright_notice
  this_year = Time.now.strftime("%Y")
  expect(has_content?("\u00A9 Prometheus Computing, LLC #{this_year}")).to(be true)
end

def verify_widget_order(widget_labels, container = nil)
  awl = actual_widget_labels(container)
  # Check for matching widgets regardless of order (easier to debug than equality check)
  awl.should =~ widget_labels
  # Check for matching widgets in correct order
  expect(awl).to eq(widget_labels)
end

def verify_widget_adjacency(first, second, container = nil)
  awl = actual_widget_labels(container)
  expect(awl).to include first
  expect(awl).to include second
  expect(awl.index(first) + 1).to eq (awl.index(second))
end

def verify_widget_position(widget, position, container = nil)
  awl = actual_widget_labels(container)
  expect(awl).to include widget
  expect(awl.index(widget)).to eq position
end

def actual_widget_labels(container)
  widget_infos(widgets(container)).collect{|w| w[:label]}
end

# Get a widget with the given label
# From is either an array of widget hashes, or a container element
def widget(label, from = nil)
  # This is really ugly here...
  # puts label.inspect
  # puts from.inspect
  from ||= find('.content > .organizer, .document')
  if from.is_a?(Array)
    from.select{|w| widget_info(w)[:label] == label}.first
  else
    sleep 0.1 # Sleep so that first will work below. Remove after upgrade to Capybara 3 -SD
    widget_label = from.first('.widget_label, label', :text => /\A#{label}\z/, :visible => false)
    get_containing_widget(widget_label)
  end
end

def get_containing_widget(widget_component)
  parent = widget_component.find(:xpath, "..")
  parent.has_class?('widget') ? parent : get_containing_widget(parent)
end

# Returns an array of the widgets on the current page in the order they are shown
# From is a container element
def widgets(from = nil)
  from ||= get_root_widget
  wait_until_attached { from.all(".widget", :visible => false).to_a }
end

def widget_infos(widget_nodes)
  widget_nodes.collect do |widget_node|
    get_widget_info(widget_node)
  end
end

def get_widget_info(widget_node)
  is_view = widget_node.has_class?('view_widget')
  label   = widget_node.first('.widget_label, label', :visible => false, :minimum => 0)
  input   = widget_node.first("##{label[:for]}", :visible => false, :minimum => 0) if !is_view && label && label[:for] && !label[:for].empty?
  info = {}
  info[:widget_node] = widget_node
  info[:type]        = get_widget_type(widget_node)
  info[:label_node]  = label
  info[:label]       = label.text(:all).strip if label
  info[:input]       = input unless is_view
  # Set the count for views
  info[:count]       = get_widget_count(widget_node) if is_view
  if info[:type] == 'text'
    lang_id = "##{label[:for]}_language"
    info[:language]  = find(lang_id, :visible => false) if widget_node.has_css?(lang_id, :wait => 0)
  end
  info
end

def widget_info(widget_node)
  get_widget_info(widget_node)
end

def get_widget_count(widget)
  entry_count = widget.first('.entry_count', :visible => false, :minimum => 0)
  count_text = entry_count ? entry_count.text : nil
  count = case count_text
  when 'Present'; 1
  when 'None'; 0
  when /(\d+) entr(y|ies)/; $1.to_i
  when /(\d+)/; $1.to_i
  when nil; nil
  end
  count
end

def get_widget_type(widget)
  widget.first('.widget_type', :visible => false).value
end


def verify_abscence(label)
  root = find('.content > .organizer, .document')
  sel  = '.widget_label'
  opts = {:text => /\A#{label}\z/}
  root.assert_no_selector(sel, opts)
end

