Feature: Uniqueness constraints
  In order to prevent accidental duplication of objects
  As a user
  I want to be warned when attempting to duplicate marked fields
  
  Background:
    Given I have reset the database
    And I have logged in as a user
    And I expand the "People" header
  
  # ----------------------------------------- Warning Behavior
  
  Scenario: Creating a new object from the home page with a duplicated should_be_unique field - Canceling
    When I create a "People" of type "Person"
    And I set the "Name" to "Bob"
    And I set the "Description" to "New Bob"
    And I select the markup language "LaTeX" for "Description"
    And I save the page
    Then I should receive a "uniqueness" popup warning
    When I cancel the "uniqueness" prompt
    Then the value of "Description" should be "New Bob"
    When I set the "Name" to "Bobby"
    And I save the page
    Then I should not receive a "uniqueness" popup warning
    And the value of "Name" should be "Bobby"
    When I choose to edit the "Description" text
    Then the value of "Description" should be "New Bob"
    And the markup language of "Description" should be "LaTeX"
  
  Scenario: Creating a new object from the home page with a duplicated should_be_unique field - Continuing anyway
    When I create a "People" of type "Person"
    And I set the "Name" to "Bob"
    And I set the "Description" to "New Bob"
    And I select the markup language "LaTeX" for "Description"
    And I save the page
    Then I should receive a "uniqueness" popup warning
    When I continue anyway on the "uniqueness" popup
    Then the value of "Name" should be "Bob"
    When I choose to edit the "Description" text
    Then the value of "Description" should be "New Bob"
    And the markup language of "Description" should be "LaTeX"
    # Check that saving again doesn't trigger a warning
    When I save the page
    Then I should not receive a "uniqueness" popup warning
    And the value of "Name" should be "Bob"
    When I choose to edit the "Description" text
    Then the value of "Description" should be "New Bob"
    And the markup language of "Description" should be "LaTeX"
  
  Scenario: Creating a new object from the home page with a duplicated should_be_unique field - Abandoning creation & viewing existing item
    When I create a "People" of type "Person"
    And I set the "Name" to "Bob"
    And I set the "Description" to "New Bob"
    And I select the markup language "LaTeX" for "Description"
    And I save the page
    Then I should receive a "uniqueness" popup warning
    When on the "uniqueness" popup, I select the conflicting "Person" whose "Name" is "Bob"
    And I abandon creation and view existing item on the "uniqueness" popup
    Then I should be at a "Person" page
    And the value of "Name" should be "Bob"
    When I choose to edit the "Description" text
    Then the value of "Description" should be "Bob's description"
    And the markup language of "Description" should be "Markdown"
  
  
  Scenario: Creating a new object from the home page with a should_be_unique field duplicated in a superclass object
    When I create a "People" of type "Mechanic"
    And I set the "Name" to "Bob"
    And I save the page
    Then I should receive a "uniqueness" popup warning
  
  Scenario: Creating a new object from the home page with a should_be_unique field duplicated in a subclass object
    When I create a "People" of type "Person"
    And I set the "Name" to "Jeff"
    And I save the page
    Then I should receive a "uniqueness" popup warning
  
  Scenario: Creating a new object from the home page with a should_be_unique field duplicated in a sibling object
    Given I expand the "Vehicles" header
    When I create an "Vehicle" of type "Electric Vehicle" where "Vehicle Model" is "S80"
    And I save the page
    Then I should receive a "uniqueness" popup warning
  
  Scenario: Creating a new object from a collection with a duplicated should_be_unique field - Continuing anyway
    Given I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S40"
    And I click on the "Owners" header
    When I create an "Owner" through "Ownership" of type "Person" whose "Name" is "Bob"
    And I save the page
    Then I should receive a "uniqueness" popup warning
    When I continue anyway on the "uniqueness" popup
    Then I should be at a "Person" page
    And the value of "Name" should be "Bob"
    And the value of "Description" should be ""
    When I expand the "Vehicles" header
    Then I should see a "Vehicle" where "Vehicle Model" is "S40"
  
  Scenario: Creating a new object from a collection with a duplicated should_be_unique field - Abandoning creation & using existing item
    Given I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S40"
    And I click on the "Owners" header
    When I create an "Owner" through "Ownership" of type "Person" whose "Name" is "Bob"
    And I save the page
    Then I should receive a "uniqueness" popup warning
    When on the "uniqueness" popup, I select the conflicting "Person" whose "Name" is "Bob"
    And I abandon creation and use existing item on the "uniqueness" popup
    Then I should be at a "Person" page
    And the value of "Name" should be "Bob"
    And the value of "Description" should be "Bob's description"
    And I should see that there are a total of "2" "Vehicles" in the header
    When I expand the "Vehicles" header
    Then I should see a "Vehicle" where "Vehicle Model" is "S40"
  
  Scenario: Editing an existing object to have a duplicated should_be_unique field - Continuing anyway
    Given I edit the "People" whose "Name" is "Sally"
    And I set the "Name" to "Bob"
    And I save the page
    Then I should receive a "uniqueness" popup warning
    When I continue anyway on the "uniqueness" popup
    Then I should be at a "Person" page
    And the value of "Name" should be "Bob"
    And the value of "Family Size" should be "1"
  
  
  # ----------------------------------------- Error Behavior
  
  Scenario: Creating a new object from the home page with a duplicated is_unique field - Canceling
    Given I expand the "VINs" header
    And I create a "VIN" where "Vin" is "1234567890" and "Issue Date" is "1995-12-31"
    And I save the page
    Then I should receive a "uniqueness" popup error
    When I cancel the "uniqueness" prompt
    And I set the "Vin" to "1234567891"
    And I save the page
    Then I should not receive a "uniqueness" popup warning
    And the value of "Vin" should be "1234567891"
    And the value of "Issue Date" should be "December 31, 1995"
  
  # Fails selenium because entering the Last Updated somehow clicks on the datetime widget, inserting todays date.
  # Test all field types are preserved when cancelling
  @fails_selenium
  Scenario: Creating a new object from the home page with a duplicated is_unique field - Canceling (example 2)
    # The 'Name' attribute will conflict
    Given I create a "People"
    And I set the "Name" to "Bob"
    And I set the "Description" to "New Bob"
    And I choose to upload the "Photo" named "default_user_replaceme.jpeg"
    And I set the "Date Of Birth" to "1991-12-21"
    And I set the "Last Updated" to "2013-01-30"
    And I set the "Family Size" to "2"
    And I set the "Weight" to "155.6"
    And I set the "Dependent" to "False"
    And I set the "Handedness" to "Left Handed"
    # Trigger the warning
    When I save the page
    Then I should receive a "uniqueness" popup warning
    When I cancel the "uniqueness" prompt
    Then the value of "Name" should be "Bob"
    And the value of "Description" should be "New Bob"
    And there should be a pending "Photo" named "default_user_replaceme.jpeg" And   
    And the value of "Date Of Birth" should be "December 21, 1991"
    And the value of "Last Updated" should be "January 30, 2013 12:00:00am"
    And the value of "Family Size" should be "2"
    And the value of "Weight" should be "155.6"
    And the value of "Dependent" should be "False"
    And the value of "Handedness" should be "Left Handed"
    When I save the page
    # Trigger the warning again
    Then I should receive a "uniqueness" popup warning
    When I cancel the "uniqueness" prompt
    Then the value of "Name" should be "Bob"
    And the value of "Description" should be "New Bob"
    And there should be a pending "Photo" named "default_user_replaceme.jpeg" And   
    And the value of "Date Of Birth" should be "December 21, 1991"
    And the value of "Last Updated" should be "January 30, 2013 12:00:00am"
    And the value of "Family Size" should be "2"
    And the value of "Weight" should be "155.6"
    And the value of "Dependent" should be "False"
    And the value of "Handedness" should be "Left Handed"
    # Change the name and save again. Warning should not be triggered
    When I set the "Name" to "New Bob"
    And I save the page
    Then I should not receive a "uniqueness" popup warning
    And the value of "Name" should be "New Bob"
    And the value of "Description" should be "New Bob"
    And there should be a stored "Photo" named "default_user_replaceme.jpeg"
    And the value of "Date Of Birth" should be "December 21, 1991"
    And the value of "Last Updated" should be "January 30, 2013 12:00:00am"
    And the value of "Family Size" should be "2"
    And the value of "Weight" should be "155.6"
    And the value of "Dependent" should be "False"
    And the value of "Handedness" should be "Left Handed"
  
  Scenario: Creating a new object from the home page with a duplicated is_unique field - File handling
    # The 'Name' attribute will conflict
    Given I create a "People"
    And I set the "Name" to "Bob"
    And I choose to upload the "Photo" named "default_user_replaceme.jpeg"
    When I save the page
    # Trigger the warning again
    Then I should receive a "uniqueness" popup warning
    When I cancel the "uniqueness" prompt
    Then there should be a pending "Photo" named "default_user_replaceme.jpeg" And   
    # Change the name and photo
    When I set the "Name" to "New Bob"
    And I choose to upload the "Photo" named "picture2.jpeg"
    And I save the page
    Then the value of "Name" should be "New Bob"
    And there should be a stored "Photo" named "picture2.jpeg"
  
  Scenario: Creating a new object from the home page with a duplicated is_unique field - Abandoning creation & viewing existing item
    Given I expand the "VINs" header
    And I create a "VIN" where "Vin" is "1234567890" and "Issue Date" is "1995-12-31"
    And I save the page
    Then I should receive a "uniqueness" popup error
    When on the "uniqueness" popup, I select the conflicting "VIN" whose "Vin" is "1234567890"
    And I abandon creation and view existing item on the "uniqueness" popup
    Then I should be at a "VIN" page
    And the value of "Vin" should be "1234567890"
    And the value of "Issue Date" should be "January 4, 1990"
  
  Scenario: Creating a new object from a collection with a duplicated is_unique field - Canceling
    Given I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S40"
    And I click on the "Components" header
    When I create a "Component" where "Name" is "Crank" and "Serial" is "12"
    And I save the page
    Then I should receive a "uniqueness" popup error
    And I cancel the "uniqueness" prompt
    Then I should be at a "Component" page
    And the value of "Name" should be "Crank"
    And the value of "Serial" should be "12"
  
  Scenario: Creating a new object from a collection with a duplicated is_unique field - Abandoning creation & using existing item
    Given I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S40"
    And I click on the "Components" header
    When I create a "Component" where "Name" is "Crank" and "Serial" is "12"
    And I save the page
    Then I should receive a "uniqueness" popup error
    When on the "uniqueness" popup, I select the conflicting "Component" where "Name" is "Engine"
    And I abandon creation and use existing item on the "uniqueness" popup
    # NOTE: should receive warning that I'm about to reassociate Component to this Vehicle destroying the existing association
    Then I should be at a "Component" page
    And the value of "Name" should be "Engine"
    And I should see that there are a total of "1" "Vehicle" in the header
    When I click on the "Vehicle" header
    Then I should see the associated "Vehicle" where "Vehicle Model" is "S40"
    # Ensure that Parts are unaffected by component serial "uniqueness" constraint
    When I click on the "Parts" header
    And I create a "Part" where "Serial" is "12" and "Name" is "Fake Part"
    And I save the page
    Then I should not receive a "uniqueness" popup error
    # Check that Parts serials are not "uniqueness" constrained against other parts
    When I set the "Serial" to "1234"
    And I save the page
    Then I should not receive a "uniqueness" popup error
    
  Scenario: Testing non-UTF-8 (US-ASCII) characters and symbols
    # Two Different dashes at the end, first one is US-ASCII, second is UTF-8
    When I create a "People" of type "Person"
    When I set the "Name" to "George – !@#$%^&*()- O'Malley ± ° ÷ ó ò ñ ¢ £ ¥ & © §"
    When I save the page
    Then the value of "Name" should be "George – !@#$%^&*()- O'Malley ± ° ÷ ó ò ñ ¢ £ ¥ & © §"
    And  I go to the home page
    And  I expand the "People" header
    When I create a "People" of type "Person"
    When I set the "Name" to "George – !@#$%^&*()- O'Malley ± ° ÷ ó ò ñ ¢ £ ¥ & © §"
    When I save the page
    Then I should receive a "uniqueness" popup warning
