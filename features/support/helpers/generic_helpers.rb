# This file contains generic HTML/JS/JQ helpers are not specific to any gui_site feature
require 'uri'

# This is not compatible with selenium
def verify_image_url(img_url, negate = false)
  visit(img_url)
  if negate
    expect(page.status_code).to eq(404)
  else
    expect(page.status_code).to eq(200)
  end
end

def current_uri
  URI.parse(current_url)
end

# Return an absolute url from a relative url
# Note that selenium already does this before returning urls, so 
# they must not be modified here.
def expand_url(relative_url)
  return relative_url if relative_url =~ /^http/
  "#{current_uri.path}/#{relative_url}"
end

def confirm_text_on_page(text, negative = false)
  if negative
    expect(page).not_to have_text(text, :normalize_ws => true)
  else
    expect(page).to have_text(text, :normalize_ws => true)
  end
end

def confirm_button_enabled_or_disabled(button, negative = false)
  find_button(button).disabled?.should_unless(negative, (be false))
end

def confirm_button_on_page(button, negative = false)
  # expect(negative ? has_no_button?(button) : has_button?(button)).to eq(true)
  expect(has_no_button?(button)).to eq(negative)
end

def confirm_input_enabled_or_disabled(button, negative = false)
  # PENDING, might be unnecessary
end

def confirm_widget_enabled_or_disabled(label, disabled = false)
  w = widget(label)
  if disabled
    expect(w.has_class?('mutable')).to eq(false)
    expect(w).not_to have_content('.editable_view') #all('.editable_view').empty?.should == true
  else
    expect(w.has_class?('mutable')).to eq(true)
    w.click
    expect(w.find('.editable_view')).not_to eq(nil)
    case Capybara.current_driver
    when :webkit, :poltergeist
      w.find('.widget_data').trigger('blur')
    else
      page.find("body").click
    end
  end
end

def find_element_by_selector(selector)
  page.find(selector)
end

