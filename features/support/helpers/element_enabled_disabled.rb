def confirm_element_parent_not_disabled(element, scope = nil)
  _element_parent_abled(element, false, scope)
end

def confirm_element_parent_disabled(element, scope = nil)
  _element_parent_abled(element, true, scope)
end

def _element_parent_abled(element, disabled, scope)
  element_to_check = _element_to_check(element, scope)
  _element_abled(element_to_check.first(:xpath,".//.."), disabled)
end

def confirm_element_not_disabled(element, scope = nil)
  element_to_check = _element_to_check(element, scope)
  _element_abled(element_to_check, false)
end

def confirm_element_disabled(element, scope = nil)
  element_to_check = _element_to_check(element, scope)
  _element_abled(element_to_check, true)
end

def _element_to_check(element, scope)
  scope ||= page
  scope.find(element, :visible => false)
end

def _element_abled(element_to_check, disabled)
  # For now, check CSS to verify that element is disabled
  case element_to_check.tag_name
  when 'button', 'fieldset', 'input', 'optgroup', 'option', 'select', 'textarea'
    is_disabled = element_to_check.disabled?
  else # This case is not really proper, since HTML only specifies that the 'disabled' property is valid on the above element types.
    # Instead of checking disabled (which does nothing) check CSS for 'disabled' class.
    is_disabled = element_to_check.has_class?('disabled')
  end
  # puts "#{element_to_check.tag_name} should be #{disabled ? 'disabled' : 'enabled'}."
  # puts Rainbow("#{element_to_check.inspect} is disabled? #{is_disabled}").orange
  is_disabled.should(be !!disabled)
end

# def confirm_element_disabled(element, negative = false, scope = nil)
#   scope ||= page
#
#   # For now, check CSS to verify that element is disabled
#   element_to_check = scope.find(element, :visible => false)
#   case element_to_check.tag_name
#   when 'button', 'fieldset', 'input', 'optgroup', 'option', 'select', 'textarea'
#     is_disabled = scope.find(element, :visible => false).disabled?
#   else # This case is not really proper, since HTML only specifies that the 'disabled' property is valid on the above element types.
#     # Instead of checking disabled (which does nothing) check CSS for 'disabled' class.
#     is_disabled = scope.find(element, :visible => false).has_class?('disabled')
#   end
#   is_disabled.should_not_unless(negative, (be false))
# end