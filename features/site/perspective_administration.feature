Feature: Administration Panel
  In order to manage the perspectives that are available to users
  As an administrator
  I want to create and delete perspectives and word substitutions
  
  Background:
    Given I have reset the database
    And I have logged in as an administrator
    And I go to the site administration panel
  
  Scenario: Adding/Deleting a perspective
    When I add a perspective named "Stakeholder"
    And I submit the page
    Then there should be a perspective named "Stakeholder"
    When I mark the perspective named "Stakeholder" to be deleted
    And I submit the page
    Then there should not be a perspective named "Stakeholder"
  
  Scenario: Adding/Deleting substitutions for a perspective
    When I add a perspective named "Stakeholder"
    And I submit the page
    And I edit the perspective named "Stakeholder"
    And I enter the substitution "Electric Vehicle" to "PEV"
    And I submit the page
    And I enter the substitution "electric efficiency" to "elec. eff."
    And I submit the page
    Then I should see the substitution "Electric Vehicle" to "PEV"
    And I should see the substitution "electric efficiency" to "elec. eff."
    When I mark the substitution on "Electric Vehicle" to be deleted
    And I submit the page
    Then I should not see the substitution "Electric Vehicle" to "PEV"
  
  Scenario: Creating a perspective based off an existing perspective
    When I add a perspective named "Stakeholder"
    And I submit the page
    And I edit the perspective named "Stakeholder"
    And I enter the substitution "Electric Vehicle" to "PEV"
    And I submit the page
    And I enter the substitution "electric efficiency" to "elec. eff."
    And I submit the page
    And I go to the site administration panel
    And I add a perspective named "GMC" based on "Stakeholder"
    And I submit the page
    Then there should be a perspective named "GMC"
    When I edit the perspective named "GMC"
    Then I should see the substitution "Electric Vehicle" to "PEV"
    And I should see the substitution "electric efficiency" to "elec. eff."
