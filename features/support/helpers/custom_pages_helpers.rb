def verify_vehicle_rows(num_vehicles)
  expect(find('#vehicles_table').all('tr td:first-child').count).to eq(num_vehicles)
end