#!/usr/bin/env ruby
require 'lodepath'
LodePath.add_project_lib
LodePath.amend if ARGV[0] =~ /test|dev/i
LodePath.display
# Where to put downloaded files
TEST_DOWNLOADS_DIR = File.expand_path(File.join(__dir__, '../features/support/test_downloads/'))
# WEB_ROOT = File.expand_path(File.join(__dir__, '../lib/gui_site/'))
# puts WEB_ROOT

$preserve_db = !ENV['POPULATE_DATA']

# Note: setup is going to put us in :live mode, even if we passed --dev.  FIXME if we need :dev mode
require_relative 'setup'
$app, _ = Rack::Builder.parse_file(LodePath.find('../lib/gui_site/config.ru'))
$suppress_output = false # Re-enable verbose output
CarExample.populate_data unless $preserve_db
require 'pry'; binding.pry
# ChangeTracker.start
