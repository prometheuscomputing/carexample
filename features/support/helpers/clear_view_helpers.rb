def select_clear_view(clear_view_name)
  select(clear_view_name, :from => 'clear_view')
end

def render_new_object(header, type = nil)
end

#Section helpers
def get_section_heading(heading_text)
  sleep 0.1
  find('.section_header .view_label', :text => heading_text)
end

def wait_for_section_content(heading_text)
  section = find('.section_header .view_label', :text => heading_text).find(:xpath, "..") .find(:xpath, "..")
  section.find('.section_content.populated')
end