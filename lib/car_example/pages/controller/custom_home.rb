module Gui
  class CustomHomeController < Gui::Controller
    map '/car_example_home/'
    layout('default') { |path, wish| !request.xhr? }
    DEFAULT_PAGE_SIZE = Gui.option(:default_page_size)
    MAX_PAGE_SIZE     = Gui.option(:max_page_size)
    
    def index
      home  = Gui::Home.new
      specs = []
      options = {}
      # TODO look into rendering without a obj and getter and just directly pass an array of objects or a type that we would get all of.
      specs << Gui::Director.new.render(home, 'bobs',   :Collection, :Bobs,    People::Person,  options)
      specs << Gui::Director.new.render(home, 'cities', :Collection, :Summary, Geography::City, options)
      
      locals = {:page_title => 'Bobs and Cities'}
      @custom_home_view = MainController.render_view(:object_wrapper, :output => specs.join("\n"), :breadcrumbs => [], :locals => locals) {|action| action.options[:is_layout] = true }
    end
  end
end # Gui