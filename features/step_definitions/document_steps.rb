# --------------------------------------- Document steps
# --------------------------------------- End Document steps


# --------------------------------------- Document Association steps
When /^I add a new "([^"]*)" document association item(?: to (.+))?$/ do |header, scope_string|#"
  add_document_association(header, parse_scope(scope_string))
end

When /^I (remove|delete) the "([^"]*)" document association items?( (?:where|whose) .+)?$/ do |operation, header, conditions_string|
  edit_document_associations(header, parse_conditions(conditions_string), operation)
end
Then /^I should see a new "([^"]*)" document association to an? (new|existing) object/ do |header, new_or_existing|
  is_existing = new_or_existing == "existing"
  verify_new_document_association(header, is_existing)
end
Then /^the "([^"]*)" document association items?( (?:where|whose) .+)? should be marked as a pending (removal|deletion)$/ do |header, conditions_string, operation|
  verify_document_association_operation(header, operation, parse_conditions(conditions_string))
end
Then /^I should see an? (saved|added|removed) "([^"]*)" document association item( (?:where|whose) .+)?$/ do |status, label, conditions_string|
  verify_document_association_item(label, parse_conditions(conditions_string), status)
end

# Association popup steps
When /^I associate to a new "([^"]*)"$/ do |type|
  associate_to_new(type)
end
When /^I associate to an existing "([^"]*)"( (?:where|whose) .+)?$/ do |_, conditions_string|
  associate_to_existing(parse_conditions(conditions_string))
end
When /^I confirm the association$/ do
  confirm_association
end
When /^I cancel the association$/ do
  cancel_association
end
# --------------------------------------- End Document Association steps