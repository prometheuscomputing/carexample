# --------------------------------------- Setup steps
Given /^I have reset the database/ do
  # Wait for outstanding requests to finish.
  # Clear view and automatically populated collections will query the database via AJAX, so give these time to finish.
  sleep 0.1
  # TODO: find a way to save baseline_db that doesn't rely on this step to always be run first.
  # This could cause an issue if the first test that is run does not call this step.
  if $baseline_db
    replace_database($baseline_db)
  else
    if ENV['BOOTSTRAP_TESTS'] && File.exists?(BOOTSTRAP_DB)
      replace_database_from_file(BOOTSTRAP_DB)
      $baseline_db = backup_database
    else
      setup_users
      $baseline_db = backup_database
      backup_database_to_file(BOOTSTRAP_DB)
    end
  end
end

Given /^I have a database with no users/ do
  sleep 0.1
  replace_database($empty_db)
end
# --------------------------------------- End Setup steps


# --------------------------------------- Tab steps
When /^I open a new tab$/ do
  page.open_new_tab
  visit '/'
end
When /^I switch to the (\d+)(?:st|nd|rd) tab$/ do |position|
  page.switch_to_tab(position)
end
# --------------------------------------- End Tab steps


# --------------------------------------- Layout steps
Then /^I should see the warning "([^"]*)"/ do |message| #"
  verify_warning(message)
end

Then /^I should see the error "([^"]*)"/ do |message| #"
  verify_error(message)
end

Then /^I should see the success message "([^"]*)"/ do |message| #"
  verify_success_message(message)
end
# --------------------------------------- End Layout steps

# --------------------------------------- Keystroke steps
# Fails because #native is returning a string. Need to update capybara for this to work I think -SD
When /^I press enter inside the "(.*)" field$/ do |widget_label|
  widget_info(widget(widget_label))[:input].native.send_keys(:return)
end
# --------------------------------------- End Keystroke steps

# --------------------------------------- Object Page steps
When /^I (save|save and leave) the( enumeration)? page/ do |save_option, is_enum|
  verify_text = is_enum ? 'Enumerations Management' : 'Car Example'
  case save_option
  when 'save'
    save_object_page(verify_text)
  when 'save and leave'
    save_and_leave_object_page(verify_text)
  end
end

#"
When /^I discard changes on the page$/ do
  discard_changes_and_stay 
end

Then /^the "(.*?)" in package "(.*?)" with id "(\d+)" should( not)? exist/ do |type, package, id, negative|
  visit("/#{package.downcase}/#{type}/#{id}")
  verify_page_error(!negative)
end
Then /^I should see the breadcrumbs: (.+)$/ do |breadcrumbs_string|
  verify_breadcrumbs(parse_quoted_string_list(breadcrumbs_string))
end
# Then /^the "(.*)" breadcrumb should link to (the home page|a new "([^"]+)"|(.*))$/ do |breadcrumb, linked_item, object_type, object_description| #"
#   case linked_item
#   when 'the home page'
#     verify_breadcrumb(breadcrumb, 'Home')
#   when /^a new/
#     verify_breadcrumb(breadcrumb, object_type, is_new = true)
#   else
#     verify_breadcrumb(breadcrumb, parse_objects_description(object_description), is_new = false)
#   end
# end

Then /^the "(.*)" breadcrumb should link to the home page$/ do |breadcrumb|
  verify_breadcrumb(breadcrumb, 'Home')
end

Then /^the "(.*)" breadcrumb should link to a new "([^"]+)"$/ do |breadcrumb, object_type| #"
  verify_breadcrumb(breadcrumb, object_type, is_new = true)
end

Then /^the "(.*)" breadcrumb should link to an existing (.*)$/ do |breadcrumb, object_description|
  verify_breadcrumb(breadcrumb, parse_objects_description(object_description), is_new = false)
end

Then /^I should see the copyright notice/ do
  verify_copyright_notice
end
Then /^I should see the form error "(.*)"$/ do |message|
  verify_form_error(message)
end
Then /^I should receive the warning "(.*)"$/ do |message|
  verify_warning(message)
end
Then /^I should receive the error "(.*)"$/ do |message|
  verify_error(message)
end
Then /^I should receive an error like "(.*)"$/ do |message|
  verify_error_like(message)
end
Then /^I should receive a warning like "(.*)"$/ do |message|
  verify_warning_like(message)
end
Then /^I should receive the message "(.*)"/ do |message|
  verify_success_message(message)
end
Then /^I should not receive the message "(.*)"/ do |message|
  verify_no_success_message(message)
end
Then /^I should not receive any messages$/ do
  verify_no_messages
end
Then /^I should see the widgets in the order:$/ do |widget_labels|
  verify_widget_order(parse_quoted_string_list(widget_labels))
end

Then /^I should see the "(.*)" widget immediately before the "(.*)" widget$/ do |first, second|
  verify_widget_adjacency(first, second)
end

Then /^I should see the "(.*)" widget first$/ do |widget|
  verify_widget_position(widget, 0)
end

# --------------------------------------- End Object Page steps


# --------------------------------------- Navigation steps
When /^I go back$/ do
  go_back_via_browser
end
When /^I refresh the page$/ do
  refresh_page
end
When /^I go to the previous page$/ do
  go_to_previous_page
end
When /^I go to the home page$/ do
  visit '/'
end
When /^I go back "(\d+)" pages$/ do |num_pages|
  go_to_previous_page(num_pages.to_i)
end
Then /^I should be at the(?: "(.*)")? home page$/ do |app_title|
  verify_homepage(app_title)
end
Then /^I should be at an? "(.*)" page$/ do |page_type|
  verify_page_type(page_type)
end
Then /^I should see only one "([^"]*)" in the breadcrumbs$/ do |type| #"
  breadcrumbs.count{|b| b.text.strip == type}.should == 1
end
When /^I go to the url "(.*)"$/ do |url|
  visit url
end
When /^I click the topbar "([^"]+)" link$/ do |link_text| #"
  click_link link_text
end
When /^I click Register$/ do
  click_register
end
When /^I click the "([^"]+)" link$/ do |link_text| #"
  click_link link_text
end
When /^I click the "([^"]+)" button$/ do |button_text| #"
  click_button button_text
end
When /^I click the (previous|next) page button$/ do |button_text|
  case button_text
  when 'previous'
    input = '<'
  when 'next'
    input = '>'
  else
    raise 'invalid use of cucumber step'
  end
  click_link input
end

# Factor the finding pretty view out to helper function, to work with multiple view-types
Then /^I click( outside)? the "([^"]+)" text$/ do |outside, label| #"
  if outside
    blur_attribute(widget_info(widget(label))[:input])
  else
    widget(label).find('.pretty_view').click
  end
end

Then /^the "([^"]+)" button should( not)? be disabled$/ do |button, negative| #"
  confirm_button_enabled_or_disabled(button, negative)
end

Then /^I should( not)? see the "([^"]+)" button$/ do |negative, button| #"
  confirm_button_on_page(button, negative)
end

Then /^the "([^"]+)" input should( not)? be disabled$/ do |button, negative| #"
  confirm_input_enabled_or_disabled(button, negative)
end

Then /^the "([^"]+)" text should( not)? be mutable$/ do |label, disabled| #"
  confirm_widget_enabled_or_disabled(label, disabled)
end

Then /^the (next|previous) page button should be disabled(?: for (.+))?$/ do |button, scope_string|
  confirm_element_parent_disabled(".#{button}_collection_page", parse_scope(scope_string))
end

Then /^the (next|previous) page button should not be disabled(?: for (.+))?$/ do |button, scope_string|
  confirm_element_parent_not_disabled(".#{button}_collection_page", parse_scope(scope_string))
end

# Then /^the (next|previous) page button should( not)? be disabled(?: for (.+))?$/ do |button, negative, scope_string|
#   if negative
#     confirm_element_disabled(".#{button}_collection_page", parse_scope(scope_string))
#   else
#     raise "There is no feature that actually does this at this time...."
#     confirm_element_not_disabled(".#{button}_collection_page", parse_scope(scope_string))
#   end
# end
# --------------------------------------- End Navigation steps

# --------------------------------------- Maintenance/Debug steps
When /^I stop the test/ do
  exit
end
When /^I (?:wait|pause|sleep) for (\d+\.?\d?)? seconds?/ do |seconds| #/
  sleep(seconds.to_f)
end
When /^I print the "(.*?)" table/ do |header|
  print_table(header)
end
Given /^PENDING$/ do
  pending
end
Given /^PENDING: (.*)/ do |pending_message|
  pending pending_message
end
# --------------------------------------- End Debug steps

Then /^the text "([^"]+)" should be on the page$/ do |text| #"
  confirm_text_on_page(text)
end

# NOTE: does not search content of input fields
# NOTE: we don't use 'still' as a variable, only for semantic meaning
Then /^I should(?: still)?( not)? see( only)? the text "(.*)"/ do |negative, only, text_str|
  if only # ...how wistful..
    text.should_unless(negative) == text_str
  else
    confirm_text_on_page(text_str, negative)
  end
end


# DOES NOT WORK! Will pass even when text is not present on page
# Then /^the following body should be on the page:$/ do |body|
#   confirm_text_on_page(text)
# end
