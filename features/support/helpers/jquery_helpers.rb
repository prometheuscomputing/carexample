# Mostly borrowed from https://gist.github.com/mttdffy/77d924435958a547834e
module JSHelpers
  module JQueryUI
 
    # type inside an autocomplete field and pick a value.
    #
    # @param [String] field selector of the autocomplete field
    # @param [Hash] options details about what to search and what to select
    # @param options [String] :with the text to type in the autocomplete field
    # @param options [String] :select the text value of the autocomplete resuilt you
    #   want to pick. If this option is not present, the first result will be picked.
    #
    # @example Auto-select first result
    #   autocomplete('product_search', with: 'example')
    #
    # @example Select specific result
    #   autocomplete('product_search', with: 'example', select: 'example product 12')
    #
    # @return nil
    #
    def autocomplete(field, options = {})
      fill_in(field, with: options.delete(:with))
 
      expect(page).not_to have_selector 'html.loading'
      page.execute_script(%{ $('##{field}').trigger('focus') })
      page.execute_script(%{ $('##{field}').trigger('keydown') })
 
      text = options.delete(:select)
      selector = if text
        %{ ul.ui-autocomplete li.ui-menu-item a:contains('#{text}') }
      else
        %{ ul.ui-autocomplete li.ui-menu-item:first-child a }
      end
 
      expect(page).to have_selector('ul.ui-autocomplete li.ui-menu-item a')
      page.execute_script(%{ $("#{selector}").trigger('mouseenter').click() })
    end
 
    # find a datetimepicker field and set the datetime
    #
    # @param [DateTime] datetime datetime to set to
    # @param [Hash] options options for setting the value
    # @param options [String] :from the id of the field to set datetime on
    #
    # @example
    #   datetimepick(2.days.from_now, from: 'event_start_datetime')
    #
    # @return nil
    #
    def datetimepick(datetime, options = {})
      set_datetimepicker_value(datetime, options.delete(:from), 'datetimepicker')
    end
 
    # find a datepicker field and set the date
    #
    # @param [Date] date date to set to
    # @param [Hash] options options for setting the value
    # @param options [String] :from the id of the field to set date on
    #
    # @example
    #   datepick(Date.tomorrow, from: 'event_start_date')
    #
    # @return nil
    #
    def datepick(date, options = {})
      set_datetimepicker_value(date, options.delete(:from), 'datepicker')
    end
 
    # find a timepicker field and set the time
    #
    # @param [Time] time time to set to
    # @param [Hash] options options for setting the value
    # @param options [String] :from the id of the field to set time on
    #
    # @example
    #   timepick(12.hours.from_now, from: 'event_start_time')
    #
    # @return nil
    #
    def timepick(time, options = {})
      set_datetimepicker_value(time, options.delete(:from), 'timepicker')
    end
 
    private
 
    def set_datetimepicker_value(datetime, selector, widget)
      # Handle empty or nil datetime value
      if datetime.nil? || datetime.empty?
        # NOTE: currently have to set this twice. Should look into why at some point.
        #       The first 'set' seems to set todays date instead of empty
        page.find(selector).set('')
        page.find(selector).set('')
        return
      end
      
      begin
        datetime = Time.parse(datetime) if datetime.is_a?(String)
        js_date_object = "new Date(#{datetime.to_i * 1000})"
        page.execute_script(%{ $("#{selector}").#{widget}("setDate", #{js_date_object})})
      rescue ArgumentError
        # Unable to parse time from datetime. Attempt to manually set field
        log "Manually setting invalid date for #{widget}"
        page.execute_script(%{ $("#{selector}").val("#{datetime}")})
      end
    end
  end
end
include JSHelpers::JQueryUI