class Module
  def const_set?(const, value)
    const_set(const, value) unless const_defined?(const, false)
  end
end
