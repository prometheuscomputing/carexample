Feature: Forgot Password
  In order to get access to my account
  As a user
  I want to reset my password
  
  Background:
    Given I have reset the database
    When I go to the url "/"
  
  Scenario: Entering an Email to recover a password from
    When I click the "Forgot Password" link
    Then I should be at the forgot password page
    When I enter the email "bobby@mycompany.net"
    And I submit the forgot password page
    Then I should see the text "An email with password recovery instructions has been sent to the user associated with the email: bobby@mycompany.net"
  
  Scenario: Entering an invalid Email to recover a password from
    When I click the "Forgot Password" link
    Then I should be at the forgot password page
    When I enter the email "nosuchemail@test.test"
    And I submit the forgot password page
    Then I should see the text "Email address not found. Please enter the email associated with your user account."
    
  Scenario: Verifying a reset password email was sent using the correct headers
    When I go to the url "/gb_users/forgot_password"
    When I enter the email "admin@mycompany.net"
    And I submit the forgot password page
    # clear emails
    # Then "admin@mycompany.net" receives an email
    Then I go to the email application
    And I click the first email
    Then I should see a from field containing the address "<no_reply@prometheuscomputing.com>"
    And I should see a to field containing the address "<admin@mycompany.net>"
    And I should see a subject field containing the subject "Account Password Reset"
    And I should see an email containing the correct reset password link
   
  Scenario:  Resetting the password using a forgot password email
    #Getting to the Reset password page
    When I go to the url "/gb_users/forgot_password"
    When I enter the email "admin@mycompany.net"
    And I submit the forgot password page
    Then I go to the email application
    And I click the first email
    And I click the link in the email
    Then I should see the text "Reset Password for admin"
    
    #Successfully resetting the password
    And I enter the new password "test1"
    And I re-enter the new password "test1"
    And I submit the reset password page
    Then I should see the text "Your password has been successfully reset. Please login using your new password."
    
    #Verifying that the reset password no longer works with the old link
    When I go to the email application
    And I click the first email
    And I click the link in the email
    Then I enter the new password "test1"
    And I re-enter the new password "test1"
    And I submit the reset password page
    Then I should receive the error "This Password Reset link is not associated with any users."
    
    #Checking if the reset was successful with login
    When I go to the url "/"
    And I enter the username "admin"
    And I enter the password "test1"
    And I submit the login
    Then I should be at the homepage
    
  # Note: Since this relies on emails created during user registration, it will fail if BOOTSTRAP_TESTS is set
  @no_bootstrap
  Scenario:  Verifying an account verification email was sent using the correct information
    When I go to the email application
    And I click the admin verify account email
    Then I should see a from field containing the address "<no_reply@prometheuscomputing.com>"
    And I should see a to field containing the address "<admin@mycompany.net>"
    And I should see a subject field containing the subject "Verify Email Account"
    And I should see an email containing the correct verify email link
    
  # Note: Since this relies on emails created during user registration, it will fail if BOOTSTRAP_TESTS is set
  @no_bootstrap
  Scenario:  Verifying and email account using the verification email
    When I go to the email application
    And I click the admin verify account email
    And I click the link in the email
    Then I should see the text "The email account admin@mycompany.net has been verified."
    
    #Check the preferences page to see if the email was verified
    Given I have logged in as an administrator
    And I go to the preferences panel
    Then I should not receive the message "This account hasn't verified its email account yet. Do this by visiting the Confirm Email sent to your account when it was created or by reentering a new email address below."
    
    