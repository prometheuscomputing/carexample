module Geography
  class Address
    # Constrain the Address -> Vehicle (Registered Vehicles) association to be a subset
    # of the vehicles owned by the person at that address.
    add_association_options(:registered_vehicles, :subset_of => 'person.vehicles')
  end
end
