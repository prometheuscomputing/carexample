# Test object history function
Feature: Object History
  In order to track changes to an object
  As a user
  I want to view the history of the object's attributes and associations
  
  Background:
    Given I have reset the database
    And I have logged in as a user
    And I expand the "People" header
    And I edit the "People" whose "Name" is "Bob"
  
  # TODO: Fix this when there is no longer a default value gets placed in int's and float's
  Scenario: Viewing changes made to an object's attributes and associations
    When I click the "History" link
    Then I should see "2" versions
    And version "1" should state that "Name", "Date Of Birth", "Family Size", "Founded", "Description", "Photo", "Addresses", "Vehicles", "Drives", "Maintains", "Code Representation", and "Occupying" were modified
    # NOTE: "Vehicles", "Drives" association classes were also modified, but we are not currently tracking attribute changes to association classes here
    And version "2" should state that "Weight" and "Repair Shops" were modified
    When I go to the previous page
    And I set the "Name" to "Charlie"
    And I click the "History" link
    Then I should see "3" versions
    When I select version "2"
    And I select version "3"
    And I click the "View Changes" button
    Then I should see "1" change
    And I should see that "Name" changed from "Bob" to "Charlie"
    When I go to the previous page
    And I set the "Description" to "Bob's description\n\nThis is a new line\n\n This is another!\n\nFinished."
    And I set the "Family Size" to "4"
    And I click the "History" link
    Then I should see "4" versions
    And version "4" should state that "Description" and "Family Size" were modified
    When I select version "2"
    And I select version "4"
    And I click the "View Changes" button
    Then I should see "3" changes
    And I should see that "Name" changed from "Bob" to "Charlie"
    And I should see that "Description" changed from "Bob's description" to "Bob's description\n\nThis is a new line\n\n This is another!\n\nFinished."
    And I should see that "Family Size" changed from "3" to "4"
    When I select the change to "Description"
    Then I should see the differences between "Bob's description" and "Bob's description\n\nThis is a new line\n\n This is another!\n\nFinished."
