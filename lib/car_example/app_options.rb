module CarExample
  # Defaults specified here can be overriden on the command line.
  DEFAULT_APP_OPTIONS = {
   # :custom_spec            => File.join(File.expand_path(File.dirname(__FILE__)), "custom_spec"),
   # :db_url                 => 'postgres://prometheus:sT0rmcR0w@localhost:5432/car_example',
   # :db_url                 => 'mysql://prometheus:sT0rmcR0w@localhost:3306/car_example',
   :puma_ctl_port          => '9294',
   :mode                   => :dev,
   :spec_name              => 'car_example_generated.rb', 
   :spec_module            => 'CarExampleGenerated',
   :spec_title             => 'Car Example',
   :additional_specs => [['gb_test_generated', 'GbTestGenerated']],
 
   # :model_extensions => model_extensions_path,
   :multivocabulary        => true,
   :default_org_user_limit => 6,
   :tree_view              => {},#{:enabled => true},
   :workflow_options       => {
     :strict_role_enforcement   => true,
     :advance_from_object_pages => true, # Does not seem to be used. Purpose is unknown -SD
     :workflow_classes          => ["Automotive::Car", "Automotive::Minivan"]
   },
   :enable_registration_without_inv_code => true,
   :disable_external_resources => true,
   # :disable_info_messages => true,
   :custom_pages => {
     # :workflow =>  {
     #   :name            => "Workflow",
     #   :url             => "/workflow/",
     #   :show_in_nav_bar => true
     # },
     :custom_home => {
       :url             => "/car_example_home",
       :show_in_nav_bar => false
     },
     :vehicles_info => {
       :name            => "Vehicle Info",
       :url             => "/vehicles_info/vehicles_info",
       :show_in_nav_bar => true
     }
   },
   :custom_page_root         => relative('pages'),
   # :layout                   => 'carexample_layout',
   :enable_db_download       => true,
   :home_title               => '',
   # :custom_home_page         => {
   #   :url => '/car_example_home',
   #   :name => 'CarHome'
   # },
   :favicon                  => '/images/car_crash.png',
 
   # Possible values in array for the next three options: ['Document', 'Organizer','Collection']
   # :disable_history_for_views     => [],
   # :disable_cloning_for_views     => [],
   # :disable_breadcrumbs_for_views => [],
   :policy_active                 => true,
   :fake_option                   => nil
  }

  # Done this way in order to support testing more easily
  DEFAULT_APP_OPTIONS[:change_tracker_mode] = :limited       if ENV['LIMITED_MODE']       == 'true'
  DEFAULT_APP_OPTIONS[:change_tracker_mode] = :extra_limited if ENV['EXTRA_LIMITED_MODE'] == 'true'
end
