# Test Site Administration
#  * Site Administrator Management
#  * Role Management
#  * Registration Requirements Management
#  * Invitation Code Management
Feature: Site Administration
  In order to manage roles, registration requirements, and invitation codes
  As an administrator
  I want to administrate the site
  
  Background:
    Given I have reset the database
    And I have logged in as an administrator
    And I go to the site administration panel
  
  Scenario: Adding/Deleting a site admin
    When I add a user named "another_admin" with the password "admin1" with the email "admin@test.com"
    And I submit the page
    Then there should be an administrator named "another_admin"
    # Refresh and double check
    When I go to the site administration panel
    Then there should be an administrator named "another_admin"
    And "another_admin" should be able to login with the password "admin1" as an administrator
    When I go to the site administration panel
    When I mark the administrator named "another_admin" to be deleted
    And I submit the page
    Then there should not be an administrator named "another_admin"
    And I submit the page
    Then there should not be an administrator named "another_admin"
    And "another_admin" should not be able to login with the password "admin1"
    
  Scenario: Adding/Deleting a role
    When I add a role named "Editor"
    And I submit the page
    Then there should be a role named "Editor"
    When I mark the role named "Editor" to be deleted
    And I submit the page
    Then there should not be a role named "Editor"
    And I submit the page
    Then there should not be a role named "Editor"
  
  Scenario: Using all options in site admin panel simultaneously
    # First round of changes
    When I add an administrator named "admin1"
    And I add a role named "Editor"
    And I add a perspective named "Stakeholder"
    And I add a password requirement with the regular expression "^.{5,10}$", the description "Check password length", and the failure message "Your password must be between 5 and 10 characters"
    And I submit the page
    
    # Check first round
    Then there should be an administrator named "admin1"
    And there should be a role named "Editor"
    And there should be a perspective named "Stakeholder"
    And I should see a password requirement with the regular expression "^.{5,10}$"
    
    # Second round of changes
    When I add an administrator named "admin2"
    And I add a role named "Manager"
    And I add a perspective named "Accounting" based on "Stakeholder"
    And I add a password requirement with the regular expression "^[^\?]*$", the description "No question marks", and the failure message "Your password may not contain '?' characters"
    And I submit the page
    
    # Check second round
    Then there should be an administrator named "admin2"
    And there should be a role named "Manager"
    And there should be a perspective named "Accounting"
    And I should see a password requirement with the regular expression "^[^\?]*$"
    
    # Final round of changes
    When I mark the administrator named "admin1" to be deleted
    And I add an administrator named "admin3"
    And I mark the role named "Editor" to be deleted
    And I add a role named "Sub-manager"
    And I mark the perspective named "Stakeholder" to be deleted
    And I add a perspective named "Executive"
    And I mark the password requirement with the regular expression "^.{5,10}$" to be deleted
    And I add a password requirement with the regular expression "[^\!]*", the description "No exclamation marks", and the failure message "Your password may not contain '!' characters"
    And I submit the page
    
    # Check final round
    # Site Admins
    Then there should be an administrator named "admin"
    And there should be an administrator named "admin2"
    And there should be an administrator named "admin3"
    And there should not be an administrator named "admin1"
    # Roles
    And there should be a role named "Administrator"
    And there should be a role named "Manager"
    And there should be a role named "Sub-manager"
    And there should not be a role named "Editor"
    # Perspectives
    And there should be a perspective named "Accounting"
    And there should be a perspective named "Executive"
    And there should not be a perspective named "Stakeholder"
    # Password requirements
    And I should see a password requirement with the regular expression "^[^\?]*$"
    And I should see a password requirement with the regular expression "[^\!]*"
    And I should not see a password requirement with the regular expression "^.{5,10}$"