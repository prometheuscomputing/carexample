# --------------------------------------- Version helpers
def verify_versions(num_versions)
  expect(get_versions.count).to eq(num_versions)
end
def verify_version_modified(version, expected_modified_properties)
  version_element = get_version(version)
  version_text = version_element.text
  modified_properties = version_text.split(' Modified ').last.strip.gsub(/,? and/, ',').split(', ')
  expect(modified_properties).to match_array(expected_modified_properties)
end
def select_version(version)
  get_version(version).find('input').set(true)
end
def deselect_version(version)
  get_version(version).find('input').set(false)
end
def get_version(version_position)
  get_versions[version_position - 1]
end
def get_versions
  page.find('#timestamp_list').all('.ct_timestamp')
end
# --------------------------------------- End Version helpers


# --------------------------------------- Change helpers
def verify_changes(num_changes)
  expect(get_changes.count).to eq(num_changes)
end
def verify_change(property, old_value, new_value)
  change_tds = get_change(property).all('td')
  expect(change_tds[1].text).to eq(old_value)
  expect(change_tds[2].text).to eq(new_value)
end
def select_change(property)
  get_change(property).click
end
def get_change(property)
  change = get_changes.select{|c| c.text =~ /^#{property}/i}.first
  raise "Could not find change row for property: #{property}" unless change
  change
end
def get_changes
  find('table.changes').all('tr')[1..-1]
end
# --------------------------------------- End Change helpers


# --------------------------------------- Diff helpers
def verify_diff(_old_value, _new_value)
  # Need to improve this. For now just check table existance
  find('table.diff')
end
# --------------------------------------- End Diff helpers