$(function() {
  setup_vehicle_details_select();
  setup_vehicles_table_button();
});

function setup_vehicle_details_select() {
  $('#vehicle_details').change(function() {
    var vehicle_model = $(this).val();
    window.location = '/vehicles_info/vehicle_details/' + escape(vehicle_model)
  });
}

function setup_vehicles_table_button() {
  $('#vehicles_table').click(function(){
    window.location = '/vehicles_info/vehicles_table/'
  });
}