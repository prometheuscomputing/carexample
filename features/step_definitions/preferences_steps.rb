When /^I go to the preferences panel/ do
  visit_preferences
end

# --------------------------------------- Accessibility steps
When /^I enable the accessibility interface$/ do
  enable_accessibility
end
When /^I disable the accessibility interface$/ do
  disable_accessibility
end
# --------------------------------------- End Accessibility steps

# --------------------------------------- Role steps
When /^I add myself to the (.+?) roles?/ do |roles_str|
  add_roles(parse_quoted_string_list(roles_str))
end
Then /^I should( not)? be a member of the (.+?) roles?/ do |negative, roles_str|
  verify_roles(parse_quoted_string_list(roles_str), negative)
end
When /^I remove myself from the (.+?) roles?/ do |roles_str|
  remove_roles(parse_quoted_string_list(roles_str))
end
Then /^I should( not)? see the (.+?) roles?/ do |negative, roles_str|
  verify_roles_present(parse_quoted_string_list(roles_str), negative)
end
# --------------------------------------- End Role steps


# --------------------------------------- Perspectives steps
When /^I add myself to the (.+?) perspectives?/ do |perspectives_str|
  add_perspectives(parse_quoted_string_list(perspectives_str))
end
Then /^I should( not)? be a member of the (.+?) perspectives?/ do |negative, perspectives_str|
  verify_perspectives(parse_quoted_string_list(perspectives_str), negative)
end
When /^I remove myself from the (.+?) perspectives?/ do |perspectives_str|
  remove_perspectives(parse_quoted_string_list(perspectives_str))
end
# --------------------------------------- End Perspectives steps