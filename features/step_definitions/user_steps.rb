# --------------------------------------- Login/Registration steps
Given /^I have logged in(?: as an? (administrator|user))?$/ do |user_level|
  pw = nil
  case user_level
  when 'administrator'
    UserHelpers.set_test_user(UserHelpers::ADMIN)
  else
    UserHelpers.set_test_user(UserHelpers::USERS.first)    
  end
  sign_in(UserHelpers.test_user, pw)
end
# This will reset the $test_user variable and prevent helper methods from automatically signing in using it
Given /^I stay logged out$/ do
  UserHelpers.set_test_user(nil)
end
When /^I sign out/ do
  sign_out
end
When /^I register as "([^"]*)"(?: with the password "([^"]*)")?(?: using the invitation code "([^"]*)")?$/ do |username, password, invite_code| #"
  register(username, password, invite_code)
end
When /^I log in as "([^"]*)"(?: with the password "([^"]*)")?$/ do |username, password|
  sign_in(username, password)
end
Then /^I should be at the login page$/ do
  verify_on_login_page
end
Then /^I should be at the homepage/ do
  verify_homepage
end
Then /^I should be at the registration page$/ do
  verify_on_registration_page
end
Then /^I should be at the forgot password page$/ do
  verify_on_forgot_password_page
end
When /^I submit the registration$/ do
  submit_registration
end
When /^I submit the login$/ do
  submit_login
end
When /^I submit the forgot password page/ do
  submit_forgot_email
end
When /^I enter the invitation code "(.*?)"$/ do |code|
  enter_invitation_code(code)
end
Then /^the username should be "(.*?)"$/ do |username|
  verify_username(username)
end
Then /^the password should be "(.*?)"$/ do |password|
  verify_password(password)
end
Then /^the re-entered password should be "(.*?)"$/ do |password|
  verify_reentered_password(password)
end
Then /^the invitation code should be "(.*?)"$/ do |code|
  verify_invitation_code(code)
end
# --------------------------------------- End Login/Registration steps


When /^I enter the email "(.*?)"$/ do |email|
  enter_email(email)
end

# --------------------------------------- Password Change steps
Then /^I should be prompted to change my password$/ do
  verify_password_change_prompt
end
When /^I enter the (new )?username "(.*?)"$/ do |is_new, username|
  is_new ? enter_new_username(username) : enter_username(username)
end
When /^I enter the (old |new )?password "(.*?)"$/ do |old_new, password|
  case old_new
  when 'old '
    enter_old_password(password)
  when 'new '
    enter_new_password(password)
  else
    enter_password(password)
  end
end
When /^I re-enter the (new )?password "(.*?)"$/ do |is_new, password|
  is_new ? reenter_new_password(password) : reenter_password(password)
end
When /^I confirm the password change$/ do
  confirm_password_change
end
When /^I skip the password change$/ do
  skip_password_change
end
# --------------------------------------- End Password Change steps