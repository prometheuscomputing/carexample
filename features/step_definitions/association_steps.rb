# --------------------------------------- Action steps
When /^I click on the "(.*)" header/ do |header|
  click_header(header)
end
When /^I expand the "(.*)" header/ do |header|
  expand_header(header)
end
When /^I click on the "(.*)" column header/ do |header|
  click_column_header(header)
end
# Generic button clicker for association widgets
When /^I click the (only )?"(.*)" button under the "(.*)" header$/ do |only_button, button, header|
  click_button_under_header(button, header, nil, only_button)
end
Then /^I should see that there are a total of "(.*)" "(.*)" in the header$/ do |number, header|
  verify_association_header_count(header, number.to_i)
end
Then /^I should see that there are a total of "(.*)" "(.*)" in the search results header$/ do |number, header|
  verify_association_search_results_count(header, number.to_i)
end
# Then /^I should see that there are a total of "(.*)" "(.*)" in the collection$/ do |number, header|
#   verify_association_contents(header, number.to_i)
# end
When /^I select the "([^"]*)"( (?:where|whose) .+)?$/ do |header, conditions_string| #"
  select_rows(header, parse_conditions(conditions_string))
end
When /^I drag the "(.*?)"( (?:where|whose) .+?) to position "(-?\d+)"/ do |header, conditions_string, position|
  drag_row_to_position(header, parse_conditions(conditions_string), position.to_i)
end
When /^I drag the "(.*?)" document association( (?:where|whose) .+?) to position "(-?\d+)"/ do |header, conditions_string, position|
  document_drag_row_to_position(header, parse_conditions(conditions_string), position.to_i)
end
When /^I manually set the position of the "(.*?)"( (?:where|whose) .+?) to "(-?\d+)"/ do |header, conditions_string, position|
  set_row_to_position(header, parse_conditions(conditions_string), position.to_i)
end
When /^I show the (possible|current) "(.*)"/ do |type, header|
  if type == 'current'
    show_current(header)
  elsif type == 'possible'
    show_possible(header)
  else
    raise 'Invalid usage of cucumber step'
  end
end
# --------------------------------------- End Action steps


# --------------------------------------- Deletion steps
Then /^I should( not)? be warned that I will also delete (.+)$/ do |negate, objects_description|
  verify_deletion_warning_for(parse_objects_description(objects_description), negate)
end
When /^I confirm the homepage deletion of the "(.*)"$/ do |header|
  confirm_homepage_delete(header)
end
# --------------------------------------- End Deletion steps


# --------------------------------------- Edit/Creation steps
# Click first edit link
When /^I edit (?:an?|the) "([^"]*)"( (?:where|whose) .+)?$/ do |header, conditions_string| #"
  edit_object(header, parse_conditions(conditions_string))
end
# Unnamed association class row edit. Slower than the named association class step definition below
When /^I edit the association class for the "([^"]*)" ((?:where|whose) .+)/ do |header, conditions_string| #"
  edit_association_class_row(header, parse_conditions(conditions_string))
end
# Named assocation class row edit
When /^I edit the "([^"]*)" for the "([^"]*)" ((?:where|whose) .+)$/ do |association_class_name, header, conditions_string|
  edit_association_class_row(header, parse_conditions(conditions_string), association_class_name)
end
Then /^the "([^"]*)"( (?:where|whose) .+?) should( not)? be(?: a)? pending( "[^"]+")?$/ do |header, conditions_string, not_pending, status|
  # Captured the quotes on status, shaving them off.
  status = status.sub(/^\s"/, '').sub(/"$/, '') if !status.nil?
  verify_row(header, parse_conditions(conditions_string), status, not_pending)
end
# Verify presence (or absence) of link(s) to object(s)
Then /^I should( not)? see an option to visit a "([^"]+)"$/ do |negate, header| #"
  verify_object_link(header, negate)
end

# Create via dropdown
When /^I create an? "([^"]*)"(?: through "([^"]+)")?(?: of type "([^"]+)")?(?: (?:where|whose) (".+")( and return)?)?/ do |header, through, type, attributes_string, go_back| #"
  associate_to_new_object(header, type, parse_attributes(attributes_string), go_back, through, {}, true)
end
# Verify presence (or absence) of creation dropdown
Then /^I should( not)? see an option to create a new "([^"]+)"$/ do |negate, header| #"
  verify_creation_dropdown(header, negate)
end
# --------------------------------------- End Edit/Creation steps

# --------------------------------------- Verify steps
# Verify count and association status of visible collection rows
Then /^I should see( the first)? "(\d+)" ?(unassociated|associated|pending|pending deletion)? "([^"]+)"/ do |_, number, assoc_status, header| #"
  verify_association_contents(header, number.to_i, assoc_status)
end
# Verify position of a single collection row
Then /^I should see the "(.*?)"( (?:where|whose) .+?) in position "(\d+)"( on the popup)?/ do |header, conditions_string, position, on_popup|
  if on_popup
    get_row_position(get_popup_widget(header), parse_conditions(conditions_string)).should == position.to_i
  else
    get_row_position(get_widget(header), parse_conditions(conditions_string)).should == position.to_i
  end
end
# Verify (non)existance of a single collection row
Then /^I should( not)? see an? "([^"]*)"( (?:where|whose) .+)?/ do |negative, header, conditions_string| #"
  row_is_visible?(header, parse_conditions(conditions_string)).should_unless(negative) == true
end
# Verify existance of an inner organizer
Then /^I should see the associated "(.*?)"( (?:where|whose) .+)?/ do |header, conditions_string|
  verify_organizer(header, parse_conditions(conditions_string))
end
# Verify properties of all visible collection rows
Then /^I should only see "([^"]*)" ((?:where|whose) .+)/ do |header, conditions_string| #"
  verify_collection_filter(header, parse_conditions(conditions_string))
end
# Verify show possible associations button is not present
Then /^I should not be able to show possible "(.*?)"/ do |header|
  verify_show_possible_not_present(header)
end
# Verify associations are present on the page
Then /^I should see the associations (.+)/ do |associations_str|
  verify_associations_present(parse_quoted_string_list(associations_str))
end
#"
# Verify the unsaved changes warning sign presence.
Then /^I should( not)? see the unsaved changes warning for "([^"]*)"$/ do |be_or_not_be, header|
  #"
  verify_message_warning_presence(header, be_or_not_be)
end

# --------------------------------------- End Verify steps


# --------------------------------------- Filtering steps
Then /^I should be on page "(\d+)" out of "(\d+)"$/ do |current_page, total_pages|
  confirm_current_page(current_page)
  confirm_total_pages(total_pages)
end
Then /^the search results count should have "(\d+)" records$/ do |records_count|
  confirm_search_results_total(records_count)
end
When /^I open the "(.*)" search/ do |header|
  toggle_filter_for(header)
  find '.search_all_filter'
end
When /^I click on the "(.*)" advanced search link( in the popup)?/ do |header, on_popup|
  if on_popup
    open_popup_advanced_filter(header)
  else
    open_advanced_filter(header)
  end
end
When /^I filter( on the popup)? to see "([^"]*)" ((?:where|whose) .+)/ do |on_popup, header, conditions_string| #"
  if on_popup
    enter_filter(get_popup_widget(header), parse_conditions(conditions_string))
  else
    enter_filter(get_widget(header), parse_conditions(conditions_string))
  end
end
When /^I search all fields( on the popup)? to see "([^"]*)" with "([^"]*)"/ do |on_popup, header, search_terms|
  if on_popup
    enter_search_all_filter(get_popup_widget(header), search_terms)
  else
    enter_search_all_filter(get_widget(header), search_terms)
  end
end
When /^I reset the "(.*?)" filter/ do |header|
  reset_filter(header)
end
When /^I submit the "(.*)" search( on the popup)?$/ do |header, on_popup|
  if on_popup
    submit_search(get_popup_widget(header))
  else
    submit_search(get_widget(header))
  end
end
# --------------------------------------- End Filtering steps

# --------------------------------------- Pagination steps
When /^I view "(\d+)" "(.*?)" per page/ do |items_per_page, header|
  set_items_per_page(header, items_per_page.to_i)
end
When /^I view "(\d+)" items per page/ do |items_per_page|
  set_items_per_page(nil, items_per_page.to_i)
end
When /^I view page "(\d+)" of "(.*?)"$/ do |page_number, header|
  set_page(header, page_number.to_i)
end
# --------------------------------------- End Pagination steps

# --------------------------------------- Disabled Actions steps
When /^I should not be able to add a "([^"]*)"$/ do |header| #"
  click_header(header)
  confirm_button_on_page("Add Association", true)
  confirm_button_on_page("Save & Create New #{header}", true)
end

When /^I should not be able to remove (?:a|the) "([^"]*)"$/ do |header| #"
  click_header(header)
  confirm_button_on_page("Delete", true)
  confirm_button_on_page("Break Association", true)
end

When /^I should not be able to delete (?:a|the) "([^"]*)"$/ do |_header| #"
  confirm_button_on_page("Delete", true)
end

When /^I should not be able to create a "([^"]*)"$/ do |header| #"
  click_header(header)
  confirm_button_on_page("Save & Create New #{header}", true)
end
# --------------------------------------- End Disabled Actions steps