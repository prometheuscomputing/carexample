# --------------------------------------- Document helpers
def add_document_association(header, scope = nil)
  click_button_under_header({:locator => '.add_association'}, header, scope)
  sleep(0.1)
end
def edit_document_associations(header, conditions, operation)
  rows = get_rows(header, conditions)
  raise "Unable to edit row(s). Could not find any rows matching conditions." if rows.empty?
  rows.each do |row|
    button_selector = operation == 'remove' ? '.break_association' : '.delete_object'
    case Capybara.current_driver
    # TODO: not sure if this is necessary. Hover should be supported in capybara-webkit -SD
    when :webkit, :poltergeist
      row.trigger(:mouseover)
    else
      row.hover
    end
    row.first(button_selector).click
  end
end
def verify_new_document_association(header, is_existing)
  w = get_widget(header)
  document_association_items = w.all('.document_association_item')
  last_item = document_association_items.last
  expect(last_item).to have_class('new_association')
  if is_existing
    expect(last_item).to have_class('existing_object')
  else
    expect(last_item).to have_class('new_object')
  end
end
def verify_document_association_operation(header, operation, conditions)
  rows = get_rows(header, conditions)
  rows.each do |row|
    case operation
    when 'addition'
      expect(row).to have_class('new_association')
    when 'removal'
      expect(row).to have_class('broken_association')
    when 'deletion'
      expect(row).to have_class('deleted_object')
    end
  end
end
def verify_document_association_item(label, conditions, status)
  rows = get_rows(label, conditions)
  expect(rows.length).to eq(1)
  row = rows.first
  case status
  when 'added'
    expect(row).to have_class('new_association')
  when 'removed'
    expect(row).to have_class('broken_association')
  when 'deleted'
    expect(row).to have_class('deleted_object')
  when 'saved'
    expect(row).not_to have_class('new_association')
  end
end


# Association popup helpers
def confirm_association
  click_popup_button('addAssociation', 'Add Item')
end
def cancel_association
  cancel_popup('addAssociation')
end
def associate_to_new(type)
  association_popup = find('#addAssociationModal')
  association_popup.first('#new_association_option', :visible => true).set(true)
  if type_select = association_popup.first('#new_association_type', :visible => true)
    type_select.find(:option, type).select_option
  end
end
# Select an existing object to associate to.
# Currently ignores the passed type
def associate_to_existing(conditions = [{}, {}, {}, {}])
  select_existing_object(conditions)
end
def association_collection_widget
  find('#addAssociationModal').find('.collection', :visible => true)
end
def open_existing_object_filter
  open_filter_for(association_collection_widget)
end
def submit_existing_object_search
  submit_search(association_collection_widget)
end
def set_existing_objects_per_page(items_per_page)
  set_items_per_page(association_collection_widget, items_per_page)
end
def enter_existing_object_filter(conditions)
  enter_filter(association_collection_widget, conditions)
end
def select_existing_object(conditions)
  select_rows(association_collection_widget, conditions)
end
# --------------------------------------- End Document helpers