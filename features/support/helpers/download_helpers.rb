module DownloadHelpers
  const_set?('TIMEOUT', 10)

  extend self

  def downloads
    Dir[File.join(TEST_DOWNLOADS_DIR, '*')]
  end

  def download
    downloads.first
  end
  
  def download_filename
    File.basename(download)
  end
  
  def download_extension
    File.extname(download)[1..-1]
  end

  def download_content
    wait_for_download
    File.read(download)
  end

  def wait_for_download
    Timeout.timeout(TIMEOUT) do
      sleep 0.1 until downloaded?
    end
  end

  def downloaded?
    !downloading? && downloads.any?
  end

  def downloading?
    case Capybara.current_driver
    when :chrome, :headless_chrome
      downloads.grep(/\.crdownload$/).any?
    when :firefox
      downloads.grep(/\.part$/).any?
    else
      raise "Only Firefox and Chrome drivers can be used with the DownloadHelpers Module"
    end
  end

  def clear_downloads
    FileUtils.rm_f(downloads)
  end
end

World(DownloadHelpers)

Before do
  clear_downloads
end

After do
  clear_downloads
end


def verify_download(filename, type = nil)
  case Capybara.current_driver
  when :webkit, :poltergeist
    mime_type = get_mime_for_type(type)
    # wait until timeout for expected mime type to occur (required for use in vagrant env)
    wait_until {page.response_headers['Content-Type'] == mime_type}
    expect(page.response_headers['Content-Type']).to eq(mime_type) if mime_type
    expect(page.response_headers['Content-Disposition']).to match(/attachment/)
    expect(page.response_headers['Content-Disposition']).to match(/filename\s*=\s*("|')?#{filename}("|')?/) if filename
  else # :chrome, :firefox, :headless_chrome
    # Assume that we can't read response headers (selenium can't)
    wait_for_download
    expect(download).not_to be_nil
    expect(download_filename).to match(/#{filename}/) if filename
    expect(download_extension).to match(get_extension_for_type(type)) if type
    clear_downloads
  end
end

def get_mime_for_type(type_string)
  return nil unless type_string
  case type_string
  when 'jpeg'
    'image/jpeg'
  when 'binary'
    'application/octet-stream'
  when 'text'
    'text/plain'
  else
    raise "Unknown file type: #{type}"
  end
end
def get_extension_for_type(type_string)
  return nil unless type_string
  case type_string
  when 'jpeg'
    /jpe?g/
  when 'binary'
    /.*/
  when 'text'
    'txt'
  else
    raise "Unknown file type: #{type_string}"
  end
end