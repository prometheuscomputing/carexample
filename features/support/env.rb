require 'fileutils'
require 'lodepath'
LodePath.amend
LodePath.display
check_coverage = ENV['TEST_COVERAGE']
if check_coverage
  puts "TESTING COVERAGE"
  require 'simplecov'
  # Set SimpleCov root directory to projects dir (one dir above gui_site) so that all local projects are tested
  SimpleCov.root(relative('../../../'))
  SimpleCov.start do
    add_filter 'gui_site/features/'
    add_filter 'gui_site/spec/'
    add_filter 'gui_site/car_example/'
    add_filter 'gui_site/tmp/'
  end
  # Set parallel test groups to true for SimpleCov (probably unnecessary since it should be the default)
  ENV['PARALLEL_TEST_GROUPS'] = 'true'
  # 2 hour timeout for merged test results
  SimpleCov.merge_timeout 2 * 3600
end

require_relative 'ruby_extensions'
require 'selenium/webdriver'
require 'rspec'
require 'rspec/expectations'
require 'capybara/cucumber'
require 'capybara-screenshot'
require 'capybara-screenshot/cucumber'
require 'capybara'
require 'ramaze'

Ramaze.options.mode  = :live # Use live mode for testing!

include ::RSpec::Matchers
RSpec.configure { |config| config.expect_with(:rspec) { |c| c.syntax = [:should, :expect] } }
RSpec::Support::ObjectFormatter.default_instance.max_formatted_output_length = 500

# Which port will app run on
GUI_SITE_PORT = ENV['GUI_SITE_PORT'] ? ENV['GUI_SITE_PORT'].to_i : 3000

# Where to put downloaded files
TEST_DOWNLOADS_DIR = relative 'test_downloads/' unless defined?(TEST_DOWNLOADS_DIR) # Shouldn't this be under tmp?   
# Where to backup the baseline db for bootstrapping testing process (skips registering users, etc)
# This is only used if ENV['BOOTSTRAP_TESTS'] is set
BOOTSTRAP_DB = relative '../../tmp/bootstrap_db.marshal'

# Ensure that the directories exist. The tmp directory should not be stored in the repository, but there are problems if it does not exist.
FileUtils.mkdir_p( TEST_DOWNLOADS_DIR ) unless File.exists?( TEST_DOWNLOADS_DIR )
parent_directory = File.dirname(BOOTSTRAP_DB)
FileUtils.mkdir_p( parent_directory ) unless File.exists?( parent_directory )

WEB_ROOT = relative('../lib/gui_site/')

# ___________________________________________________

# Clear out ARGV to avoid issues with gui_site attempting to interpret cucumber arguments
cuke_args = ARGV.dup
ARGV.replace([])

if ENV['GUI_SITE_MODEL_SETUP']
  require ENV['GUI_SITE_MODEL_SETUP']
else
  require_relative '../../car_example/setup.rb'
end

# Override #run to do nothing. This is so we can load config.ru as setup-only
# TODO: move setup code from config.ru and load the new file here
def run(*args); end

load LodePath.find('gui_site/config.ru')

Capybara.register_driver :chrome do |app|
  Capybara::Selenium::Driver.new(app,
    browser: :chrome,
    args: ['--window-size=1920,1080', '--no-sandbox', '--disable-dev-shm-usage'],
    prefs: {'download.default_directory' => TEST_DOWNLOADS_DIR.to_s}
  )
end
#
Capybara.register_driver :headless_chrome do |app|
  # Working config initially copied from: https://stackoverflow.com/questions/48810757/setting-default-download-directory-and-headless-chrome
  options = Selenium::WebDriver::Chrome::Options.new
  options.add_argument('--headless')
  options.add_argument('--no-sandbox')
  # options.add_argument('--disable-gpu')
  options.add_argument('--disable-popup-blocking')
  options.add_argument('--window-size=1920,2160')

  options.add_preference(:download,
    directory_upgrade: true,
    prompt_for_download: false,
    default_directory: TEST_DOWNLOADS_DIR.to_s
  )
  options.add_preference(:browser, set_download_behavior: { behavior: 'allow' })

  # Setup full logging capability. Logs are accessed like: `driver.browser.manage.logs.get(:browser)`
  # :browser gets you the javascript console from the browser (so, the outputs of console.log() in JS)
  # I think all the others are related to the actual communication b/w selenium and chromedriver
  caps = Selenium::WebDriver::Remote::Capabilities.chrome( "goog:loggingPrefs": {
    browser: 'ALL', client: 'ALL', driver: 'ALL', server: 'ALL'
  } )

  driver = Capybara::Selenium::Driver.new(app, browser: :chrome, options: options, desired_capabilities: caps)
  bridge = driver.browser.send(:bridge)
  path = '/session/:session_id/chromium/send_command'
  path[':session_id'] = bridge.session_id

  bridge.http.call(:post, path,
    cmd: 'Page.setDownloadBehavior',
    params: {behavior: 'allow', downloadPath: TEST_DOWNLOADS_DIR.to_s}
  )
  driver
end

Capybara.default_driver    = :headless_chrome
Capybara.javascript_driver = :headless_chrome
Capybara.server_port = GUI_SITE_PORT
# Capybara.server = :puma, { Silent: true }
Capybara.app = Innate.app
Capybara.default_max_wait_time = 5
# Capybara.configure do |config|
#   config.default_max_wait_time = 5 # because some of this stuff is super slow, like deleting an object graph
# end
# Capybara.configure do |c|
#   # You can use alternate drivers here
#   c.app = Innate.app #Ramaze.middleware(Ramaze.options[:mode])
#
#   # You might want to configure this is you want to save screenshots
#   # c.save_and_open_page_path = File.join(Ramaze.options.roots.first, 'tmp/')
# end

# Setup Capybara to run

# ARGV.replace(cuke_args)

# Capybara.server = :webrick, { Silent: true }
Capybara.server = :puma, { Silent: true }


#  TODO try this as a replacement for mailcatcher
# require 'capybara/email'
# World(Capybara::Email::DSL)




SimpleCov.command_name('normal') if check_coverage

# if ENV['GUI_SITE_MODEL_SETUP']
#   require ENV['GUI_SITE_MODEL_SETUP']
# else
#   require_relative '../../car_example/setup.rb'
# end

# require 'gui_site/meta_info'
# require 'gui_director/meta_info'
# require 'gui_builder/meta_info'
# puts Rainbow("\nI loaded gui_site: #{GuiSite::VERSION}").yellow
# puts Rainbow("I loaded gui_director: #{GuiDirector::VERSION}").yellow
# puts Rainbow("I loaded html_gui_builder: #{HtmlGuiBuilder::VERSION}\n").yellow


# Require local helper functions
require_relative 'hooks.rb'
require_dir(relative('helpers'))
# require_relative 'helpers/clear_view_helpers'
# require_relative 'helpers/generic_helpers'
# require_relative 'helpers/custom_pages_helpers'
# require_relative 'helpers/framework_helpers'
# require_relative 'helpers/user_helpers'
# require_relative 'helpers/association_helpers'
# require_relative 'helpers/attribute_helpers'
# require_relative 'helpers/collection_helpers'
# require_relative 'helpers/db_helpers'
# require_relative 'helpers/download_helpers'
# require_relative 'helpers/js_helpers'
# require_relative 'helpers/object_page_helpers'
# require_relative 'helpers/organizer_helpers'
# require_relative 'helpers/administration_helpers'
# require_relative 'helpers/document_helpers'
# require_relative 'helpers/jquery_helpers'
# require_relative 'helpers/ux_helpers'
#
# Capybara.default_max_wait_time = 2 # Seconds to wait before timeout error. Default is 2

# Saves page to place specfied at name inside of
# test.rb definition of:
#   config.integration_test_render_dir = Rails.root.join("spec", "render")
# NOTE: you must pass "js:" for the scenario definition (or else you'll see that render doesn't exist!)
# def render_page(name)
#   png_name = name.strip.gsub(/\W+/, '-')
#   path = File.join(Rails.application.config.integration_test_render_dir, "#{png_name}.png")
#   page.driver.render(path)
# end

# # shortcut for typing save_and_open_page
# def page!
#   save_and_open_page
# end

# Capybara.app = Innate.app
# # Capybara.configure do |c|
# #   # You can use alternate drivers here
# #   c.app = Innate.app #Ramaze.middleware(Ramaze.options[:mode])
# #
# #   # You might want to configure this is you want to save screenshots
# #   # c.save_and_open_page_path = File.join(Ramaze.options.roots.first, 'tmp/')
# # end
#
# # Setup Capybara to run
# Capybara.server_port = GUI_SITE_PORT

Capybara::Screenshot.register_driver(:chrome) do |driver, path|
  driver.browser.save_screenshot(path)
end
Capybara::Screenshot.register_driver(:headless_chrome) do |driver, path|
  driver.browser.save_screenshot(path)
end
Capybara::Screenshot.register_driver(:selenium_chrome_headless) do |driver, path|
  driver.browser.save_screenshot(path)
end

Capybara.asset_host = 'http://localhost:' + GUI_SITE_PORT.to_s # should assist in saved HTML pages/screenshots to look OK as long as server is running *on the same port*.
  

# NOTE: Activating this section will run the tests using Thin instead of webkit-server
#       It is much slower for tests, but can be helpful to gauge the relative performance 
#         of different Thin options. -SD
if ENV['TEST_USING_THIN']
  Capybara.register_server("thin") do |app, port, host|
    require 'rack/handler/thin'
    Rack::Handler::Thin.run(app, :Port => port, :Host => host)
  end
  Capybara.server = :thin
end

if ENV['TEST_USING_PUMA']
  puts Rainbow('Whoa! TEST_USING_PUMA').red.blink
  Capybara.register_server("puma") do |app, port, host|
    require 'rack/handler/puma'
    Rack::Handler::Puma.run(app, Host: host, Port: port, Threads: "0:16")
  end
  Capybara.server = :puma
end

# Attempt to run headlessly (required for running on headless systems like vagrant)
# Note: this will silently fail if Xvfb is not installed on the system
# headless = Headless.new rescue nil unless Capybara.default_driver == :poltergeist
# headless.start if headless

# require 'net/http' # Used to kill mailcatcher
#For testing the email start the server and end the server
# `mailcatcher`
# @pid = Process.spawn("mailcatcher")
# puts @pid
# Use alternate ports for mailcatcher, based off GUI_SITE_PORT
# FIXME mailcatcher is broken.  replace with something else.
MAILCATCHER_SMTP_PORT = GUI_SITE_PORT + 1000
MAILCATCHER_HTTP_PORT = GUI_SITE_PORT + 2000
# puts "Mailcatcher ports: SMTP: #{MAILCATCHER_SMTP_PORT} and HTTP: #{MAILCATCHER_HTTP_PORT}"
# `mailcatcher --smtp-port #{MAILCATCHER_SMTP_PORT} --http-port #{MAILCATCHER_HTTP_PORT}`
at_exit do
  # Kill mail catcher by sending delete http request
  # FIXME fix after replacing mailcatcher
  # Net::HTTP.new('localhost', MAILCATCHER_HTTP_PORT).request(Net::HTTP::Delete.new('/'))
  # Alternative kill methods:
  # `lsof -i tcp:#{MAILCATCHER_HTTP_PORT} | awk 'NR!=1 {print $2}' | xargs kill`
  # `lsof -i tcp:#{MAILCATCHER_SMTP_PORT} | awk 'NR!=1 {print $2}' | xargs kill`
  # headless.destroy if headless
  puts Rainbow("\nSCREENSHOTS should be in #{Capybara.save_path}").magenta
end
#
# # Override #run to do nothing. This is so we can load config.ru as setup-only
# # TODO: move setup code from config.ru and load the new file here
# def run(*args); end
#
# load LodePath.find('gui_site/config.ru')

# Ramaze.start(:started => true) 

# populate once, then swap out DB for additional performance
# NOTE: This will also reset auto-incrementing ids. This has caused at least one failure to present itself that was otherwise masked.
include UserHelpers

# Save empty database
$empty_db = backup_database

CarExample.populate_data
ChangeTracker.session_hash = nil
Gui.option(:concise_errors, true)

# This doesn't work well with concurrently running tests, so I'm making it conditional -SD
Capybara::Screenshot.prune_strategy = :keep_last_run if ENV['CLEAR_CAPYBARA_SCREENSHOTS'].to_s =~ /true/i
tmp_dir = File.expand_path(File.join(__dir__, '../../tmp'))
screenshot_dir = FileUtils.mkdir_p(File.join(tmp_dir, 'test_results', GUI_SITE_PORT.to_s)).first
Capybara.save_path = screenshot_dir
# Set correct asset path for HTML screenshots. Must start up server manually to use
Capybara.asset_host = 'http://localhost:7000'

# Capybara::DSL.send(:include, UserHelpers)
# Clear session_hash, so Ramaze's session will be used
# clear_terminal(ENV['ERASE_SCROLLBACK'].to_s =~ /true/i) # this clears the terminal before the tests run.  I find it nice.  I also like to get rid of the scrollback buffer.