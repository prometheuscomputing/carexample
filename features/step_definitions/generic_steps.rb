When /^I select "([^"]+)" from the "([^"]+)"$/ do |option, select|
  # Getting intermittent failures here on chrome. It's hard to debug, so i'm just adding sleeps for now. -SD
  sleep 0.1
  select(option, :from => select)
  sleep 0.1
end

When /^I should not see any mention of "([^"]+)"$/ do |label| #"
  verify_abscence(label)
end