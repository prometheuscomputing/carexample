Feature: Custom Spec
  In order to specify which widgets to display and in what order
  As a user
  I want to modify the view using a custom spec
  
  Background:
    Given I have reset the database
    And I have logged in
  
  Scenario: Collection is hidden
    And I should not see any mention of "Mechanics"
    
  Scenario: Reordering a simple widget (by label)
    Given I expand the "People" header
    And I create a "People" of type "Person"
    # "Download Serialized Information" has been reordered to go after "Name"
    Then I should see the "Name" widget immediately before the "Download Serialized Information" widget
  
  Scenario: Reordering a widget (by getter)
    Given I expand the "Vehicles" header
    And I create a "Vehicle" of type "Electric Vehicle"
    # "vehicle_model" has been reordered to go at the beginning of an ElectricVehicle page
    Then I should see the "Vehicle Model" widget first
  
  Scenario: Reordering an association widget
    Given I expand the "Vehicles" header
    And I create a "Vehicle" of type "Hybrid Vehicle"
    # "Drivers" has been reordered to go at the beginning of a Hybrid Vehicle page
    Then I should see the "Drivers" widget first
  
  Scenario: Relabel a widget & order widgets explicitly
    Given I expand the "Vehicles" header
    And I create a "Vehicle" of type "Minivan"
    # The Minivan page has been explicitly ordered
    #  and "Replacement For" has been hidden
    Then I should see the widgets in the order:
      """
      "Make", "Model", "Cost", "Sliding Doors", "Miles Per Gallon", "Owners", "Drivers", "Occupants", "Maintained By", 
      "Being Repaired By", "Components", "Warranty", "Warranty Expiration Date", "Warranty Void", "Serial", "VIN", "Registered At"
      """
  
  Scenario: Sorting using a custom sort
    Given PENDING