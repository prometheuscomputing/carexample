Feature: Multiple contexts via multiple browser tabs
  In order to edit more than one object at a time
  As a user
  I want to edit objects in multiple tabs
  
  Background:
    Given I have reset the database
    And I have logged in
  
  Scenario: Object creation / object edit & association change
    Given I expand the "Vehicles" header
    And I create a "Vehicle" of type "Motorcycle" where "Make" is "Other", "Vehicle Model" is "Tron Bike", and "Cost" is "1000000000"
    And I click on the "Occupants" header
    And I create an "Occupant" of type "Person" whose "Name" is "Jeff Bridges"
    When I open a new tab
    And I expand the "Vehicles" header
    And I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I set the "Cost" to "31337"
    And I click on the "Occupants" header
    And I create an "Occupant" of type "Person" whose "Name" is "Second tab person"
    And I save the page
    And I switch to the 1st tab
    And I save the page
    Then the value of "Name" should be "Jeff Bridges"
    When I click on the "Occupying" header
    Then I should see the associated "Occupying" where "Vehicle Model" is "Tron Bike"
    When I switch to the 2nd tab
    Then the value of "Name" should be "Second tab person"
    When I switch to the 1st tab