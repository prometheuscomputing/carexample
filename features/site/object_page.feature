# Test high-level object page (aka organizer) features here.
# Should also test object creation page
Feature: Object Page
  In order to view and edit detailed information about an object
  As a user
  I want to interact with an object page
  
  Background:
    Given I have reset the database
    And I have logged in
    And I expand the "Vehicles" header
    
  
  Scenario: Breadcrumbs interaction
    When I edit the "Vehicle" where "Vehicle Model" is "S80"
    And I click on the "Components" header
    And I edit a "Component"
    # Then I should see the breadcrumbs: "Home", "Car", and "Component"
    Then I should see the breadcrumbs: "Car", "Component"
    When I refresh the page
    # Then I should see the breadcrumbs: "Home", "Car", and "Component"
    Then I should see the breadcrumbs: "Car", "Component"
    When I click on the "Parts" header
    And I edit a "Part"
    Then I should see the breadcrumbs: "Car", "Component", "Part"
    # Then I should see the breadcrumbs: "Home", "Car", "Component", and "Part"
    When I go back "2" pages
    Then I should be at a "Car" page
    And I should see the breadcrumbs: "Car"
    # And I should see the breadcrumbs: "Home" and "Car"
    When I refresh the page
    Then I should see the breadcrumbs: "Car"
    # Then I should see the breadcrumbs: "Home" and "Car"
  
  Scenario: Breadcrumbs for a new object
    When I create a "Vehicle" of type "Motorcycle" where "Vehicle Model" is "Harley"
    Then I should see the breadcrumbs: "Motorcycle"
    # Then I should see the breadcrumbs: "Home", "Motorcycle"
    # And the "Home" breadcrumb should link to the home page
    And the "Motorcycle" breadcrumb should link to a new "Motorcycle"
    When I click on the "Owners" header
    And I create an "Owner" of type "Ownership" where "Percent Ownership" is "10.5"
    # Then I should see the breadcrumbs: "Home", "Motorcycle", "Ownership"
    Then I should see the breadcrumbs: "Motorcycle", "Ownership"
    # And the "Home" breadcrumb should link to the home page
    #TODO This step is ugly but it is only used here and I can't get a regex to work better
    And the "Motorcycle" breadcrumb should link to an existing a "Motorcycle" where "Vehicle Model" is "Harley"
    And the "Ownership" breadcrumb should link to a new "Ownership"
  
  Scenario: Copyright notice
    When I edit the "Vehicle" where "Vehicle Model" is "S80"
    Then I should see the copyright notice
  
  Scenario: Object cloning function is disabled
    Given PENDING

  Scenario: Object history function is disabled
    Given PENDING

  Scenario: Color-coding is disabled
    Given PENDING