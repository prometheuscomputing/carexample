Then /^I should see "([^"]+)" vehicle rows$/ do |num_vehicles|
  verify_vehicle_rows(num_vehicles.to_i)
end

Then /^I should see the image "([^"]+)"$/ do |image|
  page.should have_image(image)
  img = find(:xpath, "//img[contains(@src,\"#{image}\")]")
  case Capybara.current_driver
  when :chrome, :firefox, :headless_chrome
    img[:naturalWidth].should_not == 0
  when :webkit, :poltergeist
    url = current_url
    # Under poltergeist, the :src will include the host and scheme (it isn't the same as the img tag's src)
    src = Capybara.current_driver == :poltergeist ? img.native.attributes['src'] : img[:src]
    verify_image_url(src)
    # if src[0] == '/' # absolute URL
    #   verify_image_url(src)
    # else
    #   url_end_index = url.index('?') || -1
    #   verify_image_url(url[0..url_end_index] + src)
    # end
    visit(url)
    # Inelegant, but I can't figure out how to make capybara wait correctly here -SD
    # TODO: figure out why capybara isn't properly waiting for page load in "Scenario: Viewing images" in clear_view_attributes.feature.
    sleep 0.2
  else
    raise "Unsupported driver. Cannot verify image."
  end
end