class VehiclesInfo < Gui::Controller
  map '/vehicles_info/'
  
  # Use a custom layout for VehiclesInfo pages.
  # Instead of duplicating default.haml and modifying directly, read in the file
  # and make substitutions/insertions.
  def car_example_layout
    layout_path = LodePath.find('gui_site/layout/default.haml')
    default_layout = File.read(layout_path)
    # Replace '= page title' with a specific VehiclesInfo title
    default_layout.gsub!('= page_title', 'CarExample Vehicles Info')
    # Find the last %script in the layout template, and append car_example.js on the next line
    scripts = default_layout.scan(/^.*%script.*$/)
    last_script = scripts.last
    last_script_index = default_layout.index(last_script)
    car_example_script = "\n%script{:src => '/js/car_example.js'}".indent(last_script.find_least_indentation)
    default_layout.insert(last_script_index + last_script.length, car_example_script)
    default_layout
  end
  layout :car_example_layout
  
  def public_page?
    false
  end
  
  def index
    raw_redirect '/vehicles_info/vehicles_info'
  end
  
  def vehicles_info
    @vehicles = Automotive::Vehicle.all
  end
  
  def vehicle_details(vehicle_model)
    classes = Automotive::Vehicle.children
    classes.each do |klass|
      vehicle = klass.filter(:vehicle_model => vehicle_model).first
      if vehicle
        @vehicle = vehicle
        return
      end
    end
    respond "Could not find vehicle!"
  end
  
  def vehicles_table
    @vehicles = Automotive::Vehicle.all
  end
end