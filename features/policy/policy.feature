Feature: Policy
  In order to manage access control
  The application should allow or disallow access based on the user's roles.
  
  Background:
    Given I have reset the database
    And   I have logged in
    # And I expand the "Vehicles" header
    # And I create a "Vehicle" of type "Motorcycle" where "Make" is "Other", "Vehicle Model" is "Ninja 500R", and "Cost" is "10000"
    
    
  # Variations: user has 1 role.  user has no role.  user is admin.  user has multiple roles (with different access assigned to each role)  
  Scenario: allowed to view objects
  
  Scenario: not allowed to view objects
    
  Scenario: allowed to create objects
  
  Scenario: not allowed to create objects

  Scenario: allowed to edit objects
  
  Scenario: not allowed to edit objects
  
  Scenario: allowed to delete objects
  
  Scenario: not allowed to delete objects

  Scenario: allowed to navigate / associate to objects
  
  Scenario: not allowed to navigate / associate to objects
  