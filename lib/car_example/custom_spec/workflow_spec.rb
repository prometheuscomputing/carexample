#Workflow Specs
#-------------Hides workflow only attributes really only here to stop test errors-----------------------#
organizer(:Details, Automotive::Car) {
  hide('state', 'inspection_completed')
}

organizer(:Details, Automotive::Minivan) {
  hide('state', 'inspection_completed')
}

organizer(:Details, Automotive::HybridVehicle) {
  hide('state', 'inspection_completed')
}
# document(:NewCarRepairOrderCreated, Automotive::Car, :identifier => 'car_stuff', :title => 'Car Workflow Document Test Title') {|car|
#   newline
#
#   header(1, 'text-align' => 'center') {
#     write 'Report on '
#     string 'vehicle_model'
#     write '.'
#
#     newline
#     write ''
#   }
#   newline
#   write 'Add a Serial Number: '
#   integer 'serial'
#   newline
# }

# organizer(:AddComponent, Automotive::Car) {
#
# }
#
# organizer(:AddWarranty, Automotive::Car) {
#
# }

organizer(:NewCarRepairOrderCreated, Automotive::Car) {
  inherits_from_spec(:Details, Automotive::Car, nil, true)
  hide('state', 'make_test', 'car_color', 'being_repaired_for', 'being_repaired_by', 'components', 'drivers', 'maintained_by', 'occupants', 
        'vin', 'owners', 'registered_at', 'warranty_expiration_date', 'warranty_void', 'replacement_for', 'warranty', 'cost', 'miles_per_gallon', 'inspection_completed')
}

organizer(:StartedVehicleRepair, Automotive::Car) {
  inherits_from_spec(:Details, Automotive::Car, nil, true)
  hide('state', 'make_test', 'car_color', 'being_repaired_for', 'being_repaired_by', 'maintained_by', 'occupants', 
        'vin', 'owners', 'registered_at', 'warranty_expiration_date', 'warranty_void', 'replacement_for', 'warranty', 'cost', 'miles_per_gallon', 'drivers', 'inspection_completed')
  disable('serial', 'make', 'vehicle_model')
}

organizer(:StartedVehicleInspection, Automotive::Car) {
  inherits_from_spec(:Details, Automotive::Car, nil, true)
  hide('state', 'make_test', 'car_color', 'being_repaired_for', 'being_repaired_by', 'maintained_by', 'occupants', 
        'vin', 'owners', 'registered_at', 'warranty_expiration_date', 'warranty_void', 'replacement_for', 'warranty', 'cost', 'miles_per_gallon', 'drivers')
  disable('serial', 'make', 'vehicle_model', 'components')
}

organizer(:InspectedVehicle, Automotive::Car) {
  inherits_from_spec(:Details, Automotive::Car, nil, true)
  hide('state', 'make_test', 'car_color', 'being_repaired_for', 'being_repaired_by', 'maintained_by', 'occupants', 
        'vin', 'owners', 'registered_at', 'warranty_expiration_date', 'warranty_void', 'replacement_for', 'warranty', 'cost', 'miles_per_gallon', 'drivers', 'inspection_completed')
  disable('serial', 'make', 'vehicle_model', 'components')
}

organizer(:StartedEngineInspection, Automotive::Car) {
  inherits_from_spec(:Details, Automotive::Car, nil, true)
  hide('state', 'make_test', 'car_color', 'being_repaired_for', 'being_repaired_by', 'maintained_by', 'occupants', 
        'vin', 'owners', 'registered_at', 'warranty_expiration_date', 'warranty_void', 'replacement_for', 'warranty', 'cost', 'miles_per_gallon', 'drivers', 'inspection_completed')
  disable('serial', 'make', 'vehicle_model', 'components')
}
organizer(:StartedTireInspection, Automotive::Car) {
  inherits_from_spec(:Details, Automotive::Car, nil, true)
  hide('state', 'make_test', 'car_color', 'being_repaired_for', 'being_repaired_by', 'maintained_by', 'occupants', 
        'vin', 'owners', 'registered_at', 'warranty_expiration_date', 'warranty_void', 'replacement_for', 'warranty', 'cost', 'miles_per_gallon', 'drivers', 'inspection_completed')
  disable('serial', 'make', 'vehicle_model', 'components')
}

#End of Workflow Specs