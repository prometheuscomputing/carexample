# Test the various types of associations here. Exactly specify behavior of one-to-many, many-to-many, one-to-one, and association class associations. Should also test association constraints such as multiplicity upper bound.

Feature: Associations
  In order to change an object's associations to other objects
  As a user
  I want to manipulate its associations
  
  Background:
    Given I have reset the database
    And I have logged in
    And I expand the "Vehicles" header
    And I create a "Vehicle" of type "Motorcycle" where "Make" is "Other", "Vehicle Model" is "Ninja 500R", and "Cost" is "10000"
    
  # Test one-to-many
  # Using Vehicle -> Occupants relationship
  Scenario: one-to-many associations - Creating associations to existing objects
    # Add 3 Occupants
    Given I click on the "Occupants" header
    And I open the "Occupants" search
    And I click on the "Occupants" advanced search link
    And I filter to see "Occupants" whose "Family Size" is "*"
    And I show the possible "Occupants" # Will be Bob, Sally, Phil
    When I select the "Occupants"
    And I click the only "Add Associations" button under the "Occupants" header
    # Should see 3 occupants as pending, but not actually saved
    Then I should see that there are a total of "0" "Occupants" in the header
    And I should see "3" "Occupants"
    When I save the page
    When I click on the "Occupants" header
    Then I should see "3" associated "Occupants"
    And I should see that there are a total of "3" "Occupants" in the header
    # Go to an Occupant and remove association
    When I edit the "Occupant" whose "Name" is "Bob"
    And I click on the "Occupying" header
    Then I should see the associated "Occupying" where "Vehicle Model" is "Ninja 500R"
    When I click the only "Break Association" button under the "Occupying" header
    Then I should see the unsaved changes warning for "Occupying"
    And I save the page
    And I go to the previous page
    Then I should see that there are a total of "2" "Occupants" in the header
    When I click on the "Occupants" header
    Then I should see "2" associated "Occupants"
    And I should not see a "Occupants" where "Name" is "Bob"
  
  # Test that pre-populated associations are visible in collection content
  # Using RepairShop (currently_working_on) -> Vehicle (being_repaired_by) relationship
  Scenario: one-to-many associations - Checking pre-populated association content (before save)
    Given I click on the "Being Repaired By" header
    And I create an "Being Repaired By" of type "Repair Shop"
    Then I should see that there are a total of "1" "Currently Working On" in the header
    # Can't see the vehicle yet, haven't yet saved the repair shop
    When I click on the "Currently Working On" header
    And I should see a "Currently Working On" where "Make" is "Other" and "Vehicle Model" is "Ninja 500R"
    # Check that saving persists the association
    When I save the page
    And I click on the "Currently Working On" header
    Then I should see a "Currently Working On" where "Make" is "Other" and "Vehicle Model" is "Ninja 500R"
  
  # Test many-to-many - Creating associations to existing objects
  # Using Vehicle -> Maintained By relationship
  Scenario: many-to-many associations
    # Add 3 Maintainers
    Given I click on the "Maintained By" header
    And I open the "Maintained By" search
    And I click on the "Maintained By" advanced search link
    And I filter to see "Maintained By" whose "Family Size" is "*"
    And I show the possible "Maintained By" # Will be Bob, Sally, Phil
    When I select the "Maintained By"
    And I click the only "Add Associations" button under the "Maintained By" header
    And I save the page
    Then I should see that there are a total of "3" "Maintained By" in the header
    When I click on the "Maintained By" header
    Then I should see "3" associated "Maintained By"
    # Go to "Bob" and verify that he now maintains this motorcycle and another vehicle
    When I edit the "Maintained By" whose "Name" is "Bob"
    
    When I click on the "Maintains" header
    Then I should see that there are a total of "2" "Maintains" in the header
    Then I should see a "Maintains" where "Vehicle Model" is "Ninja 500R"
    And I should see a "Maintains" where "Vehicle Model" is "S80"
    # Remove Ninja from Bob's Maintains
    When I select the "Maintains" where "Vehicle Model" is "Ninja 500R"
    # NOTE: It is currently necessary to check that break associations is complete before saving the page
    #       Failure to do this can cause the save request to be sent before the populate collection response is received.
    #       This results in the breadcrumbs being lost, due to flash rotation.
    And I click the only "Break Associations" button under the "Maintains" header
    Then I should see the unsaved changes warning for "Maintains"
    And I should not see a "Maintains" where "Vehicle Model" is "Ninja 500R"
    And I save the page
    Then I should see that there are a total of "1" "Maintains" in the header
    # Doesn't do page versioning, goes back the the page's url?
    When I go to the previous page
    Then I should see that there are a total of "2" "Maintained By" in the header
    When I click on the "Maintained By" header
    Then I should see "2" associated "Maintained By"
    And I should not see a "Maintained By" where "Name" is "Bob"
  
  # Testing one-to-one - Creating an association to a new object
  # Using Repair Shop -> Chief Mechanic relationship
  
  Scenario: one-to-one associations
    Given I go to the previous page
    And I click on the "Repair Shops" header
    And I create a "Repair Shop" where "Name" is "New Repair Shop"
    And I click on the "Chief Mechanic" header
    When I create a "Chief Mechanic" where "Name" is "New Mechanic"
    Then I should see that there are a total of "1" "Chief For" in the header
    When I click on the "Chief For" header
    Then I should see the associated "Chief For" where "Name" is "New Repair Shop"
    When I save the page
    Then I should see that there are a total of "1" "Chief For" in the header
    When I click on the "Chief For" header
    Then I should see the associated "Chief For" where "Name" is "New Repair Shop"
    When I click the only "Break Association" button under the "Chief For" header
    Then I should see the unsaved changes warning for "Chief For"
    And I save the page
    And I go to the previous page
    Then I should see that there are a total of "0" "Chief Mechanic" in the header
  
  # Testing Association Class many-to-many - Creating an association to a new object
  # Using Vehicle -> Ownership -> Person relationship
  Scenario: Association Class many-to-many associations
    Given I click on the "Owners" header
    When I create an "Owner" of type "Ownership" where "Percent Ownership" is "12.3"
    And I click on the "Owner" header
    And I create an "Owner" of type "Person" whose "Name" is "New Owner"
    And I save the page
    When I expand the "Vehicles" header
    Then I should see that there are a total of "1" "Vehicles" in the header
    Then I should see a "Vehicle" where "Vehicle Model" is "Ninja 500R"
    When I select the "Vehicle"
    # NOTE: It is currently necessary to check that break associations is complete before saving the page
    #       Failure to do this can cause the save request to be sent before the populate collection response is received.
    #       This results in the breadcrumbs being lost, due to flash rotation.
    And I click the only "Break Associations" button under the "Vehicles" header
    Then I should see the unsaved changes warning for "Vehicles"
    And I should not see a "Vehicle" where "Vehicle Model" is "Ninja 500R"
    And I save the page
    When I expand the "Vehicles" header
    Then I should see that there are a total of "0" "Vehicles" in the header
    Then I should see "0" associated "Vehicles"
    When I go back "2" pages
    When I click on the "Owners" header
    Then I should see that there are a total of "0" "Owners" in the header
    Then I should see "0" associated "Owners"
  

  Scenario: Association Class many-to-many associations - Discarded changes will not take effect
    Given I click on the "Owners" header
    When I create an "Owner" of type "Ownership" where "Percent Ownership" is "12.3"
    And I click on the "Owner" header
    And I create an "Owner" of type "Person" whose "Name" is "New Owner"
    And I save the page
    When I expand the "Vehicles" header
    Then I should see that there are a total of "1" "Vehicles" in the header
    Then I should see a "Vehicle" where "Vehicle Model" is "Ninja 500R"
    When I select the "Vehicle"
    # NOTE: It is currently necessary to check that break associations is complete before saving the page
    #       Failure to do this can cause the save request to be sent before the populate collection response is received.
    #       This results in the breadcrumbs being lost, due to flash rotation.
    And I click the only "Break Associations" button under the "Vehicles" header
    Then I should see the unsaved changes warning for "Vehicles"
    And I should not see a "Vehicle" where "Vehicle Model" is "Ninja 500R"
    When I discard changes on the page
    And I click the "Discard" button on the "discard changes" popup
    And I save the page
    When I expand the "Vehicles" header
    Then I should see that there are a total of "1" "Vehicles" in the header
    Then I should see "1" associated "Vehicles"

  Scenario: Association Class many-to-many associations - Canceled discardation of changes will take effect
    Given I click on the "Owners" header
    When I create an "Owner" of type "Ownership" where "Percent Ownership" is "12.3"
    And I click on the "Owner" header
    And I create an "Owner" of type "Person" whose "Name" is "New Owner"
    And I save the page
    When I expand the "Vehicles" header
    Then I should see that there are a total of "1" "Vehicles" in the header
    Then I should see a "Vehicle" where "Vehicle Model" is "Ninja 500R"
    When I select the "Vehicle"
    # NOTE: It is currently necessary to check that break associations is complete before saving the page
    #       Failure to do this can cause the save request to be sent before the populate collection response is received.
    #       This results in the breadcrumbs being lost, due to flash rotation.
    And I click the only "Break Associations" button under the "Vehicles" header
    Then I should see the unsaved changes warning for "Vehicles"
    And I should not see a "Vehicle" where "Vehicle Model" is "Ninja 500R"
    When I discard changes on the page
    And I click the "Cancel" button on the "discard changes" popup
    And I save the page
    When I expand the "Vehicles" header
    Then I should see that there are a total of "0" "Vehicles" in the header
    Then I should see "0" associated "Vehicles"

  # Failing due to bad link in breadcrumbs - When clicking the join table association, the ID it gets is the ID for the person
  @known_failing 
  Scenario: Association Class many-to-many associations - testing breadcrumbs' paths
    Given I click on the "Owners" header
    When I create an "Owner" of type "Ownership" where "Percent Ownership" is "12.3"
    And I click on the "Owner" header
    And I create an "Owner" of type "Person" whose "Name" is "New Owner"
    And I save the page
    Then I should see that there are a total of "1" "Vehicles" in the header
    When I expand the "Vehicles" header
    Then I should see a "Vehicle" where "Vehicle Model" is "Ninja 500R"
    When I select the "Vehicle"
    # NOTE: It is currently necessary to check that break associations is complete before saving the page
    #       Failure to do this can cause the save request to be sent before the populate collection response is received.
    #       This results in the breadcrumbs being lost, due to flash rotation.
    And I click the only "Break Associations" button under the "Vehicles" header
    Then I should see the unsaved changes warning for "Vehicles"
    And I should not see a "Vehicle" where "Vehicle Model" is "Ninja 500R"
    And I save the page
    Then I should see that there are a total of "0" "Vehicles" in the header
    When I expand the "Vehicles" header
    Then I should see "0" associated "Vehicles"
    When I go to the previous page
    Then I should see that there are a total of "0" "Owner" in the header
    When I click on the "Owner" header
    Then I should see "0" associated "Owner"
    When I go to the previous page
    Then I should see that there are a total of "0" "Owners" in the header
    When I click on the "Owners" header
    Then I should see "0" associated "Owners"


  # Testing Association Class many-to-many - Testing deleting "incomplete" association class instances
  # Using Vehicle -> Ownership -> Person relationship
  Scenario: Association Class many-to-many associations - deleting incomplete association class instance
    Given I click on the "Owners" header
    When I create an "Owner" of type "Ownership" where "Percent Ownership" is "10.5"
    # Not creating an Owner for this Ownership
    And I save and leave the page
    # Back on "Vehicle" page
    # NOTE An assoc class without two ends is an incomplete association.  There is no owner here so you will see 0 owners.
    Then I should see that there are a total of "0" "Owners" in the header
    When I click on the "Owners" header
    Then I should see an "Owner" where "Percent Ownership" is "10.5"
    When I select the "Owner"
    And I click the only "Delete" button under the "Owners" header
    # This step hangs under capybara-webkit
    And I confirm the delete popup
    And I save the page
    Then I should see that there are a total of "0" "Owners" in the header
    # Check that refreshing doesn't change anything
    When I refresh the page
    And I click on the "Owners" header
    Then I should see that there are a total of "0" "Owners" in the header
    And I should see "0" associated "Owners"
    # TODO: could check DB counts to ensure AC instance was actually deleted
  
  # Testing Association Class many-to-many - Testing breaking association to "incomplete" association class instance
  # Using Vehicle -> Ownership -> Person relationship
  Scenario: Association Class many-to-many associations - breaking association to incomplete association class instance
    Given I save the page
    Given I click on the "Owners" header
    And I create a "Owner" through "Ownership" of type "Person" whose "Name" is "Martha" and return
    When I create an "Owner" of type "Ownership" where "Percent Ownership" is "10.5"
    # Not creating an Owner for this Ownership
    And I save and leave the page
    # Back on "Vehicle" page
    # NOTE An assoc class without two ends is an incomplete association.  There is only one complete association here so you will see only 1 owner.
    Then I should see that there are a total of "1" "Owners" in the header
    When I click on the "Owners" header
    When I select the "Owner" whose "Percent Ownership" is "10.5"
    # NOTE: It is currently necessary to check that break associations is complete before saving the page
    #       Failure to do this can cause the save request to be sent before the populate collection response is received.
    #       This results in the breadcrumbs being lost, due to flash rotation.
    And I click the only "Break Associations" button under the "Owners" header
    Then I should see the unsaved changes warning for "Owners"
    And I should see "1" pending deletion "Owners"
    # Was at one point complaining here that it got a class 'People::Ownership', but wanted 'People::Person'
    And I save the page
    And I click on the "Owners" header
    Then I should see that there are a total of "1" "Owners" in the header
    Then I should see an "Owner" whose "Name" is "Martha"
    # Check that refreshing doesn't change anything
    When I refresh the page
    And I click on the "Owners" header
    Then I should see that there are a total of "1" "Owners" in the header
    And I should see an "Owner" whose "Name" is "Martha"
    # TODO: could check DB counts to ensure AC instance was actually deleted
  
  # Testing Association Class many-to-many - Creating multiple associations to existing objects
  Scenario: Association Class many-to-many associations - associating to multiple existing items
    Given I click on the "Owners" header
    And I show the possible "Owners"
    When I select the "Owner" whose "Name" is "Rick"
    And I select the "Owner" whose "Name" is "Bill"
    And I select the "Owner" whose "Name" is "Jeff"
    And I click the only "Add Associations" button under the "Owners" header
    And I save the page
    And I click on the "Owners" header
    Then I should see that there are a total of "3" "Owners" in the header
    And I should see "3" associated "Owners"

  # Pending Association Checks
  Scenario: Association Class many-to-many - pending association changes
    When I click on the "Owners" header
    And I show the possible "Owners"
    And I should see "8" "Owners"
    Then I should not see the unsaved changes warning for "Occupants"
    When I select the "Owner" whose "Name" is "Rick"
    And I select the "Owner" whose "Name" is "Bill"
    And I select the "Owner" whose "Name" is "Jeff"
    And I click the only "Add Associations" button under the "Owners" header
    Then the "Owner" whose "Name" is "Rick" should be a pending "addition"
    Then the "Owner" whose "Name" is "Bill" should be a pending "addition"
    Then the "Owner" whose "Name" is "Jeff" should be a pending "addition"
    And I save the page
    Then I should not see the unsaved changes warning for "Occupants"

    And I click on the "Owners" header
    Then I should see that there are a total of "3" "Owners" in the header
    And I should see "3" associated "Owners"
    And the "Owner" whose "Name" is "Rick" should not be pending
    And the "Owner" whose "Name" is "Bill" should not be pending
    And the "Owner" whose "Name" is "Jeff" should not be pending

    When I select the "Owner" whose "Name" is "Rick"
    # NOTE: It is currently necessary to check that break associations is complete before saving the page
    #       Failure to do this can cause the save request to be sent before the populate collection response is received.
    #       This results in the breadcrumbs being lost, due to flash rotation.
    And I click the only "Break Associations" button under the "Owners" header
    Then I should see the unsaved changes warning for "Owners"
    And I should not see an "Owner" whose "Name" is "Rick"
    
    When I select the "Owner" whose "Name" is "Bill"
    When I select the "Owner" whose "Name" is "Jeff"
    And I click the only "Delete" button under the "Owners" header
    And I confirm the delete popup
    Then I should see "2" pending deletion "Owners"
    And the "Owner" whose "Name" is "Bill" should be a pending "deletion"
    And the "Owner" whose "Name" is "Jeff" should be a pending "deletion"
    When I select the "Owner" whose "Name" is "Jeff"
    And I click the only "Undelete" button under the "Owners" header
    And the "Owner" whose "Name" is "Bill" should be pending "deletion"
    And the "Owner" whose "Name" is "Jeff" should not be pending

    When I show the possible "Owners"
    And the "Owner" whose "Name" is "Rick" should be a pending "breaking"
    And I save the page

    When I click on the "Owners" header
    Then I should see that there are a total of "1" "Owners" in the header
    And I should see "1" associated "Owners"
    And the "Owner" whose "Name" is "Jeff" should not be pending
    And I show the possible "Owners"
    And I should see "7" "Owners"
    And the "Owner" whose "Name" is "Rick" should not be pending

  Scenario: Association Class One-to-One Create New Association
    #Setup for getting to a person because of where the background starts
    When I go to the home page
    And I expand the "People" header
    Then I edit the "People" whose "Name" is "Sally"
    
    #Create the city through the association class
    Given I click on the "Founded" header
    And I create a "Founded" through "Founding" of type "City" whose "Name" is "Johnson City" and return
    Then I click on the "Founded" header
    And I should see the associated "Founded" where "Name" is "Johnson City"
      
  Scenario: Association Class One-to-One Add Existing Association
    #Setup for getting to a person because of where the background starts
    When I go to the home page
    And I expand the "People" header
    Then I edit the "People" whose "Name" is "Sally"
    #Add and association to an existing city
    Given I click on the "Founded" header
    And I show the possible "Founded"
    And I select the "Founded" whose "Name" is "Cullowhee"
    And I click the only "Add Association" button under the "Founded" header
    Then I should see the unsaved changes warning for "Founded"
    And I should see "1" pending "Founded" where "Name" is "Cullowhee"
    Then I save the page
    #confirming the addition
    When I click on the "Founded" header
    Then I should see the associated "Founded" where "Name" is "Cullowhee"
  
  Scenario: Association Class One-to-One Break Association
    #Setup for getting to a person because of where the background starts
    When I go to the home page
    And I expand the "People" header
    Then I edit the "People" whose "Name" is "Bob" 
    #Confirm the association is there
    Given I click on the "Founded" header
    And I should see the associated "Founded" where "Name" is "Sylva"
    #break the association
    When I click the only "Break Association" button under the "Founded" header
    Then I should see the unsaved changes warning for "Founded"
    And I should not see a "Founded" where "Name" is "Sylva"
    #Confirm the association is not there after saving
    Then I save the page
    And I click on the "Founded" header
    And I should not see a "Founded" where "Name" is "Sylva"
    
  Scenario: Association Class One-to-One Break and Add Existing Association
    #Setup for getting to a person because of where the background starts
    When I go to the home page
    And I expand the "People" header
    Then I edit the "People" whose "Name" is "Bob"
    
    #Break the association
    Given I click on the "Founded" header
    And I click the only "Break Association" button under the "Founded" header
    Then I should see the unsaved changes warning for "Founded"
    And I should not see a "Founded" where "Name" is "Sylva"
    
    Then I show the possible "Founded"
    Then I should see "4" unassociated "Founded"
    And the "Founded" whose "Name" is "Sylva" should be a pending "breaking"
    And I select the "Founded" where "Name" is "Raleigh"
    And I click the only "Add Association" button under the "Founded" header
    Then I should see "1" pending "Founded" where "Name" is "Raleigh"
    
    #confirm it added the new association
    Then I save the page
    And I click on the "Founded" header
    Then I should see the associated "Founded" where "Name" is "Raleigh"
  
  Scenario: Assocation Class One-to-One Edit Attributes on the Association class
    #Setup for getting to a person because of where the background starts
    When I go to the home page
    And I expand the "People" header
    Then I edit the "People" whose "Name" is "Bob"
    #Edit the city founded Date
    Given I click on the "Founded" header
    And I set the "Date" to "1877-01-05"
    Then I save the page
    #confirm the date attribute change
    When I click on the "Founded" header
    Then I should see the associated "Founded" where "Name" is "Sylva" and "Date" should be "1877-01-05"

  #one to many with association class
  Scenario: Association Class One-to-Many Create New Association
    #Create a Clown to test the assoc
    When I go to the home page
    And I expand the "People" header
    And I create a "People" of type "Clown" whose "Name" is "Billy" and "Clown Name" is "Bubba"
    
    #Create a new riding
    Given I click on the "Unicycles" header
    When I create an "Unicycle" of type "Riding" where "Aquired" is "January 19, 2019" and "Is Favorite" is "True" and "Description" is "The best darn unicycle around"
    And I click on the "Unicycle" header
    And I create an "Unicycle" of type "Unicycle" whose "Color" is "Red" and "Cost" is "50"
    And I save the page
    And I go to the previous page
    When I click on the "Unicycle" header
    Then I should see the associated "Unicycle" where "Cost" is "50"
    #Check it from the clown side
    When I go to the home page
    And I expand the "People" header
    When I click the next page button
    And I edit the "People" whose "Name" is "Billy"
    When I click on the "Unicycle" header
    Then I should see that there are a total of "1" "Unicycles" in the header
    
    #Create a second riding
    When I create an "Unicycle" of type "Riding" where "Aquired" is "January 20, 2019" and "Is Favorite" is "False" and "Description" is "The second unicycle that I don't like as much"
    And I click on the "Unicycle" header
    And I create an "Unicycle" of type "Unicycle" whose "Color" is "Black" and "Cost" is "10"
    And I save the page
    And I go to the previous page
    When I click on the "Unicycle" header
    Then I should see the associated "Unicycle" where "Cost" is "10"
    When I go to the home page
    And I expand the "People" header
    When I click the next page button
    And I edit the "People" whose "Name" is "Billy"
    Then I should see that there are a total of "2" "Unicycles" in the header
    
  Scenario: Association Class One-to-Many Add and Break Existing Association
    #Create a Unicycle to test the assoc
    When I go to the home page
    And I expand the "Vehicles" header
    And I create a "Vehicle" of type "Unicycle" where "Cost" is "50" and "Color" is "Green"
    And I save the page
    #Create a Clown to test the assoc
    When I go to the home page
    And I expand the "People" header
    And I create a "People" of type "Clown" whose "Name" is "Billy" and "Clown Name" is "Bubba"
    And I save the page
    
    When I click on the "Unicycles" header
    And I show the possible "Unicycles"
    When I select the "Unicycle" whose "Cost" is "50"
    And I click the only "Add Associations" button under the "Unicycles" header
    Then the "Unicycle" whose "Cost" is "50" should be a pending "addition"
    And I save the page
    When I click on the "Unicycles" header
    Then I should see that there are a total of "1" "Unicycles" in the header
    
    #Break this association
    When I select the "Unicycle" whose "Cost" is "50"
    And I click the only "Break Associations" button under the "Unicycles" header
    And I show the possible "Unicycles"
    Then the "Unicycle" whose "Cost" is "50" should be a pending "breaking"
    And I save the page
    Then I should see that there are a total of "0" "Unicycles" in the header
  
  #many to one with association clas
  #TODO fails same as with 1 to 1 assoc class creation
  Scenario:  Association Class Many-to-One Create New Association
    #Create a Unicycle to test the assoc
    When I go to the home page
    And I expand the "Vehicles" header
    And I create a "Vehicle" of type "Unicycle" where "Cost" is "50" and "Color" is "Green"
    And I save the page
    
    When I click on the "Clown" header
    And I create a "Clown" through "Riding" of type "Clown" whose "Name" is "Gus" and return
    And I save the page
    Then I click on the "Clown" header
    Then I should see the associated "Clown" whose "Name" is "Gus"
    
  Scenario: Association Class Many-to-One Create Add and Break Existing Association
    #Create a Clown to test the assoc
    When I go to the home page
    And I expand the "People" header
    And I create a "People" of type "Clown" whose "Name" is "Billy" and "Clown Name" is "Bubba"
    And I save the page
    
    #Create a Unicycle to test the assoc
    When I go to the home page
    And I expand the "Vehicles" header
    And I create a "Vehicle" of type "Unicycle" where "Cost" is "50" and "Color" is "Green"
    And I save the page
    
    When I click on the "Clown" header
    And I show the possible "Clown"
    When I select the "Clown" whose "Name" is "Billy"
    And I click the only "Add Association" button under the "Clown" header
    Then the "Clown" whose "Name" is "Billy" should be a pending "addition"
    And I save the page
    When I click on the "Clown" header
    Then I should see the associated "Clown" where "Clown Name" is "Bubba"
    
    #Break this association
    And I click the only "Break Association" button under the "Clown" header
    And I save the page
    Then I should see that there are a total of "0" "Clown" in the header

  Scenario: one-to-many associations - pending association changes
    # Add 3 Occupants
    Given I click on the "Occupants" header
    When I open the "Occupants" search
    And I click on the "Occupants" advanced search link
    Then I should not see the unsaved changes warning for "Occupants"
    When I filter to see "Occupants" whose "Family Size" is "*"
    And I show the possible "Occupants" # Will be Bob, Sally, Phil
    And I select the "Occupants"
    And I click the only "Add Associations" button under the "Occupants" header
    # Should see 3 occupants as pending, but not actually saved
    Then I should see that there are a total of "0" "Occupants" in the header
    Then I should see the unsaved changes warning for "Occupants"
    And I should see "3" "Occupants"
    When I save the page
    Then I should not see the unsaved changes warning for "Occupants"
    When I click on the "Occupants" header
    Then I should see "3" associated "Occupants"
    And I should see that there are a total of "3" "Occupants" in the header
    # Go to an Occupant and remove association
    When I edit the "Occupant" whose "Name" is "Bob"
    And I click on the "Occupying" header
    Then I should see the associated "Occupying" where "Vehicle Model" is "Ninja 500R"
    When I click the only "Break Association" button under the "Occupying" header
    # Test pagination one the possible associations, while the current association is pending broken
    And I show the possible "Occupying"
    And I should see "8" "Occupying"
    And I should be on page "1" out of "2"
    Then the previous page button should be disabled
    And the next page button should not be disabled
    When I view "4" items per page
    And I click the "Go" button
    Then the previous page button should be disabled
    And the next page button should not be disabled
    And I should see "4" "Occupying"
    And I should be on page "1" out of "3"
    When I click the next page button
    And I should see "4" "Occupying"
    And I should be on page "2" out of "3"
    Then the previous page button should not be disabled
    And the next page button should not be disabled
    # End pagination test
    And I save the page
    And I go to the previous page
    Then I should see that there are a total of "2" "Occupants" in the header
    When I click on the "Occupants" header
    Then I should see "2" associated "Occupants"
    And I should not see a "Occupants" where "Name" is "Bob"
  
  # Test many-to-one
  # Using Vehicle -> RepairShop relationship
  Scenario: many-to-one associations - Creating an association to existing objects
    # Add a RepairShop
    Given I click on the "Being Repaired By" header
    And I open the "Being Repaired By" search
    And I click on the "Being Repaired By" advanced search link
    And I filter to see "Being Repaired By" whose "Name" is "Tires and More"
    And I show the possible "Being Repaired By"
    When I select the "Being Repaired By"
    And I click the only "Add Association" button under the "Being Repaired By" header
    # Should see 1 RepairShop as pending, but not actually saved
    Then I should see that there are a total of "0" "Being Repaired By" in the header
    And I should see "1" pending "Being Repaired By" where "Name" is "Tires and More"
    When I save the page
    When I click on the "Being Repaired By" header
    Then I should see the associated "Being Repaired By" where "Name" is "Tires and More"
    And I should see that there are a total of "1" "Being Repaired By" in the header
    # Go to the RepairShop and remove association
    When I edit the "Being Repaired By" whose "Name" is "Tires and More"
    And I click on the "Currently Working On" header
    Then I should see a "Currently Working On" where "Vehicle Model" is "Ninja 500R"
    When I select the "Currently Working On" where "Vehicle Model" is "Ninja 500R"
    # NOTE: It is currently necessary to check that break associations is complete before saving the page
    #       Failure to do this can cause the save request to be sent before the populate collection response is received.
    #       This results in the breadcrumbs being lost, due to flash rotation.
    When I click the only "Break Associations" button under the "Currently Working On" header
    Then I should see the unsaved changes warning for "Currently Working On"
    And I should not see a "Currently Working On" where "Vehicle Model" is "Ninja 500R"
    And I save the page
    And I go to the previous page
    Then I should see that there are a total of "0" "Being Repaired By" in the header
    When I click on the "Being Repaired By" header
    Then I should see "0" associated "Being Repaired By"
    And I should not see a "Being Repaired By" where "Name" is "Tires and More"