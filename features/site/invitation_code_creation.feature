Feature: Invitation Codes
  In order to allow new users to register
  As an administrator
  I want create invitation codes that users may use to register new accounts
  
  Background:
    Given I have reset the database
    And I have logged in as an administrator
    And I go to the user administration panel
      
  Scenario: Adding/Removing an invitation code
    When I add the invitation code "new_invitation_code" with expiration "3142-01-01"
    # And I submit the page
    Then I should see the invitation code "new_invitation_code"
    # TODO Separate this step out into registration.  Whether or not the invitation code works is not what is being tested here.
    And "gregory" should be able to register with the invitation code "new_invitation_code" as a user
    When I mark the invitation code "new_invitation_code" to be deleted
    And I submit the page
    Then I should not see the invitation code "new_invitation_code"
    # TODO Separate this step out into registration.  Whether or not the invitation code works is not what is being tested here.
    And "phillip" should not be able to register with the invitation code "new_invitation_code" (receives "The invitation code you entered is not valid")
  
  Scenario: Registering with an invitation code that has limited uses remaining
    When I add the invitation code "limited_invitation_code" with expiration "3142-01-01" and "2" uses remaining
    And I submit the page
    Then "gregory" should be able to register with the invitation code "limited_invitation_code" as a user
    And I should see the invitation code "limited_invitation_code" with "1" uses remaining
    And "phillip" should be able to register with the invitation code "limited_invitation_code" as a user
    And I should see the invitation code "limited_invitation_code" with "0" uses remaining
    And "norman" should not be able to register with the invitation code "limited_invitation_code" (receives "The invitation code you entered has exceeded the maximum number of uses")
  
  Scenario: Registering with an invitation code that has expired
    When I add the invitation code "old_invitation_code" with expiration "1942-01-01" in the past
    Then "gregory" should not be able to register with the invitation code "old_invitation_code" (receives "The invitation code you entered has expired")
  
  Scenario: Registering with an invitation code that grants roles
    # Sign in as site admin to add new 'Editor' role
    Given I sign out
    And I have logged in as an administrator
    And I go to the site administration panel
    And I add a role named "Editor"
    And I submit the page
    And I sign out
    And I have logged in as an administrator
    When I go to the user administration panel
    And I add the invitation code "admin_and_editor_code" with expiration "3142-01-01" that grants the roles "Administrator" and "Editor"
    Then "gregory" should be able to register with the invitation code "admin_and_editor_code" as an administrator
    And I should see that "gregory" has the "Administrator" and "Editor" roles
  
  # This fails because it's not currently possible to add a perspective to an invitation code due to a gui limitation
  @known_failing
  Scenario: Registering with an invitation code that grants perspectives
    When I go to the site administration panel
    And I add a perspective named "Stakeholder"
    And I submit the page
    And I go to the user administration panel
    When I add the invitation code "stakeholder_code" with expiration "3142-01-01" that grants the perspective "Stakeholder"
    Then "gregory" should be able to register with the invitation code "stakeholder_code" with the perspective "Stakeholder"
